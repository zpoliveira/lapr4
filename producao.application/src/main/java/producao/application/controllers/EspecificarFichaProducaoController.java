package producao.application.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import producao.domain.model.Quantidade;
import producao.domain.model.Unidade;
import producao.domain.model.materiaPrima.CodigoInternoMateriaPrima;
import producao.domain.model.produto.ComponenteMateriaPrima;
import producao.domain.model.produto.FichaProducao;
import producao.domain.model.produto.Produto;
import producao.domain.repositories.FichaProducaoRepository;
import producao.domain.repositories.ProdutoRepository;

import java.util.Set;


@Component
public class EspecificarFichaProducaoController {

    @Autowired
    private ProdutoRepository produtoRepository;
    @Autowired
    private FichaProducaoRepository fichaProducaoRepository;
    @Autowired
    private AddProdutoController addProdutoController;


    @Transactional
    public void especificarFichaProducaoProduto(Produto produto, Quantidade quantidadeProduto,
                                              Set<ComponenteMateriaPrima> componentesMateriaPrima)
            throws DataIntegrityViolationException, IllegalArgumentException {
        FichaProducao fichaProducao = new FichaProducao(quantidadeProduto, componentesMateriaPrima);
        produto.adicionarFichaProducao(fichaProducao);
        fichaProducaoRepository.save(fichaProducao);
        produtoRepository.save(produto);
    }

    @Transactional
    public void adicionarFichaProducaoProduto(String codigoComercial,
                                              String codigoFabrico,
                                              String descricaoBreve,
                                              String descricaoCompleta,
                                              Quantidade quantidadeProduto,
                                              Set<ComponenteMateriaPrima> componentesMateriaPrima)
            throws DataIntegrityViolationException, IllegalArgumentException {

        FichaProducao fichaProducao = new FichaProducao(quantidadeProduto, componentesMateriaPrima);
        fichaProducaoRepository.save(fichaProducao);
        addProdutoController.addProduto(codigoComercial, codigoFabrico, descricaoBreve, descricaoCompleta, fichaProducao);

    }

    public Quantidade createQuantidade(long quantidade, Unidade unidade) {
        return new Quantidade(quantidade, unidade);
    }

    public ComponenteMateriaPrima createComponenteMateriaPrima(CodigoInternoMateriaPrima codMatriaPrima,Quantidade quantidadeObj){
        return new ComponenteMateriaPrima(codMatriaPrima, quantidadeObj);
    }

}
