package producao.application.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import producao.domain.model.materiaPrima.CategoriaMatPrima;
import producao.domain.repositories.CategoriaMatPrimaRepository;

import java.util.Optional;

@Component
public class ListarCatMatPrimaService {

    @Autowired
    private CategoriaMatPrimaRepository repo;
    /*@Autowired
    private AuthorizationService authorizationService;
*/
    public Iterable<CategoriaMatPrima> todasCatMatPrima() {
       //authorizationService.ensureAuthenticatedUserHasAnyOf(CafeteriaRoles.POWER_USER, CafeteriaRoles.MENU_MANAGER);

        return repo.findAll();
    }
}
