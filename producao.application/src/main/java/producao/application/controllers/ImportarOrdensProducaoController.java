package producao.application.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import producao.application.importfiles.ImportadorFicheiroOrdemProducaoFactory;

import java.io.File;
import java.io.IOException;

@Component
public class ImportarOrdensProducaoController {
    @Autowired
    private ImportadorFicheiroOrdemProducaoFactory ordemProducaoFactory;

    public int importarProdutos(String filePath) throws IOException {
        int numOrdProdImported = -1;
        File file = new File(filePath);
        if (file.isFile() && file.canRead()) {
            numOrdProdImported =  ordemProducaoFactory.importFicheiro(file);
        } else {
            throw new IOException("Failed to real file " + filePath);
        }
        return numOrdProdImported;
    }
}
