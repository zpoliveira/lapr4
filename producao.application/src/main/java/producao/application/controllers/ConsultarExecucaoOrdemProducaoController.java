package producao.application.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import spm.domain.execucaoOrdemProducao.ExecucaoOrdemProducao;
import spm.repository.ExecucaoOrdemProducaoRepository;

@Component
public class ConsultarExecucaoOrdemProducaoController {

    @Autowired
    private ExecucaoOrdemProducaoRepository execucaoOrdemProducaoRepository;

    public Iterable<ExecucaoOrdemProducao> mostraTodasExecucoes(){
        return execucaoOrdemProducaoRepository.findAll();
    }
}
