package producao.application.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import producao.domain.model.ordemProducao.EstadoOrdemProducao;
import producao.domain.model.ordemProducao.OrdemProducao;
import producao.domain.repositories.OrdemProducaoRepository;

import java.util.Arrays;

@Component
public class ConsultarEstadoOrdemProducaoController {

    @Autowired
    OrdemProducaoRepository ordemProducaoRepository;

    public Iterable<EstadoOrdemProducao> mostraEstadosOrdemProducao() {
        EstadoOrdemProducao[] estados = EstadoOrdemProducao.class.getEnumConstants();
        return Arrays.asList(estados);
    }

    @Transactional
    public Iterable<OrdemProducao> mostraOrdensProducaoPorEstado(EstadoOrdemProducao oEstado) {
        return ordemProducaoRepository.findByEstadoOrdemProducao(oEstado);
    }
}
