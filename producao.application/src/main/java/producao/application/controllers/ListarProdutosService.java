package producao.application.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import producao.domain.model.produto.Produto;
import producao.domain.repositories.ProdutoRepository;

@Component
public class ListarProdutosService {

    @Autowired
    private ProdutoRepository produtoRepository;

    public Iterable<Produto> listarProdutosSemFichaProducao() {
        return produtoRepository.findByTemFichaProducaoFalse();
    }
}
