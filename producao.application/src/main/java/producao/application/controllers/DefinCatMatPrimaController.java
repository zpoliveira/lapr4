package producao.application.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import producao.domain.model.materiaPrima.CategoriaMatPrima;
import producao.domain.model.materiaPrima.NomeCategoria;
import producao.domain.repositories.CategoriaMatPrimaRepository;

@Component
public class DefinCatMatPrimaController {

    @Autowired
    private CategoriaMatPrimaRepository repository;
    /*@Autowired
    private AuthorizationService authorizationService;*/

    @Transactional
    public CategoriaMatPrima definCatMatPrima(final String nomeCategoria) throws DataIntegrityViolationException {
        /*authorizationService.ensureAuthenticatedUserHasAnyOf(CafeteriaRoles.POWER_USER, CafeteriaRoles.MENU_MANAGER);*/

        final CategoriaMatPrima newCategoriaMatPrima = new CategoriaMatPrima(new NomeCategoria(nomeCategoria));
        return repository.save(newCategoriaMatPrima);
    }
}
