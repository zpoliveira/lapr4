package producao.application.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import producao.application.importfiles.ImportadorFicheiroProdutoFactory;

import java.io.File;
import java.io.IOException;

@Component
public class ImportarCatalogoController {
    private static final Logger LOGGER = LogManager.getLogger(ImportarCatalogoController.class);

    @Autowired
    private ImportadorFicheiroProdutoFactory importadorFicheiroProdutoFactory;

    public int importarProdutos(String filePath) throws IOException {
        int numProdImported = -1;
        File file = new File(filePath);
        if (file.isFile() && file.canRead()) {
            numProdImported = importadorFicheiroProdutoFactory.importFicheiro(file);
        } else {
            String message = String.format("[ERRO] File does not exist or can't be read! -> %s", file.getAbsolutePath());
            LOGGER.error(message);
            return numProdImported;
        }
        return numProdImported;
    }

}
