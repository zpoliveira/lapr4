package producao.application.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import producao.domain.model.materiaPrima.*;
import producao.domain.repositories.MateriaPrimaRepository;

import java.io.File;

@Component
public class AdicionarMateriaPrimaController {

    @Autowired
    private ListarCatMatPrimaService svc;
    @Autowired
    private MateriaPrimaRepository materiaPrimaRepository;

    @Transactional
    public MateriaPrima addMatPrima(final String codigoInternoMatPrima, final CategoriaMatPrima categoriaMatPrima,
                                    final String descricaoMateriaPrima, final File ft) throws DataIntegrityViolationException {

        //authorizationService.ensureAuthenticatedUserHasAnyOf(CafeteriaRoles.POWER_USER, CafeteriaRoles.MENU_MANAGER);

        final MateriaPrima newMateriaPrima = new MateriaPrima(new CodigoInternoMateriaPrima(codigoInternoMatPrima),
                categoriaMatPrima, new DescricaoMateriaPrima(descricaoMateriaPrima),
                new FichaTecnica(ft));
        return materiaPrimaRepository.save(newMateriaPrima);
    }

    public Iterable<CategoriaMatPrima> todasCatMatPrima() {
        return svc.todasCatMatPrima();
    }

}
