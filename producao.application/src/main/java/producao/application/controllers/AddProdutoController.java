package producao.application.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import producao.domain.model.produto.*;
import producao.domain.repositories.ProdutoRepository;


@Component
public class AddProdutoController {

    @Autowired
    private ProdutoRepository produtoRepository;

    /*@Autowired
    private AuthorizationService authorizationService;*/
    @Transactional
    public Produto addProduto(final String codigoComercial,
                                   final String codigoFabricoProduto,
                                   final String descricaoBreve,
                                   final String descricaoCompleta)
            throws DataIntegrityViolationException, IllegalArgumentException {
        /*authorizationService.ensureAuthenticatedUserHasAnyOf(CafeteriaRoles.POWER_USER, CafeteriaRoles.MENU_MANAGER);*/
        final CodigoComercial codigoComercialObj = new CodigoComercial(codigoComercial);
        final CodigoFabricoProduto codifoFabricoObj = new CodigoFabricoProduto(codigoFabricoProduto);
        final DescricaoBreve descricaoBreveObj = new DescricaoBreve(descricaoBreve);
        final DescricaoCompleta descricaoCompletaObj = new DescricaoCompleta(descricaoCompleta);

        final ProdutoBuilder newProdutoBuilder = new ProdutoBuilder(
                codigoComercialObj,
                codifoFabricoObj,
                descricaoBreveObj,
                descricaoCompletaObj);

        final Produto newProduto = newProdutoBuilder.build();
        return produtoRepository.save(newProduto);
    }

    @Transactional
    public Produto addProduto(final String codigoComercial,
                                   final String codigoFabricoProduto,
                                   final String descricaoBreve,
                                   final String descricaoCompleta,
                                   final FichaProducao fichaProducao) throws DataIntegrityViolationException, IllegalArgumentException {
        /*authorizationService.ensureAuthenticatedUserHasAnyOf(CafeteriaRoles.POWER_USER, CafeteriaRoles.MENU_MANAGER);*/
        final CodigoComercial codigoComercialObj = new CodigoComercial(codigoComercial);
        final CodigoFabricoProduto codifoFabricoObj = new CodigoFabricoProduto(codigoFabricoProduto);
        final DescricaoBreve descricaoBreveObj = new DescricaoBreve(descricaoBreve);
        final DescricaoCompleta descricaoCompletaObj = new DescricaoCompleta(descricaoCompleta);
        final ProdutoBuilder newProdutoBuilder = new ProdutoBuilder(
                codigoComercialObj,
                codifoFabricoObj,
                descricaoBreveObj,
                descricaoCompletaObj);

        final Produto newProduto = newProdutoBuilder.addFichaProducao(fichaProducao).build();
        return produtoRepository.save(newProduto);
    }


}
