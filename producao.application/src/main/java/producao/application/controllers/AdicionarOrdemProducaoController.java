package producao.application.controllers;

import org.aspectj.weaver.ast.Or;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import producao.domain.model.Quantidade;
import producao.domain.model.ordemProducao.IdentificadorEncomenda;
import producao.domain.model.ordemProducao.IdentificadorOrdemProducao;
import producao.domain.model.ordemProducao.OrdemProducao;
import producao.domain.model.produto.CodigoFabricoProduto;
import producao.domain.model.produto.Produto;
import producao.domain.repositories.OrdemProducaoRepository;
import producao.domain.repositories.ProdutoRepository;

import java.time.LocalDate;
import java.util.Set;

@Component
public class AdicionarOrdemProducaoController {

    @Autowired
    OrdemProducaoRepository ordemProducaoRepository;
    @Autowired
    ProdutoRepository produtoRepository;

    public Iterable<Produto> todosProdutos(){
      return produtoRepository.findAll();
    }

    public Produto procuraCodigoFabricoProduto(CodigoFabricoProduto codigoFabricoProduto){
        return produtoRepository.findByCodigoFabricoProduto(codigoFabricoProduto);
    }

    public OrdemProducao procuraIdentificadorOrdemProducao(IdentificadorOrdemProducao identificadorOrdemProducao){
        return ordemProducaoRepository.findByIdentificadorOrdemProducao(identificadorOrdemProducao);
    }

    @Transactional
    public OrdemProducao addOrdemProducao(String idOrdemProducao, CodigoFabricoProduto codigoFabricoProduto,
                                          Quantidade quantidade, String dataEmissao, String dataPrevistaExecucao,
                                          Set<IdentificadorEncomenda> idsEncomenda){

        IdentificadorOrdemProducao idOP = new IdentificadorOrdemProducao(idOrdemProducao);
        LocalDate emissao = LocalDate.parse(dataEmissao);
        LocalDate execucao = LocalDate.parse(dataPrevistaExecucao);

        OrdemProducao newOrderProducao = new OrdemProducao(idOP, codigoFabricoProduto, quantidade, emissao, execucao,
                idsEncomenda);

        return ordemProducaoRepository.save(newOrderProducao);

    }

}
