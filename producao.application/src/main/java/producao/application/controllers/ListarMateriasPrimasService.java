package producao.application.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import producao.domain.model.materiaPrima.MateriaPrima;
import producao.domain.repositories.MateriaPrimaRepository;

@Component
public class ListarMateriasPrimasService {

    @Autowired
    private MateriaPrimaRepository materiaPrimaRepository;

    public Iterable<MateriaPrima> todasMatPrima() {
        //authorizationService.ensureAuthenticatedUserHasAnyOf(CafeteriaRoles.POWER_USER, CafeteriaRoles.MENU_MANAGER);

        return materiaPrimaRepository.findAll();
    }
}
