package producao.application.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import producao.domain.model.ordemProducao.OrdemProducao;
import producao.domain.repositories.OrdemProducaoRepository;

@Component
public class ConsultarOrdemProducaodeEncomendaController {


    @Autowired
    OrdemProducaoRepository ordemProducaoRepository;

    public Iterable<OrdemProducao> mostraOrdemProducaoPorEncomenda(String identificadorEncomenda){

        return ordemProducaoRepository.findByIdsEncomenda_IdEncomenda(identificadorEncomenda);
    }

}
