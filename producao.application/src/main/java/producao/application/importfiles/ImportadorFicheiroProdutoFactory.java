package producao.application.importfiles;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;
import producao.application.controllers.AddProdutoController;
import utils.impl.ImportadorFicheiro;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

@Component
public class ImportadorFicheiroProdutoFactory {

    @Autowired
    private final AddProdutoController addProdutoController = new AddProdutoController();
    private Set<String> erros = new HashSet<>();

    /*
     * constructor for Spring framework
     * */
    protected ImportadorFicheiroProdutoFactory() {
    }

    private void buildProdutoFromCsvLine(String csvLine) {
        String[] data = csvLine.split(";");// ! CódigoFabrico;CódigoComercial;Descrição Breve;Descrição Completa;Unidade;Categoria
        try {
            if (checkCsvLineContent(data)) {
                addProdutoController.addProduto(data[1], data[0], data[2], data[3]);
            } else {
                erros.add("[INVALID] Falha nos dados de produto -> " + csvLine);
            }
        } catch (DataIntegrityViolationException dvex) {
            erros.add(String.format("[DUPLICATE] Produto já existente -> %s | %s", csvLine, dvex.getMessage()));
        } catch (IllegalArgumentException ex) {
            erros.add(String.format("[INVALID] Dados inválidos -> ", csvLine, ex.getMessage()));
        }
    }

    public int importFicheiro(File importFile) throws IOException {
        Set<String> listaLinhas;

        //String type = Files.probeContentType(importFile.toPath());
        String extension = FilenameUtils.getExtension(importFile.getAbsolutePath());
        String format = extension.toLowerCase();

        switch (format) {
            case "csv":
            case "plain":

                ImportadorFicheiro<String> importadorFicheiro = new ImportadorFicheiroProdutoEstrategiaCSV(importFile);
                listaLinhas = importadorFicheiro.importarDadosFicheiro();
                erros = importadorFicheiro.getErros();

                for (String linha : listaLinhas) {
                    if (!linha.contains("CódigoFabrico")) {
                        buildProdutoFromCsvLine(linha);
                    }
                }

                break;
            case "xml":

            case "json":

            default:
                return 0;
        }

        if (!erros.isEmpty())
            writeErrorsToFile();
        return 0;
    }

    private void writeErrorsToFile() {
        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter("errorImports.txt"));

            for (String s : erros) {
                bw.write(s + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                assert bw != null;
                bw.close();
            } catch (IOException | NullPointerException e) {
                e.printStackTrace();
            }
        }
    }

    private boolean checkCsvLineContent(String[] data) {
        for (String s : data) {
            if (s.isEmpty()) {
                return false;
            }
        }
        return true;
    }
}
