package producao.application.importfiles;

import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import producao.application.controllers.AdicionarOrdemProducaoController;
import producao.domain.model.Quantidade;
import producao.domain.model.Unidade;
import producao.domain.model.ordemProducao.IdentificadorEncomenda;
import producao.domain.model.produto.CodigoFabricoProduto;
import utils.impl.ImportadorFicheiro;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Set;

@Component
public class ImportadorFicheiroOrdemProducaoFactory {
    private static final Logger LOGGER = LogManager.getLogger(ImportadorFicheiroOrdemProducaoFactory.class);
    private final DateTimeFormatter formatIn = DateTimeFormatter.ofPattern("yyyyMMdd");
    private final DateTimeFormatter formatOut = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    @Autowired
    private AdicionarOrdemProducaoController controller;
    private Set<String> erros = new HashSet<>();

    /*
     * constructor for Spring framework
     * */
    private ImportadorFicheiroOrdemProducaoFactory() {
    }

    private boolean buildOrdemProducaoFromCsvLine(String csvLine) {

        try {
            String[] data = csvLine.split(";");// ! ID_ORDEM;DataEmissão;DataPrevistaExecução;CódigoFabrico;Quantidade;Unidade;Encomendas
            if (checkCsvLineContent(data)) {

                CodigoFabricoProduto codFP = new CodigoFabricoProduto(data[3]);
                Quantidade quantidade = new Quantidade(Long.parseLong(data[4]), Unidade.valueOf(data[5]));

                Set<IdentificadorEncomenda> identificadorEncomendas = new HashSet<>();
                IdentificadorEncomenda encomenda;
                for (String s : data[6].split(",")) {
                    encomenda = new IdentificadorEncomenda(s);
                    identificadorEncomendas.add(encomenda);
                }

                String dataemissao = LocalDate.parse(data[1], formatIn).format(formatOut);
                String dataprevisao = LocalDate.parse(data[2], formatIn).format(formatOut);

                controller.addOrdemProducao(data[0], codFP, quantidade, dataemissao, dataprevisao, identificadorEncomendas);

                return true;
            } else {
                erros.add("[ERRO] Ordem de produção inválida -> " + csvLine);
                return false;
            }
        } catch (Exception e) {
            erros.add(String.format("[ERRO] Ordem de Produção -> %s", e.getMessage().trim()));
            return false;
        }
    }


    public int importFicheiro(File importFile) {
        Set<String> listaLinhas;
        int importedCounter = 0;

        String extension = FilenameUtils.getExtension(importFile.getAbsolutePath());
        String format = extension.toLowerCase();

        switch (format) {
            case "csv":
            case "plain":

                ImportadorFicheiro<String> importadorFicheiro =
                        new ImportadorFicheiroOrdemProducaoEstrategiaCSV(importFile); //Lazy instantiation
                listaLinhas = importadorFicheiro.importarDadosFicheiro();
                erros = importadorFicheiro.getErros();

                for (String linha : listaLinhas) {
                    if (!linha.contains("ID_ORDEM") && buildOrdemProducaoFromCsvLine(linha))
                        importedCounter++;
                }

                break;
            case "xml":

            case "json":

            default:
        }

        if (!erros.isEmpty())
            writeErrorsToFile();
        return importedCounter;
    }

    private void writeErrorsToFile() {
        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter("errorImports.txt"));

            for (String s : erros) {
                bw.write(s + "\n");
            }
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        } finally {
            try {
                assert bw != null;
                bw.close();
            } catch (IOException | NullPointerException e) {
                LOGGER.error(e.getMessage());
            }
        }
    }

    private boolean checkCsvLineContent(String[] data) {
        for (String s : data) {
            if (s.isEmpty()) {
                return false;
            }
        }
        return true;
    }
}
