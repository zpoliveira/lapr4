package producao.application.importfiles;

import eapli.framework.util.Strategy;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;
import utils.impl.ImportadorFicheiro;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Strategy
@Component
public class ImportadorFicheiroOrdemProducaoEstrategiaCSV implements ImportadorFicheiro<String> {

    private static final Logger LOGGER = LogManager.getLogger(ImportadorFicheiroOrdemProducaoEstrategiaCSV.class);

    private File csvFile;

    private final Set<String> erros = new HashSet<>();


    public ImportadorFicheiroOrdemProducaoEstrategiaCSV(File file) {
        this.csvFile = file;
    }

    /*
     * Constructor for Spring Framework
     * */
    protected ImportadorFicheiroOrdemProducaoEstrategiaCSV() {
    }

    @Override
    public Set<String> importarDadosFicheiro() {

        Set<String> lista = new HashSet<>();
        int lineCounter = 0;

        if (csvFile.isFile() && csvFile.canRead()) {
            try {
                BufferedReader csvReader = new BufferedReader(new FileReader(csvFile));

                for (String line : csvReader.lines().collect(Collectors.toList())) {
                    ++lineCounter;
                    if (line.isEmpty()) {
                        erros.add(String.format("[ERRO] Linha %d | Content-> %s", lineCounter, line));
                    } else {
                        lista.add(line);
                    }
                }

                csvReader.close();
            } catch (IOException e) {
                return new HashSet<>();
            }
        } else {
            String message = String.format("[ERRO] File does not exist or can't be read! -> %s", csvFile.getAbsolutePath());
            erros.add(message);
            LOGGER.error(message);

            return new HashSet<>();
        }

        return lista;
    }

    @Override
    public Set<String> getErros() {
        return this.erros;
    }


}
