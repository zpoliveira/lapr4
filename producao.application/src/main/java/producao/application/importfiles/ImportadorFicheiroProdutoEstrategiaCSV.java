package producao.application.importfiles;

import eapli.framework.util.Strategy;
import utils.impl.ImportadorFicheiro;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Strategy
public class ImportadorFicheiroProdutoEstrategiaCSV implements ImportadorFicheiro<String> {


    private final Set<String> erros = new HashSet<>();
    private File csvFile;


    public ImportadorFicheiroProdutoEstrategiaCSV(File file) {
        this.csvFile = file;
    }

    /*
     * Constructor for Spring Framework
     * */
    private ImportadorFicheiroProdutoEstrategiaCSV() {
    }

    @Override
    public Set<String> importarDadosFicheiro() {

        Set<String> lista = new HashSet<>();
        int lineCounter = 0;

        try {
            BufferedReader csvReader = new BufferedReader(new FileReader(csvFile));

            for (String line : csvReader.lines().collect(Collectors.toList())) {
                ++lineCounter;
                if (line.isEmpty()) {
                    erros.add(String.format("[ERRO] Linha %d | Content-> %s", lineCounter, line));
                } else {
                    lista.add(line);
                }
            }

            csvReader.close();
        } catch (IOException e) {
            return new HashSet<>();
        }


        return lista;
    }

    @Override
    public Set<String> getErros() {
        return this.erros;
    }

}
