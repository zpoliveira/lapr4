package producao.application.controllers;

import org.junit.Assert;
import org.junit.Test;
import producao.application.importfiles.ImportadorFicheiroProdutoEstrategiaCSV;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class ImportadorFicheiroProdutoEstrategiaCSVTest {

    private ImportadorFicheiroProdutoEstrategiaCSV estrategiaCSV;


    @Test
    public void importarFicheiroInexistente() {
        estrategiaCSV = new ImportadorFicheiroProdutoEstrategiaCSV(new File("dummyFakeFile.csv"));

        Set<String> emptySet = new HashSet<>();
        Set<String> res = estrategiaCSV.importarDadosFicheiro();

        Assert.assertEquals(emptySet.size(), res.size());
    }

    @Test
    public void importarFicheiroVazioOuInvalido() {
        File empty = new File("fake.csv");
        try {
            empty.createNewFile();
            Set<String> emptySet = new HashSet<>();
            estrategiaCSV = new ImportadorFicheiroProdutoEstrategiaCSV(empty);
            try {
                Set<String> res = estrategiaCSV.importarDadosFicheiro();
                Assert.assertEquals(emptySet.size(), res.size());
            } catch (NullPointerException npe) {
                Assert.assertTrue(true);
            }
        } catch (IOException e) {
            e.printStackTrace();
            Assert.fail();
        }
    }
}