package producao.application.controllers;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

public class ImportarCatalogoControllerTest {

    private ImportarCatalogoController catalogoController;

    @Before
    public void setUp() {
        catalogoController = new ImportarCatalogoController();
    }

    @Test(expected = IOException.class)
    public void importarProdutos() throws IOException {
        int res = catalogoController.importarProdutos("dummyFakepath");
    }
}