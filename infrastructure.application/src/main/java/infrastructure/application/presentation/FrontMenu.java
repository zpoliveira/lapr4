package infrastructure.application.presentation;

import bootstrap.presentation.BootstrapMenu;
import chaofabrica.console.presentation.ChaoFabricaMenu;
import eapli.framework.actions.menu.Menu;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.ExitWithMessageAction;
import eapli.framework.presentation.console.menu.MenuItemRenderer;
import eapli.framework.presentation.console.menu.MenuRenderer;
import eapli.framework.presentation.console.menu.VerticalMenuRenderer;
import export.application.presentation.ExportTransformMenu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import producao.console.presentation.ProducaoMenu;

@Component
public class FrontMenu extends AbstractUI {
    private static final int EXIT_OPTION = 0;


    private static final int LOGIN_OPTION = 1;
    private static final int SIGNUP_OPTION = 2;
    private static final int BOOTSTRAP_GLOBAL = 1;
    private static final int PRODUCTION = 2;
    private static final int FACTORY_FLOOR = 3;
    private static final int EXPORT_FACTORY = 4;

    @Override
    public boolean show() {
        drawFormTitle();
        return doShow();
    }

    @Autowired
    private BootstrapMenu boostrapMenu;

    @Autowired
    private ProducaoMenu prodMenu;

    @Autowired
    private ChaoFabricaMenu chaoFabricaMenu;

    @Autowired
    private ExportTransformMenu exportarFabricaMenu;
    /*@Autowired
    private LoginUI loginUI;
    @Autowired
    private SignupRequestUI signupRequestUI;
    @Autowired
    private MainMenu mainMenu;*/

    /**
     * @return true if the user selected the exit option
     */
    @Override
    public boolean doShow() {
        final Menu menu = new Menu();
        /*menu.addItem(LOGIN_OPTION, "Login",
                new ChainedAction(loginUI.withRole(CafeteriaRoles.CAFETERIA_USER)::show, () -> {
                    mainMenu.mainLoop();
                    return true;
                }));
        menu.addItem(SIGNUP_OPTION, "Sign up", signupRequestUI::show);*/
        menu.addSubMenu(BOOTSTRAP_GLOBAL, boostrapMenu.bootStrapSubMenus());//sub-bootstarp or all
        menu.addSubMenu(PRODUCTION, prodMenu.producaoSubMenus());
        menu.addSubMenu(FACTORY_FLOOR, chaoFabricaMenu.chaoFabricaSubMenus());
        menu.addSubMenu(EXPORT_FACTORY, exportarFabricaMenu.exportarSubMenu());
        //menu.addItem(FACTORY_FLOOR, "Exit", new ExitWithMessageAction());
        menu.addItem(EXIT_OPTION, "Exit", new ExitWithMessageAction("Good Bye"));

        final MenuRenderer renderer = new VerticalMenuRenderer(menu, MenuItemRenderer.DEFAULT);
        return renderer.render();
    }

    @Override
    public String headline() {
        return "Fabrica";
    }

}
