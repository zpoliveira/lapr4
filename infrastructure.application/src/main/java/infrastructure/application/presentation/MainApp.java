package infrastructure.application.presentation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


@SpringBootApplication
@EntityScan(basePackages = {"producao", "chaofabrica", "spm"})
@EnableJpaRepositories(basePackages = {"producao", "persistence", "spm"})
@ComponentScan(basePackages = {"bootstrap", "infrastructure", "producao", "chaofabrica", "export"})
public class MainApp implements CommandLineRunner {

    @Autowired
    private FrontMenu frontMenu;

    public static void main(final String[] args) {
        SpringApplicationBuilder builder = new SpringApplicationBuilder(MainApp.class);
        builder.headless(false);
        ConfigurableApplicationContext context = builder.run(args);
    }

    @Override
    public void run(final String... args) {
        frontMenu.mainLoop();
    }


}
