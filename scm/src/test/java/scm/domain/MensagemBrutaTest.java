package scm.domain;

import org.junit.Test;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class MensagemBrutaTest {

    @org.junit.Before
    public void setUp() throws Exception {
    }

    @org.junit.After
    public void tearDown() throws Exception {
    }

    @Test(expected = IllegalArgumentException.class)
    public void TestIdNotNull(){
        MensagemBruta m = new MensagemBruta(null, "c9", LocalDateTime.now(), "outraInfo;comdados");

    }

    @Test(expected = IllegalArgumentException.class)
    public void TestIdNotEmpty(){
        MensagemBruta m = new MensagemBruta("", "c9", LocalDateTime.now(), "outraInfo;comdados");
    }

    @Test(expected = IllegalArgumentException.class)
    public void TestTipoNotNull(){
        MensagemBruta m = new MensagemBruta("3", null, LocalDateTime.now(), "outraInfo;comdados");
    }

    @Test(expected = IllegalArgumentException.class)
    public void TestTipoNotEmpty(){
        MensagemBruta m = new MensagemBruta("3", "", LocalDateTime.now(), "outraInfo;comdados");
    }

    @Test(expected = IllegalArgumentException.class)
    public void TestDataNotNull(){
        MensagemBruta m = new MensagemBruta("3", "c9", null, "outraInfo;comdados");
    }

    @Test(expected = IllegalArgumentException.class)
    public void TestDataNotFuture(){
        MensagemBruta m = new MensagemBruta("3", "c9", LocalDateTime.now().plusDays(2), "outraInfo;comdados");
    }

    @Test
    public void TestNoRepeatedMensage(){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        LocalDateTime dateTime = LocalDateTime.parse("20200501120000", formatter);
        MensagemBruta m = new MensagemBruta("3", "c9", dateTime, "outraInfo;comdados");
        MensagemBruta m2 = new MensagemBruta("3", "c9", dateTime, "outraInfo;comdados");
        assertEquals(m,m2);
    }

}