#!/bin/bash
STOREPASS="factyfloor"
for ENT in ScmServer Sistema Maquina; do
 rm -f ${ENT}.jks ${ENT}.pem
 echo -e "${ENT}\nDEI\nISEP\nPORTO\nPORTO\nPT\nyes" | keytool -genkey -v -alias ${ENT} -keyalg RSA -keysize 2048 \
	-validity 365 -keystore ${ENT}.jks -storepass ${STOREPASS}
 keytool -exportcert -alias ${ENT} -keystore ${ENT}.jks -storepass ${STOREPASS} -rfc -file ${ENT}.pem
done
####
echo "Creating trust relations"
### IMPORTING TRUSTED CERTIFICATES
### (The server trusts all clients except for client4_J)
### (Every client trusts ScmServer)
for ENT in Sistema Maquina; do
 echo "yes"|keytool -import -alias ${ENT} -keystore ScmServer.jks -file ${ENT}.pem -storepass ${STOREPASS}
 echo "yes"|keytool -import -alias ScmServer -keystore ${ENT}.jks -file ScmServer.pem -storepass ${STOREPASS}
 ##generate key for C client
 keytool -importkeystore -srckeystore ${ENT}.jks -srcstorepass ${STOREPASS} -srckeypass ${STOREPASS} -destkeystore ${ENT}.p12 -deststoretype PKCS12 -srcalias ${ENT} -deststorepass ${STOREPASS} -destkeypass ${STOREPASS}
 openssl pkcs12 -in ${ENT}.p12 -passin pass:${STOREPASS} -nokeys  -out ${ENT}.pem -nodes
 openssl pkcs12 -in ${ENT}.p12 -passin pass:${STOREPASS} -nocerts -out ${ENT}.key -passout pass:${STOREPASS} -nodes
 #echo "yes"|keytool -importkeystore -srckeystore ScmServer.jks -destkeystore ${ENT}.p12 -deststoretype PKCS12 -storepass ${STOREPASS}
done
echo "############################################################################"
keytool -list -keystore ScmServer.jks -storepass ${STOREPASS}
echo "############################################################################"
echo "WARNING: For testing, client4_J is not added to the list of authorized clients"
echo "############################################################################"
#######