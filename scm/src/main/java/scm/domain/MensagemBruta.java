package scm.domain;

import eapli.framework.validations.Preconditions;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@IdClass(MensagemBrutaId.class)
public class MensagemBruta implements Serializable {

    private static final long serialVersionUID = 1L;

    @Version
    private Long version;

    @Id
    private String id;
    @Id
    private String tipo;
    @Id
    private LocalDateTime data;

    private String outraInfo;

    protected MensagemBruta() {
    }

    /**
     * Para criar a info presente nas mesagem para serem posteriormente enriquecidas e processadas.
     * @param id - id da maquina
     * @param tipo - tipo de mensagem
     * @param data - data em que ocorre
     * @param outraInfo - outros dados provenientes das mensagens gurdados no formato csv
     */
    public MensagemBruta(String id, String tipo, LocalDateTime data, String outraInfo){
        Preconditions.noneNull(id, tipo,data);
        Preconditions.nonEmpty(id);
        Preconditions.nonEmpty(tipo);
        if(data.isAfter(LocalDateTime.now().plusDays(1))){
            throw new IllegalArgumentException("the date is for future");
        }
        this.id = id;
        this.tipo = tipo;
        this.data = data;
        this.outraInfo = outraInfo;
    }

    public String id() {
        return id;
    }

    public String tipo() {
        return tipo;
    }

    public LocalDateTime data() {
        return data;
    }

    public String outraInfo() {
        return outraInfo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MensagemBruta that = (MensagemBruta) o;
        return id.equalsIgnoreCase(that.id) &&
                tipo.equalsIgnoreCase(that.tipo) &&
                data.equals(that.data);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, tipo, data);
    }
}
