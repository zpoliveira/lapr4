package scm.domain;

import java.io.Serializable;
import java.time.LocalDateTime;

public class MensagemBrutaId implements Serializable {
    private static final long serialVersionUID = 1L;
    private String id;
    private String tipo;
    private LocalDateTime data;

    protected MensagemBrutaId() {
    }

    public MensagemBrutaId(String id, String tipo, LocalDateTime data) {
        this.id = id;
        this.tipo = tipo;
        this.data = data;
    }
}
