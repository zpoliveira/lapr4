package scm.application.importfiles;

import org.springframework.dao.DataIntegrityViolationException;
import scm.domain.MensagemBruta;
import utils.impl.ImportadorFicheiro;

import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.IntStream;

public class ImportarMensagensMaquinaCSV implements ImportadorFicheiro<MensagemBruta> {

    private File file;

    private Set<String> errors = new HashSet<>();

    public ImportarMensagensMaquinaCSV(File file) {
        this.file = file;
    }

    @Override
    public Set<MensagemBruta> importarDadosFicheiro() {
        String line = "";
        Set<MensagemBruta> mensagemBrutas = new HashSet<>();
        if (this.file.isFile() && this.file.canRead()) {
            try {
                BufferedReader csvReader = new BufferedReader(new FileReader(this.file));
                String[] data;
                while ((line = csvReader.readLine()) != null) {
                    data = line.split(";");
                    if(data.length < 3){
                        this.errors.add("dados incompletos: " + line);
                        continue;
                    }
                    try{

                    }catch (DataIntegrityViolationException e){

                    }
                    LocalDateTime dateTime;
                    try{
                        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
                        dateTime = LocalDateTime.parse(data[2], formatter);
                    }catch (DateTimeParseException e){
                        this.errors.add("data com formato incorreto: " + line);
                        continue;
                    }
                    String addicionalData="";
                    if(data.length > 3){
                        String[] finalData = data;
                        String[] subarray = IntStream.range(3, data.length)
                                .mapToObj(i -> finalData[i])
                                .toArray(String[]::new);
                        addicionalData =  String.join(";", subarray);
                    }
                    try{
                        MensagemBruta novaMensagem = new MensagemBruta(data[0], data[1], dateTime, addicionalData);
                        if (mensagemBrutas.contains(novaMensagem)) {
                            this.errors.add("mensagem repetida: " + line);
                        } else {
                            mensagemBrutas.add(novaMensagem);
                        }
                    }catch (IllegalArgumentException e){
                        this.errors.add(e.getMessage() + "-> " + line);
                    }
                }
                csvReader.close();
            }catch (IOException e) {
                System.out.println(e.getMessage());
            }
        } else {
            this.errors.add("erro no ficheiro: " + this.file.getName());
        }
        return mensagemBrutas;
    }

    @Override
    public Set<String> getErros() {
        return this.errors;
    }
}
