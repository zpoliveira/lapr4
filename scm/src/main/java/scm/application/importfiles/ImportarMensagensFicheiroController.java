package scm.application.importfiles;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;
import scm.repository.MensagemBrutaRepository;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ImportarMensagensFicheiroController {

    @Value("${scm.in.folder}")
    private String inFolderPath;
    @Autowired
    MensagemBrutaRepository mensagemBrutaRepository;
    @Autowired
    private AutowireCapableBeanFactory beanFactory;

    @Autowired
    ImportarMensagensFicheiroServiceAsync importarMensagensFicheiroServiceAsync;

    public void importarMensagensFicheiros() {
        try {
            String homePath = System.getProperty("user.home");
            List<File> filesInFolder = Files.walk(Paths.get(homePath + this.inFolderPath))
                    .filter(Files::isRegularFile)
                    .map(Path::toFile)
                    .collect(Collectors.toList());
            filesInFolder.forEach(file -> {
                try{
                    mensagemBrutaRepository.deleteAll();
                    importarMensagensFicheiroServiceAsync.importMensages(file);
                }catch (DataIntegrityViolationException e){
                    System.out.println("ja existente na BD " + e.getMessage());
                }


                // IF WE WHAT TO USE THREAD WITHOUT SPRING
                    //ImportarMensagensFicheiroService imp = new ImportarMensagensFicheiroService();
                    //beanFactory.autowireBean(imp);
                    //imp.start(file.getName(), file);
                // INFORMATION END
            });
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
