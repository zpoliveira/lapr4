package scm.application.importfiles;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import scm.domain.MensagemBruta;
import scm.repository.MensagemBrutaRepository;
import utils.impl.ImportadorFicheiro;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Set;

@Service
public class ImportarMensagensFicheiroServiceAsync {

    @Autowired
    MensagemBrutaRepository mensagemBrutaRepository;
    @Autowired
    ImportarMensagensFactory importarMensagensFactory;

    @Value("${scm.out.folder}")
    private String outPath;
    private String homePath = System.getProperty("user.home");


    @Async
    @Transactional
    public void importMensages(File file) throws DataIntegrityViolationException {
        BufferedWriter threadBufferedWrite = this.createBufferedWriterForError(file);
        System.out.println("Runing ASYNC " + file.getName());
        final ImportadorFicheiro<MensagemBruta> importador = importarMensagensFactory.build(file);
        Set<MensagemBruta> mensagemBrutas = importador.importarDadosFicheiro();
        Set<String> errors = importador.getErros();
        this.copyFile(file);
        mensagemBrutaRepository.saveAll(mensagemBrutas);
        this.writeLog(errors, threadBufferedWrite);
    }

    private BufferedWriter createBufferedWriterForError(File file) {
        FileWriter fileWriter = null;
        File directory = new File(this.homePath + this.outPath);
        if (!directory.exists()) {
            directory.mkdirs();
        }
        try {
            fileWriter = new FileWriter(this.homePath + this.outPath + file.getName() + ".error_log", true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new BufferedWriter(fileWriter);
    }

    private void writeLog(Set<String> errors, BufferedWriter bufferedWriter) {
        errors.forEach(error -> {
            try {
                bufferedWriter.write(LocalDateTime.now() + ": " + error);
                bufferedWriter.write("\n");
            } catch (IOException e) {
                e.printStackTrace();
            }

        });
        try {
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void copyFile(File file) {
        File copiedFile = new File(this.homePath + this.outPath + file.getName()+"_processed");
        try {
            FileUtils.copyFile(file, copiedFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
