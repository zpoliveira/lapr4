package scm.application.importfiles;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import scm.domain.MensagemBruta;
import utils.impl.ImportadorFicheiro;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

@Component
public class ImportarMensagensFactory {
    @Value("${scm.files.formats}")
    private String[] fileFormats;
    private String format;

    public ImportadorFicheiro<MensagemBruta> build(final File file) throws IllegalArgumentException {
        String format;
        //try {
            //String type = Files.probeContentType(file.toPath());
            String extension = FilenameUtils.getExtension(file.getAbsolutePath());
            this.format = extension.toLowerCase();
            //this.format = type.split("/")[1];
        /*} catch (IOException e) {
            e.printStackTrace();
        }*/
        switch (this.format){
            case "csv":
            case "plain":
                return new ImportarMensagensMaquinaCSV(file);
            case "xml":
                //return new ImportarMensagensMaquinaCSV(file);
            case "json":
                //return new ImportarMensagensMaquinaCSV(file);
        }
        throw new IllegalArgumentException("Formato do ficheiro desconhecido");
    }
}
