package scm.application.importfiles;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scm.domain.MensagemBruta;
import scm.repository.MensagemBrutaRepository;

import java.io.File;
import java.util.Set;

@Component
public class ImportarMensagensFicheiroService implements Runnable{
    private Thread thread;
    private String threadName;
    private File file;


    @Autowired
    MensagemBrutaRepository mensagemBrutaRepository;

    @Override
    public void run() {
        System.out.println("Runing " +  threadName);
       //Set<MensagemBruta> m = new ImportarMensagensMaquinaCSV(this.file).mensagens();
        //mensagemBrutaRepository.saveAll(m);
    }

    public void start (String threadName, File file) {
        System.out.println("Starting " +  threadName );
        this.threadName = threadName;
        this.file = file;
        if (this.thread == null) {
            this.thread = new Thread (this, threadName);
            this.thread.start();
        }
    }

}
