package scm.application.socketserver;

import chaofabrica.domain.model.maquina.Maquina;
import chaofabrica.domain.repositories.MaquinaRepository;
import org.springframework.beans.factory.annotation.Value;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.net.SocketException;
import java.util.Arrays;
import java.util.HashMap;

public class ScmSocketServerThread implements Runnable {
    private Thread thread;
    private String threadName;
    private Socket socket;
    private DataInputStream sIn;
    private DataOutputStream sOut;
    private final int MAX_BUUF = 65541;
    private final int HELLO = 0;
    private final int MSG = 1;
    private final int CONFIG = 2;
    private MaquinaRepository maquinaRepository;
    private HashMap<Integer, String> enderecosMaquinas;
    private HashMap<Integer, Socket> socketMaquina;
    private final byte[] ack = new byte[2];
    private final byte[] nAck = new byte[2];
    private MensagemBrutaController mensagemBrutaController;

    @Value("${scm.port}")
    int port;

    @Override
    public void run() {
        System.out.println("Runing " + threadName);
            byte[] data = new byte[MAX_BUUF];
            this.ack[0]= 0;
            this.ack[1]=(byte)150;
            this.nAck[0] = 0;
            this.nAck[1]=(byte)151;

            try {
                sIn = new DataInputStream(socket.getInputStream());
                while(true){
                    int i = sIn.read(data);
                    if(i==-1){
                        break;
                    }
                    //hello
                    if( (data[1] & 0xFF) == HELLO){
                        this.verifyHello(data);
                    }else if((data[1] & 0xFF) == MSG){
                        this.processMensage(data);
                    }else if((data[1] & 0xFF) == CONFIG)
                        this.configMessage(data);
                    data= new byte[MAX_BUUF];
                }
            }
            catch(IOException ex) {
                this.sendResponse(socket, nAck);
                ex.printStackTrace();
                System.out.println("Error" + ex.getMessage());
            }
            //OutputStream output = socket.getOutputStream();

    }

    public void start(
            String threadName,
            Socket socket,
            HashMap<Integer, String> enderecosMaquinas,
            HashMap<Integer, Socket> socketMaquina,
            MaquinaRepository maquinaRepository,
            MensagemBrutaController mensagemBrutaController) {
        System.out.println("Starting " + threadName);
        this.threadName = threadName;
        this.socket = socket;
        this.enderecosMaquinas = enderecosMaquinas;
        this.socketMaquina = socketMaquina;
        this.maquinaRepository = maquinaRepository;
        this.mensagemBrutaController = mensagemBrutaController;
        if (this.thread == null) {
            this.thread = new Thread(this, threadName);
            this.thread.start();
        }
    }

    private void sendResponse (Socket socket, byte[] ackType){
        try {
            sOut = new DataOutputStream(socket.getOutputStream());
            sOut.write(ackType);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void verifyHello(byte[] data ){
        int codigoProtMaquina = (data[2]&0xFF) + 256 * (data[3]&0xFF);
        if(this.enderecosMaquinas.containsKey(codigoProtMaquina)){
            this.sendResponse(socket, ack);
        }else{
            Maquina m = maquinaRepository.findByCodigoUnicoProtocolo(codigoProtMaquina);
            if(m == null){
                this.sendResponse(socket, nAck);
            }else{
                this.sendResponse(socket, ack);
                this.enderecosMaquinas.put(codigoProtMaquina, socket.getInetAddress().toString());
                this.socketMaquina.put(codigoProtMaquina, socket);
            }
        }
    }

    private void configMessage(byte[] data){
        int codigoProtMaquina = (data[2]&0xFF) + 256 * (data[3]&0xFF);
        if(this.enderecosMaquinas.containsKey(codigoProtMaquina)){
            reEnviaConfigMessage(data, codigoProtMaquina);

        }else{
            this.sendResponse(socket, nAck);
        }
    }

    private void processMensage(byte[] data){
        String text = null;
        data = Arrays.copyOfRange(data, 6, MAX_BUUF);
        try {
            text = new String(data, "UTF-8");
            text = text.replace("\n", "").replace("\r", "").trim();
        } catch (UnsupportedEncodingException e) {
            this.sendResponse(socket, ack);
            e.printStackTrace();
        }
        boolean wasDataPreValidated = this.mensagemBrutaController.recebMensage(text);
        if(wasDataPreValidated){
            this.sendResponse(socket, ack);
        }
       /* else{
            this.sendResponse(socket, nAck);
        }*/
    }

    private void reEnviaConfigMessage(byte[] mensagem, int codigoProtMaquina){
    /*    byte[] dataConfig = new byte[2];
        byte[] dataResp = new byte[6];*/

        Socket maqSocket = socketMaquina.get(codigoProtMaquina);

        try {

            DataOutputStream output = new DataOutputStream(maqSocket.getOutputStream());
            System.out.println("Ficheiro de Configuração guardado com sucesso");
            output.write(mensagem);

            /*DataInputStream sIn = new DataInputStream(maqSocket.getInputStream());
            while (true) {
                int i = sIn.read(dataConfig);
                if (i == -1) {
                    break;
                }
                if (dataConfig[1] == (byte) 150) {
                    this.sendResponse(this.socket,ack);
                } else {
                    System.out.println("Nack from Máquina");
                    this.sendResponse(this.socket, nAck);
                }
            }*/

        }catch (SocketException e){
            this.sendResponse(this.socket, nAck);
            socketMaquina.remove(codigoProtMaquina);
            enderecosMaquinas.remove(codigoProtMaquina);
            System.out.println("Foi removido o socket de conectados");
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }

    }
}
