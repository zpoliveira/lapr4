package scm.application.socketserver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;
import scm.domain.MensagemBruta;
import scm.repository.MensagemBrutaRepository;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.stream.IntStream;


@Component
public class MensagemBrutaController {
    @Value("${scm.out.folder}")
    private String outPath;
    private String homePath = System.getProperty("user.home");

    @Autowired
    MensagemBrutaRepository mensagemBrutaRepository;

    public boolean recebMensage(String mensage) {
        System.out.println(mensage);
        try{
            MensagemBruta mensagemBruta = this.preValidateMensage(mensage);
            mensagemBrutaRepository.save(mensagemBruta);
        }catch (IllegalArgumentException e){
            this.writeLog(mensage, e.getMessage());
            return true;
        }catch(DataIntegrityViolationException e){
            this.writeLog(mensage, "Mensagem já existente");
            return true;
        }
        return true;
    }

    private MensagemBruta preValidateMensage(String mensage) throws IllegalArgumentException {
        String[] data = mensage.split(";");
        if (data.length < 3 || mensage.length() < 1) {
            throw new IllegalArgumentException("dados incompletos:");
        }
        LocalDateTime dateTime;
        try{
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
            dateTime = LocalDateTime.parse(data[2], formatter);
        }catch (DateTimeParseException e){
            throw new IllegalArgumentException("data deve estar no formato: yyyyMMddHHmmss");
        }
        String addicionalData = "";
        if (data.length > 3) {
            String[] finalData = data;
            String[] subarray = IntStream.range(3, data.length)
                    .mapToObj(i -> finalData[i])
                    .toArray(String[]::new);
            addicionalData = String.join(";", subarray);
        }
        MensagemBruta novaMensagem = new MensagemBruta(data[0], data[1], dateTime, addicionalData);
        return novaMensagem;
    }

    private void writeLog(String mensage, String error) {
        BufferedWriter bufferedWriter = this.writeErrorLog(mensage);
        try {
            bufferedWriter.write(LocalDateTime.now() + ": " + mensage + "->" + error);
            bufferedWriter.write("\n");
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private BufferedWriter writeErrorLog(String mensage) {
        String logName = mensage.split(";")[0];
        FileWriter fileWriter = null;
        File directory = new File(this.homePath + this.outPath);
        if (!directory.exists()) {
            directory.mkdirs();
        }
        try {
            fileWriter = new FileWriter(this.homePath + this.outPath + logName + ".error.log", true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new BufferedWriter(fileWriter);
    }

}






