package scm.application.socketserver;

import chaofabrica.domain.repositories.MaquinaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import java.io.IOException;
import java.net.Socket;
import java.util.HashMap;

@Component
public class ScmSocketServer {

    public HashMap<Integer, String> enderecosMaquinas = new HashMap<>();
    public HashMap<Integer, Socket> socketMaquina = new HashMap<>();

    @Value("${scm.cetificates}")
    private String certificatesPath;

    @Autowired
    MaquinaRepository maquinaRepository;
    @Autowired
    MensagemBrutaController mensagemBrutaController;

    private static final int SERVER_PORT = 31403;



    public ScmSocketServer() {

    }

    public void start(){

        try {
            String homePath = System.getProperty("user.home");
            // Trust these certificates provided by authorized clients
            System.setProperty("javax.net.ssl.trustStore", homePath + certificatesPath + "ScmServer.jks");
            System.setProperty("javax.net.ssl.trustStorePassword","factyfloor");

            // Use this certificate and private key as server certificate
            System.setProperty("javax.net.ssl.keyStore", homePath + certificatesPath +"ScmServer.jks");
            System.setProperty("javax.net.ssl.keyStorePassword","factyfloor");

            SSLServerSocketFactory sslServerSocketFactory = (SSLServerSocketFactory) SSLServerSocketFactory.getDefault();
            SSLServerSocket serverSocket = (SSLServerSocket) sslServerSocketFactory.createServerSocket(SERVER_PORT);
            serverSocket.setNeedClientAuth(true);
            System.out.println("Server is listening on port " + SERVER_PORT);

            while (true) {
                Socket socket = serverSocket.accept();
                System.out.println("New client connected " + socket.getInetAddress().toString());
                new ScmSocketServerThread().start(
                        "scmServer",
                        socket,
                        enderecosMaquinas,
                        socketMaquina,
                        maquinaRepository,
                        mensagemBrutaController);
            }
        } catch (IOException e) {
            System.out.println("Server exception: " + e.getMessage());
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
