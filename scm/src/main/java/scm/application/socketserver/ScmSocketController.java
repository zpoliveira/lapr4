package scm.application.socketserver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.stereotype.Component;

@Component
public class ScmSocketController {

    @Autowired
    ScmSocketServer scmSocketServer;

    public void startSocketServer(){
        scmSocketServer.start();
    }
}
