package scm.persistence;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.Repository;
import scm.domain.MensagemBruta;
import scm.repository.MensagemBrutaRepository;

import javax.persistence.OneToMany;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

public interface SpringDataMensagemBrutaRepository extends MensagemBrutaRepository, Repository<MensagemBruta, Long> {

    @Override
    Iterable<MensagemBruta> saveAll(Iterable<MensagemBruta> mensagensBrutas);

    @Override
    MensagemBruta save(MensagemBruta mensagemBruta);

    @Override
    Iterable<MensagemBruta> findAllByDataBetweenAndIdInOrderByDataAsc(LocalDateTime start, LocalDateTime end, List<String> idsMaquinas);

    @Override
    Iterable<MensagemBruta> findAllByDataBetween(LocalDateTime dataInicio, LocalDateTime dataFim);

    @Override
    void  deleteAll();

    @Override
    Iterable<MensagemBruta> findAll();

    @Override
    Iterable<MensagemBruta> findAllByDataGreaterThanEqual(LocalDateTime data);
}
