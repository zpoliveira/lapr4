package scm.repository;

import org.springframework.data.jpa.repository.Modifying;
import scm.domain.MensagemBruta;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

public interface MensagemBrutaRepository {

    Iterable<MensagemBruta> saveAll(Iterable<MensagemBruta> mensagensBrutas);

    MensagemBruta save(MensagemBruta mensagemBruta);

    Iterable<MensagemBruta> findAllByDataBetweenAndIdInOrderByDataAsc(LocalDateTime start, LocalDateTime end, List<String> idsMaquinas);

    Iterable<MensagemBruta> findAllByDataBetween(LocalDateTime dataInicio, LocalDateTime dataFim);

    void  deleteAll();

    Iterable<MensagemBruta> findAll();

    Iterable<MensagemBruta> findAllByDataGreaterThanEqual(LocalDateTime data);

}

