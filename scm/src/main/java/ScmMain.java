import chaofabrica.domain.repositories.MaquinaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import scm.application.importfiles.ImportarMensagensFicheiroController;
import scm.application.socketserver.ScmSocketController;

import java.util.concurrent.Executor;

@SpringBootApplication
@EntityScan( basePackages = {"scm", "chaofabrica", "producao"})
@EnableJpaRepositories(basePackages = {"scm", "persistence"})
@ComponentScan(basePackages = {"scm", "chaofabrica", "producao"})
@EnableAsync
public class ScmMain implements CommandLineRunner {

    @Autowired
    ImportarMensagensFicheiroController importarMensagensFicheiroController;
    @Autowired
    ScmSocketController scmSocketController;

    @Autowired
    MaquinaRepository maquinaRepository;

    public static void main(final String[] args){
        SpringApplication.run(ScmMain.class, args);

    }

    @Override
    public void run(final String... args) {
        importarMensagensFicheiroController.importarMensagensFicheiros();
        scmSocketController.startSocketServer();
    }

    @Bean
    public Executor taskExecuter(){
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(4);
        executor.setMaxPoolSize(4);
        executor.setQueueCapacity(500);
        executor.setThreadNamePrefix("scm-");
        executor.initialize();
        return executor;
    }


}
