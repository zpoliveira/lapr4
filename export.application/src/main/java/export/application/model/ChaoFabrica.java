package export.application.model;


import chaofabrica.domain.dto.deposito.TDepositos;
import chaofabrica.domain.dto.linhasproducao.TLinhasProducao;
import chaofabrica.domain.dto.maquina.TMaquinas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;all&gt;
 *         &lt;element name="Depositos" type="{http://factyfloor.com/Depositos}TDepositos"/&gt;
 *         &lt;element name="Maquinas" type="{http://factyfloor.com/Maquinas}TMaquinas"/&gt;
 *         &lt;element name="LinhasProducao" type="{http://factyfloor.com/LinhasProducao}TLinhasProducao"/&gt;
 *       &lt;/all&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {

})
public class ChaoFabrica {

    @XmlElement(name = "Depositos", namespace = "http://factyfloor.com/Fabrica", required = true)
    protected TDepositos depositos;
    @XmlElement(name = "Maquinas", namespace = "http://factyfloor.com/Fabrica", required = true)
    protected TMaquinas maquinas;
    @XmlElement(name = "LinhasProducao", namespace = "http://factyfloor.com/Fabrica", required = true)
    protected TLinhasProducao linhasProducao;

    /**
     * Gets the value of the depositos property.
     *
     * @return possible object is
     * {@link TDepositos }
     */
    public TDepositos getDepositos() {
        return depositos;
    }

    /**
     * Sets the value of the depositos property.
     *
     * @param value allowed object is
     *              {@link TDepositos }
     */
    public void setDepositos(TDepositos value) {
        this.depositos = value;
    }

    /**
     * Gets the value of the maquinas property.
     *
     * @return possible object is
     * {@link TMaquinas }
     */
    public TMaquinas getMaquinas() {
        return maquinas;
    }

    /**
     * Sets the value of the maquinas property.
     *
     * @param value allowed object is
     *              {@link TMaquinas }
     */
    public void setMaquinas(TMaquinas value) {
        this.maquinas = value;
    }

    /**
     * Gets the value of the linhasProducao property.
     *
     * @return possible object is
     * {@link TLinhasProducao }
     */
    public TLinhasProducao getLinhasProducao() {
        return linhasProducao;
    }

    /**
     * Sets the value of the linhasProducao property.
     *
     * @param value allowed object is
     *              {@link TLinhasProducao }
     */
    public void setLinhasProducao(TLinhasProducao value) {
        this.linhasProducao = value;
    }

}