package export.application.model;


import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ChaoFabrica"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;all&gt;
 *                   &lt;element name="Depositos" type="{http://factyfloor.com/Depositos}TDepositos"/&gt;
 *                   &lt;element name="Maquinas" type="{http://factyfloor.com/Maquinas}TMaquinas"/&gt;
 *                   &lt;element name="LinhasProducao" type="{http://factyfloor.com/LinhasProducao}TLinhasProducao"/&gt;
 *                 &lt;/all&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Producao"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;all&gt;
 *                   &lt;element name="CategoriasMateriaPrima" type="{http://factyfloor.com/CategoriaMateriaPrima}TCategoriasMateriaPrima"/&gt;
 *                   &lt;element name="MateriasPrimas" type="{http://factyfloor.com/MateriaPrima}TMateriasPrimas"/&gt;
 *                   &lt;element name="Produtos" type="{http://factyfloor.com/Produtos}TProdutos"/&gt;
 *                 &lt;/all&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "chaoFabrica",
        "producao"
})
@XmlRootElement(name = "Fabrica")//, namespace = "http://factyfloor.com/Fabrica")
public class Fabrica {

    @XmlElement(name = "ChaoFabrica", namespace = "http://factyfloor.com/Fabrica")
    protected ChaoFabrica chaoFabrica;
    @XmlElement(name = "Producao", namespace = "http://factyfloor.com/Fabrica")
    protected Producao producao;

    /**
     * Gets the value of the chaoFabrica property.
     *
     * @return possible object is
     * {@link ChaoFabrica }
     */
    public ChaoFabrica getChaoFabrica() {
        return chaoFabrica;
    }

    /**
     * Sets the value of the chaoFabrica property.
     *
     * @param value allowed object is
     *              {@link ChaoFabrica }
     */
    public void setChaoFabrica(ChaoFabrica value) {
        this.chaoFabrica = value;
    }

    /**
     * Gets the value of the producao property.
     *
     * @return possible object is
     * {@link Producao }
     */
    public Producao getProducao() {
        return producao;
    }

    /**
     * Sets the value of the producao property.
     *
     * @param value allowed object is
     *              {@link Producao }
     */
    public void setProducao(Producao value) {
        this.producao = value;
    }

}
