package export.application.model;

import producao.dto.categoriamateriaprima.TCategoriasMateriaPrima;
import producao.dto.materiaprima.TMateriasPrimas;
import producao.dto.ordemproducao.TOrdens;
import producao.dto.produtos.TProdutos;
import spm.dto.execucaoOrdemProducao.TExecucoes;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;all&gt;
 *         &lt;element name="CategoriasMateriaPrima" type="{http://factyfloor.com/CategoriaMateriaPrima}TCategoriasMateriaPrima"/&gt;
 *         &lt;element name="MateriasPrimas" type="{http://factyfloor.com/MateriaPrima}TMateriasPrimas"/&gt;
 *         &lt;element name="Produtos" type="{http://factyfloor.com/Produtos}TProdutos"/&gt;
 *         &lt;element name="OrdensProducao" type="{http://factyfloor.com/OrdensProducao}TOrdens"/&gt;
 *         &lt;element name="ExecucaoOrdensProducao" type="{http://factyfloor.com/ExecucaoOrdensProducao}TExecucoes"/&gt;
 *       &lt;/all&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {

})
public class Producao {

    @XmlElement(name = "CategoriasMateriaPrima", namespace = "http://factyfloor.com/Fabrica", required = true)
    protected TCategoriasMateriaPrima categoriasMateriaPrima;
    @XmlElement(name = "MateriasPrimas", namespace = "http://factyfloor.com/Fabrica", required = true)
    protected TMateriasPrimas materiasPrimas;
    @XmlElement(name = "Produtos", namespace = "http://factyfloor.com/Fabrica", required = true)
    protected TProdutos produtos;
    @XmlElement(name = "OrdensProducao", namespace = "http://factyfloor.com/Fabrica", required = true)
    protected TOrdens ordensProducao;
    @XmlElement(name = "ExecucaoOrdensProducao", namespace = "http://factyfloor.com/Fabrica", required = true)
    protected TExecucoes execucaoOrdensProducao;

    /**
     * Gets the value of the categoriasMateriaPrima property.
     *
     * @return possible object is
     * {@link TCategoriasMateriaPrima }
     */
    public TCategoriasMateriaPrima getCategoriasMateriaPrima() {
        return categoriasMateriaPrima;
    }

    /**
     * Sets the value of the categoriasMateriaPrima property.
     *
     * @param value allowed object is
     *              {@link TCategoriasMateriaPrima }
     */
    public void setCategoriasMateriaPrima(TCategoriasMateriaPrima value) {
        this.categoriasMateriaPrima = value;
    }

    /**
     * Gets the value of the materiasPrimas property.
     *
     * @return possible object is
     * {@link TMateriasPrimas }
     */
    public TMateriasPrimas getMateriasPrimas() {
        return materiasPrimas;
    }

    /**
     * Sets the value of the materiasPrimas property.
     *
     * @param value allowed object is
     *              {@link TMateriasPrimas }
     */
    public void setMateriasPrimas(TMateriasPrimas value) {
        this.materiasPrimas = value;
    }

    /**
     * Gets the value of the produtos property.
     *
     * @return possible object is
     * {@link TProdutos }
     */
    public TProdutos getProdutos() {
        return produtos;
    }

    /**
     * Sets the value of the produtos property.
     *
     * @param value allowed object is
     *              {@link TProdutos }
     */
    public void setProdutos(TProdutos value) {
        this.produtos = value;
    }

    /**
     * Gets the value of the ordensProducao property.
     *
     * @return possible object is
     * {@link TOrdens }
     */
    public TOrdens getOrdensProducao() {
        return ordensProducao;
    }

    /**
     * Sets the value of the ordensProducao property.
     *
     * @param value allowed object is
     *              {@link TOrdens }
     */
    public void setOrdensProducao(TOrdens value) {
        this.ordensProducao = value;
    }

    /**
     * Gets the value of the execucaoOrdensProducao property.
     *
     * @return possible object is
     * {@link TExecucoes }
     */

    public TExecucoes getExecucaoOrdensProducao() {
        return execucaoOrdensProducao;
    }

    /**
     * Sets the value of the execucaoOrdensProducao property.
     *
     * @param value allowed object is
     *              {@link TExecucoes }
     */

    public void setExecucaoOrdensProducao(TExecucoes value) {
        this.execucaoOrdensProducao = value;
    }

}

