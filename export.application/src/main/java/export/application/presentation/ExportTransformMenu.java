package export.application.presentation;

import eapli.framework.actions.Actions;
import eapli.framework.actions.menu.Menu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ExportTransformMenu {
    private static final String MENU_TITLE = "Exportar Fabrica e dados";

    private static final int EXIT_OPTION = 0;
    private static final int EXPORT_TO_XML = 1;
    private static final int TRANSFORM_XML = 2;


    @Autowired
    private ExportarFabricaXmlUI exportarFabricaXmlUI;
    @Autowired
    private TransformXmlDataUI transformXmlDataUI;


    public Menu exportarSubMenu() {
        final Menu prodSubMenu = new Menu(MENU_TITLE);

        prodSubMenu.addItem(EXPORT_TO_XML, "Exportar dados da Fábrica", exportarFabricaXmlUI::show);
        prodSubMenu.addItem(TRANSFORM_XML, "Exportar noutro formato", transformXmlDataUI::show);
        prodSubMenu.addItem(EXIT_OPTION, "Return ", Actions.SUCCESS);

        return prodSubMenu;
    }
}
