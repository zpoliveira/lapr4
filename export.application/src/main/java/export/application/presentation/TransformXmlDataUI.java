package export.application.presentation;

import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import export.application.controllers.TransformXmlDataController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import utils.console.FileSelector;
import utils.impl.ExportDataType;

import java.io.FileNotFoundException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class TransformXmlDataUI extends AbstractUI {
    private static final Logger LOGGER = LogManager.getLogger(TransformXmlDataUI.class);


    private final List<String> LIST_DATA_GROUP = new ArrayList<>(Arrays.asList(
            "Maquinas e Linhas Produção", "Ordens e Execuções de Produção", "Produtos e Fichas Técnicas"
    ));
    private final List<ExportDataType> LIST_DATA_FORMAT = new ArrayList<>(Arrays.asList(ExportDataType.values()));
    private final String BASE_FILE_NAME = "transform";

    @Autowired
    private TransformXmlDataController dataController;

    @Override
    protected boolean doShow() {

        SelectWidget<ExportDataType> menuFormato = new SelectWidget<>("SELECIONE FORMATO PARA EXPORTAR", LIST_DATA_FORMAT);
        menuFormato.show();
        int inputFormato = menuFormato.selectedOption();

        if (inputFormato != 0) {

            SelectWidget<String> menuGroupData = new SelectWidget<>("SELECIONE GRUPO DE DADOS A EXPORTAR", LIST_DATA_GROUP);
            menuGroupData.show();
            int inputGD = menuGroupData.selectedOption();

            if (inputGD != 0) {
                try {
                    String inputDataFilePath = FileSelector.selectFile().getAbsolutePath();

                    String outputFileName = BASE_FILE_NAME + Instant.now().getEpochSecond();

                    dataController = new TransformXmlDataController(inputDataFilePath, outputFileName);

                    dataController.transformData(inputGD, ExportDataType.values()[inputFormato - 1]);
                } catch (FileNotFoundException e) {
                    LOGGER.error(e.getMessage());
                }

                return true;
            }
        }
        return false;
    }

    @Override
    public String headline() {
        return "EXPORTAR XML PARA FORMATO:";
    }
}
