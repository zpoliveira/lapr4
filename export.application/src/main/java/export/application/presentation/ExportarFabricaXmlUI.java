package export.application.presentation;

import eapli.framework.io.util.Console;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import export.application.controllers.ExportarFabricaController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class ExportarFabricaXmlUI extends AbstractUI {
    private static final Logger LOGGER = LogManager.getLogger(ExportarFabricaXmlUI.class);

    private final String LBL_MAQ = "Maquinas";
    private final String LBL_LPR = "Linhas Produção";
    private final String LBL_DEP = "Depósitos";
    private final String LBL_PCF = "Produtos com Ficha de Produção";
    private final String LBL_PSF = "Produtos sem Ficha de Produção";
    private final String LBL_MPR = "Matérias-Prima";
    private final String LBL_CAT = "Categoria Matérias-Primas";
    private final String LBL_ORP = "Ordens Produção";
    private final String LBL_EXE = "Execução de Ordens Produção";

    private final List<String> listYesNo = new ArrayList<>(Arrays.asList("YES", "NO"));

    private final List<String> listMenu = new ArrayList<>(
            Arrays.asList(
                    LBL_MAQ, LBL_LPR, LBL_DEP,
                    LBL_PCF, LBL_PSF, LBL_MPR,
                    LBL_CAT, LBL_ORP
            )
    );
    private LocalDate datainicio;
    private LocalDate datafim;
    @Autowired
    private ExportarFabricaController fabricaController;

    @Override
    protected boolean doShow() {
        boolean menuHasElements = true;
        int input = 0;
        String res = "";
        do {
            SelectWidget<String> menu = new SelectWidget<>("Selecione Objetos a exportar", listMenu);
            menu.show();
            input = menu.selectedOption();

            if (input != 0) {
                res = menu.selectedElement();

                switch (res) {
                    case LBL_PCF:
                    case LBL_PSF:
                        listMenu.remove(LBL_PCF);
                        listMenu.remove(LBL_PSF);
                        break;
                    case LBL_ORP:
                        if (yesno("Deseja filtar Ordens de Produção?")) {
                            if (yesno("Introduzir inicio do filtro?")) {
                                datainicio = Console.readDate("Introduza data [yyyy/MM/dd]:")
                                        .toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                            }
                            if (yesno("Introduzir fim do filtro?")) {
                                datafim = Console.readDate("Introduza data [yyyy/MM/dd]:")
                                        .toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                            }
                        }
                        fabricaController.defineDateInterval(datainicio, datafim);
                        listMenu.remove(res);
                        break;
                    default:
                        listMenu.remove(res);
                }

                fabricaController.exportDomainGroup(res);

                menuHasElements = !listMenu.isEmpty();
            } else {
                menuHasElements = false;
            }
        } while (menuHasElements);

        return fabricaController.finalizeExport();
    }

    private boolean yesno(String label) {
        SelectWidget<String> menu = new SelectWidget<>(label, listYesNo);
        menu.show();
        return menu.selectedElement().equals("YES");
    }

    @Override
    public String headline() {
        return "Exportar Fabrica em XML";
    }
}
