package export.application.controllers;

import export.application.transform.strategies.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;
import utils.impl.ExportDataType;

import java.io.File;
import java.io.IOException;

@Component
public class TransformXmlDataController {
    private static final Logger LOGGER = LogManager.getLogger(TransformXmlDataController.class);

    private final String IN_FILE_START = "xml2";

    private String baseOutputPath = "/LAPR4/transformations/out/";

    private String baseInputPath = "/LAPR4/transformations/in/";

    private String outputFileName;
    private String inputDataFileName;

    public TransformXmlDataController() {
    }

    public TransformXmlDataController(String inputDataFileName, String outputFileName) {
        this.outputFileName = outputFileName;
        this.inputDataFileName = inputDataFileName;
    }

    public boolean transformData(int selectedGroupOption, ExportDataType type) {

        String typeParamLower = type.name().toLowerCase();
        String userPath = System.getProperty("user.home");

        String sourceXslFilePath =
                new StringBuilder().append(userPath)
                        .append(baseInputPath)
                        .append(IN_FILE_START)
                        .append(typeParamLower)
                        .append(selectedGroupOption)
                        .append(".xsl")
                        .toString();

        String outputFilePath =
                new StringBuilder().append(userPath)
                        .append(baseOutputPath)
                        .append(outputFileName)
                        .append(".")
                        .append(typeParamLower)
                        .toString();

        try {
            if (!new File(outputFilePath).createNewFile())
                return false;
        } catch (IOException e) {
            e.printStackTrace();
        }

        DataTransformer dataTransformer = null;
        switch (type) {
            case XML:
                dataTransformer = new TransformToXmlStrategy(sourceXslFilePath, inputDataFileName, outputFilePath);
                break;
            case JSON:
                dataTransformer = new TransformToJsonStrategy(sourceXslFilePath, inputDataFileName, outputFilePath);
                break;
            case HTML:
                dataTransformer = new TransformToHtmlStrategy(sourceXslFilePath, inputDataFileName, outputFilePath);
                break;
            case PLAIN:
                dataTransformer = new TransformToTextStrategy(sourceXslFilePath, inputDataFileName, outputFilePath);
                break;
            default:
        }


        if (dataTransformer != null) {
            return dataTransformer.transform();
        } else {
            return false;
        }
    }
}
