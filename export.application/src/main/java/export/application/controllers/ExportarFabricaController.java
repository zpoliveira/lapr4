package export.application.controllers;

import chaofabrica.domain.model.deposito.Deposito;
import chaofabrica.domain.model.linhaproducao.LinhaProducao;
import chaofabrica.domain.model.maquina.Maquina;
import chaofabrica.domain.repositories.DepositoRepository;
import chaofabrica.domain.repositories.LinhaProducaoRepository;
import chaofabrica.domain.repositories.MaquinaRepository;
import export.application.builder.FabricaXmlBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import producao.domain.model.materiaPrima.CategoriaMatPrima;
import producao.domain.model.materiaPrima.MateriaPrima;
import producao.domain.model.ordemProducao.OrdemProducao;
import producao.domain.model.produto.Produto;
import producao.domain.repositories.CategoriaMatPrimaRepository;
import producao.domain.repositories.MateriaPrimaRepository;
import producao.domain.repositories.OrdemProducaoRepository;
import producao.domain.repositories.ProdutoRepository;
import spm.domain.execucaoOrdemProducao.ExecucaoOrdemProducao;
import spm.repository.ExecucaoOrdemProducaoRepository;

import java.time.LocalDate;

@Component
public class ExportarFabricaController {
    private static final Logger LOGGER = LogManager.getLogger(ExportarFabricaController.class);

    private final FabricaXmlBuilder fabricaXmlBuilder = new FabricaXmlBuilder();
    private final Long MIN_DATE_OFFSET = 10L;
    private boolean hasExportedProdutos = false;
    private boolean hasExportedMaquinas = false;
    private boolean hasExportedLinhaProd = false;
    private boolean hasExportedCatMatPrima = false;
    private boolean hasExportedDeposito = false;
    private boolean hasExportedMatPrima = false;
    private boolean hasExportedOrdensProducao = false;
    private LocalDate dataInicio = LocalDate.MIN;
    private LocalDate dataFim = LocalDate.MAX;
    @Autowired
    private MaquinaRepository maquinaRepository;
    @Autowired
    private LinhaProducaoRepository linhaProducaoRepository;
    @Autowired
    private CategoriaMatPrimaRepository categoriaMatPrimaRepository;
    @Autowired
    private DepositoRepository depositoRepository;
    @Autowired
    private ProdutoRepository produtoRepository;
    @Autowired
    private MateriaPrimaRepository materiaPrimaRepository;
    @Autowired
    private OrdemProducaoRepository ordemProducaoRepository;
    @Autowired
    private ExecucaoOrdemProducaoRepository execucaoOrdemProducaoRepository;

    public void defineDateInterval(LocalDate start, LocalDate end) {
        dataFim = end == null ? LocalDate.now() : end;
        dataInicio = start == null ? LocalDate.now().minusYears(MIN_DATE_OFFSET) : start;
    }

    public boolean exportDomainGroup(String pedido) {
        switch (pedido) {
            case "Maquinas":
                if (!hasExportedMaquinas) {
                    for (Maquina obj : maquinaRepository.findAll())
                        fabricaXmlBuilder.append(obj);
                    hasExportedMaquinas = true;
                }
                break;
            case "Linhas Produção":
                if (!hasExportedLinhaProd) {
                    for (LinhaProducao obj : linhaProducaoRepository.findAll())
                        fabricaXmlBuilder.append(obj);
                    hasExportedLinhaProd = true;
                }
                break;
            case "Depósitos":
                if (!hasExportedDeposito) {
                    for (Deposito obj : depositoRepository.findAll())
                        fabricaXmlBuilder.append(obj);
                    hasExportedDeposito = true;
                }
                break;
            case "Categoria Matérias-Primas":
                if (!hasExportedCatMatPrima) {
                    for (CategoriaMatPrima obj : categoriaMatPrimaRepository.findAll())
                        fabricaXmlBuilder.append(obj);
                    hasExportedCatMatPrima = true;
                }
                break;
            case "Produtos com Ficha de Produção":
                if (!hasExportedProdutos) {
                    fabricaXmlBuilder.exportProdutosWithFP(true);
                    for (Produto obj : produtoRepository.findAll())
                        fabricaXmlBuilder.append(obj);
                    hasExportedProdutos = true;
                }
                break;
            case "Produtos sem Ficha de Produção":
                if (!hasExportedProdutos) {
                    fabricaXmlBuilder.exportProdutosWithFP(false);
                    for (Produto obj : produtoRepository.findAll())
                        fabricaXmlBuilder.append(obj);
                    hasExportedProdutos = true;
                }
                break;
            case "Matérias-Prima":
                if (!hasExportedMatPrima) {
                    for (MateriaPrima obj : materiaPrimaRepository.findAll())
                        fabricaXmlBuilder.append(obj);
                    hasExportedMatPrima = true;
                }
                break;
            case "Ordens Produção":
                if (!hasExportedOrdensProducao) {
                    for (OrdemProducao obj : ordemProducaoRepository.findAllByDataPrevistaExecucaoBetween(dataInicio, dataFim)) {
                        fabricaXmlBuilder.append(obj);

                        ExecucaoOrdemProducao exec = execucaoOrdemProducaoRepository.findByIdentificadorOrdemProducao(obj.identity());
                        fabricaXmlBuilder.append(exec);
                    }
                    hasExportedOrdensProducao = true;
                }
                break;
            default:
                LOGGER.error(String.format("Não existe nenhuma classe com o nome %s!", pedido));
                return false;
        }
        return true;
    }

    public boolean finalizeExport() {
        return fabricaXmlBuilder.build();
    }


}
