package export.application.builder;

import eapli.framework.representations.RepresentationBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import producao.dto.communtypes.TEstadoOrdemProducao;
import producao.dto.communtypes.TUnidade;
import producao.dto.ordemproducao.TOrdemProducao;

import java.math.BigInteger;

public class TOrdemProducaoRepresentationalBuilder implements RepresentationBuilder<TOrdemProducao> {
    private static final Logger LOGGER = LogManager.getLogger(TOrdemProducaoRepresentationalBuilder.class);

    private final TOrdemProducao tOrdemProducao = new TOrdemProducao();

    private String childObject = "";

    @Override
    public RepresentationBuilder<TOrdemProducao> startObject(String name) {
        this.childObject = name;
        return this;
    }

    @Override
    public RepresentationBuilder<TOrdemProducao> endObject() {
        this.childObject = "";
        return this;
    }

    @Override
    public RepresentationBuilder<TOrdemProducao> withElement(String value) {
        LOGGER.warn(String.format("%s doesn't need or use %s for building.", this.getClass().getSimpleName(), value));
        return this;
    }

    @Override
    public RepresentationBuilder<TOrdemProducao> withProperty(String name, String value) {
        switch (name) {
            case "codigoproduto":
                this.tOrdemProducao.setCodigoFabricoProduto(value);
                break;
            case "idordem":
                this.tOrdemProducao.setIdOrdemProducao(value);
                break;
            case "estado":
                this.tOrdemProducao.setEstadoOrdemProducao(TEstadoOrdemProducao.fromValue(value));
                break;
            case "dataemissao":
                this.tOrdemProducao.setDataEmissao(value);
                break;
            case "dataprevisao":
                this.tOrdemProducao.setDataPrevistaExecucao(value);
                break;
            case "quantidade-un":
                this.tOrdemProducao.getQuantidade().setUnidade(TUnidade.fromValue(value));
                break;
            case "idencomenda":
                if (childObject.equals("encomendas")) {
                    this.tOrdemProducao.getIdEncomenda().add(value);
                } else {
                    this.withElement(String.format("%s.%s", childObject, name));
                }
        }
        return this;
    }

    @Override
    public RepresentationBuilder<TOrdemProducao> withProperty(String name, Long value) {
        if (name.equals("quantidade-val")) {
            this.tOrdemProducao.getQuantidade().setValue(BigInteger.valueOf(value));
        }
        return this;
    }

    @Override
    public TOrdemProducao build() {
        return tOrdemProducao;
    }
}
