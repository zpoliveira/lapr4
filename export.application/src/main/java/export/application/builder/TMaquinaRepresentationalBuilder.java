package export.application.builder;

import chaofabrica.domain.dto.maquina.TMaquina;
import eapli.framework.representations.RepresentationBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TMaquinaRepresentationalBuilder implements RepresentationBuilder<TMaquina> {
    private static final Logger LOGGER = LogManager.getLogger(TMaquinaRepresentationalBuilder.class);


    private final TMaquina tMaquina = new TMaquina();

    @Override
    public RepresentationBuilder<TMaquina> withProperty(String name, String value) {
        switch (name) {
            case "codigo":
                tMaquina.setCodigoInterno(value);
                break;
            case "descricao":
                tMaquina.setDescricao(value);
                break;
            case "marca":
                tMaquina.setMarca(value);
                break;
            case "modelo":
                tMaquina.setModelo(value);
                break;
            case "dataInstall":
                tMaquina.setDataInstalacao(value);
                break;
            case "numSerie":
                tMaquina.setNumeroSerie(value);
                break;
            default:
                withElement(name);
        }
        return this;
    }

    @Override
    public TMaquina build() {
        return tMaquina;
    }

    @Override
    public RepresentationBuilder<TMaquina> withElement(String value) {
        LOGGER.warn(String.format("%s doesn't need or use %s for building.", this.getClass().getSimpleName(), value));
        return this;
    }
}
