package export.application.builder;

import chaofabrica.domain.dto.deposito.TDeposito;
import eapli.framework.representations.RepresentationBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TDepositoRepresentationalBuilder implements RepresentationBuilder<TDeposito> {
    private static final Logger LOGGER = LogManager.getLogger(TDepositoRepresentationalBuilder.class);

    private TDeposito tDeposito = new TDeposito();

    @Override
    public RepresentationBuilder<TDeposito> withProperty(String name, String value) {
        switch (name) {
            case "codigo":
                tDeposito.setCodigoAlfanumerico(value);
                break;
            case "descricao":
                tDeposito.setDescricao(value);
                break;
            default:
                withElement(name);
        }
        return this;
    }

    @Override
    public TDeposito build() {
        return tDeposito;
    }

    @Override
    public RepresentationBuilder<TDeposito> withElement(String value) {
        LOGGER.warn(String.format("%s doesn't need or use %s for building.", this.getClass().getSimpleName(), value));
        return this;
    }
}
