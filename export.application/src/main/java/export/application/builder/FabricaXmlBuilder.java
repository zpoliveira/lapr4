package export.application.builder;

import chaofabrica.domain.dto.deposito.TDepositos;
import chaofabrica.domain.dto.linhasproducao.TLinhasProducao;
import chaofabrica.domain.dto.maquina.TMaquinas;
import eapli.framework.representations.Representationable;
import export.application.model.ChaoFabrica;
import export.application.model.Fabrica;
import export.application.model.Producao;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import producao.dto.categoriamateriaprima.TCategoriasMateriaPrima;
import producao.dto.materiaprima.TMateriasPrimas;
import producao.dto.ordemproducao.TOrdens;
import producao.dto.produtos.TProdutos;
import spm.dto.execucaoOrdemProducao.TExecucoes;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;

/*
 * Buider for the XML document that represents the whole factory floor
 * Implemented with the use of the Builder pattern
 *
 * */
public class FabricaXmlBuilder {
    private static final Logger LOGGER = LogManager.getLogger(FabricaXmlBuilder.class);
    private Path  outPath ;
    private Fabrica fabrica;
    private ChaoFabrica chaoFabrica;
    private Producao producao;
    private boolean fabsContent = false;
    private boolean chaoContent = false;
    private boolean prodContent = false;
    private boolean prodComFP = false;


    public FabricaXmlBuilder() {
        outPath = Paths.get(System.getProperty("user.home")
                + "/LAPR4/exports/FabricaXml"
                + Instant.now().getEpochSecond()
                + ".xml");

        initialize();
    }

    private void initialize() {
        this.fabrica = new Fabrica();
        this.chaoFabrica = new ChaoFabrica();
        this.producao = new Producao();

        fabsContent = false;
        chaoContent = false;
        prodContent = false;
    }

    public void exportProdutosWithFP(boolean bool) {
        this.prodComFP = bool;
    }

    public FabricaXmlBuilder append(Representationable domainObject) {
        fabsContent = true;
        switch (domainObject.getClass().getSimpleName()) {
            case "Maquina":
                if (chaoFabrica.getMaquinas() == null)
                    chaoFabrica.setMaquinas(new TMaquinas());
                chaoContent = true;
                chaoFabrica
                        .getMaquinas()
                        .getMaquina()
                        .add(domainObject.buildRepresentation(new TMaquinaRepresentationalBuilder()));
                break;
            case "LinhaProducao":
                if (chaoFabrica.getLinhasProducao() == null)
                    chaoFabrica.setLinhasProducao(new TLinhasProducao());
                chaoContent = true;
                chaoFabrica
                        .getLinhasProducao()
                        .getLinhaProducao()
                        .add(domainObject.buildRepresentation(new TLinhaProducaoRepresentationalBuilder()));
                break;
            case "Deposito":
                if (chaoFabrica.getDepositos() == null)
                    chaoFabrica.setDepositos(new TDepositos());
                chaoContent = true;
                chaoFabrica
                        .getDepositos()
                        .getDeposito()
                        .add(domainObject.buildRepresentation(new TDepositoRepresentationalBuilder()));
                break;
            case "Produto":
                if (producao.getProdutos() == null)
                    producao.setProdutos(new TProdutos());
                prodContent = true;
                producao
                        .getProdutos()
                        .getProduto()
                        .add(domainObject.buildRepresentation(new TProdutoRepresentationBuilder(prodComFP)));
                break;
            case "MateriaPrima":
                if (producao.getMateriasPrimas() == null)
                    producao.setMateriasPrimas(new TMateriasPrimas());
                prodContent = true;
                producao
                        .getMateriasPrimas()
                        .getMateriaPrima()
                        .add(domainObject.buildRepresentation(new TMateriaPrimaRepresentationalBuilder()));
                break;
            case "CategoriaMatPrima":
                if (producao.getCategoriasMateriaPrima() == null)
                    producao.setCategoriasMateriaPrima(new TCategoriasMateriaPrima());
                prodContent = true;
                producao
                        .getCategoriasMateriaPrima()
                        .getCategoriaMateriaPrima()
                        .add(domainObject.buildRepresentation(new TCategoriaMateriaPrimaRepresentationalBuilder()));
                break;
            case "OrdemProducao":
                if (producao.getOrdensProducao() == null)
                    producao.setOrdensProducao(new TOrdens());
                prodContent = true;
                producao
                        .getOrdensProducao()
                        .getOrdemProducao()
                        .add(domainObject.buildRepresentation(new TOrdemProducaoRepresentationalBuilder()));
                break;
            case "ExecucaoOrdemProducao":
                if (producao.getExecucaoOrdensProducao() == null)
                    producao.setExecucaoOrdensProducao(new TExecucoes());
                prodContent = true;
                producao
                        .getExecucaoOrdensProducao()
                        .getExecucaoOrdemProducao()
                        .add(domainObject.buildRepresentation(new TExecucaoOrdemProducaoRepresentationBuilder()));
                break;
            default:
                LOGGER.error(String.format("No class %s exists on domain", domainObject.getClass().getSimpleName()));
        }
        return this;
    }


    public FabricaXmlBuilder reset() {
        initialize();
        return this;
    }


    public boolean build() {
        if (fabsContent) {
            fabrica.setChaoFabrica(chaoContent ? chaoFabrica : null);
            fabrica.setProducao(prodContent ? producao : null);

            JAXBContext jaxbContext = null;

            try {
                jaxbContext = JAXBContext.newInstance(fabrica.getClass());
                Marshaller marshaller = jaxbContext.createMarshaller();
                marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
                marshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, "http://factyfloor.com/Fabrica");

                File directory = new File(outPath.toString());
                if (!directory.exists()) {
                    directory.createNewFile();
                }

                marshaller.marshal(fabrica, directory);

                LOGGER.info("Good Export!");
            } catch (JAXBException | IOException e) {
                LOGGER.error(String.format("BAD Export! %s", e.getMessage()));
            }
        }
        return fabsContent;
    }
}
