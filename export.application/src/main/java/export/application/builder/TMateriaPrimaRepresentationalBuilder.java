package export.application.builder;

import eapli.framework.representations.RepresentationBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import producao.dto.materiaprima.TMateriaPrima;

public class TMateriaPrimaRepresentationalBuilder implements RepresentationBuilder<TMateriaPrima> {
    private static final Logger LOGGER = LogManager.getLogger(TMateriaPrimaRepresentationalBuilder.class);


    private final TMateriaPrima tMateriaPrima = new TMateriaPrima();

    @Override
    public RepresentationBuilder<TMateriaPrima> withProperty(String name, String value) {
        switch (name) {
            case "codigo":
                tMateriaPrima.setCodigoInterno(value);
                break;
            case "descricao":
                tMateriaPrima.setDescricao(value);
                break;
            case "nomeCat":
                tMateriaPrima.setCategoria(value);
                break;
            case "fichaTec":
                tMateriaPrima.setFichaTecnica(value);
                break;
            default:
                withElement(name);
        }
        return this;
    }

    @Override
    public TMateriaPrima build() {
        return tMateriaPrima;
    }

    @Override
    public RepresentationBuilder<TMateriaPrima> withElement(String value) {
        LOGGER.warn(String.format("%s doesn't need or use %s for building.", this.getClass().getSimpleName(), value));
        return this;
    }
}
