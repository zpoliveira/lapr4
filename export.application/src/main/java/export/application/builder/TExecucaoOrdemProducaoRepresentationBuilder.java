package export.application.builder;

import eapli.framework.representations.RepresentationBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import spm.dto.execucaoOrdemProducao.*;

public class TExecucaoOrdemProducaoRepresentationBuilder implements RepresentationBuilder<TExecucaoOrdemProducao> {
    private static final Logger LOGGER = LogManager.getLogger(TExecucaoOrdemProducaoRepresentationBuilder.class);
    private final String ERROR_STR_FORMAT = "%s on %s";
    private TExecucaoOrdemProducao parent = new TExecucaoOrdemProducao();
    private TMovimentoMateriaPrima tMovimentoMateriaPrima;
    private TDesvioConsumoMP tDesvioConsumoMP;
    private TDesvioProducao tDesvioProducao;
    private TProducaoReal tProducaoReal;
    private TProducaoPorDeposito tProducaoPorDeposito;
    private TProducaoPorLote tProducaoPorLote;
    private TTempoExecMaquina tTempoExecMaquina;
    private TEstorno tEstorno;
    private String childObj = "";

    @Override
    public RepresentationBuilder<TExecucaoOrdemProducao> startObject(String name) {
        switch (name) {
            case "estorno":
            case "consumoRealMP":
                tMovimentoMateriaPrima = new TMovimentoMateriaPrima();
                break;
            case "desvioConsumoMP":
                tDesvioConsumoMP = new TDesvioConsumoMP();
                break;
            case "desvioProducao":
                tDesvioProducao = new TDesvioProducao();
                break;
            case "producaoReal":
                tProducaoReal = new TProducaoReal();
                break;
            case "producaoReal_END":
                break;
            case "prodPD":
                tProducaoPorDeposito = new TProducaoPorDeposito();
                break;
            case "prodPL":
                tProducaoPorLote = new TProducaoPorLote();
                break;
            case "temposExecMaq":
                tTempoExecMaquina = new TTempoExecMaquina();
                break;
            default:
                return this;
        }
        this.childObj = name;
        return this;
    }

    @Override
    public RepresentationBuilder<TExecucaoOrdemProducao> endObject() {
        switch (childObj) {
            case "estorno":
                parent.getEstorno().getMovimentoEstorno().add(tMovimentoMateriaPrima);
                break;
            case "consumoRealMP":
                parent.getConsumoRealMateriaPrima().getMovimentoConsumoMP().add(tMovimentoMateriaPrima);
                break;
            case "desvioConsumoMP":
                parent.getDesviosConsumoMateriaPrima().getDesvioConsumoMateriaPrima().add(tDesvioConsumoMP);
                break;
            case "producaoReal_END":
                parent.getProducoesReais().getProducaoReal().add(tProducaoReal);
                break;
            case "prodPD":
                tProducaoReal.getProducoesPorDeposito().getProducaoPorDeposito().add(tProducaoPorDeposito);
                break;
            case "prodPL":
                tProducaoReal.getProducoesPorLotes().getProducaoPorLote().add(tProducaoPorLote);
                break;
            case "temposExecMaq":
                parent.getTemposExecucaoMaquinas().getTemposExecucaoMaquinas().add(tTempoExecMaquina);
                break;
            case "desvioProducao":
            default:
                break;
        }
        childObj = "";
        return this;
    }

    @Override
    public RepresentationBuilder<TExecucaoOrdemProducao> withElement(String value) {
        LOGGER.warn(String.format("%s doesn't need or use %s for building.", this.getClass().getSimpleName(), value));
        return this;
    }

    @Override
    public RepresentationBuilder<TExecucaoOrdemProducao> withProperty(String name, String value) {
        switch (name) {
            case "idOrdem":
                parent.setIdOrdemProducao(value);
                break;
            case "tempoEfetivoExec":
                switch (childObj) {
                    case "temposExecMaq":
                        tTempoExecMaquina.setTempoEfetivoExecucao(value);
                        break;
                    case "":
                        parent.setTempoEfetivoExecucao(value);
                        break;
                    default:
                        withElement(String.format(ERROR_STR_FORMAT, name, childObj));
                }
                break;
            case "tempoBrutoExec":
                switch (childObj) {
                    case "temposExecMaq":
                        tTempoExecMaquina.setTempoBrutoExecucao(value);
                        break;
                    case "":
                        parent.setTempoBrutoExecucao(value);
                        break;
                    default:
                        withElement(String.format(ERROR_STR_FORMAT, name, childObj));
                }
                break;
            case "codMP":
                switch (childObj) {
                    case "estorno":
                    case "consumoRealMP":
                        tMovimentoMateriaPrima.setCodigoInternoMP(value);
                        break;
                    case "desvioConsumoMP":
                        tDesvioConsumoMP.setIdMateriaPrima(value);
                        break;
                    default:
                        withElement(String.format(ERROR_STR_FORMAT, name, childObj));
                }
                break;
            case "codDepo":
                switch (childObj) {
                    case "estorno":
                    case "consumoRealMP":
                        tMovimentoMateriaPrima.setCodigoDepesito(value);
                        break;
                    case "prodPD":
                        tProducaoPorDeposito.setIdDeposito(value);
                        break;
                    default:
                        withElement(String.format(ERROR_STR_FORMAT, name, childObj));
                }
                break;
            case "codMaq":
                if (childObj.equals("temposExecMaq")) {
                    tTempoExecMaquina.setCodigoInternoMaquina(value);
                } else {
                    withElement(String.format(ERROR_STR_FORMAT, name, childObj));
                }
                break;
            case "codLote":
                if (childObj.equals("prodPL")) {
                    tProducaoPorLote.setIdLote(value);
                } else {
                    withElement(String.format(ERROR_STR_FORMAT, name, childObj));
                }
                break;
            case "idProd":
                if (childObj.equals("producaoReal")) {
                    tProducaoReal.setIdProduto(value);
                } else {
                    withElement(String.format(ERROR_STR_FORMAT, name, childObj));
                }
                break;

            default:
                withElement(String.format(ERROR_STR_FORMAT, name, parent.getClass().getSimpleName()));
        }

        return this;
    }

    @Override
    public RepresentationBuilder<TExecucaoOrdemProducao> withProperty(String name, Long value) {
        switch (name) {
            case "consumo":
                switch (childObj) {
                    case "estorno":
                    case "consumoRealMP":
                        tMovimentoMateriaPrima.setConsumo(value);
                        break;
                    default:
                        withElement(String.format(ERROR_STR_FORMAT, name, childObj));
                }
                break;
            case "desvio":
                switch (childObj) {
                    case "desvioConsumoMP":
                        tDesvioConsumoMP.setDesvio(value);
                        break;
                    case "desvioProducao":
                        tDesvioProducao.setDesvio(value);
                        break;
                    default:
                        withElement(String.format(ERROR_STR_FORMAT, name, childObj));
                }
                break;
            case "qtdOrdProd":
                if (childObj.equals("desvioProducao")) {
                    tDesvioProducao.setQuantidadeOrdemProducao(value);
                } else {
                    withElement(String.format(ERROR_STR_FORMAT, name, childObj));
                }
                break;
            case "qtdRealProd":
                if (childObj.equals("desvioProducao")) {
                    tDesvioProducao.setQuantidadeRealProduzida(value);
                } else {
                    withElement(String.format(ERROR_STR_FORMAT, name, childObj));
                }
                break;
            case "qtdProd":
                if (childObj.equals("producaoReal")) {
                    tProducaoReal.setQuantidadeProduzida(String.format("%d", value));
                } else {
                    withElement(String.format(ERROR_STR_FORMAT, name, childObj));
                }
                break;
            case "qtd":
                switch (childObj) {
                    case "prodPD":
                        tProducaoPorDeposito.setQuantidade(value);
                        break;
                    case "prodPL":
                        tProducaoPorLote.setQuantidade(value);
                        break;
                    default:
                        withElement(String.format(ERROR_STR_FORMAT, name, childObj));
                }
                break;
            default:
                withElement(String.format(ERROR_STR_FORMAT, name, childObj));
        }
        return this;
    }

    @Override
    public TExecucaoOrdemProducao build() {
        return parent;
    }
}
