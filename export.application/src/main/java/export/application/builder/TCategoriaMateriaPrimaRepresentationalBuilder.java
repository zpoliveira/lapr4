package export.application.builder;

import eapli.framework.representations.RepresentationBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TCategoriaMateriaPrimaRepresentationalBuilder implements RepresentationBuilder<String> {
    private static final Logger LOGGER = LogManager.getLogger(TCategoriaMateriaPrimaRepresentationalBuilder.class);
    private String tCategoriasMateriaPrima = "";

    @Override
    public RepresentationBuilder<String> withElement(String value) {
        LOGGER.warn(String.format("%s doesn't need or use %s for building.", this.getClass().getSimpleName(), value));
        return this;
    }

    @Override
    public RepresentationBuilder<String> withProperty(String name, String value) {
        if (name.equals("nome")) {
            tCategoriasMateriaPrima = value;
        } else {
            LOGGER.warn(String.format("%s doesn't need or use %s for building.", this.getClass().getSimpleName(), value));
        }
        return this;
    }

    @Override
    public String build() {
        return tCategoriasMateriaPrima;
    }
}
