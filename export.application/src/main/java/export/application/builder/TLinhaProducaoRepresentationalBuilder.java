package export.application.builder;

import chaofabrica.domain.dto.linhasproducao.TLinhaProducao;
import chaofabrica.domain.dto.linhasproducao.TSequenciaMaquina;
import eapli.framework.representations.RepresentationBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigInteger;

public class TLinhaProducaoRepresentationalBuilder implements RepresentationBuilder<TLinhaProducao> {
    private static final Logger LOGGER = LogManager.getLogger(TLinhaProducaoRepresentationalBuilder.class);

    private final TLinhaProducao tLinhaProducao = new TLinhaProducao();

    private String childObject = "";

    @Override
    public RepresentationBuilder<TLinhaProducao> startObject(String name) {
        childObject = name;
        return this;
    }

    @Override
    public RepresentationBuilder<TLinhaProducao> endObject() {
        childObject = "";
        return this;
    }

    @Override
    public RepresentationBuilder<TLinhaProducao> withElement(String value) {
        LOGGER.warn(String.format("%s doesn't need or use %s for building.", this.getClass().getSimpleName(), value));
        return this;
    }

    @Override
    public RepresentationBuilder<TLinhaProducao> withProperty(String name, String value) {
        switch (name) {
            case "codigo":
                tLinhaProducao.setCodigo(value);
                break;
            case "maquina":
                if (childObject.equals("sequencia") && value.split(";").length == 2) {
                    TSequenciaMaquina temp = new TSequenciaMaquina(); // TSequenciaMaquina é sempre introduzido sem filtro, pois nao faz sentido um linha nao ter maquinas
                    temp.setIndex(BigInteger.valueOf(Long.parseLong(value.split(";")[0])));
                    temp.setValue(value.split(";")[1]);
                    tLinhaProducao.getSequenciaMaquina().add(temp);
                } else {
                    LOGGER.warn(String.format("BAD CONTENT ON %s DTO BUILDER VALUE", this.getClass().getName()));
                }
                break;
            default:
                withElement(name);
        }
        return this;
    }

    @Override
    public TLinhaProducao build() {
        return tLinhaProducao;
    }
}
