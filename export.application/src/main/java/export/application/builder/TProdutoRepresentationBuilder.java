package export.application.builder;

import eapli.framework.representations.RepresentationBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import producao.dto.communtypes.TUnidade;
import producao.dto.produtos.TComponenteMateriaPrima;
import producao.dto.produtos.TFichaProducao;
import producao.dto.produtos.TProduto;

import java.math.BigInteger;

public class TProdutoRepresentationBuilder implements RepresentationBuilder<TProduto> {
    private static final Logger LOGGER = LogManager.getLogger(TProdutoRepresentationBuilder.class);
    private final String TAG_FICHAPROD = "fichaProd";
    private final String TAG_COMPMATPRIMA = "compMatPrima";

    private final TProduto tProduto = new TProduto();
    private TComponenteMateriaPrima tComponenteMateriaPrima = new TComponenteMateriaPrima();
    private String childObject = "";
    private boolean showFichaProducao = true; // flag para nao necessitar de outro builder, evitando assim codigo repetido

    public TProdutoRepresentationBuilder(boolean showFichaProducao) {
        this.showFichaProducao = showFichaProducao;
    }

    @Override
    public RepresentationBuilder<TProduto> startObject(String name) {
        this.childObject = name;
        if (childObject.equals(TAG_FICHAPROD) && showFichaProducao) {
            if (tProduto.getFichaProducao() == null)
                tProduto.setFichaProducao(new TFichaProducao());
        } else if (childObject.equals(TAG_COMPMATPRIMA) && showFichaProducao) {
            tComponenteMateriaPrima = new TComponenteMateriaPrima();
        }
        return this;
    }

    @Override
    public RepresentationBuilder<TProduto> endObject() {
        if (childObject.equals(TAG_COMPMATPRIMA) && showFichaProducao) {
            tProduto.getFichaProducao().getComponenteMateriaPrimas().add(tComponenteMateriaPrima);
            tComponenteMateriaPrima = new TComponenteMateriaPrima();
        }
        this.childObject = "";
        return this;
    }

    @Override
    public RepresentationBuilder<TProduto> withElement(String value) {
        LOGGER.warn(String.format("%s doesn't need or use %s for building.", this.getClass().getSimpleName(), value));
        return this;
    }

    @Override
    public RepresentationBuilder<TProduto> withProperty(String name, String value) {
        switch (name) {
            case "codigoCom":
                this.tProduto.setCodigoComercial(value);
                break;
            case "codigoFab":
                this.tProduto.setCodigoFabrico(value);
                break;
            case "descBreve":
                this.tProduto.setDescricaoBreve(value);
                break;
            case "descComp":
                this.tProduto.setDescricaoCompleta(value);
                break;
            case "quantidade-un":
                if (childObject.equals(TAG_FICHAPROD) && showFichaProducao) {
                    this.tProduto.getFichaProducao().getQuantidade().setUnidade(TUnidade.fromValue(value));
                } else if (childObject.equals(TAG_COMPMATPRIMA)) {
                    tComponenteMateriaPrima.setUnidade(TUnidade.fromValue(value));
                } else {
                    this.withElement(String.format("%s.%s", childObject, name));
                }
                break;
            case "compMatPrima-val":
                if (childObject.equals(TAG_COMPMATPRIMA) && showFichaProducao) {
                    tComponenteMateriaPrima.setValue(value);
                } else {
                    this.withElement(String.format("%s.%s", childObject, name));
                }
        }
        return this;
    }

    @Override
    public RepresentationBuilder<TProduto> withProperty(String name, Long value) {
        if (name.equals("quantidade-val")) {
            if (childObject.equals(TAG_FICHAPROD) && showFichaProducao) {
                this.tProduto.getFichaProducao().getQuantidade().setValue(BigInteger.valueOf(value));
            } else if (childObject.equals(TAG_COMPMATPRIMA) && showFichaProducao) {
                tComponenteMateriaPrima.setQuantidade(BigInteger.valueOf(value));
            }
        } else {
            this.withElement(String.format("%s.%s", childObject, name));
        }
        return this;
    }

    @Override
    public TProduto build() {
        return tProduto;
    }
}
