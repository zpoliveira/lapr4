package export.application.transform.strategies;

import eapli.framework.util.Strategy;

@Strategy
public class TransformToJsonStrategy extends DataTransformer {

    public TransformToJsonStrategy(String xslFilePath, String xmlFilePath, String outputFilePath) {
        super(xslFilePath, xmlFilePath, outputFilePath);
    }

    @Override
    public boolean transform() {
        return executeTransformation("text");
    }
}
