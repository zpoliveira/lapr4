package export.application.transform.strategies;

import eapli.framework.util.Strategy;

@Strategy
public class TransformToTextStrategy extends DataTransformer {

    public TransformToTextStrategy(String xslFilePath, String xmlFilePath, String outputFilePath) {
        super(xslFilePath, xmlFilePath, outputFilePath);
    }

    @Override
    public boolean transform() {
        return executeTransformation("text");
    }
}
