package export.application.transform.strategies;

import eapli.framework.util.Strategy;

@Strategy
public class TransformToHtmlStrategy extends DataTransformer {

    public TransformToHtmlStrategy(String xslFilePath, String xmlFilePath, String outputFilePath) {
        super(xslFilePath, xmlFilePath, outputFilePath);
    }

    @Override
    public boolean transform() {
        return executeTransformation("html");
    }
}
