package export.application.transform.strategies;

import net.sf.saxon.TransformerFactoryImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public abstract class DataTransformer {

    private static final Logger LOGGER = LogManager.getLogger(DataTransformer.class);

    protected String xslFilePath;
    protected String xmlFilePath;
    protected String outputFilePath;


    public DataTransformer(String xslFilePath, String xmlFilePath, String outputFilePath) {
        this.xslFilePath = xslFilePath;
        this.xmlFilePath = xmlFilePath;
        this.outputFilePath = outputFilePath;

    }

    protected boolean executeTransformation(String propType)  {
        /*try {
           File output = new File(outputFilePath);
            File xsl = new File(xslFilePath);
            File xml = new File(xmlFilePath);

            if (xsl.canRead() && xml.canRead()) {

                Source sourcexsl = new StreamSource(xslFilePath);
                Source sourcexml = new StreamSource(xmlFilePath);
                OutputStream outputStream = new FileOutputStream(outputFilePath);

                Processor processor = new Processor(false);

                XsltCompiler compiler = processor.newXsltCompiler();
                XsltExecutable stylesheet = compiler.compile(sourcexsl);


                Serializer out = processor.newSerializer(outputStream);

                out.setOutputProperty(Serializer.Property.METHOD, propType);
                out.setOutputProperty(Serializer.Property.INDENT, "yes");

                Destination destination = new XdmDestination();
                destination.setDestinationBaseURI(output.toURI());

                XsltTransformer transformer = stylesheet.load();
                transformer.setSource(sourcexml);
                transformer.setDestinationBaseURI(output.toURI());

                transformer.transform();

                out.close();
            }
            return true;
        } catch (SaxonApiException | FileNotFoundException e) {
            LOGGER.error(e.getMessage());
            return false;
        }*/
        try {
            TransformerFactoryImpl f = new net.sf.saxon.TransformerFactoryImpl();
            f.setAttribute("http://saxon.sf.net/feature/version-warning", Boolean.FALSE);
            StreamSource xsrc = new StreamSource(new FileInputStream(xslFilePath));
            Transformer t = f.newTransformer(xsrc);
            StreamSource src = new StreamSource(new FileInputStream(xmlFilePath));
            File file = new File(outputFilePath);
            StreamResult res = new StreamResult(file);
            t.transform(src, res);
        }catch (FileNotFoundException | TransformerException e){
            System.out.println(e.getMessage());
        }



       return true;

    }

    public abstract boolean transform();

}
