package utils.console;

import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;

public final class FileSelector {

    public static File selectFile() throws FileNotFoundException {
        System.out.println("Selecione o ficheiro na janela aberta");
        FileDialog dialog = new FileDialog((Frame)null, "Select File to Open");
        dialog.setMode(FileDialog.LOAD);
        dialog.setVisible(true);
        if(dialog.getFiles().length == 0){
            throw new FileNotFoundException("Não selecionou nenhum ficheiro - execute novamente o caso de uso ");
        }
        File file = dialog.getFiles()[0];
        return file;
    }

    public static File selectFile(String ext) throws FileNotFoundException {
        System.out.println("Selecione o ficheiro na janela aberta");
        FileDialog dialog = new FileDialog((Frame)null, "Select File to Open");
        dialog.setMode(FileDialog.LOAD);
        dialog.setFile(ext);
        dialog.setVisible(true);
        if(dialog.getFiles().length == 0){
            throw new FileNotFoundException("Não selecionou nenhum ficheiro - execute novamente o caso de uso ");
        }
        File file = dialog.getFiles()[0];
        return file;
    }
}
