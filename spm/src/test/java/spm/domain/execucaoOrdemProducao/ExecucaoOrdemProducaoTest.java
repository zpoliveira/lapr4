package spm.domain.execucaoOrdemProducao;


import org.junit.*;
import producao.domain.model.ordemProducao.EstadoOrdemProducao;
import producao.domain.model.ordemProducao.IdentificadorOrdemProducao;

import java.util.ArrayList;
import java.util.List;

public class ExecucaoOrdemProducaoTest {

    IdentificadorOrdemProducao identificadorOrdemProducao = new IdentificadorOrdemProducao("22");
    ConsumoRealMateriaPrima consumoRealMateriaPrima = new ConsumoRealMateriaPrima();
    DesvioConsumoMateriaPrima desvioConsumoMateriaPrima = new DesvioConsumoMateriaPrima();
    DesvioProducao desvioProducao = new DesvioProducao();
    ProducaoReal producaoReal = new ProducaoReal();
    TempoExecucaoMaquina tempoExecucaoMaquina1 = new TempoExecucaoMaquina();
    TempoExecucaoMaquina tempoExecucaoMaquina2 = new TempoExecucaoMaquina();
    List<TempoExecucaoMaquina> tempoExecucaoMaquinas = new ArrayList<>();
    TempoEfetivoExecucao tempoEfetivoExecucao = new TempoEfetivoExecucao();
    TempoBrutoExecucao tempoBrutoExecucao = new TempoBrutoExecucao();
    Estorno estorno = new Estorno();
    EstadoOrdemProducao ordemProducaoCocluida = EstadoOrdemProducao.CONCLUIDA;

    @Before
    public void setUp() throws Exception {
        tempoExecucaoMaquinas.add(tempoExecucaoMaquina1);
        tempoExecucaoMaquinas.add(tempoExecucaoMaquina2);
    }

    @After
    public void tearDown() throws Exception {
    }



    @Test(expected = IllegalArgumentException.class)
    public void testeIdentificadorordemProducaoNotNull() {
        new ExecucaoOrdemProducao(
                null,
                consumoRealMateriaPrima,
                desvioConsumoMateriaPrima,
                desvioProducao, producaoReal,
                tempoExecucaoMaquinas,
                tempoEfetivoExecucao,
                tempoBrutoExecucao, estorno, ordemProducaoCocluida );
    }

}