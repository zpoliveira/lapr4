package spm.domain.mensagem;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class LoteIdTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test(expected = IllegalArgumentException.class)
    public void testeLoteIdNotNull() {
        new LoteId(null);
    }
    @Test(expected = IllegalArgumentException.class)
    public void testeLoteIdNotEmpty() {
        new LoteId("");
    }
}