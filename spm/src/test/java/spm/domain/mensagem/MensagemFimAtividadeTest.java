package spm.domain.mensagem;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MensagemFimAtividadeTest extends MensagemTest{

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test(expected = IllegalArgumentException.class)
    public void testeDataHoraNotNull() {
        new MensagemFimAtividade(null,
                codigoInternoMaquina,
                identificadorOrdemProducao,
                linhaProducaoIdentificador);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testeCodigoInternoMaquinaNotNull() {
        new MensagemFimAtividade(dataHora,
                null,
                identificadorOrdemProducao,
                linhaProducaoIdentificador);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testeIdentificadorordemProducaoNotNull() {
        new MensagemFimAtividade(dataHora,
                codigoInternoMaquina,
                null,
                linhaProducaoIdentificador);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testeLinhaProducaoIdentificadorNotNull() {
        new MensagemFimAtividade(dataHora,
                codigoInternoMaquina,
                identificadorOrdemProducao,
                null);
    }

}