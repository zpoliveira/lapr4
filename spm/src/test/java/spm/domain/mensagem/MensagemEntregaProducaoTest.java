package spm.domain.mensagem;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MensagemEntregaProducaoTest extends MensagemTest{

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test(expected = IllegalArgumentException.class)
    public void testeDataHoraNotNull() {
        new MensagemEntregaProducao(null,
                codigoInternoMaquina,
                identificadorOrdemProducao,
                linhaProducaoIdentificador,
                codigoFabricoProduto,
                codigoAlfanumerico,
                quantidade);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testeCodigoInternoMaquinaNotNull() {
        new MensagemEntregaProducao(dataHora,
                null,
                identificadorOrdemProducao,
                linhaProducaoIdentificador,
                codigoFabricoProduto,
                codigoAlfanumerico,
                quantidade);

    }

    @Test(expected = IllegalArgumentException.class)
    public void testeIdentificadorordemProducaoNotNull() {
        new MensagemEntregaProducao(dataHora,
                codigoInternoMaquina,
                null,
                linhaProducaoIdentificador,
                codigoFabricoProduto,
                codigoAlfanumerico,
                quantidade);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testeLinhaProducaoIdentificadorNotNull() {
        new MensagemEntregaProducao(dataHora,
                codigoInternoMaquina,
                identificadorOrdemProducao,
                null,
                codigoFabricoProduto,
                codigoAlfanumerico,
                quantidade);
    }


    @Test(expected = IllegalArgumentException.class)
    public void testeCodigoAlfanumericoNotNull() {
        new MensagemEntregaProducao(dataHora,
                codigoInternoMaquina,
                identificadorOrdemProducao,
                linhaProducaoIdentificador,
                codigoFabricoProduto,
                null,
                quantidade);

    }

    @Test(expected = IllegalArgumentException.class)
    public void testeQuantidadeNotNull() {
        new MensagemEntregaProducao(dataHora,
                codigoInternoMaquina,
                identificadorOrdemProducao,
                linhaProducaoIdentificador,
                codigoFabricoProduto,
                codigoAlfanumerico,
                null);

    }

    @Test(expected = IllegalArgumentException.class)
    public void testeCodigoFabricoProdutoNotNull() {
        new MensagemEntregaProducao(dataHora,
                codigoInternoMaquina,
                identificadorOrdemProducao,
                linhaProducaoIdentificador,
                null,
                codigoAlfanumerico,
                quantidade);

    }

}