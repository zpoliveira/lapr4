package spm.domain.mensagem;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MensagemInicioAtividadeTest extends MensagemTest{

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test(expected = IllegalArgumentException.class)
    public void testeDataHoraNotNull() {
        new MensagemInicioAtividade(null,
                codigoInternoMaquina,
                identificadorOrdemProducao,
                linhaProducaoIdentificador);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testeCodigoInternoMaquinaNotNull() {
        new MensagemInicioAtividade(dataHora,
                null,
                identificadorOrdemProducao,
                linhaProducaoIdentificador);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testeIdentificadorordemProducaoNotNull() {
        new MensagemInicioAtividade(dataHora,
                codigoInternoMaquina,
                null,
                linhaProducaoIdentificador);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testeLinhaProducaoIdentificadorNotNull() {
        new MensagemInicioAtividade(dataHora,
                codigoInternoMaquina,
                identificadorOrdemProducao,
                null);
    }

}