package spm.domain.mensagem;

import chaofabrica.domain.model.deposito.CodigoAlfanumerico;
import chaofabrica.domain.model.linhaproducao.LinhaProducaoIdentificador;
import chaofabrica.domain.model.maquina.CodigoInternoMaquina;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import producao.domain.model.Quantidade;
import producao.domain.model.Unidade;
import producao.domain.model.materiaPrima.CodigoInternoMateriaPrima;
import producao.domain.model.ordemProducao.IdentificadorOrdemProducao;
import producao.domain.model.produto.CodigoFabricoProduto;

import java.time.LocalDateTime;

import static org.junit.Assert.*;

public class MensagemTest {

    LocalDateTime dataHora = LocalDateTime.now();
    CodigoInternoMaquina codigoInternoMaquina = new CodigoInternoMaquina("maq1");
    IdentificadorOrdemProducao identificadorOrdemProducao = new IdentificadorOrdemProducao("22");
    LinhaProducaoIdentificador linhaProducaoIdentificador = new LinhaProducaoIdentificador("re");
    CodigoInternoMateriaPrima codigoInternoMateriaPrima = new CodigoInternoMateriaPrima("c1");
    CodigoAlfanumerico codigoAlfanumerico = new CodigoAlfanumerico("alfa1");
    Quantidade quantidade = new Quantidade(1l, Unidade.UN);
    CodigoFabricoProduto codigoFabricoProduto = new CodigoFabricoProduto("234");
    LoteId loteId = new LoteId("ul");
    CodigoParagem codigoParagem = new CodigoParagem("para");
    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }


}