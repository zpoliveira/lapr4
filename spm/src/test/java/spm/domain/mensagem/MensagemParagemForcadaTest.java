package spm.domain.mensagem;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MensagemParagemForcadaTest extends MensagemTest{

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test(expected = IllegalArgumentException.class)
    public void testeDataHoraNotNull() {
        new MensagemParagemForcada(null,
                codigoInternoMaquina,
                identificadorOrdemProducao,
                linhaProducaoIdentificador, codigoParagem);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testeCodigoInternoMaquinaNotNull() {
        new MensagemParagemForcada(dataHora,
                null,
                identificadorOrdemProducao,
                linhaProducaoIdentificador, codigoParagem);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testeIdentificadorordemProducaoNotNull() {
        new MensagemParagemForcada(dataHora,
                codigoInternoMaquina,
                null,
                linhaProducaoIdentificador, codigoParagem);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testeLinhaProducaoIdentificadorNotNull() {
        new MensagemParagemForcada(dataHora,
                codigoInternoMaquina,
                identificadorOrdemProducao,
                null, codigoParagem);
    }


    @Test(expected = IllegalArgumentException.class)
    public void testeCodigoParagemNotNull() {
        new MensagemParagemForcada(dataHora,
                codigoInternoMaquina,
                identificadorOrdemProducao,
                linhaProducaoIdentificador, null);
    }
}