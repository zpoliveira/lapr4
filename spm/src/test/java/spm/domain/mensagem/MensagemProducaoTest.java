package spm.domain.mensagem;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MensagemProducaoTest extends MensagemTest{

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test(expected = IllegalArgumentException.class)
    public void testeDataHoraNotNull() {
        new MensagemProducao(null,
                codigoInternoMaquina,
                identificadorOrdemProducao,
                linhaProducaoIdentificador,codigoFabricoProduto, quantidade, loteId);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testeCodigoInternoMaquinaNotNull() {
        new MensagemProducao(dataHora,
                null,
                identificadorOrdemProducao,
                linhaProducaoIdentificador,codigoFabricoProduto, quantidade, loteId);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testeIdentificadorordemProducaoNotNull() {
        new MensagemProducao(dataHora,
                codigoInternoMaquina,
                null,
                linhaProducaoIdentificador,codigoFabricoProduto, quantidade, loteId);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testeLinhaProducaoIdentificadorNotNull() {
        new MensagemProducao(dataHora,
                codigoInternoMaquina,
                identificadorOrdemProducao,
                null,codigoFabricoProduto, quantidade, loteId);
    }


    @Test(expected = IllegalArgumentException.class)
    public void testeQuantidadeNotNull() {
        new MensagemProducao(dataHora,
                codigoInternoMaquina,
                identificadorOrdemProducao,
                linhaProducaoIdentificador,codigoFabricoProduto, null, loteId);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testeCodigoFabricoProdutoNotNull() {
        new MensagemProducao(dataHora,
                codigoInternoMaquina,
                identificadorOrdemProducao,
                linhaProducaoIdentificador,null, quantidade, loteId);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testeLoteIdNotNull() {
        new MensagemProducao(dataHora,
                codigoInternoMaquina,
                identificadorOrdemProducao,
                linhaProducaoIdentificador,codigoFabricoProduto, quantidade, null);
    }

}