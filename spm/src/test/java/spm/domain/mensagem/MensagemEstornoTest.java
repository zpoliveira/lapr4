package spm.domain.mensagem;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MensagemEstornoTest extends MensagemTest{

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test(expected = IllegalArgumentException.class)
    public void testeDataHoraNotNull() {
        new MensagemEstorno(null,
                codigoInternoMaquina,
                identificadorOrdemProducao,
                linhaProducaoIdentificador,
                codigoInternoMateriaPrima,
                codigoAlfanumerico,
                quantidade);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testeCodigoInternoMaquinaNotNull() {
        new MensagemEstorno(dataHora,
                null,
                identificadorOrdemProducao,
                linhaProducaoIdentificador,
                codigoInternoMateriaPrima,
                codigoAlfanumerico,
                quantidade);

    }

    @Test(expected = IllegalArgumentException.class)
    public void testeIdentificadorordemProducaoNotNull() {
        new MensagemEstorno(dataHora,
                codigoInternoMaquina,
                null,
                linhaProducaoIdentificador,
                codigoInternoMateriaPrima,
                codigoAlfanumerico,
                quantidade);

    }

    @Test(expected = IllegalArgumentException.class)
    public void testeLinhaProducaoIdentificadorNotNull() {
        new MensagemEstorno(dataHora,
                codigoInternoMaquina,
                identificadorOrdemProducao,
                null,
                codigoInternoMateriaPrima,
                codigoAlfanumerico,
                quantidade);
    }


    @Test(expected = IllegalArgumentException.class)
    public void testeCondigoInternoMateriaPrimaNotNull() {
        new MensagemEstorno(dataHora,
                codigoInternoMaquina,
                identificadorOrdemProducao,
                linhaProducaoIdentificador,
                null,
                codigoAlfanumerico,
                quantidade);

    }


    @Test(expected = IllegalArgumentException.class)
    public void testeCodigoAlfanumericoNotNull() {
        new MensagemEstorno(dataHora,
                codigoInternoMaquina,
                identificadorOrdemProducao,
                linhaProducaoIdentificador,
                codigoInternoMateriaPrima,
                null,
                quantidade);

    }

    @Test(expected = IllegalArgumentException.class)
    public void testeQuantidadeNotNull() {
        new MensagemEstorno(dataHora,
                codigoInternoMaquina,
                identificadorOrdemProducao,
                linhaProducaoIdentificador,
                codigoInternoMateriaPrima,
                codigoAlfanumerico,
                null);
    }

}