package spm.domain.mensagem;

import org.junit.Test;

import static org.junit.Assert.*;

public class MensagemRetomaAtividadeTest extends MensagemTest{

    @Test(expected = IllegalArgumentException.class)
    public void testeDataHoraNotNull() {
        new MensagemRetomaAtividade(null,
                codigoInternoMaquina,
                identificadorOrdemProducao,
                linhaProducaoIdentificador);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testeCodigoInternoMaquinaNotNull() {
        new MensagemRetomaAtividade(dataHora,
                null,
                identificadorOrdemProducao,
                linhaProducaoIdentificador);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testeIdentificadorordemProducaoNotNull() {
        new MensagemRetomaAtividade(dataHora,
                codigoInternoMaquina,
                null,
                linhaProducaoIdentificador);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testeLinhaProducaoIdentificadorNotNull() {
        new MensagemRetomaAtividade(dataHora,
                codigoInternoMaquina,
                identificadorOrdemProducao,
                null);
    }


}