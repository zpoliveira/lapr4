package spm.repository;

import chaofabrica.domain.model.maquina.CodigoInternoMaquina;
import chaofabrica.domain.model.maquina.CodigoInternoMaquina;
import org.springframework.data.repository.query.Param;
import producao.domain.model.ordemProducao.IdentificadorOrdemProducao;
import spm.domain.mensagem.Mensagem;

import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.List;


public interface MensagemRepository{
    Mensagem save(Mensagem mensagem);


    Iterable<Mensagem> listaMensagemConsumoPorOP(IdentificadorOrdemProducao idOP);
    Iterable<Mensagem> listaMensagemEstornoPorOP(IdentificadorOrdemProducao idOP);
    Iterable<Mensagem> listaMensagemEntregaProducaoPorOP(IdentificadorOrdemProducao idOP);
    Iterable<Mensagem> listaMensagemProducaoPorOP(IdentificadorOrdemProducao idOP);
    Iterable<Mensagem> findByCodigoInternoMaquina(CodigoInternoMaquina codigoInternoMaquina);
    Mensagem findByPk(Long pk);
    Iterable<Mensagem> findByDataHoraBetween(LocalDateTime inicio, LocalDateTime fim);

    List<Mensagem> findByIdentificadorOrdemProducaoOrderByDataHoraAsc(IdentificadorOrdemProducao identificadorOrdemProducao);


    List<Mensagem> findByIdentificadorOrdemProducaoAndByTipoIn(IdentificadorOrdemProducao identificadorOrdemProducao,List<String> tipo);

    List<Mensagem> findByIdentificadorOrdemProducaoAndCodigoInternoMaquina(
            IdentificadorOrdemProducao identificadorOrdemProducao,
            CodigoInternoMaquina codigoInternoMaquina);


    Mensagem findFirstByTipoAndCodigoInternoMaquinaOrderByDataHoraDesc(String tipo, CodigoInternoMaquina codigoInternoMaquina);
}
