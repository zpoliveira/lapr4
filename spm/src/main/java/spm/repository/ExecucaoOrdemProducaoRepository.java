package spm.repository;

import producao.domain.model.ordemProducao.IdentificadorOrdemProducao;
import spm.domain.execucaoOrdemProducao.ExecucaoOrdemProducao;

public interface ExecucaoOrdemProducaoRepository {

    ExecucaoOrdemProducao save(ExecucaoOrdemProducao execucaoOrdemProducao);

    ExecucaoOrdemProducao findByIdentificadorOrdemProducao(IdentificadorOrdemProducao identificadorOrdemProducao);

    Iterable<ExecucaoOrdemProducao> findAll();

    boolean existsByIdentificadorOrdemProducao(IdentificadorOrdemProducao identificadorOrdemProducao);
}
