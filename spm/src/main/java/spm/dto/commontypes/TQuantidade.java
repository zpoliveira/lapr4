package spm.dto.commontypes;

import javax.xml.bind.annotation.*;
import java.math.BigInteger;


/**
 * <p>Java class for TQuantidade complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="TQuantidade"&gt;
 *   &lt;simpleContent&gt;
 *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;positiveInteger"&gt;
 *       &lt;attribute name="Unidade" use="required" type="{http://factyfloor.com/CommunTypes}TUnidade" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/simpleContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TQuantidade", namespace = "http://factyfloor.com/ExecucaoOrdensProducao", propOrder = {
        "value"
})
public class TQuantidade {

    @XmlValue
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger value;
    @XmlAttribute(name = "Unidade", required = true)
    protected TUnidade unidade;

    /**
     * Gets the value of the value property.
     *
     * @return possible object is
     * {@link BigInteger }
     */
    public BigInteger getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     *
     * @param value allowed object is
     *              {@link BigInteger }
     */
    public void setValue(BigInteger value) {
        this.value = value;
    }

    /**
     * Gets the value of the unidade property.
     *
     * @return possible object is
     * {@link TUnidade }
     */
    public TUnidade getUnidade() {
        return unidade;
    }

    /**
     * Sets the value of the unidade property.
     *
     * @param value allowed object is
     *              {@link TUnidade }
     */
    public void setUnidade(TUnidade value) {
        this.unidade = value;
    }

}
