
package spm.dto.commontypes;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TUnidade.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TUnidade"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Kg"/&gt;
 *     &lt;enumeration value="Un"/&gt;
 *     &lt;enumeration value="L"/&gt;
 *     &lt;enumeration value="ton"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "TUnidade", namespace = "http://factyfloor.com/CommunTypes")
@XmlEnum
public enum TUnidade {

    @XmlEnumValue("Kg")
    KG("Kg"),
    @XmlEnumValue("Un")
    UN("Un"),
    L("L"),
    @XmlEnumValue("ton")
    TON("ton");
    private final String value;

    TUnidade(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TUnidade fromValue(String v) {
        for (TUnidade c: TUnidade.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
