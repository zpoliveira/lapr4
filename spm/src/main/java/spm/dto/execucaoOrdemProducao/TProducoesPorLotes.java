
package spm.dto.execucaoOrdemProducao;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for TProducoesPorLotes complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TProducoesPorLotes"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ProducaoPorLote" type="{http://factyfloor.com/ExecucaoOrdensProducao}TProducaoPorLote"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TProducoesPorLotes", namespace = "http://factyfloor.com/ExecucaoOrdensProducao", propOrder = {
    "producaoPorLote"
})
public class TProducoesPorLotes {

    @XmlElement(name = "ProducaoPorLote", namespace = "http://factyfloor.com/ExecucaoOrdensProducao", required = true)
    protected List<TProducaoPorLote> producaoPorLote;

    /**
     * Gets the value of the producaoPorLote property.
     * 
     * @return
     *     possible object is
     *     {@link TProducaoPorLote }
     *     
     */
    public List<TProducaoPorLote> getProducaoPorLote() {
        if(producaoPorLote == null)
            producaoPorLote = new ArrayList<>();
        return producaoPorLote;
    }

    /**
     * Sets the value of the producaoPorLote property.
     * 
     * @param value
     *     allowed object is
     *     {@link TProducaoPorLote }
     *     
     */
    public void setProducaoPorLote(List<TProducaoPorLote> value) {
        this.producaoPorLote = value;
    }

}
