
package spm.dto.execucaoOrdemProducao;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TExecucoes complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TExecucoes"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ExecucaoOrdemProducao" type="{http://factyfloor.com/ExecucaoOrdensProducao}TExecucaoOrdemProducao" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TExecucoes", namespace = "http://factyfloor.com/ExecucaoOrdensProducao", propOrder = {
    "execucaoOrdemProducao"
})
public class TExecucoes {

    @XmlElement(name = "ExecucaoOrdemProducao", namespace = "http://factyfloor.com/ExecucaoOrdensProducao", required = true)
    protected List<TExecucaoOrdemProducao> execucaoOrdemProducao;

    /**
     * Gets the value of the execucaoOrdemProducao property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the execucaoOrdemProducao property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getExecucaoOrdemProducao().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TExecucaoOrdemProducao }
     * 
     * 
     */
    public List<TExecucaoOrdemProducao> getExecucaoOrdemProducao() {
        if (execucaoOrdemProducao == null) {
            execucaoOrdemProducao = new ArrayList<TExecucaoOrdemProducao>();
        }
        return this.execucaoOrdemProducao;
    }

}
