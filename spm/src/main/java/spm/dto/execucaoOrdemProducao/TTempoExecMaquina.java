
package spm.dto.execucaoOrdemProducao;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TTempoExecMaquina complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TTempoExecMaquina"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TempoBrutoExecucao" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="TempoEfetivoExecucao" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="CodigoInternoMaquina" type="{http://factyfloor.com/CommunTypes}TOneWord"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TTempoExecMaquina", namespace = "http://factyfloor.com/ExecucaoOrdensProducao", propOrder = {
    "tempoBrutoExecucao",
    "tempoEfetivoExecucao",
    "codigoInternoMaquina"
})
public class TTempoExecMaquina {

    @XmlElement(name = "TempoBrutoExecucao", namespace = "http://factyfloor.com/ExecucaoOrdensProducao", required = true)
    protected String tempoBrutoExecucao;
    @XmlElement(name = "TempoEfetivoExecucao", namespace = "http://factyfloor.com/ExecucaoOrdensProducao", required = true)
    protected String tempoEfetivoExecucao;
    @XmlElement(name = "CodigoInternoMaquina", namespace = "http://factyfloor.com/ExecucaoOrdensProducao", required = true)
    protected String codigoInternoMaquina;

    /**
     * Gets the value of the tempoBrutoExecucao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTempoBrutoExecucao() {
        return tempoBrutoExecucao;
    }

    /**
     * Sets the value of the tempoBrutoExecucao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTempoBrutoExecucao(String value) {
        this.tempoBrutoExecucao = value;
    }

    /**
     * Gets the value of the tempoEfetivoExecucao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTempoEfetivoExecucao() {
        return tempoEfetivoExecucao;
    }

    /**
     * Sets the value of the tempoEfetivoExecucao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTempoEfetivoExecucao(String value) {
        this.tempoEfetivoExecucao = value;
    }

    /**
     * Gets the value of the codigoInternoMaquina property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoInternoMaquina() {
        return codigoInternoMaquina;
    }

    /**
     * Sets the value of the codigoInternoMaquina property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoInternoMaquina(String value) {
        this.codigoInternoMaquina = value;
    }

}
