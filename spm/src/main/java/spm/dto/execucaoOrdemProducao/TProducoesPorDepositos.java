
package spm.dto.execucaoOrdemProducao;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for TProducoesPorDepositos complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TProducoesPorDepositos"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ProducaoPorDeposito" type="{http://factyfloor.com/ExecucaoOrdensProducao}TProducaoPorDeposito"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TProducoesPorDepositos", namespace = "http://factyfloor.com/ExecucaoOrdensProducao", propOrder = {
    "producaoPorDeposito"
})
public class TProducoesPorDepositos {

    @XmlElement(name = "ProducaoPorDeposito", namespace = "http://factyfloor.com/ExecucaoOrdensProducao", required = true)
    protected List<TProducaoPorDeposito> producaoPorDeposito;

    /**
     * Gets the value of the producaoPorDeposito property.
     * 
     * @return
     *     possible object is
     *     {@link TProducaoPorDeposito }
     *     
     */
    public List<TProducaoPorDeposito> getProducaoPorDeposito() {
        if(producaoPorDeposito == null)
            producaoPorDeposito = new ArrayList<>();
        return producaoPorDeposito;
    }

    /**
     * Sets the value of the producaoPorDeposito property.
     * 
     * @param value
     *     allowed object is
     *     {@link TProducaoPorDeposito }
     *     
     */
    public void setProducaoPorDeposito(List<TProducaoPorDeposito> value) {
        this.producaoPorDeposito = value;
    }

}
