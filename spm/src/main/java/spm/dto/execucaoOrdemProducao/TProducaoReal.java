
package spm.dto.execucaoOrdemProducao;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TProducaoReal complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TProducaoReal"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="IdProduto" type="{http://factyfloor.com/CommunTypes}TOneWord"/&gt;
 *         &lt;element name="QuantidadeProduzida" type="{http://factyfloor.com/CommunTypes}TOneWord"/&gt;
 *         &lt;element name="ProducoesPorLotes" type="{http://factyfloor.com/ExecucaoOrdensProducao}TProducoesPorLotes"/&gt;
 *         &lt;element name="ProducoesPorDeposito" type="{http://factyfloor.com/ExecucaoOrdensProducao}TProducoesPorDepositos"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TProducaoReal", namespace = "http://factyfloor.com/ExecucaoOrdensProducao", propOrder = {
    "idProduto",
    "quantidadeProduzida",
    "producoesPorLotes",
    "producoesPorDeposito"
})
public class TProducaoReal {

    @XmlElement(name = "IdProduto", namespace = "http://factyfloor.com/ExecucaoOrdensProducao", required = true)
    protected String idProduto;
    @XmlElement(name = "QuantidadeProduzida", namespace = "http://factyfloor.com/ExecucaoOrdensProducao", required = true)
    protected String quantidadeProduzida;
    @XmlElement(name = "ProducoesPorLotes", namespace = "http://factyfloor.com/ExecucaoOrdensProducao", required = true)
    protected TProducoesPorLotes producoesPorLotes = new TProducoesPorLotes();
    @XmlElement(name = "ProducoesPorDeposito", namespace = "http://factyfloor.com/ExecucaoOrdensProducao", required = true)
    protected TProducoesPorDepositos producoesPorDeposito = new TProducoesPorDepositos();

    /**
     * Gets the value of the idProduto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdProduto() {
        return idProduto;
    }

    /**
     * Sets the value of the idProduto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdProduto(String value) {
        this.idProduto = value;
    }

    /**
     * Gets the value of the quantidadeProduzida property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuantidadeProduzida() {
        return quantidadeProduzida;
    }

    /**
     * Sets the value of the quantidadeProduzida property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuantidadeProduzida(String value) {
        this.quantidadeProduzida = value;
    }

    /**
     * Gets the value of the producoesPorLotes property.
     * 
     * @return
     *     possible object is
     *     {@link TProducoesPorLotes }
     *     
     */
    public TProducoesPorLotes getProducoesPorLotes() {
        return producoesPorLotes;
    }

    /**
     * Sets the value of the producoesPorLotes property.
     * 
     * @param value
     *     allowed object is
     *     {@link TProducoesPorLotes }
     *     
     */
    public void setProducoesPorLotes(TProducoesPorLotes value) {
        this.producoesPorLotes = value;
    }

    /**
     * Gets the value of the producoesPorDeposito property.
     * 
     * @return
     *     possible object is
     *     {@link TProducoesPorDepositos }
     *     
     */
    public TProducoesPorDepositos getProducoesPorDeposito() {
        return producoesPorDeposito;
    }

    /**
     * Sets the value of the producoesPorDeposito property.
     * 
     * @param value
     *     allowed object is
     *     {@link TProducoesPorDepositos }
     *     
     */
    public void setProducoesPorDeposito(TProducoesPorDepositos value) {
        this.producoesPorDeposito = value;
    }

}
