
package spm.dto.execucaoOrdemProducao;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TDesvioConsumoMP complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TDesvioConsumoMP"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="IdMateriaPrima" type="{http://factyfloor.com/CommunTypes}TOneWord"/&gt;
 *         &lt;element name="Desvio" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TDesvioConsumoMP", namespace = "http://factyfloor.com/ExecucaoOrdensProducao", propOrder = {
    "idMateriaPrima",
    "desvio"
})
public class TDesvioConsumoMP {

    @XmlElement(name = "IdMateriaPrima", namespace = "http://factyfloor.com/ExecucaoOrdensProducao", required = true)
    protected String idMateriaPrima;
    @XmlElement(name = "Desvio", namespace = "http://factyfloor.com/ExecucaoOrdensProducao")
    protected long desvio;

    /**
     * Gets the value of the idMateriaPrima property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdMateriaPrima() {
        return idMateriaPrima;
    }

    /**
     * Sets the value of the idMateriaPrima property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdMateriaPrima(String value) {
        this.idMateriaPrima = value;
    }

    /**
     * Gets the value of the desvio property.
     * 
     */
    public long getDesvio() {
        return desvio;
    }

    /**
     * Sets the value of the desvio property.
     * 
     */
    public void setDesvio(long value) {
        this.desvio = value;
    }

}
