
package spm.dto.execucaoOrdemProducao;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TProducaoPorLote complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TProducaoPorLote"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="IdLote" type="{http://factyfloor.com/CommunTypes}TOneWord"/&gt;
 *         &lt;element name="Quantidade" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TProducaoPorLote", namespace = "http://factyfloor.com/ExecucaoOrdensProducao", propOrder = {
    "idLote",
    "quantidade"
})
public class TProducaoPorLote {

    @XmlElement(name = "IdLote", namespace = "http://factyfloor.com/ExecucaoOrdensProducao", required = true)
    protected String idLote;
    @XmlElement(name = "Quantidade", namespace = "http://factyfloor.com/ExecucaoOrdensProducao")
    protected long quantidade;

    /**
     * Gets the value of the idLote property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdLote() {
        return idLote;
    }

    /**
     * Sets the value of the idLote property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdLote(String value) {
        this.idLote = value;
    }

    /**
     * Gets the value of the quantidade property.
     * 
     */
    public long getQuantidade() {
        return quantidade;
    }

    /**
     * Sets the value of the quantidade property.
     * 
     */
    public void setQuantidade(long value) {
        this.quantidade = value;
    }

}
