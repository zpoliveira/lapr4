
package spm.dto.execucaoOrdemProducao;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TExecucaoOrdemProducao complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TExecucaoOrdemProducao"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="IdOrdemProducao" type="{http://factyfloor.com/CommunTypes}TOneWord"/&gt;
 *         &lt;element name="ConsumoRealMateriaPrima" type="{http://factyfloor.com/ExecucaoOrdensProducao}TConsumoRealMP" minOccurs="0"/&gt;
 *         &lt;element name="DesviosConsumoMateriaPrima" type="{http://factyfloor.com/ExecucaoOrdensProducao}TDesviosConsumoMP" minOccurs="0"/&gt;
 *         &lt;element name="DesvioProducao" type="{http://factyfloor.com/ExecucaoOrdensProducao}TDesvioProducao" minOccurs="0"/&gt;
 *         &lt;element name="ProducoesReais" type="{http://factyfloor.com/ExecucaoOrdensProducao}TProducoesReais" minOccurs="0"/&gt;
 *         &lt;element name="TemposExecucaoMaquinas" type="{http://factyfloor.com/ExecucaoOrdensProducao}TTemposExecMaquinas" minOccurs="0"/&gt;
 *         &lt;element name="TempoEfetivoExecucao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TempoBrutoExecucao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Estorno" type="{http://factyfloor.com/ExecucaoOrdensProducao}TEstorno" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TExecucaoOrdemProducao", namespace = "http://factyfloor.com/ExecucaoOrdensProducao", propOrder = {
    "idOrdemProducao",
    "consumoRealMateriaPrima",
    "desviosConsumoMateriaPrima",
    "desvioProducao",
    "producoesReais",
    "temposExecucaoMaquinas",
    "tempoEfetivoExecucao",
    "tempoBrutoExecucao",
    "estorno"
})
public class TExecucaoOrdemProducao {

    @XmlElement(name = "IdOrdemProducao", namespace = "http://factyfloor.com/ExecucaoOrdensProducao", required = true)
    protected String idOrdemProducao;
    @XmlElement(name = "ConsumoRealMateriaPrima", namespace = "http://factyfloor.com/ExecucaoOrdensProducao")
    protected TConsumoRealMP consumoRealMateriaPrima = new TConsumoRealMP();
    @XmlElement(name = "DesviosConsumoMateriaPrima", namespace = "http://factyfloor.com/ExecucaoOrdensProducao")
    protected TDesviosConsumoMP desviosConsumoMateriaPrima = new TDesviosConsumoMP();
    @XmlElement(name = "DesvioProducao", namespace = "http://factyfloor.com/ExecucaoOrdensProducao")
    protected TDesvioProducao desvioProducao = new TDesvioProducao();
    @XmlElement(name = "ProducoesReais", namespace = "http://factyfloor.com/ExecucaoOrdensProducao")
    protected TProducoesReais producoesReais = new TProducoesReais();
    @XmlElement(name = "TemposExecucaoMaquinas", namespace = "http://factyfloor.com/ExecucaoOrdensProducao")
    protected TTemposExecMaquinas temposExecucaoMaquinas = new TTemposExecMaquinas();
    @XmlElement(name = "TempoEfetivoExecucao", namespace = "http://factyfloor.com/ExecucaoOrdensProducao")
    protected String tempoEfetivoExecucao;
    @XmlElement(name = "TempoBrutoExecucao", namespace = "http://factyfloor.com/ExecucaoOrdensProducao")
    protected String tempoBrutoExecucao;
    @XmlElement(name = "Estorno", namespace = "http://factyfloor.com/ExecucaoOrdensProducao")
    protected TEstorno estorno = new TEstorno();

    /**
     * Gets the value of the idOrdemProducao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdOrdemProducao() {
        return idOrdemProducao;
    }

    /**
     * Sets the value of the idOrdemProducao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdOrdemProducao(String value) {
        this.idOrdemProducao = value;
    }

    /**
     * Gets the value of the consumoRealMateriaPrima property.
     * 
     * @return
     *     possible object is
     *     {@link TConsumoRealMP }
     *     
     */
    public TConsumoRealMP getConsumoRealMateriaPrima() {
        return consumoRealMateriaPrima;
    }

    /**
     * Sets the value of the consumoRealMateriaPrima property.
     * 
     * @param value
     *     allowed object is
     *     {@link TConsumoRealMP }
     *     
     */
    public void setConsumoRealMateriaPrima(TConsumoRealMP value) {
        this.consumoRealMateriaPrima = value;
    }

    /**
     * Gets the value of the desviosConsumoMateriaPrima property.
     * 
     * @return
     *     possible object is
     *     {@link TDesviosConsumoMP }
     *     
     */
    public TDesviosConsumoMP getDesviosConsumoMateriaPrima() {
        return desviosConsumoMateriaPrima;
    }

    /**
     * Sets the value of the desviosConsumoMateriaPrima property.
     * 
     * @param value
     *     allowed object is
     *     {@link TDesviosConsumoMP }
     *     
     */
    public void setDesviosConsumoMateriaPrima(TDesviosConsumoMP value) {
        this.desviosConsumoMateriaPrima = value;
    }

    /**
     * Gets the value of the desvioProducao property.
     * 
     * @return
     *     possible object is
     *     {@link TDesvioProducao }
     *     
     */
    public TDesvioProducao getDesvioProducao() {
        return desvioProducao;
    }

    /**
     * Sets the value of the desvioProducao property.
     * 
     * @param value
     *     allowed object is
     *     {@link TDesvioProducao }
     *     
     */
    public void setDesvioProducao(TDesvioProducao value) {
        this.desvioProducao = value;
    }

    /**
     * Gets the value of the producoesReais property.
     * 
     * @return
     *     possible object is
     *     {@link TProducoesReais }
     *     
     */
    public TProducoesReais getProducoesReais() {
        return producoesReais;
    }

    /**
     * Sets the value of the producoesReais property.
     * 
     * @param value
     *     allowed object is
     *     {@link TProducoesReais }
     *     
     */
    public void setProducoesReais(TProducoesReais value) {
        this.producoesReais = value;
    }

    /**
     * Gets the value of the temposExecucaoMaquinas property.
     * 
     * @return
     *     possible object is
     *     {@link TTemposExecMaquinas }
     *     
     */
    public TTemposExecMaquinas getTemposExecucaoMaquinas() {
        return temposExecucaoMaquinas;
    }

    /**
     * Sets the value of the temposExecucaoMaquinas property.
     * 
     * @param value
     *     allowed object is
     *     {@link TTemposExecMaquinas }
     *     
     */
    public void setTemposExecucaoMaquinas(TTemposExecMaquinas value) {
        this.temposExecucaoMaquinas = value;
    }

    /**
     * Gets the value of the tempoEfetivoExecucao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTempoEfetivoExecucao() {
        return tempoEfetivoExecucao;
    }

    /**
     * Sets the value of the tempoEfetivoExecucao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTempoEfetivoExecucao(String value) {
        this.tempoEfetivoExecucao = value;
    }

    /**
     * Gets the value of the tempoBrutoExecucao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTempoBrutoExecucao() {
        return tempoBrutoExecucao;
    }

    /**
     * Sets the value of the tempoBrutoExecucao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTempoBrutoExecucao(String value) {
        this.tempoBrutoExecucao = value;
    }

    /**
     * Gets the value of the estorno property.
     * 
     * @return
     *     possible object is
     *     {@link TEstorno }
     *     
     */
    public TEstorno getEstorno() {
        return estorno;
    }

    /**
     * Sets the value of the estorno property.
     * 
     * @param value
     *     allowed object is
     *     {@link TEstorno }
     *     
     */
    public void setEstorno(TEstorno value) {
        this.estorno = value;
    }

}
