
package spm.dto.execucaoOrdemProducao;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TMovimentoMateriaPrima complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TMovimentoMateriaPrima"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CodigoInternoMP" type="{http://factyfloor.com/CommunTypes}TOneWord"/&gt;
 *         &lt;element name="CodigoDepesito" type="{http://factyfloor.com/CommunTypes}TOneWord"/&gt;
 *         &lt;element name="Consumo" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TMovimentoMateriaPrima", namespace = "http://factyfloor.com/ExecucaoOrdensProducao", propOrder = {
    "codigoInternoMP",
    "codigoDepesito",
    "consumo"
})
public class TMovimentoMateriaPrima {

    @XmlElement(name = "CodigoInternoMP", namespace = "http://factyfloor.com/ExecucaoOrdensProducao", required = true)
    protected String codigoInternoMP;
    @XmlElement(name = "CodigoDepesito", namespace = "http://factyfloor.com/ExecucaoOrdensProducao", required = true)
    protected String codigoDepesito;
    @XmlElement(name = "Consumo", namespace = "http://factyfloor.com/ExecucaoOrdensProducao")
    protected long consumo;

    /**
     * Gets the value of the codigoInternoMP property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoInternoMP() {
        return codigoInternoMP;
    }

    /**
     * Sets the value of the codigoInternoMP property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoInternoMP(String value) {
        this.codigoInternoMP = value;
    }

    /**
     * Gets the value of the codigoDepesito property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoDepesito() {
        return codigoDepesito;
    }

    /**
     * Sets the value of the codigoDepesito property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoDepesito(String value) {
        this.codigoDepesito = value;
    }

    /**
     * Gets the value of the consumo property.
     * 
     */
    public long getConsumo() {
        return consumo;
    }

    /**
     * Sets the value of the consumo property.
     * 
     */
    public void setConsumo(long value) {
        this.consumo = value;
    }

}
