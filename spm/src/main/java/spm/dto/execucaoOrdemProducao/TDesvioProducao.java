
package spm.dto.execucaoOrdemProducao;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TDesvioProducao complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TDesvioProducao"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Desvio" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *         &lt;element name="QuantidadeOrdemProducao" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *         &lt;element name="QuantidadeRealProduzida" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TDesvioProducao", namespace = "http://factyfloor.com/ExecucaoOrdensProducao", propOrder = {
    "desvio",
    "quantidadeOrdemProducao",
    "quantidadeRealProduzida"
})
public class TDesvioProducao {

    @XmlElement(name = "Desvio", namespace = "http://factyfloor.com/ExecucaoOrdensProducao")
    protected long desvio;
    @XmlElement(name = "QuantidadeOrdemProducao", namespace = "http://factyfloor.com/ExecucaoOrdensProducao")
    protected long quantidadeOrdemProducao;
    @XmlElement(name = "QuantidadeRealProduzida", namespace = "http://factyfloor.com/ExecucaoOrdensProducao")
    protected long quantidadeRealProduzida;

    /**
     * Gets the value of the desvio property.
     * 
     */
    public long getDesvio() {
        return desvio;
    }

    /**
     * Sets the value of the desvio property.
     * 
     */
    public void setDesvio(long value) {
        this.desvio = value;
    }

    /**
     * Gets the value of the quantidadeOrdemProducao property.
     * 
     */
    public long getQuantidadeOrdemProducao() {
        return quantidadeOrdemProducao;
    }

    /**
     * Sets the value of the quantidadeOrdemProducao property.
     * 
     */
    public void setQuantidadeOrdemProducao(long value) {
        this.quantidadeOrdemProducao = value;
    }

    /**
     * Gets the value of the quantidadeRealProduzida property.
     * 
     */
    public long getQuantidadeRealProduzida() {
        return quantidadeRealProduzida;
    }

    /**
     * Sets the value of the quantidadeRealProduzida property.
     * 
     */
    public void setQuantidadeRealProduzida(long value) {
        this.quantidadeRealProduzida = value;
    }

}
