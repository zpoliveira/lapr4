
package spm.dto.execucaoOrdemProducao;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TEstorno complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TEstorno"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="MovimentoEstorno" type="{http://factyfloor.com/ExecucaoOrdensProducao}TMovimentoMateriaPrima" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TEstorno", namespace = "http://factyfloor.com/ExecucaoOrdensProducao", propOrder = {
    "movimentoEstorno"
})
public class TEstorno {

    @XmlElement(name = "MovimentoEstorno", namespace = "http://factyfloor.com/ExecucaoOrdensProducao", required = true)
    protected List<TMovimentoMateriaPrima> movimentoEstorno;

    /**
     * Gets the value of the movimentoEstorno property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the movimentoEstorno property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMovimentoEstorno().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TMovimentoMateriaPrima }
     * 
     * 
     */
    public List<TMovimentoMateriaPrima> getMovimentoEstorno() {
        if (movimentoEstorno == null) {
            movimentoEstorno = new ArrayList<TMovimentoMateriaPrima>();
        }
        return this.movimentoEstorno;
    }

}
