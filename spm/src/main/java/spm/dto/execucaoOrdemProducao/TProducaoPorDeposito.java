
package spm.dto.execucaoOrdemProducao;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TProducaoPorDeposito complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TProducaoPorDeposito"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="IdDeposito" type="{http://factyfloor.com/CommunTypes}TOneWord"/&gt;
 *         &lt;element name="Quantidade" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TProducaoPorDeposito", namespace = "http://factyfloor.com/ExecucaoOrdensProducao", propOrder = {
    "idDeposito",
    "quantidade"
})
public class TProducaoPorDeposito {

    @XmlElement(name = "IdDeposito", namespace = "http://factyfloor.com/ExecucaoOrdensProducao", required = true)
    protected String idDeposito;
    @XmlElement(name = "Quantidade", namespace = "http://factyfloor.com/ExecucaoOrdensProducao")
    protected long quantidade;

    /**
     * Gets the value of the idDeposito property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDeposito() {
        return idDeposito;
    }

    /**
     * Sets the value of the idDeposito property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDeposito(String value) {
        this.idDeposito = value;
    }

    /**
     * Gets the value of the quantidade property.
     * 
     */
    public long getQuantidade() {
        return quantidade;
    }

    /**
     * Sets the value of the quantidade property.
     * 
     */
    public void setQuantidade(long value) {
        this.quantidade = value;
    }

}
