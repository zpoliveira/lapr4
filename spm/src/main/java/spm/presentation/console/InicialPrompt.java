package spm.presentation.console;

import chaofabrica.domain.model.linhaproducao.LinhaProducao;
import chaofabrica.domain.repositories.LinhaProducaoRepository;
import eapli.framework.presentation.console.SelectWidget;
import eapli.framework.io.util.Console;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

@Component
public class InicialPrompt {

    @Autowired
    LinhaProducaoRepository linhaProducaoRepository;

    Calendar inicio;
    Calendar fim;
    List<LinhaProducao> linhas = new ArrayList<>();

    public LocalDateTime inicio(){
        DateTimeFormatter ft = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

        inicio = Console.readCalendar("Insira inicio do período de processamento (Formato AAAA-MM-DD HH:MM)",
                "yyyy-MM-dd HH:mm");
        TimeZone tz = inicio.getTimeZone();
        ZoneId zid = tz == null ? ZoneId.systemDefault() : tz.toZoneId();
        return LocalDateTime.ofInstant(inicio.toInstant(), zid);
    }

    public LocalDateTime fim(){
        DateTimeFormatter ft = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime finalPeriodo;


        String opcao = Console.readLine("Pretende indicar fim do período de processamento? (S/N)");

        if("S".trim().equalsIgnoreCase(opcao)){
            fim = Console.readCalendar("Insira fim do período de processamento (Formato AAAA-MM-DD HH:MM)",
                    "yyyy-MM-dd HH:mm");
            TimeZone tz = fim.getTimeZone();
            ZoneId zid = tz == null ? ZoneId.systemDefault() : tz.toZoneId();
            finalPeriodo = LocalDateTime.ofInstant(fim.toInstant(), zid);
        }else{
            finalPeriodo = LocalDateTime.now();
        }

        return finalPeriodo;
    }

    public List<LinhaProducao> linhas(){
        List<LinhaProducao> linhas = new ArrayList<>();
        boolean flag = true;

        do {
            final SelectWidget<LinhaProducao> selectorErro = new SelectWidget<>("Selecione Linha de Produção",
                    linhaProducaoRepository.findAll());
            selectorErro.show();
            int op = selectorErro.selectedOption();
            if(op != 0){
                final LinhaProducao linha = selectorErro.selectedElement();
                if(linhas.contains(linha)){
                    System.out.println("Linha já escolhida");
                }else {
                    linhas.add(linha);
                }
                String opcao = "";
                do {
                    opcao = Console.readLine("Pretende selecionar outra linha? (S/N)");
                    if ("N".equalsIgnoreCase(opcao)) {
                        flag = false;
                        break;
                    }
                }while(!"S".equalsIgnoreCase(opcao) && !("N").equalsIgnoreCase(opcao));
            }else{
                flag = false;
            }

        }while(flag);

        if(linhas.size() == 0){
            Iterable<LinhaProducao> aux = linhaProducaoRepository.findAll();
            for(LinhaProducao l: aux){
                linhas.add(l);
            }
        }
        return linhas;
    }

    private LinhaProducao validaLinhas(String linha) {

        LinhaProducao li = linhaProducaoRepository.findByCodigo_linhaProducaoID(linha);
        if (li == null) {
            return null;
        }
        System.out.println("A linha " + li + " é valida");
        return li;
    }



}
