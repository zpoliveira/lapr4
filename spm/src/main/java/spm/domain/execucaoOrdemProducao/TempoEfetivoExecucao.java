package spm.domain.execucaoOrdemProducao;

import chaofabrica.domain.model.maquina.CodigoInternoMaquina;
import producao.domain.model.ordemProducao.IdentificadorOrdemProducao;
import spm.domain.mensagem.Mensagem;
import spm.domain.mensagem.MensagemInicioAtividade;
import spm.domain.mensagem.MensagemParagemForcada;
import spm.domain.mensagem.MensagemRetomaAtividade;
import spm.repository.MensagemRepository;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Embeddable;
import javax.persistence.Transient;
import java.io.Serializable;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Embeddable
@Access(AccessType.FIELD)
public class TempoEfetivoExecucao implements Serializable {
    private String tempoEfetivoExecucao;
    @Transient
    private MensagemRepository mensagemRepository;
    @Transient
    private IdentificadorOrdemProducao identificadorOrdemProducao;
    protected TempoEfetivoExecucao() {
    }

    public TempoEfetivoExecucao(IdentificadorOrdemProducao identificadorOrdemProducao, MensagemRepository mensagemRepository) {
        this.mensagemRepository = mensagemRepository;
        this.identificadorOrdemProducao = identificadorOrdemProducao;
    }

    public TempoEfetivoExecucao calcularTempoEfetivoMaquina(CodigoInternoMaquina codigoInternoMaquina){
        Duration paragem = this.tempoParagemMaquina();
        Duration duration;
        List<Mensagem> me = this.mensagemRepository.findByIdentificadorOrdemProducaoAndCodigoInternoMaquina(identificadorOrdemProducao, codigoInternoMaquina);
        if(me.size() == 0){
            return null;
        }
        LocalDateTime start = me.get(0).dataHora();
        LocalDateTime end = me.get(me.size()-1).dataHora();
        duration = Duration.between(start, end);
        if(paragem !=null){
            duration = duration.minus(paragem);
        }
        long millis = duration.toMillis();
        this.tempoEfetivoExecucao = String.format("%02dH:%02dM:%02ds",
                TimeUnit.MILLISECONDS.toHours(millis),
                TimeUnit.MILLISECONDS.toMinutes(millis) -
                        TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                TimeUnit.MILLISECONDS.toSeconds(millis) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
        return this;
    }

    private Duration tempoParagemMaquina(){
        Duration duration = null;
        List<Mensagem> me = this.mensagemRepository.findByIdentificadorOrdemProducaoAndByTipoIn(identificadorOrdemProducao, Arrays.asList("MensagemParagemForcada", "MensagemRetomaAtividade"));
        List<LocalDateTime> paragens = new ArrayList<>();
        List<LocalDateTime> retomas = new ArrayList<>();
        for(Mensagem m : me){
            if(m instanceof MensagemParagemForcada){
                paragens.add(m.dataHora());
            }else{
                retomas.add(m.dataHora());
            }
        }
        int i =0;
        for(LocalDateTime data : paragens){
            duration.plus(Duration.between(paragens.get(i), retomas.get(i)));
        }
        return duration;
    }


    public TempoEfetivoExecucao calcularTempoEfetivoOrdemProducao(List<String> idMaquinas){
        Duration paragem = this.tempoParagemOrdemProducao(idMaquinas);
        Duration duration;
        List<Mensagem> me = this.mensagemRepository.findByIdentificadorOrdemProducaoOrderByDataHoraAsc(identificadorOrdemProducao);
        LocalDateTime start = me.get(0).dataHora();
        LocalDateTime end = me.get(me.size()-1).dataHora();
        duration = Duration.between(start, end);
        if(paragem !=null){
            duration = duration.minus(paragem);
        }
        long millis = duration.toMillis();
        this.tempoEfetivoExecucao = String.format("%02dH:%02dM:%02ds",
                TimeUnit.MILLISECONDS.toHours(millis),
                TimeUnit.MILLISECONDS.toMinutes(millis) -
                        TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                TimeUnit.MILLISECONDS.toSeconds(millis) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
        return this;
    }

    private Duration tempoParagemOrdemProducao(List<String> idMaquinas){
        Duration duration = Duration.ZERO;
        LocalDateTime dataParagem = null;
        boolean ordemParada = true;
        HashMap<String,Boolean> maquinasAtivasMap = new HashMap<>();
        idMaquinas.forEach(id->{
            maquinasAtivasMap.put(id, false);
        });
        List<Mensagem> me = this.mensagemRepository.findByIdentificadorOrdemProducaoAndByTipoIn(
                identificadorOrdemProducao,
                Arrays.asList("MensagemInicioAtividade", "MensagemParagemForcada", "MensagemRetomaAtividade", "MensagemFimAtividade"));
        for(Mensagem m :me){
            if(m instanceof MensagemInicioAtividade || m instanceof MensagemRetomaAtividade){
                maquinasAtivasMap.put(m.codigoInternoMaquina().codigoInternoMaquina(), true);
                if(ordemParada && dataParagem !=null){
                    duration = duration.plus(Duration.between(dataParagem, m.dataHora()));
                }
            }else if(me.indexOf(m) != me.size()-1){
                maquinasAtivasMap.put(m.codigoInternoMaquina().codigoInternoMaquina(), false);
            }
            ordemParada = this.areMaquinasParadas(maquinasAtivasMap);
            if(ordemParada){
                dataParagem = m.dataHora();
            }
        }
        return duration;
    }

    private boolean areMaquinasParadas(HashMap<String,Boolean> maquinasAtivasMap){
        List<Boolean> listagemAtividade = new ArrayList<>(maquinasAtivasMap.values());
        return !listagemAtividade.contains(true);
    }

    @Override
    public String toString() {
        return tempoEfetivoExecucao;
    }
}
