package spm.domain.execucaoOrdemProducao;

import chaofabrica.domain.model.deposito.CodigoAlfanumerico;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import producao.domain.model.materiaPrima.CodigoInternoMateriaPrima;
import producao.domain.model.ordemProducao.IdentificadorOrdemProducao;
import spm.domain.mensagem.Mensagem;
import spm.domain.mensagem.MensagemEstorno;
import spm.repository.MensagemRepository;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Embeddable
@Access(AccessType.FIELD)
public class Estorno implements Serializable {

    @Transient
    private IdentificadorOrdemProducao identificadorOrdemProducao;
    @Transient
    private MensagemRepository mensagemRepository;
    @ElementCollection(targetClass=MovMatPrimPorDeposito.class)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<MovMatPrimPorDeposito> estornos = new ArrayList<>();

    protected Estorno() {
    }

    public Estorno(IdentificadorOrdemProducao identificadorOrdemProducao, MensagemRepository mensagemRepository) {
        this.identificadorOrdemProducao = identificadorOrdemProducao;
        this.mensagemRepository = mensagemRepository;
    }

    public Estorno calcularEstorno() {
        Iterable<Mensagem> mConsumo = this.mensagemRepository.listaMensagemEstornoPorOP(this.identificadorOrdemProducao);
        for (Mensagem m : mConsumo) {
            MensagemEstorno me = (MensagemEstorno) m;
            MovMatPrimPorDeposito estornoPorDeposito = new MovMatPrimPorDeposito(me.idMatPrima(), me.codigoAlfanumerico(), me.quantidadeEstorno());
            int index = this.estornos.indexOf(estornoPorDeposito);
            if (index != -1) {
                this.estornos.get(index).somaConsumo(estornoPorDeposito.consumo());
            } else {
                this.estornos.add(estornoPorDeposito);
            }
        }
        return this;
    }

    public List<MovMatPrimPorDeposito> estornos() {
        return estornos;
    }

    @Override
    public String toString() {

        StringBuilder est = new StringBuilder();
        est.append("Estorno:\n");
        this.estornos.forEach(estorno->{
            est.append(estorno.toString() +"\n");
        });
        return est.toString();
    }
}
