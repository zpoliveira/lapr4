package spm.domain.execucaoOrdemProducao;

import chaofabrica.domain.model.deposito.CodigoAlfanumerico;
import producao.domain.model.materiaPrima.CodigoInternoMateriaPrima;


import javax.persistence.*;
import java.util.Objects;

@Embeddable
@Access(AccessType.FIELD)
public class MovMatPrimPorDeposito {

    private CodigoInternoMateriaPrima codigoInternoMateriaPrima;
    private CodigoAlfanumerico codigoAlfanumerico;
    private Long consumo;

    protected MovMatPrimPorDeposito() {
    }

    public MovMatPrimPorDeposito(CodigoInternoMateriaPrima codigoInternoMateriaPrima,
                                 CodigoAlfanumerico codigoAlfanumerico, Long consumo) {
        this.codigoInternoMateriaPrima = codigoInternoMateriaPrima;
        this.codigoAlfanumerico = codigoAlfanumerico;
        this.consumo = consumo;
    }

    public void somaConsumo(Long consumo){
        this.consumo += consumo;
    }

    public String codigoInternoMateriaPrima() {
        return codigoInternoMateriaPrima.codigoInternoMatPrima();
    }

    public String codigoAlfanumerico() {
        return codigoAlfanumerico.codigo();
    }

    public Long consumo(){
        return this.consumo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MovMatPrimPorDeposito that = (MovMatPrimPorDeposito) o;
        return codigoInternoMateriaPrima.equals(that.codigoInternoMateriaPrima) &&
                codigoAlfanumerico.equals(that.codigoAlfanumerico);
    }

    @Override
    public int hashCode() {
        return Objects.hash(codigoInternoMateriaPrima, codigoAlfanumerico);
    }


    @Override
    public String toString() {
        return "Matéria Prima " + codigoInternoMateriaPrima.codigoInternoMatPrima() +
                " codigoAlfanumérico: " + codigoAlfanumerico.codigo() +
                " quantidade: " + consumo;
    }
}
