package spm.domain.execucaoOrdemProducao;

import producao.domain.model.materiaPrima.CodigoInternoMateriaPrima;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
@Access(AccessType.FIELD)
public class DesvioMateriaPrima implements Serializable {

    private CodigoInternoMateriaPrima codigoInternoMateriaPrima;
    Long desvio;

    protected DesvioMateriaPrima() {
    }

    public DesvioMateriaPrima(CodigoInternoMateriaPrima codigoInternoMateriaPrima, Long desvio) {
        this.codigoInternoMateriaPrima = codigoInternoMateriaPrima;
        this.desvio = desvio;
    }

    public CodigoInternoMateriaPrima codigoInternoMateriaPrima() {
        return codigoInternoMateriaPrima;
    }

    public Long desvio() {
        return desvio;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DesvioMateriaPrima that = (DesvioMateriaPrima) o;
        return codigoInternoMateriaPrima.equals(that.codigoInternoMateriaPrima);
    }

    @Override
    public int hashCode() {
        return Objects.hash(codigoInternoMateriaPrima);
    }

    @Override
    public String toString() {
        return"codigoInternoMateriaPrima: " + codigoInternoMateriaPrima +
                " desvio: " + desvio;
    }
}
