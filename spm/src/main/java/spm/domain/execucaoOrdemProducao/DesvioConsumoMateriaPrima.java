package spm.domain.execucaoOrdemProducao;

import chaofabrica.domain.model.deposito.CodigoAlfanumerico;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.beans.factory.annotation.Autowired;
import producao.domain.model.ordemProducao.IdentificadorOrdemProducao;
import producao.domain.model.ordemProducao.OrdemProducao;
import producao.domain.model.produto.ComponenteMateriaPrima;
import producao.domain.model.produto.FichaProducao;
import producao.domain.model.produto.Produto;
import producao.domain.repositories.OrdemProducaoRepository;
import producao.domain.repositories.ProdutoRepository;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Embeddable
@Access(AccessType.FIELD)
public class DesvioConsumoMateriaPrima implements Serializable {

    @Transient
    private IdentificadorOrdemProducao identificadorOrdemProducao;
    @Transient
    private ConsumoRealMateriaPrima consumoRealMateriaPrima;
    @Transient
    private OrdemProducaoRepository ordemProducaoRepository;
    @Transient
    private ProdutoRepository produtoRepository;
    @Transient
    List<MovMatPrimPorDeposito> consumosPorDeposito;
    @OneToOne
    OrdemProducao op;
    @Transient
    Long total;
    @Transient
    Produto pro;
    @OneToOne
    FichaProducao fp;

    @ElementCollection(targetClass=DesvioMateriaPrima.class)
    @LazyCollection(LazyCollectionOption.FALSE)
    List<DesvioMateriaPrima> desvios = new ArrayList<>();

    protected DesvioConsumoMateriaPrima() {
    }

    public DesvioConsumoMateriaPrima(IdentificadorOrdemProducao identificadorOrdemProducao,
                                     ConsumoRealMateriaPrima consumoRealMateriaPrima,
                                     OrdemProducaoRepository ordemProducaoRepository,
                                     ProdutoRepository produtoRepository) {
        this.identificadorOrdemProducao = identificadorOrdemProducao;
        this.consumoRealMateriaPrima = consumoRealMateriaPrima;
        this.ordemProducaoRepository = ordemProducaoRepository;
        this.produtoRepository = produtoRepository;
    }

    public DesvioConsumoMateriaPrima calculaDesvioConsumoMateriaPrima(){

        op = this.ordemProducaoRepository.findByIdentificadorOrdemProducao(this.identificadorOrdemProducao);
        if(op == null){
            throw new IllegalArgumentException("ordem de produção: " + this.identificadorOrdemProducao.identificadorOrdemProducao() +  " não registada");
        }
        pro = this.produtoRepository.findByCodigoFabricoProduto(op.codigoFabricoProduto());
        if(pro == null){
            throw new IllegalArgumentException("produto: " +op.codigoFabricoProduto().toString() + " não registado");
        }
        fp = pro.fichaProducao();
        if(fp ==  null ){
            throw new IllegalArgumentException("produto: " +op.codigoFabricoProduto().toString() + " sem ficha de produção");
        }
        consumosPorDeposito = consumoRealMateriaPrima.consumosPorDeposito();

        for(ComponenteMateriaPrima cmp: fp.componenteMateriaPrima()){
            Long estimado = (op.quantidade().valorNumerico() * cmp.quantidade().valorNumerico()) / fp.quantidade().valorNumerico();
            total = 0L;

            for(MovMatPrimPorDeposito consumo : this.consumosPorDeposito){
                total += consumo.consumo();
            }
            desvios.add(new DesvioMateriaPrima(cmp.codigoInternoMateriaPrima(), total - estimado));
        }

        return this;
    }

    @Override
    public String toString() {

        StringBuilder desvioConsumo = new StringBuilder();

        desvioConsumo.append("Desvio de Consumo de Matéria-Prima\n");
        desvioConsumo.append("Quantidade de matérias-primas necessárias para produzir: " + op.quantidade() + " de " +
                op.codigoFabricoProduto() + "\n");

        for(ComponenteMateriaPrima cmp: fp.componenteMateriaPrima()) {
            Long estimado = (op.quantidade().valorNumerico() * cmp.quantidade().valorNumerico()) / fp.quantidade().valorNumerico();
            desvioConsumo.append(cmp.codigoInternoMateriaPrima() + " || Quantidadade: " +
                    estimado + " UN\n");
        }

        desvioConsumo.append("Desvios\n");
        for(DesvioMateriaPrima des : desvios){
            desvioConsumo.append(des.codigoInternoMateriaPrima() + " || Quantidade: " + des.desvio() + "UN\n");
        }

        return desvioConsumo.toString();
    }
}
