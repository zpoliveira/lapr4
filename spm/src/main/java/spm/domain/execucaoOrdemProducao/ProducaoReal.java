package spm.domain.execucaoOrdemProducao;

import chaofabrica.domain.model.deposito.CodigoAlfanumerico;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import producao.domain.model.ordemProducao.IdentificadorOrdemProducao;
import producao.domain.model.produto.CodigoFabricoProduto;
import spm.domain.mensagem.LoteId;
import spm.domain.mensagem.Mensagem;
import spm.domain.mensagem.MensagemEntregaProducao;
import spm.domain.mensagem.MensagemProducao;
import spm.repository.MensagemRepository;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Embeddable
@Access(AccessType.FIELD)
public class ProducaoReal implements Serializable {

    @Transient
    private MensagemRepository mensagemRepository;
    @Transient
    private IdentificadorOrdemProducao identificadorOrdemProducao;
    @ElementCollection(targetClass=ProducaoPorDeposito.class)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<ProducaoPorDeposito> producaoPorDepositos = new ArrayList<>();
    @ElementCollection(targetClass=ProducaoPorLote.class)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<ProducaoPorLote> producaoPorLotes = new ArrayList<>();
    private long quantidadeProduzida;
    private String idProduto;


    protected ProducaoReal() {
    }

    public ProducaoReal(MensagemRepository mensagemRepository, IdentificadorOrdemProducao identificadorOrdemProducao) {
        this.mensagemRepository = mensagemRepository;
        this.identificadorOrdemProducao = identificadorOrdemProducao;
    }

    public ProducaoReal calcularProducaoPorDeposito() {
        this.quantidadeProduzida = 0;
        Iterable<Mensagem> mProducao = this.mensagemRepository.listaMensagemEntregaProducaoPorOP(this.identificadorOrdemProducao);
        for (Mensagem m : mProducao) {
            MensagemEntregaProducao mep = (MensagemEntregaProducao) m;
            ProducaoPorDeposito producaoPorDeposito = new ProducaoPorDeposito(mep.codigoAlfanumerico(), mep.quantidade().valorNumerico());
            this.idProduto = mep.codigoFabricoProduto().toString();
            int index = this.producaoPorDepositos.indexOf(producaoPorDeposito);
            this.quantidadeProduzida += producaoPorDeposito.quantidade();
            if (index != -1) {
                this.producaoPorDepositos.get(index).somaQuantidade(producaoPorDeposito.quantidade());
            } else {
                this.producaoPorDepositos.add(producaoPorDeposito);
            }
        }

        return this;
    }

    public ProducaoReal calcularProducaoPorLote() {
        this.quantidadeProduzida = 0;
        Iterable<Mensagem> mProducao = this.mensagemRepository.listaMensagemProducaoPorOP(this.identificadorOrdemProducao);
        for (Mensagem m : mProducao) {
            MensagemProducao mep = (MensagemProducao) m;
            ProducaoPorLote producaoPorLote = new ProducaoPorLote(mep.loteId(), mep.quantidade().valorNumerico());
            this.idProduto = mep.codigoFabricoProduto().toString();
            int index = this.producaoPorLotes.indexOf(producaoPorLote);
            this.quantidadeProduzida += producaoPorLote.quantidade();
            if (index != -1) {
                this.producaoPorLotes.get(index).somaQuantidade(producaoPorLote.quantidade());
            } else {
                this.producaoPorLotes.add(producaoPorLote);
            }
        }
        return this;
    }

    public long quantidadeProduzida(){
        return this.quantidadeProduzida;
    }

    public String idProduto() {
        return idProduto;
    }

    public List<ProducaoPorDeposito> producaoPorDepositos() {
        return producaoPorDepositos;
    }

    public List<ProducaoPorLote> producaoPorLotes() {
        return producaoPorLotes;
    }

    @Override
    public String toString() {
        StringBuilder deposito = new StringBuilder();
        deposito.append("Producao por depósito\n");

        this.producaoPorDepositos.forEach(producaoPorDeposito -> {
            deposito.append("\nProduto: " +  this.idProduto + " " + producaoPorDeposito.toString());
        });

        deposito.append("\nProducao por lote\n");
        this.producaoPorLotes.forEach(producaoPorLote -> {
            deposito.append("\nProduto: " +  this.idProduto + " " + producaoPorLote.toString());
        });
        return deposito.toString();
    }

}
