package spm.domain.execucaoOrdemProducao;

import spm.domain.mensagem.LoteId;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
@Access(AccessType.FIELD)
public class ProducaoPorLote implements Serializable {

    private LoteId loteId;
    private long quantidade;

    protected ProducaoPorLote() {
    }

    public ProducaoPorLote(LoteId loteId, long quantidade) {
        this.loteId = loteId;
        this.quantidade = quantidade;
    }

    public void somaQuantidade(long quantidade){
        this.quantidade += quantidade;
    }

    public long quantidade(){
        return this.quantidade;
    }

    public String loteId() {
        return loteId.lote();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProducaoPorLote that = (ProducaoPorLote) o;
        return loteId.equals(that.loteId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(loteId);
    }

    @Override
    public String toString() {
        return loteId + " quantidade: " + quantidade;
    }
}
