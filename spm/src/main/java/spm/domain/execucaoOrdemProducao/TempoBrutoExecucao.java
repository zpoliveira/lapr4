package spm.domain.execucaoOrdemProducao;

import chaofabrica.domain.model.maquina.CodigoInternoMaquina;
import chaofabrica.domain.model.maquina.Maquina;
import org.springframework.beans.factory.annotation.Autowired;
import producao.domain.model.ordemProducao.IdentificadorOrdemProducao;
import spm.domain.mensagem.Mensagem;
import spm.repository.MensagemRepository;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Embeddable;
import javax.persistence.Transient;
import java.io.Serializable;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Embeddable
@Access(AccessType.FIELD)
public class TempoBrutoExecucao implements Serializable {

    private String tempoBrutoExecucao;
    @Transient
    private MensagemRepository mensagemRepository;
    @Transient
    private IdentificadorOrdemProducao identificadorOrdemProducao;

    protected TempoBrutoExecucao() {
    }

    public TempoBrutoExecucao(IdentificadorOrdemProducao identificadorOrdemProducao, MensagemRepository mensagemRepository){
        this.identificadorOrdemProducao = identificadorOrdemProducao;
        this.mensagemRepository = mensagemRepository;
    }

    public TempoBrutoExecucao calcularTempoBrutoOrdemProducao(){
        List<Mensagem> me = this.mensagemRepository.findByIdentificadorOrdemProducaoOrderByDataHoraAsc(this.identificadorOrdemProducao);
        LocalDateTime start = me.get(0).dataHora();
        LocalDateTime end = me.get(me.size()-1).dataHora();
        Duration duration = Duration.between(start, end);
        long millis = duration.toMillis();
        this.tempoBrutoExecucao = String.format("%02dH:%02dM:%02ds",
                TimeUnit.MILLISECONDS.toHours(millis),
                TimeUnit.MILLISECONDS.toMinutes(millis) -
                        TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                TimeUnit.MILLISECONDS.toSeconds(millis) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
        return this;
    }

    public TempoBrutoExecucao calcularTempoBrutoMaquina(CodigoInternoMaquina codigoInternoMaquina){
        List<Mensagem> me = this.mensagemRepository.findByIdentificadorOrdemProducaoAndCodigoInternoMaquina(this.identificadorOrdemProducao, codigoInternoMaquina);
        if(me.size() == 0){
            return null;
        }
        LocalDateTime start = me.get(0).dataHora();
        LocalDateTime end = me.get(me.size()-1).dataHora();
        Duration duration = Duration.between(start, end);
        long millis = duration.toMillis();
        this.tempoBrutoExecucao = String.format("%02dH:%02dM:%02ds",
                TimeUnit.MILLISECONDS.toHours(millis),
                TimeUnit.MILLISECONDS.toMinutes(millis) -
                        TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                TimeUnit.MILLISECONDS.toSeconds(millis) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
        return this;
    }

    @Override
    public String toString() {
        return tempoBrutoExecucao;
    }
}
