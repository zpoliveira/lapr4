package spm.domain.execucaoOrdemProducao;

import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.representations.RepresentationBuilder;
import eapli.framework.representations.Representationable;
import eapli.framework.validations.Preconditions;
import org.apache.commons.lang3.builder.EqualsBuilder;
import producao.domain.model.ordemProducao.EstadoOrdemProducao;
import producao.domain.model.ordemProducao.IdentificadorOrdemProducao;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"idOrdemProducao"}))
public class ExecucaoOrdemProducao implements AggregateRoot<IdentificadorOrdemProducao>, Serializable, Representationable {

    private static final long serialVersionUID = 1L;

    @Version
    private Long version;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long pk;

    private IdentificadorOrdemProducao identificadorOrdemProducao;

    private ConsumoRealMateriaPrima consumoRealMateriaPrima;

    private DesvioConsumoMateriaPrima desvioConsumoMateriaPrima;

    private DesvioProducao desvioProducao;

    private ProducaoReal producaoReal;

    @ElementCollection(targetClass = TempoExecucaoMaquina.class, fetch = FetchType.EAGER)
    private List<TempoExecucaoMaquina> tempoExecucaoMaquinas = new ArrayList<>();

    private TempoEfetivoExecucao tempoEfetivoExecucao;

    private TempoBrutoExecucao tempoBrutoExecucao;

    private Estorno estorno;

    private EstadoOrdemProducao estadoOrdemProducao;

    protected ExecucaoOrdemProducao() {
    }

    public ExecucaoOrdemProducao(IdentificadorOrdemProducao identificadorOrdemProducao,
                                 ConsumoRealMateriaPrima consumoRealMateriaPrima,
                                 DesvioConsumoMateriaPrima desvioConsumoMateriaPrima,
                                 DesvioProducao desvioProducao,
                                 ProducaoReal producaoReal,
                                 List<TempoExecucaoMaquina> tempoExecucaoMaquinas,
                                 TempoEfetivoExecucao tempoEfetivoExecucao,
                                 TempoBrutoExecucao tempoBrutoExecucao,
                                 Estorno estorno,
                                 EstadoOrdemProducao estadoOrdemProducao) {
        Preconditions.nonNull(identificadorOrdemProducao, "O identificador da Ordem de Produção não pode ser nulo");
        this.identificadorOrdemProducao = identificadorOrdemProducao;
        this.consumoRealMateriaPrima = consumoRealMateriaPrima;
        this.desvioConsumoMateriaPrima = desvioConsumoMateriaPrima;
        this.desvioProducao = desvioProducao;
        this.producaoReal = producaoReal;
        this.tempoExecucaoMaquinas = tempoExecucaoMaquinas;
        this.tempoEfetivoExecucao = tempoEfetivoExecucao;
        this.tempoBrutoExecucao = tempoBrutoExecucao;
        this.estorno = estorno;
        this.estadoOrdemProducao = estadoOrdemProducao;
    }

    public void atualizarConsumoRealMateriaPrima(ConsumoRealMateriaPrima consumoRealMateriaPrima) {
        this.consumoRealMateriaPrima = consumoRealMateriaPrima;
    }

    public void atualizarDesvioConsumoMateriaPrima(DesvioConsumoMateriaPrima desvioConsumoMateriaPrima) {
        this.desvioConsumoMateriaPrima = desvioConsumoMateriaPrima;
    }

    public void atualizarDesvioProducao(DesvioProducao desvioProducao) {
        this.desvioProducao = desvioProducao;
    }

    public void atualizarProducaoReal(ProducaoReal producaoReal) {
        this.producaoReal = producaoReal;
    }

    public void atualizarTempoExecucaoMaquinas(List<TempoExecucaoMaquina> tempoExecucaoMaquinas) {
        this.tempoExecucaoMaquinas = tempoExecucaoMaquinas;
    }

    public void atualizarTempoEfetivoExecucao(TempoEfetivoExecucao tempoEfetivoExecucao) {
        this.tempoEfetivoExecucao = tempoEfetivoExecucao;
    }

    public void atualizarTempoBrutoExecucao(TempoBrutoExecucao tempoBrutoExecucao) {
        this.tempoBrutoExecucao = tempoBrutoExecucao;
    }

    public void atualizarEstorno(Estorno estorno) {
        this.estorno = estorno;
    }

    public void atualizarEstadoOrdemProducao(EstadoOrdemProducao estadoOrdemProducao) {
        this.estadoOrdemProducao = estadoOrdemProducao;
    }

    @Override
    public boolean sameAs(Object other) {
        if (!(other instanceof ExecucaoOrdemProducao)) {
            return false;
        } else {
            ExecucaoOrdemProducao that = (ExecucaoOrdemProducao) other;
            return (new EqualsBuilder()).append(this.identificadorOrdemProducao, that.identificadorOrdemProducao).isEquals();
        }
    }

    @Override
    public IdentificadorOrdemProducao identity() {
        return this.identificadorOrdemProducao;
    }

    @Override
    public String toString() {
        StringBuilder execucao = new StringBuilder();
        execucao.append("Estado da Ordem de Produção: " + this.estadoOrdemProducao + "\n");
        if (!this.estadoOrdemProducao.equals(EstadoOrdemProducao.CONCLUIDA)) {
            execucao.append("ATENÇÃO - A Ordem de Produção ainda não se encontra concluída...");
        }
        execucao.append("Execução da Ordem de Produção: " + identificadorOrdemProducao + "\n");
        execucao.append("Consumo Real MateriaPrima: \n" + consumoRealMateriaPrima + "\n");
        execucao.append("\n" + desvioConsumoMateriaPrima + "\n");
        execucao.append("\n" + desvioProducao + "\n");
        execucao.append("Produção Real: \n" + producaoReal + "\n");
        execucao.append("Tempo Execucao Maquinas: \n");
        for (TempoExecucaoMaquina tmp : tempoExecucaoMaquinas) {
            execucao.append(tmp + "\n");
        }
        execucao.append("Tempo Efetivo Execucao: " + tempoEfetivoExecucao + "\n");
        execucao.append("Tempo Bruto Execucao: " + tempoBrutoExecucao + "\n");
        execucao.append(estorno + "\n");
        return execucao.toString();

    }

    @Override
    public <R> R buildRepresentation(RepresentationBuilder<R> builder) {

        builder.withProperty("idOrdem", this.identificadorOrdemProducao.identificadorOrdemProducao())
                .withProperty("tempoEfetivoExec", this.tempoEfetivoExecucao.toString())
                .withProperty("tempoBrutoExec", this.tempoBrutoExecucao.toString());

        this.consumoRealMateriaPrima.consumosPorDeposito.forEach(movMatPrimPorDeposito -> {
            builder.startObject("consumoRealMP")
                    .withProperty("consumo", movMatPrimPorDeposito.consumo())
                    .withProperty("codMP", movMatPrimPorDeposito.codigoInternoMateriaPrima())
                    .withProperty("codDepo", movMatPrimPorDeposito.codigoAlfanumerico())
                    .endObject();
        });

        this.desvioConsumoMateriaPrima.desvios.forEach(desvioMateriaPrima -> {
            builder.startObject("desvioConsumoMP")
                    .withProperty("desvio", desvioMateriaPrima.desvio())
                    .withProperty("codMP", desvioMateriaPrima.codigoInternoMateriaPrima().codigoInternoMatPrima())
                    .endObject();
        });

        builder.startObject("desvioProducao")
                .withProperty("desvio", this.desvioProducao.desvioProducao())
                .withProperty("qtdOrdProd", this.desvioProducao.quantidadeOrdemProducao())
                .withProperty("qtdRealProd", this.desvioProducao.quantidadeReal())
                .endObject();

        builder.startObject("producaoReal")
                .withProperty("idProd", this.producaoReal.idProduto())
                .withProperty("qtdProd", this.producaoReal.quantidadeProduzida());
        this.producaoReal.producaoPorDepositos().forEach(producaoPorDeposito -> {
            builder.startObject("prodPD")
                    .withProperty("qtd", producaoPorDeposito.quantidade())
                    .withProperty("codDepo", producaoPorDeposito.codigoAlfanumerico())
                    .endObject();
        });
        this.producaoReal.producaoPorLotes().forEach(producaoPorLote -> {
            builder.startObject("prodPL")
                    .withProperty("qtd", producaoPorLote.quantidade())
                    .withProperty("codLote", producaoPorLote.loteId())
                    .endObject();
        });
        builder.startObject("producaoReal_END").endObject();

        this.tempoExecucaoMaquinas.forEach(tempoExecucaoMaquina -> {
            builder.startObject("temposExecMaq")
                    .withProperty("codMaq", tempoExecucaoMaquina.codigoInternoMaquina())
                    .withProperty("tempoBrutoExec", tempoExecucaoMaquina.tempoBrutoExecucao())
                    .withProperty("tempoEfetivoExec", tempoExecucaoMaquina.tempoEfetivoExecucao())
                    .endObject();
        });

        this.estorno.estornos().forEach(movMatPrimPorDeposito -> {
            builder.startObject("estorno")
                    .withProperty("consumo", movMatPrimPorDeposito.consumo())
                    .withProperty("codMP", movMatPrimPorDeposito.codigoInternoMateriaPrima())
                    .withProperty("codDepo", movMatPrimPorDeposito.codigoAlfanumerico())
                    .endObject();
        });

        return builder.build();
    }
}
