package spm.domain.execucaoOrdemProducao;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import producao.domain.model.ordemProducao.IdentificadorOrdemProducao;
import spm.domain.mensagem.Mensagem;
import spm.domain.mensagem.MensagemConsumo;
import spm.repository.MensagemRepository;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Embeddable
@Access(AccessType.FIELD)
public class ConsumoRealMateriaPrima implements Serializable {

    @Transient
    private MensagemRepository mensagemRepository;
    @Transient
    private IdentificadorOrdemProducao identificadorOrdemProducao;
    @ElementCollection(targetClass=MovMatPrimPorDeposito.class)
    @LazyCollection(LazyCollectionOption.FALSE)
    List<MovMatPrimPorDeposito> consumosPorDeposito = new ArrayList<>();



    protected ConsumoRealMateriaPrima() {
    }

    public ConsumoRealMateriaPrima(MensagemRepository mensagemRepository,
                                   IdentificadorOrdemProducao identificadorOrdemProducao) {
        this.mensagemRepository = mensagemRepository;
        this.identificadorOrdemProducao = identificadorOrdemProducao;
    }

    public ConsumoRealMateriaPrima calcularConsumoRealMateriaPrima(){

        Iterable<Mensagem> mConsumo = mensagemRepository.listaMensagemConsumoPorOP(this.identificadorOrdemProducao);

        for(Mensagem m: mConsumo){
            MensagemConsumo mc = (MensagemConsumo)m;
            MovMatPrimPorDeposito consumoPorDeposito = new MovMatPrimPorDeposito(mc.idMatPrima(), mc.codigoAlfanumerico(), mc.quantidadeConsumo());
            int index = this.consumosPorDeposito.indexOf(consumoPorDeposito);
            if(index !=-1){
                this.consumosPorDeposito.get(index).somaConsumo(consumoPorDeposito.consumo());
            }else{
                this.consumosPorDeposito.add(consumoPorDeposito);
            }
        }
        return this;
    }

    public  List<MovMatPrimPorDeposito> consumosPorDeposito(){
        return this.consumosPorDeposito;
    }


    @Override
    public String toString() {

        StringBuilder cons = new StringBuilder();
        cons.append("Consumo Real de Matérias-Primas por depósito\n");
        this.consumosPorDeposito.forEach(consumo->{
            cons.append( consumo.toString() +"\n");
        });

        return cons.toString();
    }
}
