package spm.domain.execucaoOrdemProducao;

import chaofabrica.domain.model.deposito.CodigoAlfanumerico;
import producao.domain.model.produto.CodigoFabricoProduto;
import spm.domain.mensagem.LoteId;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
@Access(AccessType.FIELD)
public class ProducaoPorDeposito implements Serializable {

    private CodigoAlfanumerico codigoAlfanumerico;
    private long quantidade;

    protected ProducaoPorDeposito() {
    }

    public ProducaoPorDeposito(CodigoAlfanumerico codigoAlfanumerico, long quantidade) {
        this.codigoAlfanumerico = codigoAlfanumerico;
        this.quantidade = quantidade;
    }

    public void somaQuantidade(long quantidade){
        this.quantidade += quantidade;
    }

    public String codigoAlfanumerico() {
        return codigoAlfanumerico.codigo();
    }

    public long quantidade(){
        return this.quantidade;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProducaoPorDeposito that = (ProducaoPorDeposito) o;
        return codigoAlfanumerico.equals(that.codigoAlfanumerico);
    }

    @Override
    public int hashCode() {
        return Objects.hash(codigoAlfanumerico);
    }

    @Override
    public String toString() {
        return "Deposito: " + codigoAlfanumerico.codigo() +
                ", quantidade: " + quantidade;
    }
}
