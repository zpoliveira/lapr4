package spm.domain.execucaoOrdemProducao;

import chaofabrica.domain.model.maquina.CodigoInternoMaquina;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Access(AccessType.FIELD)
public class TempoExecucaoMaquina implements Serializable {
    private static final long serialVersionUID = 1L;
    private TempoBrutoExecucao tempoBrutoExecucao;
    private TempoEfetivoExecucao tempoEfetivoExecucao;
    private CodigoInternoMaquina codigoInternoMaquina;

    protected TempoExecucaoMaquina() {
    }

    public TempoExecucaoMaquina(TempoBrutoExecucao tempoBrutoExecucao, TempoEfetivoExecucao tempoEfetivoExecucao, CodigoInternoMaquina codigoInternoMaquina) {
        this.tempoBrutoExecucao = tempoBrutoExecucao;
        this.tempoEfetivoExecucao = tempoEfetivoExecucao;
        this.codigoInternoMaquina = codigoInternoMaquina;
    }

    public String tempoBrutoExecucao() {
        return tempoBrutoExecucao.toString();
    }

    public String tempoEfetivoExecucao() {
        return tempoEfetivoExecucao.toString();
    }

    public String codigoInternoMaquina() {
        return codigoInternoMaquina.codigoInternoMaquina();
    }

    @Override
    public String toString() {
        return "TempoExecucaoMaquina : " +codigoInternoMaquina+
                " tempo Bruto: " + tempoBrutoExecucao +
                " tempoEfetivo: " + tempoEfetivoExecucao;
    }
}
