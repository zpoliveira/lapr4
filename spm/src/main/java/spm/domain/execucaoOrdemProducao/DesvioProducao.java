package spm.domain.execucaoOrdemProducao;

import chaofabrica.domain.model.deposito.CodigoAlfanumerico;
import producao.domain.model.ordemProducao.IdentificadorOrdemProducao;
import producao.domain.model.ordemProducao.OrdemProducao;
import producao.domain.model.produto.CodigoFabricoProduto;
import producao.domain.repositories.OrdemProducaoRepository;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Embeddable;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Map;

@Embeddable
@Access(AccessType.FIELD)
public class DesvioProducao implements Serializable {

    @Transient
    private IdentificadorOrdemProducao identificadorOrdemProducao;
    @Transient
    private ProducaoReal producaoReal;
    @Transient
    private OrdemProducaoRepository ordemProducaoRepository;

    private long desvioProducao;

    private long quantidadeOrdemProducao;

    private long quantidadeReal;

    protected DesvioProducao() {
    }

    public DesvioProducao(IdentificadorOrdemProducao identificadorOrdemProducao, ProducaoReal producaoReal,
                          OrdemProducaoRepository ordemProducaoRepository) {
        this.identificadorOrdemProducao = identificadorOrdemProducao;
        this.producaoReal = producaoReal;
        this.ordemProducaoRepository = ordemProducaoRepository;
    }

    public DesvioProducao calculaDesvioProducao(){

        OrdemProducao op = this.ordemProducaoRepository.findByIdentificadorOrdemProducao(this.identificadorOrdemProducao);
        this.quantidadeOrdemProducao = op.quantidade().valorNumerico();
        this.quantidadeReal = this.producaoReal.quantidadeProduzida();
        this.desvioProducao = this.quantidadeReal - quantidadeOrdemProducao;
        return this;
    }

    public long desvioProducao() {
        return desvioProducao;
    }

    public long quantidadeOrdemProducao() {
        return quantidadeOrdemProducao;
    }

    public long quantidadeReal() {
        return quantidadeReal;
    }

    @Override
    public String toString() {

        StringBuilder desvioProducao = new StringBuilder();
        desvioProducao.append("Desvio de Produção\n");
        desvioProducao.append("Quantidade na Ordem de Produção: " + this.quantidadeOrdemProducao + "UN || Quantidade Produzida: " +
                this.quantidadeReal + "UN\n");
        desvioProducao.append("Desvio: " + this.desvioProducao + "UN\n");

        return desvioProducao.toString();
    }
}
