package spm.domain.mensagem;

import chaofabrica.domain.model.linhaproducao.LinhaProducaoIdentificador;
import chaofabrica.domain.model.maquina.CodigoInternoMaquina;
import eapli.framework.util.Strategy;
import producao.domain.model.materiaPrima.CodigoInternoMateriaPrima;
import producao.domain.model.ordemProducao.IdentificadorOrdemProducao;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "Mensagens", uniqueConstraints = @UniqueConstraint(columnNames = {"dataHora", "codigoInternoMaquina", "tipo"}))
@DiscriminatorColumn(name="tipo", discriminatorType =DiscriminatorType.STRING)
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Mensagem implements Serializable {


    @Version
    private Long version;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long pk;

    protected LocalDateTime dataHora;
    @Embedded
    protected CodigoInternoMaquina codigoInternoMaquina;
    @Embedded
    protected IdentificadorOrdemProducao identificadorOrdemProducao;
    @Embedded
    protected LinhaProducaoIdentificador linhaProducaoIdentificador;


    public CodigoInternoMaquina codigoInternoMaquina(){
        return this.codigoInternoMaquina;
    };

    public IdentificadorOrdemProducao identificadorOrdemProducao(){
        return this.identificadorOrdemProducao;
    };

    public LinhaProducaoIdentificador linhaProducaoIdentificador(){
        return this.linhaProducaoIdentificador;
    };

    public LocalDateTime dataHora(){
        return this.dataHora;
    };

    public Long identity(){
        return this.pk;
    }


    protected Mensagem() {
    }

}
