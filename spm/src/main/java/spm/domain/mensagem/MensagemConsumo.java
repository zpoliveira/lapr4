package spm.domain.mensagem;

import chaofabrica.domain.model.deposito.CodigoAlfanumerico;
import chaofabrica.domain.model.linhaproducao.LinhaProducaoIdentificador;
import chaofabrica.domain.model.maquina.CodigoInternoMaquina;
import eapli.framework.validations.Preconditions;
import producao.domain.model.Quantidade;
import producao.domain.model.materiaPrima.CodigoInternoMateriaPrima;
import producao.domain.model.ordemProducao.IdentificadorOrdemProducao;
import producao.domain.model.produto.CodigoFabricoProduto;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
//@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"dataHora", "codigoInternoMaquina"}))
public class MensagemConsumo extends Mensagem implements Serializable {

    private static final long serialVersionUID = 1L;

    @Version
    private Long version;

    @Embedded
    private CodigoInternoMateriaPrima condigoInternoMateriaPrima;
    @Embedded
    private CodigoAlfanumerico codigoAlfanumerico;
    @Embedded
    private Quantidade quantidade;

    public MensagemConsumo() {
    }

    public MensagemConsumo(LocalDateTime dataHora, CodigoInternoMaquina codigoInternoMaquina,
                           IdentificadorOrdemProducao identificadorOrdemProducao,
                           LinhaProducaoIdentificador linhaProducaoIdentificador,
                           CodigoInternoMateriaPrima condigoInternoMateriaPrima,
                           CodigoAlfanumerico codigoAlfanumerico,
                           Quantidade quantidade) {
        Preconditions.noneNull(dataHora,codigoInternoMaquina,identificadorOrdemProducao,linhaProducaoIdentificador,
                condigoInternoMateriaPrima,codigoAlfanumerico,quantidade);
        this.dataHora = dataHora;
        this.codigoInternoMaquina = codigoInternoMaquina;
        this.identificadorOrdemProducao = identificadorOrdemProducao;
        this.linhaProducaoIdentificador = linhaProducaoIdentificador;
        this.condigoInternoMateriaPrima = condigoInternoMateriaPrima;
        this.codigoAlfanumerico = codigoAlfanumerico;
        this.quantidade = quantidade;
    }


    public CodigoAlfanumerico codigoAlfanumerico(){
        return this.codigoAlfanumerico;
    }
    public CodigoInternoMateriaPrima idMatPrima(){
        return this.condigoInternoMateriaPrima;
    }

    public Long quantidadeConsumo(){
        return this.quantidade.valorNumerico();
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MensagemConsumo that = (MensagemConsumo) o;
        return dataHora.equals(that.dataHora) &&
                codigoInternoMaquina.equals(that.codigoInternoMaquina) &&
                identificadorOrdemProducao.equals(that.identificadorOrdemProducao) &&
                linhaProducaoIdentificador.equals(that.linhaProducaoIdentificador) &&
                condigoInternoMateriaPrima.equals(that.condigoInternoMateriaPrima) &&
                codigoAlfanumerico.equals(that.codigoAlfanumerico) &&
                quantidade.equals(that.quantidade);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dataHora, codigoInternoMaquina, identificadorOrdemProducao, linhaProducaoIdentificador, condigoInternoMateriaPrima, codigoAlfanumerico, quantidade);
    }

    @Override
    public String toString() {
        return "MensagemConsumo{" +
                "dataHora=" + dataHora +
                ", codigoInternoMaquina=" + codigoInternoMaquina +
                ", identificadorOrdemProducao=" + identificadorOrdemProducao +
                ", linhaProducaoIdentificador=" + linhaProducaoIdentificador +
                ", condigoInternoMateriaPrima=" + condigoInternoMateriaPrima +
                ", codigoAlfanumerico=" + codigoAlfanumerico +
                ", quantidade=" + quantidade +
                '}';
    }
}
