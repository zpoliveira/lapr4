package spm.domain.mensagem;

import chaofabrica.domain.model.deposito.CodigoAlfanumerico;
import chaofabrica.domain.model.linhaproducao.LinhaProducaoIdentificador;
import chaofabrica.domain.model.maquina.CodigoInternoMaquina;
import eapli.framework.validations.Preconditions;
import producao.domain.model.Quantidade;
import producao.domain.model.ordemProducao.IdentificadorOrdemProducao;
import producao.domain.model.produto.CodigoFabricoProduto;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
//@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"dataHora", "codigoInternoMaquina"}))
public class MensagemProducao extends Mensagem implements Serializable {
    private static final long serialVersionUID = 1L;
    @Version
    private Long version;


    @Embedded
    private CodigoFabricoProduto codigoFabricoProduto;
    @Embedded
    private LoteId loteId;
    @Embedded
    private Quantidade quantidade;

    protected MensagemProducao() {
    }

    public MensagemProducao(
            LocalDateTime dataHora,
            CodigoInternoMaquina codigoInternoMaquina,
            IdentificadorOrdemProducao identificadorOrdemProducao,
            LinhaProducaoIdentificador linhaProducaoIdentificador,
            CodigoFabricoProduto codigoFabricoProduto,
            Quantidade quantidade,
            LoteId loteId) {
        Preconditions.noneNull(dataHora, codigoInternoMaquina, identificadorOrdemProducao,
                linhaProducaoIdentificador, codigoFabricoProduto, loteId, quantidade);
        this.dataHora = dataHora;
        this.codigoInternoMaquina = codigoInternoMaquina;
        this.identificadorOrdemProducao = identificadorOrdemProducao;
        this.linhaProducaoIdentificador = linhaProducaoIdentificador;
        this.codigoFabricoProduto = codigoFabricoProduto;
        this.loteId = loteId;
        this.quantidade = quantidade;
    }

    public MensagemProducao(
            LocalDateTime dataHora,
            CodigoInternoMaquina codigoInternoMaquina,
            IdentificadorOrdemProducao identificadorOrdemProducao,
            LinhaProducaoIdentificador linhaProducaoIdentificador,
            CodigoFabricoProduto codigoFabricoProduto,
            Quantidade quantidade) {
        Preconditions.noneNull(dataHora, codigoInternoMaquina, identificadorOrdemProducao,
                linhaProducaoIdentificador, codigoFabricoProduto, quantidade);
        this.dataHora = dataHora;
        this.codigoInternoMaquina = codigoInternoMaquina;
        this.identificadorOrdemProducao = identificadorOrdemProducao;
        this.linhaProducaoIdentificador = linhaProducaoIdentificador;
        this.codigoFabricoProduto = codigoFabricoProduto;
        this.quantidade = quantidade;
    }

    public LoteId loteId() {
        return this.loteId;
    }

    public Quantidade quantidade() {
        return this.quantidade;
    }

    public CodigoFabricoProduto codigoFabricoProduto() {
        return codigoFabricoProduto;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MensagemProducao that = (MensagemProducao) o;
        return dataHora.equals(that.dataHora) &&
                codigoInternoMaquina.equals(that.codigoInternoMaquina) &&
                identificadorOrdemProducao.equals(that.identificadorOrdemProducao) &&
                linhaProducaoIdentificador.equals(that.linhaProducaoIdentificador) &&
                codigoFabricoProduto.equals(that.codigoFabricoProduto) &&
                loteId.equals(that.loteId) &&
                quantidade.equals(that.quantidade);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dataHora, codigoInternoMaquina, identificadorOrdemProducao, linhaProducaoIdentificador,
                codigoFabricoProduto, loteId, quantidade);
    }

    @Override
    public String toString() {
        return "MensagemProducao{" +
                "dataHora=" + dataHora +
                ", codigoInternoMaquina=" + codigoInternoMaquina +
                ", identificadorOrdemProducao=" + identificadorOrdemProducao +
                ", linhaProducaoIdentificador=" + linhaProducaoIdentificador +
                ", codigoFabricoProduto=" + codigoFabricoProduto +
                ", loteId=" + loteId +
                ", quantidade=" + quantidade +
                '}';
    }
}
