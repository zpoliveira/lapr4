package spm.domain.mensagem;

import chaofabrica.domain.model.linhaproducao.LinhaProducaoIdentificador;
import chaofabrica.domain.model.maquina.CodigoInternoMaquina;
import eapli.framework.validations.Preconditions;
import producao.domain.model.ordemProducao.IdentificadorOrdemProducao;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
//@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"dataHora", "codigoInternoMaquina"}))
public class MensagemFimAtividade extends Mensagem implements Serializable {

    private static final long serialVersionUID = 1L;

    @Version
    private Long version;



    public MensagemFimAtividade() {
    }

    public MensagemFimAtividade(LocalDateTime dataHora, CodigoInternoMaquina codigoInternoMaquina,
                                IdentificadorOrdemProducao identificadorOrdemProducao,
                                LinhaProducaoIdentificador linhaProducaoIdentificador) {
        Preconditions.noneNull(dataHora,codigoInternoMaquina,identificadorOrdemProducao,linhaProducaoIdentificador);
        this.dataHora = dataHora;
        this.codigoInternoMaquina = codigoInternoMaquina;
        this.identificadorOrdemProducao = identificadorOrdemProducao;
        this.linhaProducaoIdentificador = linhaProducaoIdentificador;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MensagemFimAtividade that = (MensagemFimAtividade) o;
        return dataHora.equals(that.dataHora) &&
                codigoInternoMaquina.equals(that.codigoInternoMaquina) &&
                identificadorOrdemProducao.equals(that.identificadorOrdemProducao) &&
                linhaProducaoIdentificador.equals(that.linhaProducaoIdentificador);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dataHora, codigoInternoMaquina, identificadorOrdemProducao, linhaProducaoIdentificador);
    }

    @Override
    public String toString() {
        return "MensagemFimAtividade{" +
                "dataHora=" + dataHora +
                ", codigoInternoMaquina=" + codigoInternoMaquina +
                ", identificadorOrdemProducao=" + identificadorOrdemProducao +
                ", linhaProducaoIdentificador=" + linhaProducaoIdentificador +
                '}';
    }
}
