package spm.domain.mensagem;

import chaofabrica.domain.model.deposito.CodigoAlfanumerico;
import chaofabrica.domain.model.linhaproducao.LinhaProducaoIdentificador;
import chaofabrica.domain.model.maquina.CodigoInternoMaquina;
import eapli.framework.validations.Preconditions;
import producao.domain.model.Quantidade;
import producao.domain.model.ordemProducao.IdentificadorOrdemProducao;
import producao.domain.model.produto.CodigoFabricoProduto;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
//@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"dataHora", "codigoInternoMaquina"}))
public class MensagemEntregaProducao extends Mensagem implements  Serializable {

    private static final long serialVersionUID = 1L;

    @Version
    private Long version;


    @Embedded
    private CodigoFabricoProduto codigoFabricoProduto;
    @Embedded
    private CodigoAlfanumerico codigoAlfanumerico;
    @Embedded
    private LoteId loteId;
    @Embedded
    private Quantidade quantidade;


    public MensagemEntregaProducao() {
    }

    public MensagemEntregaProducao(LocalDateTime dataHora, CodigoInternoMaquina codigoInternoMaquina,
                                   IdentificadorOrdemProducao identificadorOrdemProducao,
                                   LinhaProducaoIdentificador linhaProducaoIdentificador,
                                   CodigoFabricoProduto codigoFabricoProduto,
                                   CodigoAlfanumerico codigoAlfanumerico,
                                   Quantidade quantidade, LoteId loteId) {
        Preconditions.noneNull(dataHora,codigoInternoMaquina,identificadorOrdemProducao,linhaProducaoIdentificador,
                codigoFabricoProduto,codigoAlfanumerico,loteId,quantidade);
        this.dataHora = dataHora;
        this.codigoInternoMaquina = codigoInternoMaquina;
        this.identificadorOrdemProducao = identificadorOrdemProducao;
        this.linhaProducaoIdentificador = linhaProducaoIdentificador;
        this.codigoFabricoProduto = codigoFabricoProduto;
        this.codigoAlfanumerico = codigoAlfanumerico;
        this.loteId = loteId;
        this.quantidade = quantidade;
    }

    public MensagemEntregaProducao(LocalDateTime dataHora, CodigoInternoMaquina codigoInternoMaquina,
                                   IdentificadorOrdemProducao identificadorOrdemProducao,
                                   LinhaProducaoIdentificador linhaProducaoIdentificador,
                                   CodigoFabricoProduto codigoFabricoProduto,
                                   CodigoAlfanumerico codigoAlfanumerico,
                                   Quantidade quantidade) {
        Preconditions.noneNull(dataHora,codigoInternoMaquina,identificadorOrdemProducao,linhaProducaoIdentificador,
                codigoFabricoProduto,codigoAlfanumerico,quantidade);
        this.dataHora = dataHora;
        this.codigoInternoMaquina = codigoInternoMaquina;
        this.identificadorOrdemProducao = identificadorOrdemProducao;
        this.linhaProducaoIdentificador = linhaProducaoIdentificador;
        this.codigoFabricoProduto = codigoFabricoProduto;
        this.codigoAlfanumerico = codigoAlfanumerico;
        this.quantidade = quantidade;
    }

        public CodigoFabricoProduto codigoFabricoProduto(){
        return this.codigoFabricoProduto;
        }

        public CodigoAlfanumerico codigoAlfanumerico(){
        return this.codigoAlfanumerico;
        }

        public Quantidade quantidade(){
        return this.quantidade;
        }

        public LoteId loteId(){
        return this.loteId;
        }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MensagemEntregaProducao that = (MensagemEntregaProducao) o;
        return dataHora.equals(that.dataHora) &&
                codigoInternoMaquina.equals(that.codigoInternoMaquina) &&
                identificadorOrdemProducao.equals(that.identificadorOrdemProducao) &&
                linhaProducaoIdentificador.equals(that.linhaProducaoIdentificador) &&
                codigoFabricoProduto.equals(that.codigoFabricoProduto) &&
                codigoAlfanumerico.equals(that.codigoAlfanumerico) &&
                loteId.equals(that.loteId) &&
                quantidade.equals(that.quantidade);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dataHora, codigoInternoMaquina, identificadorOrdemProducao,
                linhaProducaoIdentificador, codigoFabricoProduto, codigoAlfanumerico, loteId, quantidade);
    }

    @Override
    public String toString() {
        return "MensagemEntregaProducao{" +
                "dataHora=" + dataHora +
                ", codigoInternoMaquina=" + codigoInternoMaquina +
                ", identificadorOrdemProducao=" + identificadorOrdemProducao +
                ", linhaProducaoIdentificador=" + linhaProducaoIdentificador +
                ", codigoFabricoProduto=" + codigoFabricoProduto +
                ", codigoAlfanumerico=" + codigoAlfanumerico +
                ", loteId=" + loteId +
                ", quantidade=" + quantidade +
                '}';
    }
}
