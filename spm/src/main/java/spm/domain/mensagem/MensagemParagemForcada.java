package spm.domain.mensagem;

import chaofabrica.domain.model.linhaproducao.LinhaProducaoIdentificador;
import chaofabrica.domain.model.maquina.CodigoInternoMaquina;
import eapli.framework.validations.Preconditions;
import producao.domain.model.ordemProducao.IdentificadorOrdemProducao;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;
@Entity
//@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"dataHora", "codigoInternoMaquina"}))
public class MensagemParagemForcada extends Mensagem implements Serializable {

    private static final long serialVersionUID = 1L;

    @Version
    private Long version;



    @Embedded
    private CodigoParagem codigoParagem;

    public MensagemParagemForcada() {
    }

    public MensagemParagemForcada(LocalDateTime dataHora, CodigoInternoMaquina codigoInternoMaquina,
                                  IdentificadorOrdemProducao identificadorOrdemProducao,
                                  LinhaProducaoIdentificador linhaProducaoIdentificador, CodigoParagem codigoParagem) {
        Preconditions.noneNull(dataHora,codigoInternoMaquina,identificadorOrdemProducao, linhaProducaoIdentificador,
                codigoParagem);
        this.dataHora = dataHora;
        this.codigoInternoMaquina = codigoInternoMaquina;
        this.identificadorOrdemProducao = identificadorOrdemProducao;
        this.linhaProducaoIdentificador = linhaProducaoIdentificador;
        this.codigoParagem = codigoParagem;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MensagemParagemForcada that = (MensagemParagemForcada) o;
        return dataHora.equals(that.dataHora) &&
                codigoInternoMaquina.equals(that.codigoInternoMaquina) &&
                identificadorOrdemProducao.equals(that.identificadorOrdemProducao) &&
                linhaProducaoIdentificador.equals(that.linhaProducaoIdentificador) &&
                codigoParagem.equals(that.codigoParagem);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dataHora, codigoInternoMaquina, identificadorOrdemProducao,
                linhaProducaoIdentificador, codigoParagem);
    }

    @Override
    public String toString() {
        return "MensagemParagemForcada{" +
                "dataHora=" + dataHora +
                ", codigoInternoMaquina=" + codigoInternoMaquina +
                ", identificadorOrdemProducao=" + identificadorOrdemProducao +
                ", linhaProducaoIdentificador=" + linhaProducaoIdentificador +
                ", codigoParagem=" + codigoParagem +
                '}';
    }
}
