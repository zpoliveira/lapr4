package spm.domain.mensagem;

import eapli.framework.domain.model.ValueObject;
import eapli.framework.validations.Preconditions;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Embeddable;
import java.util.Objects;

@Embeddable
@Access(AccessType.FIELD)
public class LoteId implements ValueObject {
    private String LoteId;

    protected LoteId() {
    }

    public LoteId(String loteId) {
        Preconditions.nonEmpty(loteId);
        Preconditions.nonNull(loteId);
        LoteId = loteId;
    }

    public String lote(){
        return LoteId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LoteId loteId = (LoteId) o;
        return LoteId.equals(loteId.LoteId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(LoteId);
    }

    @Override
    public String toString() {
        return "Lote ID: " + this.LoteId;
    }
}
