package spm.domain.mensagem;

import chaofabrica.domain.model.linhaproducao.LinhaProducaoIdentificador;
import chaofabrica.domain.model.maquina.CodigoInternoMaquina;
import eapli.framework.validations.Preconditions;
import producao.domain.model.ordemProducao.IdentificadorOrdemProducao;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
@Entity
//@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"dataHora", "codigoInternoMaquina"}))
public class MensagemRetomaAtividade extends Mensagem implements Serializable {

    private static final long serialVersionUID = 1L;
    @Version
    private Long version;


    public MensagemRetomaAtividade() {
    }

    public MensagemRetomaAtividade(LocalDateTime dataHora, CodigoInternoMaquina codigoInternoMaquina,
                                   IdentificadorOrdemProducao identificadorOrdemProducao,
                                   LinhaProducaoIdentificador linhaProducaoIdentificador) {
        Preconditions.noneNull(dataHora, codigoInternoMaquina,identificadorOrdemProducao,linhaProducaoIdentificador);
        this.dataHora = dataHora;
        this.codigoInternoMaquina = codigoInternoMaquina;
        this.identificadorOrdemProducao = identificadorOrdemProducao;
        this.linhaProducaoIdentificador = linhaProducaoIdentificador;
    }

}
