package spm.domain.mensagem;

import eapli.framework.validations.Preconditions;

import javax.persistence.Embeddable;
import java.util.Objects;

@Embeddable
public class CodigoParagem {

    private String codigoParagem;

    public CodigoParagem() {
    }

    public CodigoParagem(String codigoParagem) {
        Preconditions.nonNull(codigoParagem, "codigo paragem null");
        Preconditions.nonEmpty(codigoParagem, "codigo paragem vazio");
        this.codigoParagem = codigoParagem;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CodigoParagem that = (CodigoParagem) o;
        return codigoParagem.equals(that.codigoParagem);
    }

    @Override
    public int hashCode() {
        return Objects.hash(codigoParagem);
    }

    @Override
    public String toString() {
        return "CodigoParagem{" +
                "codigoParagem='" + codigoParagem + '\'' +
                '}';
    }
}
