package spm.domain.mensagem;

import chaofabrica.domain.model.deposito.CodigoAlfanumerico;
import chaofabrica.domain.model.linhaproducao.LinhaProducaoIdentificador;
import chaofabrica.domain.model.maquina.CodigoInternoMaquina;
import eapli.framework.validations.Preconditions;
import producao.domain.model.Quantidade;
import producao.domain.model.materiaPrima.CodigoInternoMateriaPrima;
import producao.domain.model.ordemProducao.IdentificadorOrdemProducao;
import producao.domain.model.produto.CodigoFabricoProduto;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
//@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"dataHora", "codigoInternoMaquina"}))
public class MensagemEstorno extends Mensagem implements Serializable {

    private static final long serialVersionUID = 1L;

    @Version
    private Long version;



    @Embedded
    private CodigoInternoMateriaPrima codigoInternoMateriaPrima;
    @Embedded
    private CodigoAlfanumerico codigoAlfanumerico;
    @Embedded
    private Quantidade quantidade;

    public MensagemEstorno() {
    }

    public MensagemEstorno(LocalDateTime dataHora, CodigoInternoMaquina codigoInternoMaquina,
                           IdentificadorOrdemProducao identificadorOrdemProducao,
                           LinhaProducaoIdentificador linhaProducaoIdentificador,
                           CodigoInternoMateriaPrima codigoInternoMateriaPrima,
                           CodigoAlfanumerico codigoAlfanumerico,
                           Quantidade quantidade) {
        Preconditions.noneNull(dataHora,codigoInternoMaquina,identificadorOrdemProducao,linhaProducaoIdentificador,
                codigoInternoMateriaPrima,codigoAlfanumerico,quantidade);
        this.dataHora = dataHora;
        this.codigoInternoMaquina = codigoInternoMaquina;
        this.identificadorOrdemProducao = identificadorOrdemProducao;
        this.linhaProducaoIdentificador = linhaProducaoIdentificador;
        this.codigoInternoMateriaPrima = codigoInternoMateriaPrima;
        this.codigoAlfanumerico = codigoAlfanumerico;
        this.quantidade = quantidade;
    }

    public CodigoInternoMateriaPrima idMatPrima(){
        return this.codigoInternoMateriaPrima;
    }

    public Long quantidadeEstorno(){
        return this.quantidade.valorNumerico();
    }

    public CodigoAlfanumerico codigoAlfanumerico(){
        return this.codigoAlfanumerico;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MensagemEstorno that = (MensagemEstorno) o;
        return dataHora.equals(that.dataHora) &&
                codigoInternoMaquina.equals(that.codigoInternoMaquina) &&
                identificadorOrdemProducao.equals(that.identificadorOrdemProducao) &&
                linhaProducaoIdentificador.equals(that.linhaProducaoIdentificador) &&
                codigoInternoMateriaPrima.equals(that.codigoInternoMateriaPrima) &&
                codigoAlfanumerico.equals(that.codigoAlfanumerico) &&
                quantidade.equals(that.quantidade);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dataHora, codigoInternoMaquina, identificadorOrdemProducao,
                linhaProducaoIdentificador, codigoInternoMateriaPrima, codigoAlfanumerico, quantidade);
    }

    @Override
    public String toString() {
        return "MensagemEstorno{" +
                "dataHora=" + dataHora +
                ", codigoInternoMaquina=" + codigoInternoMaquina +
                ", identificadorOrdemProducao=" + identificadorOrdemProducao +
                ", linhaProducaoIdentificador=" + linhaProducaoIdentificador +
                ", codigoInternoMateriaPrima=" + codigoInternoMateriaPrima +
                ", codigoAlfanumerico=" + codigoAlfanumerico +
                ", quantidade=" + quantidade +
                '}';
    }
}
