package spm.application;

import chaofabrica.domain.model.linhaproducao.LinhaProducao;
import chaofabrica.domain.model.linhaproducao.SequenciaMaquina;
import chaofabrica.domain.repositories.LinhaProducaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


@Component
public class ProcessamentoController {

    @Autowired
    ProcessamentoService processamentoService;
    @Autowired
    EnriquecimentoService enriquecimentoService;
    @Autowired
    LinhaProducaoRepository linhaProducaoRepository;
    @Autowired
    ProcessamentoRecorrenteService processamentoRecorrenteService;


    public void processarMensagensIntervaloPorLinha(List<String> idsLinhas, LocalDateTime start, LocalDateTime end) {
        idsLinhas.forEach((idLinha -> {
            LinhaProducao linhaProducao = this.linhaProducaoRepository.findByCodigo_linhaProducaoID(idLinha);
            List<SequenciaMaquina> sequenciaMaquinaList = linhaProducao.listaMaquinas();
            List<String> idsMaquinas = new ArrayList<>();
            sequenciaMaquinaList.forEach((sequenciaMaquina -> {
                idsMaquinas.add(sequenciaMaquina.maquina().identity().codigoInternoMaquina());
            }));
            processamentoService.processarMensagens(linhaProducao, start, end, idsMaquinas);
        }));
    }

    public void processarRecorrentemente(String idLinha){
        LinhaProducao linhaProducao = this.linhaProducaoRepository.findByCodigo_linhaProducaoID(idLinha);
        if(linhaProducao.estadoRecorrencia() == true){
            this.processamentoRecorrenteService.activarRecorrencia(linhaProducao);
        }else {
            this.processamentoRecorrenteService.desativarRecorrencia(linhaProducao.identity().linhaProducaoID());
        }

    }
}

