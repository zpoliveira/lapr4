package spm.application;

import chaofabrica.domain.model.deposito.CodigoAlfanumerico;
import chaofabrica.domain.model.linhaproducao.LinhaProducaoIdentificador;
import chaofabrica.domain.model.maquina.CodigoInternoMaquina;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import producao.domain.model.Quantidade;
import producao.domain.model.Unidade;
import producao.domain.model.materiaPrima.CodigoInternoMateriaPrima;
import producao.domain.model.ordemProducao.IdentificadorOrdemProducao;
import producao.domain.model.ordemProducao.OrdemProducao;
import producao.domain.model.produto.CodigoFabricoProduto;
import producao.domain.repositories.OrdemProducaoRepository;
import scm.domain.MensagemBruta;
import spm.domain.mensagem.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class MensagemFactory {

    @Autowired
    OrdemProducaoRepository ordemProducaoRepository;

/*  S0 -> Inicio de Atividade
    C0 -> Consumo
    C9 -> Entrega de Produção
    P1 -> Produção
    P2 -> Estorno
    S1 -> Retoma de Atividade
    S8 -> Paragem
    S9 -> Fim de Atividade*/


    private CodigoAlfanumerico codigoAlfanumerico;
    private Quantidade quantidade;
    private CodigoInternoMateriaPrima codigoInternoMateriaPrima;
    private CodigoFabricoProduto codigoFabricoProduto;
    private LoteId loteId;

    public Mensagem gerarMensagemEnriquecida(
            MensagemBruta mensagemBruta,
            LinhaProducaoIdentificador linhaProducaoIdentificador,
            IdentificadorOrdemProducao identificadorOrdemProducao) {

        CodigoInternoMaquina codInterMaquina = new CodigoInternoMaquina(mensagemBruta.id());
        List<String> outraInfo = this.gerarOutraInfo(mensagemBruta.outraInfo());
        OrdemProducao ordemProducao = this.ordemProducaoRepository.findByIdentificadorOrdemProducao(identificadorOrdemProducao);
        switch (mensagemBruta.tipo()) {
            case "S0":
               Mensagem mensagemInicio =
                       new MensagemInicioAtividade(
                               mensagemBruta.data(),
                               codInterMaquina,
                               identificadorOrdemProducao,
                               linhaProducaoIdentificador);
                ordemProducao.mudaEstadoParaExecucao();
                this.ordemProducaoRepository.save(ordemProducao);
               return mensagemInicio;
            case "C0":
                this.codigoInternoMateriaPrima = new CodigoInternoMateriaPrima(outraInfo.get(0));
                this.codigoAlfanumerico = new CodigoAlfanumerico(outraInfo.get(2));
                this.quantidade = new Quantidade(Long.parseLong(outraInfo.get(1)), Unidade.UN);
                Mensagem mensagemConsumo = new MensagemConsumo(
                        mensagemBruta.data(),
                        codInterMaquina,
                        identificadorOrdemProducao,
                        linhaProducaoIdentificador,
                        codigoInternoMateriaPrima,
                        codigoAlfanumerico,
                        quantidade);
                return mensagemConsumo;
            case "C9":
                this.codigoFabricoProduto = new CodigoFabricoProduto(outraInfo.get(0));
                this.codigoAlfanumerico = new CodigoAlfanumerico(outraInfo.get(2));
                this.quantidade = new Quantidade(Long.parseLong(outraInfo.get(1)), Unidade.UN);
                Mensagem mensagemEntragaProducao = new MensagemEntregaProducao(mensagemBruta.data(),
                        codInterMaquina,
                        identificadorOrdemProducao,
                        linhaProducaoIdentificador,
                        codigoFabricoProduto,
                        codigoAlfanumerico,
                        quantidade);
                return mensagemEntragaProducao;
            case "P1":
                this.codigoFabricoProduto = new CodigoFabricoProduto(outraInfo.get(0));
                this.quantidade = new Quantidade(Long.parseLong(outraInfo.get(1)), Unidade.UN);
                this.loteId = new LoteId(outraInfo.get(2));
                Mensagem mensagemProducao = new MensagemProducao(
                        mensagemBruta.data(),
                        codInterMaquina,
                        identificadorOrdemProducao,
                        linhaProducaoIdentificador,
                        codigoFabricoProduto,
                        quantidade,
                        loteId);
                return mensagemProducao;
            case "P2":
                this.codigoInternoMateriaPrima = new CodigoInternoMateriaPrima(outraInfo.get(0));
                this.codigoAlfanumerico = new CodigoAlfanumerico(outraInfo.get(2));
                this.quantidade = new Quantidade(Long.parseLong(outraInfo.get(1)), Unidade.UN);
                Mensagem mensagemEstorno = new MensagemEstorno(mensagemBruta.data(),
                        codInterMaquina,
                        identificadorOrdemProducao,
                        linhaProducaoIdentificador,
                        codigoInternoMateriaPrima,
                        codigoAlfanumerico,
                        quantidade);
                return mensagemEstorno;
            case "S1":
                Mensagem mensagemRetomaAtividade = new MensagemRetomaAtividade(mensagemBruta.data(),
                        codInterMaquina,
                        identificadorOrdemProducao,
                        linhaProducaoIdentificador);
                ordemProducao.mudaEstadoParaExecucao();
                this.ordemProducaoRepository.save(ordemProducao);
                return mensagemRetomaAtividade;
            case "S8":
                Mensagem mensagemParagem = new MensagemParagemForcada(mensagemBruta.data(),
                        codInterMaquina,
                        identificadorOrdemProducao,
                        linhaProducaoIdentificador,
                        new CodigoParagem(mensagemBruta.outraInfo()));
                ordemProducao.mudaEstadoParaInterrompida();
                this.ordemProducaoRepository.save(ordemProducao);
                return mensagemParagem;
            case "S9":
                Mensagem mensagemFimAtividade = new MensagemFimAtividade(mensagemBruta.data(),
                        codInterMaquina,
                        identificadorOrdemProducao,
                        linhaProducaoIdentificador);
                ordemProducao.mudaEstadoParaConcluida();
                this.ordemProducaoRepository.save(ordemProducao);
                return mensagemFimAtividade;

        }
        return null;
    }

    /**
     * outra info encontra-se no formato csv com :
     * ### Estrutura das Mensagens (Formato normalizado): ###
     *
     * C0 -> Produto;Quantidade;Depósito
     * C9 -> Produto;Quantidade;Depósito
     * P1 -> Produto;Quantidade;Lote
     * P2 -> Produto;Quantidade;Depósito
     *
     * S0 -> OrdemProducao
     * S1 ->
     * S8 -> Erro
     * S9 -> OrdemProducao
     *
     */
    private List<String> gerarOutraInfo(String outraInfo){
        List<String> infoSeparada = new ArrayList<>();
        outraInfo = outraInfo.trim();
        if(outraInfo.length() > 0 && outraInfo.contains(";")){
            infoSeparada = Arrays.asList(outraInfo.split(";"));
        }else if(outraInfo.length()>0 && !outraInfo.contains(";")){
            infoSeparada.add(outraInfo.trim());
        }
        return infoSeparada;
    }
}
