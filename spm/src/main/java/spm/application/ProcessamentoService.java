package spm.application;

import chaofabrica.domain.model.linhaproducao.LinhaProducao;
import chaofabrica.domain.model.maquina.CodigoInternoMaquina;
import chaofabrica.domain.model.notificacao.Notificacao;
import chaofabrica.domain.model.notificacao.TipoErro;
import chaofabrica.domain.repositories.NotificacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import producao.domain.model.ordemProducao.EstadoOrdemProducao;
import producao.domain.model.ordemProducao.IdentificadorOrdemProducao;
import producao.domain.repositories.OrdemProducaoRepository;
import producao.domain.repositories.ProdutoRepository;
import scm.repository.MensagemBrutaRepository;
import spm.domain.execucaoOrdemProducao.*;
import spm.domain.mensagem.Mensagem;
import spm.repository.ExecucaoOrdemProducaoRepository;
import spm.repository.MensagemRepository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Component
public class ProcessamentoService {

    @Autowired
    MensagemBrutaRepository mensagemBrutaRepository;
    @Autowired
    OrdemProducaoRepository ordemProducaoRepository;
    @Autowired
    MensagemFactory mensagemFactory;
    @Autowired
    MensagemRepository mensagemRepository;
    @Autowired
    ProdutoRepository produtoRepository;
    @Autowired
    ExecucaoOrdemProducaoRepository execucaoOrdemProducaoRepository;
    @Autowired
    EnriquecimentoService enriquecimentoService;
    @Autowired
    NotificacaoRepository notificacaoRepository;

    private String idOrdemProducao = "";

    private List<Mensagem> mensagensEnriquecidas = new ArrayList<>();

    @Async
    public void processarMensagens(LinhaProducao linhaProducao,
                                   LocalDateTime start,
                                   LocalDateTime end, List<String> idsMaquinas) {
        try {
            IdentificadorOrdemProducao identificadorOrdemProducao =
                    enriquecimentoService.gerarMensagensEnriquecidas(linhaProducao, start, end, idsMaquinas);
            this.processarMensagensEnriquecidas(identificadorOrdemProducao, idsMaquinas);
        } catch (IllegalArgumentException e) {
            Notificacao notificacao = new Notificacao(TipoErro.ERRO_ENRIQUECIMENTO, e.getMessage(), linhaProducao.identity().linhaProducaoID());
            notificacaoRepository.save(notificacao);
            System.out.println(e.getMessage() + " da linha produção " + linhaProducao.identity().linhaProducaoID());
        }

    }

    public void processarMensagensEnriquecidas(IdentificadorOrdemProducao identificadorOrdemProducao,
                                               List<String> idsMaquinas) throws IllegalArgumentException {
        TempoBrutoExecucao tempoBrutoExecucaoOrdemProducao = this.calcularTempoBrutoExecucao(identificadorOrdemProducao);

        TempoEfetivoExecucao tempoEfetivoExecucaoOrdemProducao =
                this.calcularTempoEfetivoExecucao(identificadorOrdemProducao, idsMaquinas);

        List<TempoExecucaoMaquina> tempoExecucaoMaquinas =
                this.calcularTempoExecucaoMaquinas(identificadorOrdemProducao, idsMaquinas);

        ConsumoRealMateriaPrima consumoRealMateriaPrima =
                new ConsumoRealMateriaPrima(mensagemRepository, identificadorOrdemProducao).calcularConsumoRealMateriaPrima();

        Estorno estorno =
                new Estorno(identificadorOrdemProducao, mensagemRepository).calcularEstorno();

        ProducaoReal producaoReal = new ProducaoReal(mensagemRepository, identificadorOrdemProducao);
        producaoReal.calcularProducaoPorDeposito();
        producaoReal.calcularProducaoPorLote();

        DesvioConsumoMateriaPrima desvioConsumoMateriaPrima =
                new DesvioConsumoMateriaPrima(identificadorOrdemProducao,
                consumoRealMateriaPrima, ordemProducaoRepository, produtoRepository);
        desvioConsumoMateriaPrima.calculaDesvioConsumoMateriaPrima();

        DesvioProducao desvioProducao =
                new DesvioProducao(identificadorOrdemProducao, producaoReal, ordemProducaoRepository);
        desvioProducao.calculaDesvioProducao();
        this.gerarExecucaoOrdemProducao(identificadorOrdemProducao,
                consumoRealMateriaPrima, desvioConsumoMateriaPrima, desvioProducao,
                estorno, producaoReal,tempoBrutoExecucaoOrdemProducao,
                tempoEfetivoExecucaoOrdemProducao, tempoExecucaoMaquinas);

    }

    private TempoBrutoExecucao calcularTempoBrutoExecucao(IdentificadorOrdemProducao identificadorOrdemProducao) {
        return new TempoBrutoExecucao(
                identificadorOrdemProducao,
                this.mensagemRepository).calcularTempoBrutoOrdemProducao();
    }

    private TempoEfetivoExecucao calcularTempoEfetivoExecucao(
            IdentificadorOrdemProducao identificadorOrdemProducao, List<String> idsMaquinas){
        return new TempoEfetivoExecucao(
                identificadorOrdemProducao,
                this.mensagemRepository).calcularTempoEfetivoOrdemProducao(idsMaquinas);
    }

    private List<TempoExecucaoMaquina> calcularTempoExecucaoMaquinas(
            IdentificadorOrdemProducao identificadorOrdemProducao, List<String> idsMaquinas){
        List<TempoExecucaoMaquina> tempoExecucaoMaquinas = new ArrayList<>();
        idsMaquinas.forEach((idMaquina) -> {
            CodigoInternoMaquina codigoInternoMaquina = new CodigoInternoMaquina(idMaquina);
            TempoBrutoExecucao tempoBrutoExecucaoMaquina =
                    new TempoBrutoExecucao(
                            identificadorOrdemProducao,
                            this.mensagemRepository).calcularTempoBrutoMaquina(codigoInternoMaquina);
            TempoEfetivoExecucao tempoEfetivoExecucaoMaquina =
                    new TempoEfetivoExecucao(identificadorOrdemProducao,
                            this.mensagemRepository).calcularTempoEfetivoMaquina(codigoInternoMaquina);
            if (tempoBrutoExecucaoMaquina != null && tempoEfetivoExecucaoMaquina != null) {
                TempoExecucaoMaquina tempoExecucaoMaquina =
                        new TempoExecucaoMaquina(tempoBrutoExecucaoMaquina, tempoEfetivoExecucaoMaquina, codigoInternoMaquina);
                tempoExecucaoMaquinas.add(tempoExecucaoMaquina);
            }
        });
        return tempoExecucaoMaquinas;
    }

    private void gerarExecucaoOrdemProducao(IdentificadorOrdemProducao identificadorOrdemProducao,
                                            ConsumoRealMateriaPrima consumoRealMateriaPrima,
                                            DesvioConsumoMateriaPrima desvioConsumoMateriaPrima,
                                            DesvioProducao desvioProducao,
                                            Estorno estorno, ProducaoReal producaoReal,
                                            TempoBrutoExecucao tempoBrutoExecucaoOrdemProducao,
                                            TempoEfetivoExecucao tempoEfetivoExecucaoOrdemProducao,
                                            List<TempoExecucaoMaquina> tempoExecucaoMaquinas){
        EstadoOrdemProducao estadoOrdemProducao =
                this.ordemProducaoRepository.findByIdentificadorOrdemProducao(identificadorOrdemProducao).estadoOrdemProducao();
        ExecucaoOrdemProducao execucaoOrdemProducao =
                this.execucaoOrdemProducaoRepository.findByIdentificadorOrdemProducao(identificadorOrdemProducao);
        if (execucaoOrdemProducao != null) {
            execucaoOrdemProducao.atualizarConsumoRealMateriaPrima(consumoRealMateriaPrima);
            execucaoOrdemProducao.atualizarDesvioConsumoMateriaPrima(desvioConsumoMateriaPrima);
            execucaoOrdemProducao.atualizarDesvioProducao(desvioProducao);
            execucaoOrdemProducao.atualizarEstorno(estorno);
            execucaoOrdemProducao.atualizarProducaoReal(producaoReal);
            execucaoOrdemProducao.atualizarTempoBrutoExecucao(tempoBrutoExecucaoOrdemProducao);
            execucaoOrdemProducao.atualizarTempoEfetivoExecucao(tempoEfetivoExecucaoOrdemProducao);
            execucaoOrdemProducao.atualizarTempoExecucaoMaquinas(tempoExecucaoMaquinas);
            execucaoOrdemProducao.atualizarEstadoOrdemProducao(estadoOrdemProducao);
        } else {
            execucaoOrdemProducao = new ExecucaoOrdemProducao(identificadorOrdemProducao, consumoRealMateriaPrima,
                    desvioConsumoMateriaPrima, desvioProducao, producaoReal, tempoExecucaoMaquinas, tempoEfetivoExecucaoOrdemProducao,
                    tempoBrutoExecucaoOrdemProducao, estorno, estadoOrdemProducao);
        }
        this.execucaoOrdemProducaoRepository.save(execucaoOrdemProducao);
    }
}
