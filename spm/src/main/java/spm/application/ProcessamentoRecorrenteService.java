package spm.application;

import chaofabrica.domain.model.linhaproducao.LinhaProducao;
import chaofabrica.domain.model.linhaproducao.SequenciaMaquina;
import chaofabrica.domain.repositories.LinhaProducaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
public class ProcessamentoRecorrenteService {
    @Autowired
    ProcessamentoService processamentoService;
    @Autowired
    LinhaProducaoRepository linhaProducaoRepository;

    private HashMap<String, ScheduledExecutorService> linhasRecorrencia = new HashMap<>();
    private ScheduledExecutorService exec = Executors.newScheduledThreadPool(1);


    public void activarRecorrencia(LinhaProducao linhaProducao){
        int intervalo = linhaProducao.mostraIntervaloProcessamento();
        List<SequenciaMaquina> sequenciaMaquinaList = linhaProducao.listaMaquinas();
        List<String> idsMaquinas = new ArrayList<>();
        sequenciaMaquinaList.forEach((sequenciaMaquina -> {
            idsMaquinas.add(sequenciaMaquina.maquina().identity().codigoInternoMaquina());
        }));
        Runnable processarLinhaRecorrente = new Runnable() {
            LocalDateTime start = linhaProducao.mostraInicioPeriodo();
            public void run() {
                LocalDateTime end = this.start.plusMinutes(intervalo);
                processamentoService.processarMensagens(linhaProducao, this.start, end, idsMaquinas);
                linhaProducao.mudaUltimoProcesamento(end);
                linhaProducaoRepository.save(linhaProducao);
                this.start = this.start.plusMinutes(intervalo);
            }
        };
        this.exec.scheduleAtFixedRate(processarLinhaRecorrente,intervalo, intervalo,TimeUnit.MINUTES);
        this.linhasRecorrencia.put(linhaProducao.identity().linhaProducaoID(), this.exec);
    }

    public void desativarRecorrencia(String idLinha){
        this.linhasRecorrencia.get(idLinha).shutdown();
    }

}
