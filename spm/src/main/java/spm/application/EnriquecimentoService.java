package spm.application;

import chaofabrica.domain.model.linhaproducao.LinhaProducao;
import chaofabrica.domain.model.maquina.CodigoInternoMaquina;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;
import producao.domain.model.ordemProducao.EstadoOrdemProducao;
import producao.domain.model.ordemProducao.IdentificadorOrdemProducao;
import producao.domain.model.ordemProducao.OrdemProducao;
import producao.domain.repositories.OrdemProducaoRepository;
import scm.domain.MensagemBruta;
import scm.repository.MensagemBrutaRepository;
import spm.domain.mensagem.Mensagem;
import spm.repository.ExecucaoOrdemProducaoRepository;
import spm.repository.MensagemRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.StreamSupport;

@Component
public class EnriquecimentoService {

    @Autowired
    MensagemBrutaRepository mensagemBrutaRepository;
    @Autowired
    MensagemFactory mensagemFactory;
    @Autowired
    MensagemRepository mensagemRepository;
    @Autowired
    ExecucaoOrdemProducaoRepository execucaoOrdemProducaoRepository;
    @Autowired
    OrdemProducaoRepository ordemProducaoRepository;

    private String idOrdemProducao = "";


    public IdentificadorOrdemProducao gerarMensagensEnriquecidas(LinhaProducao linhaProducao,
                                                                 LocalDateTime startDateTime,
                                                                 LocalDateTime endDateTime,
                                                                 List<String> idsMaquinas) throws IllegalArgumentException {
        Iterable<MensagemBruta> mensagensFiltradas =
                mensagemBrutaRepository.findAllByDataBetweenAndIdInOrderByDataAsc(startDateTime, endDateTime, idsMaquinas);
        if(StreamSupport.stream(mensagensFiltradas.spliterator(), false).count() == 0){
            throw new IllegalArgumentException("sem mensagens no intervalo definido");
        }
        return this.enriquecerMensagens(mensagensFiltradas, idsMaquinas, linhaProducao);
    }

    public IdentificadorOrdemProducao enriquecerMensagens(Iterable<MensagemBruta> mensagensFiltradas,
                                                          List<String> idsMaquinas,
                                                          LinhaProducao linhaProducao) throws IllegalArgumentException {
        IdentificadorOrdemProducao identificadorOrdemProducao = null;
        for (MensagemBruta mensagemFiltrada : mensagensFiltradas) {
            this.obterIdentificadorOrdemProduçao(mensagemFiltrada, idsMaquinas);
            identificadorOrdemProducao = new IdentificadorOrdemProducao(this.idOrdemProducao);
            if (!idOrdemProducao.equals("") && identificadorOrdemProducao != null) {
                Mensagem mensagem =
                        mensagemFactory.gerarMensagemEnriquecida(
                                mensagemFiltrada,
                                linhaProducao.identity(),
                                identificadorOrdemProducao);
                try{
                    this.mensagemRepository.save(mensagem);
                }catch (DataIntegrityViolationException e){
                    System.out.println(e.getMessage());
                }

            }else{
                throw new IllegalArgumentException("Id ordem de produção não pode ser null ou Nenhuma MSG foi encontrada");
            }
        }
        return identificadorOrdemProducao;
    }


    private void obterIdentificadorOrdemProduçao(MensagemBruta mensagemFiltrada, List<String> idsMaquinas) {
        if (mensagemFiltrada.tipo().equalsIgnoreCase("S0") && idsMaquinas.indexOf(mensagemFiltrada.id()) == 0) {
            if (mensagemFiltrada.outraInfo().contains(";")) {
                this.idOrdemProducao = mensagemFiltrada.outraInfo().split(";")[0];
            } else if (mensagemFiltrada.outraInfo().length() > 0 && !mensagemFiltrada.outraInfo().contains(";")) {
                this.idOrdemProducao = mensagemFiltrada.outraInfo();
            }
            if (this.idOrdemProducao == "") {
                Mensagem m = this.mensagemRepository.findFirstByTipoAndCodigoInternoMaquinaOrderByDataHoraDesc(
                        "MensagemInicioAtividade", new CodigoInternoMaquina(mensagemFiltrada.id()));
                OrdemProducao ordemProducao =
                        this.ordemProducaoRepository.findByIdentificadorOrdemProducao(m.identificadorOrdemProducao());
                if (EstadoOrdemProducao.CONCLUIDA.equals(ordemProducao.estadoOrdemProducao())) {
                    this.idOrdemProducao = "";
                } else {
                    this.idOrdemProducao = m.identificadorOrdemProducao().identificadorOrdemProducao();
                }
            }
        }
    }
}
