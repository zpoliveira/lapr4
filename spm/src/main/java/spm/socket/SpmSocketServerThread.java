package spm.socket;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import spm.application.ProcessamentoController;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class SpmSocketServerThread implements Runnable {
    private Thread thread;
    private String threadName;
    private Socket socket;
    private ProcessamentoController processamentoController;


    @Override
    public void run() {
        System.out.println("Running " + threadName);
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            String line = reader.readLine();
            if(line.contains(";")){
                String[] intervalo;
                intervalo = line.split(";");
                List<String> idsLinhas = new ArrayList<>();
                idsLinhas.add(intervalo[0]);
                LocalDateTime inicio = LocalDateTime.parse(intervalo[1]);
                LocalDateTime fim = LocalDateTime.parse(intervalo[2]);
                this.processamentoController.processarMensagensIntervaloPorLinha(idsLinhas,inicio,fim);
            }else{
                this.processamentoController.processarRecorrentemente(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            this.socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void start(String threadName, Socket socket, ProcessamentoController processamentoController) {
        this.threadName = threadName;
        this.socket = socket;
        this.processamentoController = processamentoController;
        if (this.thread == null) {
            this.thread = new Thread(this, threadName);
            this.thread.start();
        }
    }
}
