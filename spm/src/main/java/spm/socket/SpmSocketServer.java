package spm.socket;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import scm.application.socketserver.ScmSocketServerThread;
import spm.application.ProcessamentoController;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

@Component
public class SpmSocketServer {

    private static final int SERVER_PORT = 31403;
    @Autowired
    ProcessamentoController processamentoController;

    protected SpmSocketServer() {

    }

    @Async
    public void start(){
        try {
            ServerSocket serverSocket = new ServerSocket(SERVER_PORT);
            System.out.println("Server is listening on port " + SERVER_PORT);

            while (true) {
                Socket socket = serverSocket.accept();
                System.out.println("New client connected");
                new SpmSocketServerThread().start(
                        "spmServer",
                        socket,
                        this.processamentoController);
            }
        } catch (IOException e) {
            System.out.println("Server exception: " + e.getMessage());

        }
    }
}
