package spm.persistence;

import org.springframework.data.repository.Repository;
import producao.domain.model.ordemProducao.IdentificadorOrdemProducao;
import spm.domain.execucaoOrdemProducao.ExecucaoOrdemProducao;
import spm.repository.ExecucaoOrdemProducaoRepository;

public interface SpringDataExecucaoOrdemProducaoRepository extends ExecucaoOrdemProducaoRepository, Repository<ExecucaoOrdemProducao, Long> {
    @Override
    ExecucaoOrdemProducao save(ExecucaoOrdemProducao execucaoOrdemProducao);

    @Override
    boolean existsByIdentificadorOrdemProducao(IdentificadorOrdemProducao identificadorOrdemProducao);

    @Override
    ExecucaoOrdemProducao findByIdentificadorOrdemProducao(IdentificadorOrdemProducao identificadorOrdemProducao);

    @Override
    Iterable<ExecucaoOrdemProducao> findAll();
}
