package spm.persistence;

import chaofabrica.domain.model.maquina.CodigoInternoMaquina;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import producao.domain.model.ordemProducao.IdentificadorOrdemProducao;
import spm.domain.mensagem.Mensagem;
import spm.repository.MensagemRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface SpringDataMensagemRepository extends MensagemRepository, Repository<Mensagem, Long> {
    /*@Override
    Iterable<Mensagem> saveAll(List<Mensagem> mensagens);*/
    @Override
    @Modifying
    Mensagem save(Mensagem mensagem);


    @Override
    @Query("SELECT m FROM MensagemConsumo m WHERE m.identificadorOrdemProducao =:idOrdemProducao AND m.codigoAlfanumerico.codigoAlfaNumerico LIKE 'L0%'")
    Iterable<Mensagem> listaMensagemConsumoPorOP(@Param("idOrdemProducao") IdentificadorOrdemProducao idOP);

    @Override
    @Query("SELECT m FROM MensagemEstorno m WHERE m.identificadorOrdemProducao =:idOrdemProducao")
    Iterable<Mensagem> listaMensagemEstornoPorOP(@Param("idOrdemProducao") IdentificadorOrdemProducao idOP);

    @Override
    @Query("SELECT m FROM MensagemEntregaProducao m WHERE m.identificadorOrdemProducao =:idOrdemProducao")
    Iterable<Mensagem> listaMensagemEntregaProducaoPorOP(@Param("idOrdemProducao") IdentificadorOrdemProducao idOP);

    @Override
    @Query("SELECT m FROM MensagemProducao m WHERE m.identificadorOrdemProducao =:idOrdemProducao")
    Iterable<Mensagem> listaMensagemProducaoPorOP(@Param("idOrdemProducao") IdentificadorOrdemProducao idOP);

    @Override
    Iterable<Mensagem> findByCodigoInternoMaquina(CodigoInternoMaquina codigoInterno);

    @Override
    Mensagem findByPk(Long pk);

    @Override
    Iterable<Mensagem> findByDataHoraBetween(LocalDateTime inicio, LocalDateTime fim);

    @Override
    List<Mensagem> findByIdentificadorOrdemProducaoOrderByDataHoraAsc(IdentificadorOrdemProducao identificadorOrdemProducao);

    @Override
    @Query("select m from Mensagem m where tipo IN(:tipomsg) And m.identificadorOrdemProducao=:identificadorOrdemProducao")
    List<Mensagem> findByIdentificadorOrdemProducaoAndByTipoIn(
            @Param("identificadorOrdemProducao") IdentificadorOrdemProducao identificadorOrdemProducao,
            @Param("tipomsg") List<String> tipomsg);

    @Override
    List<Mensagem> findByIdentificadorOrdemProducaoAndCodigoInternoMaquina(
            IdentificadorOrdemProducao identificadorOrdemProducao,
            CodigoInternoMaquina codigoInternoMaquina);

    @Override
    @Query("select m from Mensagem m where tipo=:tipomsg And m.codigoInternoMaquina=:codigoInternoMaquina order by m.dataHora DESC")
    Mensagem findFirstByTipoAndCodigoInternoMaquinaOrderByDataHoraDesc(
            @Param("tipomsg") String tipo,
            @Param("codigoInternoMaquina") CodigoInternoMaquina codigoInternoMaquina);

}
