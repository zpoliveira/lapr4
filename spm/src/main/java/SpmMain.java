
import chaofabrica.domain.model.linhaproducao.LinhaProducao;
import chaofabrica.domain.repositories.LinhaProducaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import spm.application.ProcessamentoController;
import spm.application.ProcessamentoRecorrenteService;
import spm.presentation.console.InicialPrompt;
import spm.socket.SpmSocketServer;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.concurrent.Executor;


@SpringBootApplication
@EntityScan(basePackages = {"spm", "scm", "chaofabrica", "producao"})
@EnableJpaRepositories(basePackages = {"spm", "scm", "persistence"})
@ComponentScan(basePackages = {"spm", "scm", "chaofabrica", "producao"})
@EnableAsync
public class SpmMain implements CommandLineRunner {

    @Autowired
    InicialPrompt inicialPrompt;
    @Autowired
    ProcessamentoController processamentoController;
    @Autowired
    SpmSocketServer spmSocketServer;
    @Autowired
    LinhaProducaoRepository linhaProducaoRepository;
    @Autowired
    ProcessamentoRecorrenteService processamentoRecorrenteService;

    public static void main(final String[] args) {
        SpringApplication.run(SpmMain.class, args);

    }

    @Override
    public void run(final String... args) {
        List<LinhaProducao> linhasRecooretnteAtivo =
                this.linhaProducaoRepository.findByRecorrenciaProcessamento_estadoProcessamentoTrue();
        linhasRecooretnteAtivo.forEach(linhaRecAtivo->{
            this.processamentoRecorrenteService.activarRecorrencia(linhaRecAtivo);
        });
        spmSocketServer.start();
        //System.out.println("Saindo do SPM...");
        // System.exit(0);

    }

    @Bean
    public Executor taskExecuter() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(4);
        executor.setMaxPoolSize(4);
        executor.setQueueCapacity(500);
        executor.setThreadNamePrefix("spm-");
        executor.initialize();
        return executor;
    }


}
