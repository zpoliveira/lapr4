package utils.impl;

import java.util.Set;

public interface ImportadorFicheiro<T> {
    Set<T> importarDadosFicheiro();

    Set<String> getErros();
}
