package utils.impl;

import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Network {

    public static boolean isBroadcast(InetAddress inetAddress){
        if(inetAddress.getHostAddress().equalsIgnoreCase("255.255.255.255")){
            return true;
        }
        String ss = inetAddress.getHostAddress();

        Optional<InetAddress> address = getBroadcast(inetAddress);
        if (address.isPresent()){
            return address.get().equals(inetAddress);
        }
       return false;
    }

    public static Optional<InetAddress> getBroadcast(InetAddress inetAddress) {
        NetworkInterface nif = null;
        List<InterfaceAddress> list = new ArrayList<>();
        try {
            nif = NetworkInterface.getByInetAddress(inetAddress);
            list = nif.getInterfaceAddresses();
        } catch (Exception e) {
            list = new ArrayList<>();
        }
        return list.stream().filter(x-> x.getAddress().equals(inetAddress)).map(x->x.getBroadcast()).findFirst();
    }

}
