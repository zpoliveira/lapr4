package utils.impl;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Ficheiro {

    public static List<String> getLinhas(String path) {
        List<String> lines = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(Ficheiro.class.getClassLoader().getResourceAsStream(path)))) {
            while (reader.ready()) {
                lines.add(reader.readLine());
            }
        }catch (FileNotFoundException e) {

            e.printStackTrace();
        } catch (IOException e) {

            e.printStackTrace();
        }
        return lines;
    }
}
