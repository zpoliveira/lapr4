package utils.impl;

public enum ExportDataType {
    XML,
    JSON,
    HTML,
    PLAIN
}
