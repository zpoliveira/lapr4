function refreshLinhasProducao() {
    var request = new XMLHttpRequest();
    var vBoard = document.getElementById("linhasProducao");

    request.onload = function () {
        vBoard.innerHTML = this.responseText;
        vBoard.style.color = "black";
        setTimeout(refreshLinhasProducao, 2000);
    };

    request.ontimeout = function () {
        vBoard.innerHTML = "Server timeout, still trying ...";
        vBoard.style.color = "red";
        setTimeout(refreshLinhasProducao, 100);
    };

    request.onerror = function () {
        vBoard.innerHTML = "No server reply, still trying ...";
        vBoard.style.color = "red";
        setTimeout(refreshLinhasProducao, 5000);
    };

    request.open("GET", "/linhasProducao", true);
    request.timeout = 5000;
    request.send();
}

function resetFor(option) {
    var request = new XMLHttpRequest();
    request.open("POST", "/reset/" + option, true);
    request.send();
    var vBoard = document.getElementById("reset");
    vBoard.innerHTML =  "A enviar pedido de reset para a máquina "+ option +"... Please wait.";

}