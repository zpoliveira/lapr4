import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import smm.application.SharedData;
import smm.application.http.HttpServer;
import smm.application.udp.UdpStart;
import smm.application.udp.ValidacaoComunicacao;
import utils.impl.Ficheiro;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

@SpringBootApplication
@EntityScan(basePackages = {"smm"})
@EnableJpaRepositories(basePackages = {"smm"})
@ComponentScan(basePackages = {"smm"})
public class SmmMain implements CommandLineRunner {

    @Value("${udpSender.timer}")
    private long udpTimer;

    @Value("${porta}")
    private int porta;

    @Value("${ficheiro}")
    private String file;

    @Value("${maquinas.maxTempoSemAtualizacao}")
    private int maxTempoSemAtualizacao;

    public static void main(final String[] args) {
        SpringApplication.run(SmmMain.class, args);

    }

    @Override
    public void run(final String... args) throws Exception {


        //ler o ficheiro dos IPS
        List<String> ips = new ArrayList<>();
        try {
            ips = Ficheiro.getLinhas(file);
        } catch (Exception ex) {
            System.out.println("Erro ao ler o ficheiro de Ips.");
            ips = new ArrayList<>();
        }


        //Thread para UDP
        Timer increaserTimerUdpServer = new Timer("Sender");
        UdpStart udp = new UdpStart(ips, porta);
        //starts a timer 10ms later
        increaserTimerUdpServer.scheduleAtFixedRate(udp, 10, 1000 * udpTimer);

        //Tread para validação de falta de comunicação das máquinas.
        Timer increaserTimerValidacaoComunicacao = new Timer("Sender");
        ValidacaoComunicacao val = new ValidacaoComunicacao(maxTempoSemAtualizacao);
        //starts a timer 10ms later
        increaserTimerValidacaoComunicacao.scheduleAtFixedRate(val, 100, (1000 * maxTempoSemAtualizacao)/2);

        //Tread para HTTP
        HttpServer server = new HttpServer(porta);
        server.start();

    }

}
