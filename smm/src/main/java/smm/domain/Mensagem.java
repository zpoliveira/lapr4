package smm.domain;

import org.apache.commons.lang3.ArrayUtils;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Mensagem {

    private static final int MAX_LENGTH_RAWDATA = 65535;

    private static final int MAX_UDP = 512;

    private final byte version = 0;

    private TipoMensagem code;

    private Integer id;

    private byte rawData[];


    public Mensagem(TipoMensagem code, Integer idMaquina, String mensagem) {
        this.code = code;
        this.id = idMaquina;
        if (mensagem != null && !mensagem.isEmpty()) {
            this.rawData = new byte[]{};
        } else {
            this.rawData = mensagem.getBytes();
        }
    }

    public Mensagem(TipoMensagem code, Integer idMaquina) {
        this.code = code;
        this.id = idMaquina;
        this.rawData = new byte[]{};
    }

    public Mensagem(TipoMensagem code) {
        this.code = code;
        this.id = 0;
        this.rawData = new byte[]{};
    }

    public TipoMensagem tipoMensagem() {
        return this.code;
    }

    public Integer maquinaId() {
        return this.id;
    }

    public String innerMessage() {
        return new String(this.rawData, StandardCharsets.UTF_8);
    }


    public EstadoMaquina estadoMaquina() {
        return new EstadoMaquina(this.code,this.id, new String(this.rawData, 0, this.rawData.length));
    }

    public byte[] MensagemBytes() {
        List<Byte> finalMsg = new ArrayList<>();

        //Version
        finalMsg.add(this.version);

        //Codigo Mensagem
        finalMsg.add(this.code.getValue());

        // Coverter o nr da Máquina
        ByteBuffer nrMaq = ByteBuffer.allocate(2);
        short maq = (short) (this.id >> 16);
        nrMaq.putShort(maq);
        Collections.addAll(finalMsg, ArrayUtils.toObject(nrMaq.array()));

        //Tamanho da raw data
        ByteBuffer sizeRawData = ByteBuffer.allocate(2);
        short size = (short) (this.rawData.length >> 16);
        sizeRawData.putShort(size);
        Collections.addAll(finalMsg, ArrayUtils.toObject(sizeRawData.array()));

        //Raw data
        Collections.addAll(finalMsg, ArrayUtils.toObject(this.rawData));

        return ArrayUtils.toPrimitive(finalMsg.toArray(new Byte[0]));

    }

    public static Mensagem Converter(byte[] data, int lenght) {
        TipoMensagem tm = TipoMensagem.findBy(data[1]);

        //Nr Maquina
        byte[] arr = ArrayUtils.subarray(data, 2, 4);
        short num = (short) ((arr[1] & 0xFF) << 8 | (arr[0] & 0xFF));
        Integer nrMaq = Short.toUnsignedInt(num);

        // innerString
        arr = ArrayUtils.subarray(data, 4, 6);
        num = (short) ((arr[1] & 0xFF) << 8 | (arr[0] & 0xFF));
        Integer strSize = Short.toUnsignedInt(num);

        if (strSize < 0) {
            strSize = 0;
        }

        if (strSize > MAX_LENGTH_RAWDATA) {
            strSize = MAX_LENGTH_RAWDATA;
        }

        String ss = new String(data, 6, strSize);

        return new Mensagem(tm, nrMaq, ss);

    }

}
