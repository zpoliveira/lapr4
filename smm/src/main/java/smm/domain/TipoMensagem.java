package smm.domain;

public enum TipoMensagem {
    HELLO((byte)0),
    MSG((byte)1),
    CONFIG((byte)2),
    RESET((byte)3),
    ACK((byte)150),
    NACK((byte)151);

    private final byte value;
    private TipoMensagem(byte val){
        this.value = val;
    }

    public byte getValue(){
        return  this.value;
    }

    public static TipoMensagem findBy(byte key){
        for(TipoMensagem v : values()){
            if( v.value == (key)){
                return v;
            }
        }
        return null;
    }

}
