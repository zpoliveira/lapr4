package smm.domain;

import java.time.LocalDateTime;
import java.util.Objects;

public class EstadoMaquina {

    private int maquinaIdProtocolo;
    private LocalDateTime data;
    private String mensagem;
    private TipoMensagem tipoMensagem;

    public EstadoMaquina(TipoMensagem tm,int maquinaIdProtocolo, String mensagem) {
        this.maquinaIdProtocolo = maquinaIdProtocolo;
        this.data = LocalDateTime.now();
        this.tipoMensagem = tm;
        this.mensagem = mensagem;
    }

    public int maquinaIdProtocolo(){
        return  this.maquinaIdProtocolo;
    }

    public TipoMensagem tipoMensagem() {
        return this.tipoMensagem;
    }

    public String mensagem(){
        return this.mensagem;
    }

    public LocalDateTime getData() {
        return data;
    }

    public void setNACK(){
        this.tipoMensagem = TipoMensagem.NACK;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EstadoMaquina that = (EstadoMaquina) o;
        return maquinaIdProtocolo == that.maquinaIdProtocolo;
    }

    @Override
    public int hashCode() {
        return Objects.hash(maquinaIdProtocolo);
    }
}
