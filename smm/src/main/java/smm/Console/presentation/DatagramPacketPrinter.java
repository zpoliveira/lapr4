package smm.Console.presentation;

import eapli.framework.visitor.Visitor;

import java.net.DatagramPacket;


public class DatagramPacketPrinter implements Visitor<DatagramPacket> {

    @Override
    public void visit(DatagramPacket visitee) {
        System.out.printf("%-5s%s%-19s\n", "","ip:", visitee.getAddress().getHostAddress());
        System.out.printf("%-5s%s%-19s\n", "","port:", visitee.getPort());
    }
}
