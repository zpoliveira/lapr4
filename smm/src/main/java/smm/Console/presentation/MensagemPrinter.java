package smm.Console.presentation;

import eapli.framework.visitor.Visitor;
import smm.domain.Mensagem;

public class MensagemPrinter implements Visitor<Mensagem> {

    @Override
    public void visit(final Mensagem visitee) {
        System.out.printf("%-5s%s%-19s\n", "","id:", visitee.maquinaId());
        System.out.printf("%-5s%s%-19s\n", "","code:", visitee.tipoMensagem());
        System.out.printf("%-5s%s%-19s\n", "","rawData:", visitee.innerMessage());
    }
}
