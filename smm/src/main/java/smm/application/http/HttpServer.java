package smm.application.http;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class HttpServer extends Thread {
    static private final String BASE_FOLDER = "www";
    static private ServerSocket sock;
    private int porta;

    public HttpServer(int porta) {
        this.porta = porta;
    }

    @Override
    public void run() {
        Socket cliSock = null;
        try {
            sock = new ServerSocket(porta);
        } catch (IOException ex) {
            System.out.println("Server failed to open local port " + porta);
            System.exit(1);
        }
        System.out.println("Server ready, listening on port number " + porta);

        while (true) {
            try {
                cliSock = sock.accept();
                HttpRequest req = new HttpRequest(cliSock, BASE_FOLDER);
                req.start();
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }
    }

}
