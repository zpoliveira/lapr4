package smm.application.http;

import smm.application.SharedData;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class HttpRequest extends Thread {

    String baseFolder;
    Socket sock;
    DataInputStream inS;
    DataOutputStream outS;

    public HttpRequest(Socket s, String f) {
        baseFolder = f;
        sock = s;
    }

    @Override
    public void run() {
        try {
            outS = new DataOutputStream(sock.getOutputStream());
            inS = new DataInputStream(sock.getInputStream());
        } catch (IOException ex) {
            System.out.println("Thread error on data streams creation");
        }
        try {
            HttpMessage request = new HttpMessage(inS);
            HttpMessage response = new HttpMessage();

            if (request.getMethod().equals("GET")) {
                if (request.getURI().equals("/linhasProducao")) {
                    response.setContentFromString(
                            SharedData.getMaquinasInHTML(), "text/html");
                    response.setResponseStatus("200 Ok");
                } else {
                    String fullname =  baseFolder;

                    if (request.getURI().equals("/")) fullname = fullname + "/index.html";
                    else fullname = fullname + request.getURI();

                    if (response.setContentFromFile(fullname)) {
                        response.setResponseStatus("200 Ok");
                    } else {
                        response.setContentFromString(
                                "<html><body><h1>404 File not found</h1></body></html>",
                                "text/html");
                        response.setResponseStatus("404 Not Found");
                    }
                }
                response.send(outS);
            } else { // NOT GET
                if (request.getMethod().equals("POST")
                        && request.getURI().startsWith("/reset")) {
                     SharedData.sendReset(request.getURI().substring(7),
                             sock.getLocalPort());
                    response.setResponseStatus("200 Ok");
                } else {
                    response.setContentFromString(
                            "<html><body><h1>ERROR: 405 Method Not Allowed</h1></body></html>",
                            "text/html");
                    response.setResponseStatus("405 Method Not Allowed");
                }
                response.send(outS);
            }
        } catch (Exception ex) {
            System.out.println("Thread error when reading request");
        }
        try {
            sock.close();
        } catch (IOException ex) {
            System.out.println("CLOSE IOException");
        }
    }
}
