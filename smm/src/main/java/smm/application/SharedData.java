package smm.application;

import smm.application.udp.UdpSenderHello;
import smm.application.udp.UdpSenderReset;
import smm.domain.EstadoMaquina;
import smm.domain.TipoMensagem;

import java.net.InetAddress;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SharedData {

    private static HashMap<InetAddress, EstadoMaquina> peersList = new HashMap<>();

    /**
     * @param maxDifSec tempo em segundos
     */
    public static synchronized void checkTimeDiff(int maxDifSec){
        for(Map.Entry<InetAddress, EstadoMaquina> entry : peersList.entrySet()) {
            Duration dur = Duration.between(entry.getValue().getData(), LocalDateTime.now());
            long millis = dur.toMillis();
            if( millis > new Long(maxDifSec)*1000 ){
                entry.getValue().setNACK();
            }
        }
    }

    public static synchronized void addIP(InetAddress ip, EstadoMaquina em) {

        // Validar se já tem key
        boolean hasKey = peersList.containsKey(ip);
        boolean possoAdd = true;
        for(Map.Entry<InetAddress, EstadoMaquina> entry : peersList.entrySet()) {
            if(entry.getValue().equals(em)){
                if(!entry.getKey().equals(ip)){
                    possoAdd = false;
                }
            }
        }

        if(possoAdd && hasKey){
            peersList.replace(ip,em);
        }

        if(possoAdd && !hasKey){
            peersList.put(ip,em);
        }
    }


    public static synchronized String getMaquinasInHTML() {
        String textHtml = "";
        textHtml += "<p id=reset></p>";
        textHtml += "<table>";
        //Cabeçalho
        textHtml += "<tr>";
        textHtml += "<th>Código Máquina</td>";
        textHtml += "<th>IP</th>";
        textHtml += "<th>Estado</th>";
        textHtml += "<th>Mensagem</th>";
        textHtml += "<th>Ações</th>";
        textHtml += "</tr>";
        //linhas
        for(Map.Entry<InetAddress, EstadoMaquina> entry : peersList.entrySet()) {
            textHtml += "<tr>";
            textHtml += "<td>" + entry.getValue().maquinaIdProtocolo() + "</td>";
            textHtml += "<td>"+ entry.getKey().getHostAddress() +"</td>";
            if(entry.getValue().tipoMensagem() == TipoMensagem.ACK){
                textHtml += "<td style='color:green;font-weight:bold;'>OK</td>";
            }else{
                textHtml += "<td style='color:red;font-weight:bold;'>NOT OK</td>";
            }
            textHtml += "<td>" + entry.getValue().mensagem() + "</td>";
            textHtml += "<td><button type=button onclick=resetFor("+ entry.getValue().maquinaIdProtocolo() +")>RESET</button></td>";
            textHtml += "</tr>";
        }
        textHtml += "</table>";

        return textHtml;
    }


    public static synchronized void  sendReset(String codigo,int porta) throws Exception {

        int maquinaID = Integer.parseInt(codigo);
        InetAddress ip = null;
        for(Map.Entry<InetAddress, EstadoMaquina> entry : peersList.entrySet()) {
            if(entry.getValue().maquinaIdProtocolo() == maquinaID){
                ip=entry.getKey();
            }
        }

        UdpSenderReset udp = new UdpSenderReset(ip.getHostAddress(),porta);
        udp.start();

    }

}
