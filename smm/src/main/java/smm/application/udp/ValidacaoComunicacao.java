package smm.application.udp;

import smm.application.SharedData;

import java.util.TimerTask;

public class ValidacaoComunicacao extends TimerTask {

    private int maxTempoSemAtualizacao;

    /***
     * @param maxTempoSemAtualizacao tempo em segundos
     */
    public ValidacaoComunicacao(int maxTempoSemAtualizacao) {
        this.maxTempoSemAtualizacao = maxTempoSemAtualizacao;
    }

    @Override
    public void run() {
        System.out.println("Início validação da falta de comunicação.");
        SharedData.checkTimeDiff(maxTempoSemAtualizacao);
        System.out.println("Fim validação da falta de comunicação.");
    }
}
