package smm.application.udp;

import java.util.List;
import java.util.TimerTask;

public class UdpStart extends TimerTask {

    private List<String> lstIps;
    private int port;

    public UdpStart(List<String> lstIps, int port) {
        this.lstIps = lstIps;
        this.port = port;
    }

    @Override
    public void run() {
        UdpSenderHello us;
        for (String ip : lstIps) {
            us = new UdpSenderHello(ip, port);

            us.run();
            try {
                us.join();
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            }

        }
    }
}
