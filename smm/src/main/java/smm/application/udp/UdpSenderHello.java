package smm.application.udp;

import smm.Console.presentation.DatagramPacketPrinter;
import smm.Console.presentation.MensagemPrinter;
import smm.application.SharedData;
import smm.domain.Mensagem;
import smm.domain.TipoMensagem;
import utils.impl.Network;

import java.io.IOException;
import java.net.*;

public class UdpSenderHello extends Thread {
    private static final int MAX_UDP = 512;
    private static final int TIMEOUT = 3; // seconds
    private static final int TIMEOUT_BROADCAST = 20; // seconds
    private final String ip;
    private final int port;
    private boolean isBroadcast;

    private static InetAddress castAddress;
    private static DatagramSocket sock;

    public UdpSenderHello(String ip, int port) {
        this.ip = ip;
        this.port = port;
    }


    @Override
    public void run() {
        DatagramPacketPrinter datagramPacketPrinter = new DatagramPacketPrinter();
        MensagemPrinter mensagemPrinter = new MensagemPrinter();
        byte[] data = new byte[300];
        DatagramPacket udpPacket;

        try {
            sock = new DatagramSocket();
        } catch (IOException ex) {
            System.out.println("Failed to open local port");
        }

        try {
            castAddress = InetAddress.getByName(ip);
        } catch (UnknownHostException ex) {
            System.out.println("Invalid server address supplied: " + ip);
        }


        isBroadcast = Network.isBroadcast(castAddress);
        if (isBroadcast) {
            try {
                sock.setBroadcast(true);
                sock.setSoTimeout(1000 * TIMEOUT_BROADCAST); // set the socket timeout
            } catch (SocketException e) {
                System.out.println(e.getMessage());
            }

        } else {
            try {
                sock.setSoTimeout(1000 * TIMEOUT); // set the socket timeout
            } catch (SocketException e) {
                System.out.println(e.getMessage());
            }
        }

        Mensagem msg = new Mensagem(TipoMensagem.HELLO);
        byte[] send = msg.MensagemBytes();

        udpPacket = new DatagramPacket(data, data.length, castAddress, port);
        udpPacket.setData(send);
        udpPacket.setLength(send.length);
        System.out.println();
        System.out.println("Send HELLO : " + ip);

        try {
            sock.send(udpPacket);
        } catch (IOException e) {
            System.out.println(e.getMessage());
            return;
        }

        do { // handle response
            udpPacket.setData(data);
            udpPacket.setLength(data.length);
            try {
                sock.receive(udpPacket);
            } catch (SocketTimeoutException ex) {
                System.out.println("Timeout from ip " + ip);
                break;
            } catch (Exception ex) {
                System.out.println(ex.getMessage() + " " + ip);
                break;
            }
            String str = new String(udpPacket.getData(), 0, udpPacket.getLength());
            // Ler mensagem
            Mensagem resposta = Mensagem.Converter(udpPacket.getData(), udpPacket.getLength());
            datagramPacketPrinter.visit(udpPacket);
            mensagemPrinter.visit(resposta);
            System.out.println();
            // Colocar a mensagem
            SharedData.addIP(udpPacket.getAddress(), resposta.estadoMaquina());
        } while (isBroadcast);

        sock.close();

    }
}
