package smm.application.udp;

import smm.Console.presentation.DatagramPacketPrinter;
import smm.Console.presentation.MensagemPrinter;
import smm.domain.Mensagem;
import smm.domain.TipoMensagem;

import java.io.IOException;
import java.net.*;

public class UdpSenderReset {
    private static final int TIMEOUT = 3; // seconds
    private final String ip;
    private final int port;

    public UdpSenderReset(String ip, int port) {
        this.ip = ip;
        this.port = port;
    }

    static InetAddress castAddress;
    static DatagramSocket sock;

    public void start() throws Exception {
        DatagramPacketPrinter prt = new DatagramPacketPrinter();
        byte[] data = new byte[300];
        DatagramPacket udpPacket;

        try {
            sock = new DatagramSocket();
        } catch (IOException ex) {
            System.out.println("Failed to open local port");
        }

        try {
            castAddress = InetAddress.getByName(ip);
        } catch (UnknownHostException ex) {
            System.out.println("Invalid server address supplied: " + ip);
        }

            sock.setSoTimeout(1000 * TIMEOUT); // set the socket timeout


        Mensagem msg = new Mensagem(TipoMensagem.RESET);
        byte[] send = msg.MensagemBytes();

        udpPacket = new DatagramPacket(data, data.length, castAddress, port);
        udpPacket.setData(send);
        udpPacket.setLength(send.length);
        System.out.println();
        System.out.println("Send RESET : " + ip);
        sock.send(udpPacket);
        prt.visit(udpPacket);
        sock.close();
    }
}
