#!/usr/bin/env bash

java -cp h2-1.4.200.jar org.h2.tools.Server -tcp -ifNotExists &
java -jar infrastructure.application/target/infrastructure.application-0.0.1-SNAPSHOT-exec.jar
