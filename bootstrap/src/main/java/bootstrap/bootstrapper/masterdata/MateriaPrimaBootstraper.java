package bootstrap.bootstrapper.masterdata;

import eapli.framework.actions.Action;
import eapli.framework.domain.repositories.ConcurrencyException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;
import producao.application.controllers.AdicionarMateriaPrimaController;
import producao.application.controllers.ListarCatMatPrimaService;
import producao.domain.model.materiaPrima.CategoriaMatPrima;

import java.io.File;
import java.util.*;

@Component
public class MateriaPrimaBootstraper implements Action {

    private static final Logger LOGGER = LogManager.getLogger(MateriaPrimaBootstraper.class);

    @Autowired
    private AdicionarMateriaPrimaController theController;

    @Autowired
    private ListarCatMatPrimaService listarCatMatPrimaService;

    /*@Autowired
    private AuthorizationService authorizationService;*/

    @Override
    public boolean execute() {
        Set<String[]> matPrima = new LinkedHashSet<>();
        String matPrima1[] = {"V1", "Placa de Vidro"};
        String matPrima2[] = {"M1", "Tábua de Madeira"};
        String matPrima3[] = {"A1", "Barra de Aço"};
        String matPrima4[] = {"F1", "Barra de Ferro"};
        String matPrima5[] = {"C1", "Rolha de Cortiça"};
        String matPrima6[] = {"32000142", "Matéria-Prima teste"};
        matPrima.add(matPrima1);
        matPrima.add(matPrima2);
        matPrima.add(matPrima3);
        matPrima.add(matPrima4);
        matPrima.add(matPrima5);
        matPrima.add(matPrima6);

        File ft[] = new File[6];
        ft[0] = new File(getClass().getClassLoader().getResource("pdf/V1.pdf").getFile());
        ft[1] = new File(getClass().getClassLoader().getResource("pdf/M1.pdf").getFile());
        ft[2] = new File(getClass().getClassLoader().getResource("pdf/A1.pdf").getFile());
        ft[3] = new File(getClass().getClassLoader().getResource("pdf/F1.pdf").getFile());
        ft[4] = new File(getClass().getClassLoader().getResource("pdf/C1.pdf").getFile());
        ft[5] = new File(getClass().getClassLoader().getResource("pdf/C1.pdf").getFile());
        Iterable<File> iterable = Arrays.asList(ft);


        final Iterable<CategoriaMatPrima> categorias = theController.todasCatMatPrima();
        Iterator iteratorCat = categorias.iterator();
        Iterator iteratorMat = matPrima.iterator();
        Iterator iteratorFT = iterable.iterator();


        System.out.println("=======================Bootstrap de Matérias-Primas=======================");
        while (iteratorCat.hasNext()) {

            String c[] = (String[]) iteratorMat.next();
            CategoriaMatPrima cat = (CategoriaMatPrima) iteratorCat.next();
            File ftec = (File) iteratorFT.next();
            register(c[0], cat, c[1], ftec);

        }
        System.out.println("==========================================================================");

        return true;
    }


    private void register(String codigo, CategoriaMatPrima cat, String desc, File ft) {
        try {
            theController.addMatPrima(codigo, cat, desc, ft);
            System.out.println("Carregada a Matéria-Prima: " + "Código Interno: " + codigo + " | Categoria: " + cat
                    + " | Descrição: " + desc + " | Nome da Ficha Técnica: " + ft.getName() + " | Caminho da Ficha Técnica: "
                    + ft.getPath());

        } catch (final DataIntegrityViolationException | ConcurrencyException e) {
            System.out.println("Código Interno já utilizado");
        } catch (final IllegalArgumentException e) {
            LOGGER.error("Isto não devia acontecer", e);
            System.out.println(
                    "Erro inesperado na aplicação. Por favor, tente de novo, se erro continuar contactar administrador.");
        }
    }
}



