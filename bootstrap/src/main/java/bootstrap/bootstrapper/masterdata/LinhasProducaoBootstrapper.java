package bootstrap.bootstrapper.masterdata;

import chaofabrica.application.controllers.EspecificarNovaLinhaProducaoController;
import chaofabrica.application.controllers.ListMaquinasWithoutLinhaProducaoService;
import chaofabrica.domain.model.linhaproducao.LinhaProducao;
import chaofabrica.domain.model.maquina.Maquina;
import eapli.framework.actions.Action;
import eapli.framework.domain.repositories.ConcurrencyException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class LinhasProducaoBootstrapper implements Action {
    private static final Logger LOGGER = LogManager.getLogger(LinhasProducaoBootstrapper.class);

    @Autowired
    private EspecificarNovaLinhaProducaoController novaLinhaProducaoController;

    @Autowired
    private ListMaquinasWithoutLinhaProducaoService listMaquinasWithoutLinhaProducaoService;

    @Override
    public boolean execute() {
        System.out.println("=======================Bootstrap de Linhas Produção iniciado=======================");
        Iterable<Maquina> maquinas = listMaquinasWithoutLinhaProducaoService.allMaquinasWithoutLinhaProducao();
        Iterator<Maquina> iteratorMaq = maquinas.iterator();

        Maquina tempMaq = null;
        String cod = "";
        String codBase = "LP_";
        List<Maquina> setMaq = new ArrayList<>();

        while (iteratorMaq.hasNext()) {
            setMaq.add(iteratorMaq.next());

            if (setMaq.size() >= 5 || !iteratorMaq.hasNext()) {
                cod = codBase + setMaq.hashCode();
                register(cod, setMaq);
                System.out.println(String.format("Adicionada linha de produção: %s", cod));
                setMaq = new ArrayList<>();
            }
        }
        System.out.println("======================Bootstrap de Linhas Produção finalizado=======================");
        return true;
    }

    private void register(String cod, List<Maquina> lstMaquinas) {
        try {
            LinhaProducao linha = novaLinhaProducaoController.addLinhaProducao(cod, lstMaquinas);
            LOGGER.debug(linha);
        } catch (final DataIntegrityViolationException | ConcurrencyException e) {
            LOGGER.warn("Assuming {} already exists (activate trace log for details)", cod);
            LOGGER.trace("Assuming existing record", e);
        } catch (IllegalArgumentException e) {
            LOGGER.info(e.getMessage());
        }
    }
}
