package bootstrap.bootstrapper.masterdata;

import eapli.framework.actions.Action;
import eapli.framework.domain.repositories.ConcurrencyException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;
import producao.application.controllers.AdicionarOrdemProducaoController;
import producao.domain.model.Quantidade;
import producao.domain.model.Unidade;
import producao.domain.model.ordemProducao.IdentificadorEncomenda;
import producao.domain.model.produto.CodigoFabricoProduto;
import java.util.HashSet;
import java.util.Set;

@Component
public class OrdemProducaoBootstraper implements Action {
    private static final Logger LOGGER = LogManager.getLogger(OrdemProducaoBootstraper.class);

    @Autowired
    AdicionarOrdemProducaoController theController;

    @Override
    public boolean execute() {

        Set<IdentificadorEncomenda> enc = new HashSet<>();
        enc.add(new IdentificadorEncomenda("Enc11"));

        register("100003363", new CodigoFabricoProduto("50000106"), new Quantidade(65000L, Unidade.UN),
                "2020-06-06", "2020-06-06", enc);

        return false;
    }

    private void register(String idOrdemProducao, CodigoFabricoProduto codigoFabricoProduto,
                          Quantidade quantidade, String dataEmissao, String dataPrevistaExecucao,
                          Set<IdentificadorEncomenda> idsEncomenda) {
        try {
            theController.addOrdemProducao(idOrdemProducao, codigoFabricoProduto, quantidade, dataEmissao, dataPrevistaExecucao,
                    idsEncomenda);
            System.out.println("Carregada a Ordem de Produção " + idOrdemProducao);
        } catch (final DataIntegrityViolationException | ConcurrencyException e) {
            System.out.println("Ordem de Produção já existente");
        } catch (final IllegalArgumentException e) {
            LOGGER.error("Isto não devia acontecer", e);
            System.out.println(
                    "Erro inesperado na aplicação. Por favor, tente de novo, se erro continuar contactar administrador.");
        }
    }
}
