package bootstrap.bootstrapper.masterdata;

import chaofabrica.application.controllers.DefinirNovaMaquinaController;
import chaofabrica.domain.model.maquina.Maquina;
import eapli.framework.actions.Action;
import eapli.framework.domain.repositories.ConcurrencyException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class MaquinasBootstraper implements Action {
    private static final Logger LOGGER = LogManager.getLogger(MaquinasBootstraper.class);

    @Autowired
    private DefinirNovaMaquinaController definirNovaMaquinaController;

    @Override
    public boolean execute() {
        System.out.println("=======================Bootstrap de Maquinas =======================");
        register("001", "s001","marca1", "modelo1", "maquina1",LocalDate.now().minusDays(3),1);
        register("002", "s002","marca1", "modelo2", "maquina2",LocalDate.now().minusDays(3),2);
        register("003", "s003","marca2", "modelo1", "maquina3",LocalDate.now().minusDays(2),3);
        register("004", "s004","marca2", "modelo2", "maquina4",LocalDate.now().minusDays(2),4);
        register("005", "s005","marca3", "modelo1", "maquina5",LocalDate.now(),5);
        register("006", "s006","marca1", "modelo1", "maquina6",LocalDate.now().minusDays(3),6);
        register("007", "s007","marca1", "modelo2", "maquina7",LocalDate.now().minusDays(3),7);
        register("008", "s008","marca2", "modelo1", "maquina8",LocalDate.now().minusDays(2),8);
        register("009", "s009","marca2", "modelo2", "maquina9",LocalDate.now().minusDays(2),9);
        register("0010", "s0010","marca3", "modelo1", "maquina10",LocalDate.now(),10);
        register("0011", "s0011","marca1", "modelo1", "maquina11",LocalDate.now().minusDays(3),11);
        register("0012", "s0012","marca1", "modelo2", "maquina12",LocalDate.now().minusDays(3),12);
        register("0013", "s0013","marca2", "modelo1", "maquina13",LocalDate.now().minusDays(2),13);
        register("0014", "s0014","marca2", "modelo2", "maquina14",LocalDate.now().minusDays(2),14);
        register("0015", "s0015","marca3", "modelo1", "maquina15",LocalDate.now(),15);
        register("T3", "s0017","marca3", "modelo1", "maquina17",LocalDate.now(),17);
        register("DD4", "s0016","marca3", "modelo1", "maquina16",LocalDate.now(),16);
        System.out.println("=============Bootstrap de Maquinas Efetuado com Sucesso =======================");
        return true;
    }

    private void register(final String codigoInterno, final String numSerie, final String marca,
                          final String modelo, final String descricao, final LocalDate dtInstalacao,Integer codigoUnicoProtocolo) {
        try {
            Maquina maquina = definirNovaMaquinaController.addMaquina(
                    codigoInterno,
                    numSerie,
                    marca,
                    modelo,
                    descricao,
                    dtInstalacao,
                    codigoUnicoProtocolo
            );
            LOGGER.debug(maquina);
        }catch (final DataIntegrityViolationException | ConcurrencyException e ){
            LOGGER.warn("Assuming {} already exists (activate trace log for details)", codigoInterno);
            LOGGER.trace("Assuming existing record", e);
        }catch (IllegalArgumentException e){
            LOGGER.info(e.getMessage());
        }

    }


}
