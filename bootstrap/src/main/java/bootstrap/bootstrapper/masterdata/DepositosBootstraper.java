package bootstrap.bootstrapper.masterdata;

import chaofabrica.application.controllers.EspecificarNovoDepositoController;
import chaofabrica.domain.model.deposito.Deposito;
import eapli.framework.actions.Action;
import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

@Component
public class DepositosBootstraper implements Action {
    private static final Logger LOGGER = LogManager.getLogger(DepositosBootstraper.class);

    @Autowired
    private EspecificarNovoDepositoController theController;


    @Override
    public boolean execute() {
        register("001", "Depósito 1");
        register("002", "Depósito 2");
        register("003", "Depósito 3");
        register("004", "Depósito 4");
        register("005", "Depósito 5");
        return true;
    }

    private void register(final String codigo, final String description) {
        try {
           Deposito deposito = theController.addDeposito(codigo, description);
            LOGGER.debug(deposito);
        } catch (final DataIntegrityViolationException | ConcurrencyException e) {
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated user
            LOGGER.warn("Assuming {} already exists (activate trace log for details)", codigo);
            LOGGER.trace("Assuming existing record", e);
        } catch(final IllegalArgumentException e){
            LOGGER.info(e.getMessage(), codigo);
        }
    }
}
