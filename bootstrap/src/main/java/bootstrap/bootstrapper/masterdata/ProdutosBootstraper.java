package bootstrap.bootstrapper.masterdata;

import eapli.framework.actions.Action;
import eapli.framework.domain.repositories.ConcurrencyException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;
import producao.application.controllers.AddProdutoController;
import producao.domain.model.Quantidade;
import producao.domain.model.Unidade;
import producao.domain.model.materiaPrima.CodigoInternoMateriaPrima;
import producao.domain.model.produto.ComponenteMateriaPrima;
import producao.domain.model.produto.FichaProducao;
import producao.domain.model.produto.Produto;
import producao.domain.repositories.FichaProducaoRepository;

import java.util.HashSet;
import java.util.Set;

@Component
public class ProdutosBootstraper implements Action {

    private static final Logger LOGGER = LogManager.getLogger(ProdutosBootstraper.class);

    @Autowired
    private AddProdutoController addProdutoController;
    @Autowired
    private FichaProducaoRepository fichaProducaoRepository;

    @Override
    public boolean execute() {

        /*Produtos sem Ficha de Produção*/
        register("320030000002", "1100001", "Aparas", "Aparas");
        register("320030000003", "1200001", "Amostras", "Amostras");
        register("320030000004", "1200002", "Blocos", "Blocos");
        register("320030000005", "1200003", "Blocos Borracha", "Blocos Borracha");

        /*Produtos com Ficha de Produção*/
        Set<ComponenteMateriaPrima> CMP6 = new HashSet<>();
        ComponenteMateriaPrima C1 = new ComponenteMateriaPrima(new CodigoInternoMateriaPrima("M1"),
                new Quantidade(10L, Unidade.KG));
        ComponenteMateriaPrima C2 = new ComponenteMateriaPrima(new CodigoInternoMateriaPrima("A1"),
                new Quantidade(5L, Unidade.UN));
        CMP6.add(C1);
        CMP6.add(C2);
        FichaProducao F1 = new FichaProducao(new Quantidade(1L, Unidade.UN), CMP6);
        fichaProducaoRepository.save(F1);

        register("320030000006", "1200004", "Borracha DS", "Borracha DS", F1);

        Set<ComponenteMateriaPrima> CMP7 = new HashSet<>();
        ComponenteMateriaPrima C3 = new ComponenteMateriaPrima(new CodigoInternoMateriaPrima("A1"),
                new Quantidade(100L, Unidade.UN));
        ComponenteMateriaPrima C4 = new ComponenteMateriaPrima(new CodigoInternoMateriaPrima("F1"),
                new Quantidade(20L, Unidade.L));
        CMP7.add(C3);
        CMP7.add(C4);
        FichaProducao F2 = new FichaProducao(new Quantidade(2L, Unidade.UN), CMP7);
        fichaProducaoRepository.save(F2);

        register("320030000007", "1200005", "Cilindros", "Cilindros", F2);

        Set<ComponenteMateriaPrima> CMP8 = new HashSet<>();
        ComponenteMateriaPrima C5 = new ComponenteMateriaPrima(new CodigoInternoMateriaPrima("C1"),
                new Quantidade(1L, Unidade.TON));
        ComponenteMateriaPrima C6 = new ComponenteMateriaPrima(new CodigoInternoMateriaPrima("V1"),
                new Quantidade(20L, Unidade.UN));
        CMP8.add(C5);
        CMP8.add(C6);
        FichaProducao F3 = new FichaProducao(new Quantidade(1l, Unidade.UN), CMP8);
        fichaProducaoRepository.save(F3);

        register("320030000008", "1200006", "Fol. e rolos/Spheroil", "Fol. e rolos/Spheroil", F3);

        Set<ComponenteMateriaPrima> CMP9 = new HashSet<>();
        ComponenteMateriaPrima C7 = new ComponenteMateriaPrima(new CodigoInternoMateriaPrima("C1"),
                new Quantidade(100L, Unidade.KG));
        ComponenteMateriaPrima C8 = new ComponenteMateriaPrima(new CodigoInternoMateriaPrima("M1"),
                new Quantidade(200L, Unidade.L));
        CMP9.add(C7);
        CMP9.add(C8);
        FichaProducao F4 = new FichaProducao(new Quantidade(2l, Unidade.UN), CMP9);
        fichaProducaoRepository.save(F4);

        register("320030000009", "1200007", "Folhas", "Folhas", F4);

        Set<ComponenteMateriaPrima> CMP10 = new HashSet<>();
        ComponenteMateriaPrima C9 = new ComponenteMateriaPrima(new CodigoInternoMateriaPrima("F1"),
                new Quantidade(500L, Unidade.UN));
        ComponenteMateriaPrima C10 = new ComponenteMateriaPrima(new CodigoInternoMateriaPrima("M1"),
                new Quantidade(150L, Unidade.UN));
        CMP10.add(C9);
        CMP10.add(C10);
        FichaProducao F5 = new FichaProducao(new Quantidade(50l, Unidade.UN), CMP10);
        fichaProducaoRepository.save(F5);

        register("320030000010", "1200008", "Juntas", "Juntas", F5);

        Set<ComponenteMateriaPrima> CMP11 = new HashSet<>();
        ComponenteMateriaPrima C11 = new ComponenteMateriaPrima(new CodigoInternoMateriaPrima("32000142"),
                new Quantidade(244L, Unidade.UN));
        CMP11.add(C11);
        FichaProducao F6 = new FichaProducao(new Quantidade(10000l, Unidade.UN), CMP11);
        fichaProducaoRepository.save(F6);

        register("320030000010", "50000106", "Juntas", "Juntas", F6);


        return true;
    }

    private void register(
            final String codComercial, final String codFabrico,
            final String descBreve, final String descCompleta
    ) {
        try {
            Produto produto =
                    addProdutoController.addProduto(codComercial, codFabrico, descBreve, descCompleta);
            System.out.println(String.format("Adicionado produto %s - %s", codComercial, descBreve));
            LOGGER.debug(produto);
        } catch (final DataIntegrityViolationException | ConcurrencyException e) {
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated user
            LOGGER.warn("Assuming {} already exists (activate trace log for details)", codComercial);
            LOGGER.trace("Assuming existing record", e);
        } catch (final IllegalArgumentException e) {
        LOGGER.error("Isto não devia acontecer", e);
        System.out.println(
                "Erro inesperado na aplicação. Por favor, tente de novo, se erro continuar contactar administrador.");
    }
    }

    private void register(
            final String codComercial, final String codFabrico,
            final String descBreve, final String descCompleta,
            final FichaProducao fichProd
    ) {
        try {
            Produto produto =
                    addProdutoController.addProduto(codComercial, codFabrico, descBreve, descCompleta, fichProd);
            System.out.println(String.format("Adicionado produto %s - %s", codComercial, descBreve));
            LOGGER.debug(produto);
        } catch (final DataIntegrityViolationException | ConcurrencyException e) {
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated user
            LOGGER.warn("Assuming {} already exists (activate trace log for details)", codComercial);
            LOGGER.trace("Assuming existing record", e);
        } catch (final IllegalArgumentException e) {
        LOGGER.error("Isto não devia acontecer", e);
        System.out.println(
                "Erro inesperado na aplicação. Por favor, tente de novo, se erro continuar contactar administrador.");
    }
    }
}
