package bootstrap.bootstrapper.masterdata;

import chaofabrica.application.controllers.ArquivarNotificacaoController;
import chaofabrica.domain.model.notificacao.Notificacao;
import chaofabrica.domain.model.notificacao.TipoErro;
import eapli.framework.actions.Action;
import eapli.framework.domain.repositories.ConcurrencyException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

@Component
public class NotificacoesBootstraper implements Action {

    private static final Logger LOGGER = LogManager.getLogger(NotificacoesBootstraper.class);

    @Autowired
    ArquivarNotificacaoController theController;

    @Override
    public boolean execute() {

        register(new Notificacao(TipoErro.DADOS_INVALIDOS, "Erro 1","LP_152758"));
        register(new Notificacao(TipoErro.ELEMENTOS_NAO_ESPECIFICADOS, "Erro 2","LP_152758"));
        register(new Notificacao(TipoErro.PARAGEM_FORCADA_MAQUINA, "Erro 3","LP_1480698303"));
        register(new Notificacao(TipoErro.DADOS_INVALIDOS, "Erro 4","LP_1480698303"));
        register(new Notificacao(TipoErro.ELEMENTOS_NAO_ESPECIFICADOS, "Erro 5","LP_1480698303"));

        return true;
    }

    private void register(Notificacao notificacao) {
        try {
            theController.saveNotificacao(notificacao);
            System.out.println("Carregada a notificação da mensagem" + notificacao.identity() + " e do tipo "
                    + notificacao.tipoErro());
        } catch (final DataIntegrityViolationException | ConcurrencyException e) {
            System.out.println("Notificação já existente");
        } catch (final IllegalArgumentException e) {
            LOGGER.error("Isto não devia acontecer", e);
            System.out.println(
                    "Erro inesperado na aplicação. Por favor, tente de novo, se erro continuar contactar administrador.");
        }
    }
}
