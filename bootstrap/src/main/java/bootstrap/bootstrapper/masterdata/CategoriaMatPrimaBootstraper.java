package bootstrap.bootstrapper.masterdata;

import eapli.framework.actions.Action;
import eapli.framework.domain.repositories.ConcurrencyException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;
import producao.application.controllers.DefinCatMatPrimaController;

import java.util.HashSet;
import java.util.Set;

@Component
public class CategoriaMatPrimaBootstraper implements Action {

    private static final Logger LOGGER = LogManager.getLogger(CategoriaMatPrimaBootstraper.class);

    @Autowired
    private DefinCatMatPrimaController theController;
    /*@Autowired
    private AuthorizationService authorizationService;*/

    @Override
    public boolean execute() {

        System.out.println("=======================Bootstrap de Categorias de Matérias-Primas=======================");
        Set<String> categorias = new HashSet<>();
        register("Vidro");
        register("Ferro");
        register("Aço");
        register("Madeira");
        register("Cortiça");
        register("Resina");
        System.out.println("======================================================================================");


        return true;
    }


    private void register(String nomeCategoria) {
        try {
            theController.definCatMatPrima(nomeCategoria);
            System.out.println("Carregada a categoria: " + nomeCategoria);
        } catch (final DataIntegrityViolationException | ConcurrencyException e) {
            System.out.println("Nome de categoria já utilizado");
        } catch (final IllegalArgumentException e) {
            LOGGER.error("Isto não devia acontecer", e);
            System.out.println(
                    "Erro inesperado na aplicação. Por favor, tente de novo, se erro continuar contactar administrador.");
        }

    }
}
