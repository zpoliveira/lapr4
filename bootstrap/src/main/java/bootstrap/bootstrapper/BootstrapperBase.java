package bootstrap.bootstrapper;

import eapli.framework.actions.Action;
import eapli.framework.strings.util.Strings;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Base class for all bootstrappers.
 *
 * @author Paulo Gandra de Sousa
 *
 */
public abstract class BootstrapperBase implements Action {

    private static final Logger LOGGER = LogManager.getLogger(BootstrapperBase.class);

    //@Autowired
    //private AuthenticationService authenticationService;

    /**
     * Execute all bootstrapping returning false if any of the bootstrapper actions signals to end the
     * bootstraping.
     *
     * @param actions
     * @return
     */
    protected boolean bootstrap(final Action... actions) {
        boolean ret = true;
        for (final Action boot : actions) {
            LOGGER.info("Bootstrapping {}...", nameOfEntity(boot));
            ret &= boot.execute();
        }
        return ret;
    }

    /**
     * authenticate a super user to be able to register new users
     *
     */
    //protected void authenticateForBootstrapping() {
       // authenticationService.authenticate(MasterDataConstants.POWERUSER, MasterDataConstants.PASSWORD1);
    //}

    protected String nameOfEntity(final Action boot) {
        final String name = boot.getClass().getSimpleName();
        return Strings.left(name, name.length() - "Bootstrapper".length());
    }
}
