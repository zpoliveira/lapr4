package bootstrap.bootstrapper;

import bootstrap.bootstrapper.masterdata.*;
import eapli.framework.actions.Action;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class InitAllBootstrapper extends BootstrapperBase {

    @Autowired
    private DepositosBootstraper depositosBootstraper;

    @Autowired
    private CategoriaMatPrimaBootstraper categoriaMatPrimaBootstraper;

    @Autowired
    private MateriaPrimaBootstraper materiaPrimaBootstraper;

    @Autowired
    private ProdutosBootstraper produtosBootstraper;

    @Autowired
    private MaquinasBootstraper maquinasBootstraper;

    @Autowired
    private LinhasProducaoBootstrapper linhasProducaoBootstrapper;

    @Autowired
    private NotificacoesBootstraper notificacoesBootstraper;

    @Autowired
    private OrdemProducaoBootstraper ordemProducaoBootstraper;

    @Override
    public boolean execute() {
        /*if (!createPowerUser()) {
            return false;
        }*/

        //authenticateForBootstrapping();
        if (!bootstrapMasterData()) {
            return false;
        }

        // nothing more to do; everything went well
        return true;
    }

    /*private boolean createPowerUser() {
        return bootstrap(masterUsersBootstrapper);
    }*/

    private boolean bootstrapMasterData() {
        if (!applicationMasterData()) {
            return false;
        }

        return true;
    }

    private boolean applicationMasterData() {
        final Action[] masterData = {categoriaMatPrimaBootstraper, materiaPrimaBootstraper, depositosBootstraper,
                produtosBootstraper, maquinasBootstraper,linhasProducaoBootstrapper, notificacoesBootstraper,
                ordemProducaoBootstraper};
        return bootstrap(masterData);
    }
}

