package bootstrap.presentation;

import bootstrap.bootstrapper.InitAllBootstrapper;
import bootstrap.bootstrapper.masterdata.*;
import eapli.framework.actions.Actions;
import eapli.framework.actions.menu.Menu;
import eapli.framework.presentation.console.AbstractUI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BootstrapMenu extends AbstractUI {

    @Autowired
    private CategoriaMatPrimaBootstraper categoriaMatPrimaBootstraper;
    @Autowired
    private ProdutosBootstraper produtosBootstraper;
    @Autowired
    private DepositosBootstraper depositosBootstraper;
    @Autowired
    private MateriaPrimaBootstraper materiaPrimaBootstraper;
    @Autowired
    private MaquinasBootstraper maquinasBootstraper;
    @Autowired
    private LinhasProducaoBootstrapper linhasProducaoBootstrapper;
    @Autowired
    private InitAllBootstrapper initAllBootstrapper;
    @Autowired
    private NotificacoesBootstraper notificacoesBootstraper;

    private static final String MENU_TITLE = "BootStrapers sub menu";

    private static final int BOOT_CATEGORIAS_MATERIA_PRIMAS = 1;
    private static final int BOOT_PRODUTOS = 2;
    private static final int BOOT_DEPOSITOS = 3;
    private static final int BOOT_MATERIAS_PRIMAS = 4;
    private static final int BOOT_MAQUINAS = 5;
    private static final int BOOT_LINHAS_PRODUCAO = 6;
    private static final int BOOT_NOTIFICACOES = 7;
    private static final int BOOT_ALL = 10;
    private static final int EXIT_OPTION = 0;

    public Menu bootStrapSubMenus() {
        final Menu bootSubMenu = new Menu(MENU_TITLE);

        bootSubMenu.addItem(BOOT_CATEGORIAS_MATERIA_PRIMAS, "Bootstrap Categorias de Matérias-Primas", categoriaMatPrimaBootstraper::execute);
        bootSubMenu.addItem(BOOT_PRODUTOS, "Bootstrap de Produtos", produtosBootstraper::execute);
        bootSubMenu.addItem(BOOT_DEPOSITOS, "Bootstrap Depósitos", depositosBootstraper::execute);
        bootSubMenu.addItem(BOOT_MATERIAS_PRIMAS, "Bootstrap de Matérias-Primas", materiaPrimaBootstraper::execute);
        bootSubMenu.addItem(BOOT_MAQUINAS, "Bootstrap Máquinas", maquinasBootstraper::execute);
        bootSubMenu.addItem(BOOT_LINHAS_PRODUCAO, "Bootstrap Linhas Produção", linhasProducaoBootstrapper::execute);
        bootSubMenu.addItem(BOOT_NOTIFICACOES, "Bootstrap de notificações", notificacoesBootstraper::execute);
        bootSubMenu.addItem(BOOT_ALL, "Bootstrap Todos", initAllBootstrapper::execute);
        bootSubMenu.addItem(EXIT_OPTION, "Return ", Actions.SUCCESS);

        return bootSubMenu;
    }

    @Override
    protected boolean doShow() {
        return false;
    }

    @Override
    public String headline() {
            return "Bootstrap";
        }

}
