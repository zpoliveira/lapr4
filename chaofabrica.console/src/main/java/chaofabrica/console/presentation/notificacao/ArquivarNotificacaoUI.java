package chaofabrica.console.presentation.notificacao;

import chaofabrica.application.controllers.ArquivarNotificacaoController;
import chaofabrica.domain.model.notificacao.Notificacao;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class ArquivarNotificacaoUI extends AbstractUI {

    private static final Logger LOGGER = LogManager.getLogger(ArquivarNotificacaoUI.class);

    @Autowired
    private ArquivarNotificacaoController theController;



    @Override
    protected boolean doShow() {

        int option;
        do {
            final Iterable<Notificacao> notificacoesAtivas = theController.listaNotificacoesAtivas();
            final SelectWidget<Notificacao> selector = new SelectWidget<>("Selecione notificação",
                    notificacoesAtivas, new NotificacaoPrinter());
            selector.show();
            final Notificacao notificacao = selector.selectedElement();
            option = selector.selectedOption();
            if (notificacao == null) {
                return false;
            }

            int ret = theController.arquivaNotificacao(notificacao);

            if(ret == 1){
                System.out.println("Notificação da mensagem " + notificacao.identity() + " arquivada com sucesso");
            }else{
                System.out.println("Ocorreu um erro no arquivamento da notificação");
            }

        }while (option != 0);

        return false;
    }

    @Override
    public String headline() {
        return "Arquivar Notificações";
    }
}
