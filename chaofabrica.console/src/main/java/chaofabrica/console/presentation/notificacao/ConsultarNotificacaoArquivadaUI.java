package chaofabrica.console.presentation.notificacao;

import chaofabrica.application.controllers.ConsultarNotificacaoArquivadaController;
import chaofabrica.domain.model.linhaproducao.LinhaProducao;
import chaofabrica.domain.model.linhaproducao.SequenciaMaquina;
import chaofabrica.domain.model.maquina.CodigoInternoMaquina;
import chaofabrica.domain.model.maquina.Maquina;
import chaofabrica.domain.model.notificacao.Notificacao;
import chaofabrica.domain.model.notificacao.TipoErro;
import eapli.framework.io.util.Console;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.json.GsonBuilderUtils;
import org.springframework.stereotype.Component;
import org.w3c.dom.ls.LSOutput;
import spm.domain.mensagem.Mensagem;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

@Component
public class ConsultarNotificacaoArquivadaUI extends AbstractUI {

    private static final Logger LOGGER = LogManager.getLogger(ConsultarNotificacaoArquivadaUI.class);

    @Autowired
    ConsultarNotificacaoArquivadaController theController;

    @Override
    protected boolean doShow() {


        int option;

        List<String>opcoes = new ArrayList<>();
        opcoes.add("Tipo de Erro");
        opcoes.add("Linha de Produção");
        opcoes.add("Máquina");
        opcoes.add("Data do Erro");
        opcoes.add("Todas");

        do {
            final SelectWidget<String> selector = new SelectWidget<>("Selecione filtro de Notificações arquivadas",
                    opcoes);
            selector.show();
            option = selector.selectedOption();

            switch (option){
                case 1:
                    opcaoTipoErro();
                    break;
                case 2:
                    opcaoLinhaProducao();
                    break;
                case 3:
                    opcaoMaquina();
                    break;
                case 4:
                    opcaoData();
                    break;
                case 5:
                    opcaoTodas();
                    break;
            }

        }while (option != 0);

        return false;
    }

    private void opcaoTipoErro(){

        NotificacaoPrinter np = new NotificacaoPrinter();

        List<TipoErro> erros = theController.mostraTipoErro();
        final SelectWidget<TipoErro> selectorErro = new SelectWidget<>("Selecione Tipo de Erro",
                erros);
        selectorErro.show();
        final TipoErro erro = selectorErro.selectedElement();
        final Iterable<Notificacao> porErro = theController.listaNotificacoesPorTipoErro(erro);
        for(Notificacao not: porErro){
            np.visit(not);
            if(not.identity() != null){
                mostraMensagem(theController.mostraMensagemPorId(not.identity()));
            }



        }
    }

    private void opcaoLinhaProducao(){

        NotificacaoPrinter np = new NotificacaoPrinter();

        Iterable<LinhaProducao> linhas = theController.mostraTodasLinhas();
        final SelectWidget<LinhaProducao> selectorLinha = new SelectWidget<>("Selecione Linha de Produção",
                linhas);
        selectorLinha.show();
        final LinhaProducao linha = selectorLinha.selectedElement();
        if(linha != null) {
            final List<SequenciaMaquina> maquinas = linha.listaMaquinas();
            Iterator<SequenciaMaquina> itMaquina = maquinas.iterator();
            while (itMaquina.hasNext()) {
                CodigoInternoMaquina idMaquina = itMaquina.next().maquina().identity();
                mostraNotificacoePorMaquina(idMaquina);
            }
        }else{
            System.out.println("Não estão definidas Linhas de Produção");
        }
    }

    private void opcaoMaquina(){

        NotificacaoPrinter np = new NotificacaoPrinter();

        Iterable<Maquina> todasMaquinas = theController.mostraTodasMaquinas();
        final SelectWidget<Maquina> selectorMaquina = new SelectWidget<>("Selecione Máquina",
                todasMaquinas);
        selectorMaquina.show();
        final Maquina maq = selectorMaquina.selectedElement();
        if(maq == null) {
            System.out.println("Não estão definidas máquinas");
        }else {
            mostraNotificacoePorMaquina(maq.identity());
        }
    }

    private void opcaoData(){

        NotificacaoPrinter np = new NotificacaoPrinter();

        Calendar dataInicio = Console.readCalendar("Insira data inicial (Formato AAAA-MM-DD)", "yyyy-MM-dd");
        Calendar datafim = Console.readCalendar("Insira data final (Formato AAAA-MM-DD)", "yyyy-MM-dd");


        Iterable<Mensagem> mensagensPorData = theController.mostraMensagensPorData(converteCalendarEmLocalDateTime(dataInicio),
                converteCalendarEmLocalDateTime(datafim));
        for(Mensagem m:mensagensPorData){
            Iterable<Notificacao> notPorData = theController.listaNotificacoesPorMensagem(m.identity());
            for(Notificacao not:notPorData){
                np.visit(not);
                if(not.identity() != null){
                    mostraMensagem(theController.mostraMensagemPorId(not.identity()));
                }

            }
        }
    }

    private void opcaoTodas(){

        NotificacaoPrinter np = new NotificacaoPrinter();

        Iterable<Notificacao> todasArquivadas = theController.listaNotificacoesArquivadas();
        for(Notificacao not:todasArquivadas){
            np.visit(not);
            if(not.identity() != null){
                mostraMensagem(theController.mostraMensagemPorId(not.identity()));
            }

        }
    }

    private void mostraNotificacoePorMaquina(CodigoInternoMaquina codigoInternoMaquina){
        NotificacaoPrinter np = new NotificacaoPrinter();
        Iterable<Mensagem> mensagemPorMaquina = theController.listaMensagensPorMaquina(codigoInternoMaquina);
        for(Mensagem m: mensagemPorMaquina){
            Iterable<Notificacao> not = theController.listaNotificacoesPorMensagem(m.identity());
            for(Notificacao n : not){
                np.visit(n);
                if(n.identity() != null){
                    mostraMensagem(theController.mostraMensagemPorId(n.identity()));
                }

            }
        }

    }

    private LocalDateTime converteCalendarEmLocalDateTime(Calendar calendar){
        TimeZone tz2 = calendar.getTimeZone();
        ZoneId zid2 = tz2 == null ? ZoneId.systemDefault() : tz2.toZoneId();
        return LocalDateTime.ofInstant(calendar.toInstant(), zid2);
    }

    private void mostraMensagem(Mensagem mensagem){
        System.out.println("*** Detalhes da Mensagem ***");
        System.out.println(mensagem.toString());
    }

    @Override
    public String headline() {
        return "Consultar Notificações Arquivadas";
    }
}
