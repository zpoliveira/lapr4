package chaofabrica.console.presentation.notificacao;

import chaofabrica.domain.model.notificacao.Notificacao;
import eapli.framework.visitor.Visitor;

public class NotificacaoPrinter implements Visitor<Notificacao> {

    @Override
    public void visit(final Notificacao visitee) {
        if(visitee.identity() != null){
            System.out.printf("ID Mensagem: " + visitee.identity().toString()
                    + " | Tipo de Erro: " + visitee.tipoErro().label + "\n");
        }else{
            System.out.printf("Tipo de Erro: " + visitee.tipoErro().label +
                    " | Descrição: " + visitee.descricaoErro() + " da linha " + visitee.idLinha() + "\n");
        }

    }
}
