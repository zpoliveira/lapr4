package chaofabrica.console.presentation.notificacao;

import chaofabrica.application.controllers.ConsultarNotificacaoPorTratarController;
import chaofabrica.console.presentation.linhaproducao.LinhaProducaoBasePrinter;
import chaofabrica.console.presentation.linhaproducao.LinhaProducaoPrinter;
import chaofabrica.domain.model.linhaproducao.LinhaProducao;
import chaofabrica.domain.model.notificacao.Notificacao;
import chaofabrica.domain.model.notificacao.TipoErro;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.ListWidget;
import eapli.framework.presentation.console.SelectWidget;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ConsultarNotificacaoPorTratarUI extends AbstractUI {
    private static final Logger LOGGER = LogManager.getLogger(ConsultarNotificacaoArquivadaUI.class);
    private static final int PAGE_SIZE = 10;
    private final static String NEXT = "Próxima";
    private final static String PREVIOUS = "Anterior";
    private final List<String> opcoesSimNao;
    @Autowired
    ConsultarNotificacaoPorTratarController theController;
    Pageable page;
    Slice<Notificacao> slice;
    List<String> lstPage = new ArrayList<>();
    private TipoErro optionTipoErro;
    private LinhaProducao optionLinhaProdicao;

    public ConsultarNotificacaoPorTratarUI() {
        opcoesSimNao = new ArrayList<>();
        opcoesSimNao.add("Sim");
        opcoesSimNao.add("Não");
        page = PageRequest.of(0, PAGE_SIZE);
    }

    @Override
    protected boolean doShow() {

        int optionSimNao;

        optionSimNao = OpcaoSimNao("Deseja filtar por Tipo de erro?");

        if (optionSimNao == 0) {
            return false;
        } else if (optionSimNao == 1) {
            // Lista tipos de erro
            optionTipoErro = opcaoTipoErro();
        }

        optionSimNao = OpcaoSimNao("Deseja filtar por Linha Produção?");
        if (optionSimNao == 0) {
            return false;
        } else if (optionSimNao == 1) {
            // Lista tipos de erro
            optionLinhaProdicao = opcaoLinhaProducao();
        }

        String opcaoPage;

        // Ler as notificaçoes de erros por tratar
        if (optionTipoErro != null && optionLinhaProdicao != null) {
            do {
                slice = theController.listaNotificacoesPorTratarTipoErroLinhaProducao(optionLinhaProdicao.identity().linhaProducaoID(), optionTipoErro, page);
                opcaoPage = opcaoPage();
                if (opcaoPage == NEXT) {
                    page = page.next();
                } else if (opcaoPage == PREVIOUS) {
                    page = page.previousOrFirst();
                }
            } while (opcaoPage != null);
        } else if (optionTipoErro != null) {
            do {
                slice = theController.listaNotificacoesPorTratarTipoErro(optionTipoErro, page);
                opcaoPage = opcaoPage();
                if (opcaoPage == NEXT) {
                    page = page.next();
                } else if (opcaoPage == PREVIOUS) {
                    page = page.previousOrFirst();
                }
            } while (opcaoPage != null);
        } else if (optionLinhaProdicao != null) {
            do {
                slice = theController.listaNotificacoesPorTratarLinhaProducao(optionLinhaProdicao.identity().linhaProducaoID(), page);
                opcaoPage = opcaoPage();
                if (opcaoPage == NEXT) {
                    page = page.next();
                } else if (opcaoPage == PREVIOUS) {
                    page = page.previousOrFirst();
                }
            } while (opcaoPage != null);
        } else {
            do {
                slice = theController.listaNotificacoesPorTratar(page);
                opcaoPage = opcaoPage();
                if (opcaoPage == NEXT) {
                    page = page.next();
                } else if (opcaoPage == PREVIOUS) {
                    page = page.previousOrFirst();
                }
            } while (opcaoPage != null);
        }

        return false;
    }

    private Integer OpcaoSimNao(String msg) {
        SelectWidget<String> selector = new SelectWidget<>(msg,
                opcoesSimNao);
        selector.show();
        return selector.selectedOption();
    }

    private String opcaoPage() {
        NotificacaoPrinter printer = new NotificacaoPrinter();
        final ListWidget<Notificacao> widget = new ListWidget<>("Notificações", slice.getContent(), printer);
        widget.show();
        System.out.println();

        lstPage.clear();

        if (slice.hasPrevious())
            lstPage.add(PREVIOUS);
        if (slice.hasNext())
            lstPage.add(NEXT);

        final SelectWidget<String> selectWidget = new SelectWidget<>("Opções", lstPage);
        selectWidget.show();
        return selectWidget.selectedElement();

    }

    private TipoErro opcaoTipoErro() {

        List<TipoErro> erros = theController.mostraTipoErro();
        final SelectWidget<TipoErro> selectorErro = new SelectWidget<>("Selecione Tipo de Erro",
                erros);
        selectorErro.show();
        return selectorErro.selectedElement();

    }

    private LinhaProducao opcaoLinhaProducao() {

        LinhaProducaoBasePrinter printer = new LinhaProducaoBasePrinter();

        Iterable<LinhaProducao> linhas = theController.listLinhasProducao();
        final SelectWidget<LinhaProducao> selectorLinha = new SelectWidget<>("Selecione Linha de Produção",
                linhas, printer);
        selectorLinha.show();
        return selectorLinha.selectedElement();
    }


    @Override
    public String headline() {
        return "Consultar Notificações por tratar";
    }
}
