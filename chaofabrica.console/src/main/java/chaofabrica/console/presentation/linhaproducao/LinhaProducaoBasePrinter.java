package chaofabrica.console.presentation.linhaproducao;

import chaofabrica.domain.model.linhaproducao.LinhaProducao;
import eapli.framework.visitor.Visitor;

public class LinhaProducaoBasePrinter implements Visitor<LinhaProducao> {

    @Override
    public void visit(final LinhaProducao visitee) {
        System.out.printf(visitee.identity().linhaProducaoID());
    }
}
