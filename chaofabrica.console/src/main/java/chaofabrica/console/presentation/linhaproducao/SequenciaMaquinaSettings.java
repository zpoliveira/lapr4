package chaofabrica.console.presentation.linhaproducao;

import chaofabrica.domain.model.maquina.Maquina;
import eapli.framework.io.util.Console;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SequenciaMaquinaSettings {
    private final List<Maquina> maquinasSet;
    private final List<Maquina> list;
    private int option = -1;

    public SequenciaMaquinaSettings(final Iterable<Maquina> all){
        maquinasSet = new ArrayList<>();
        list = makeList(all);
    }

    private List<Maquina> makeList(final Iterable<Maquina> maquinas){
        final List<Maquina> result = new ArrayList<>();
        for(final Maquina item: maquinas){
            result.add(item);
        }
        return result;
    }

    private void showMaquinas(){
        int count = 0;
        System.out.println(" Escolha as Máquinas para a Linha de Produção");
        System.out.println("0.  Terminar escolha");
        for(final Maquina maq: list){
            count++;
            System.out.println(count + ".  " + maq.identity().codigoInternoMaquina() + " - " + maq.descricao());
        }
    }

    public List<Maquina> setSequenciaMaquinas(){
        showMaquinas();
        while (option != 0){
            option = Console.readOption(1, list.size() , 0);
            if (option != 0) {
                final Maquina elem = list.get(option - 1);
                maquinasSet.add(elem);
            }
        }
        return maquinasSet;
    }
}
