package chaofabrica.console.presentation.linhaproducao;

import chaofabrica.domain.model.linhaproducao.LinhaProducao;
import chaofabrica.domain.model.notificacao.Notificacao;
import eapli.framework.visitor.Visitor;

public class LinhaProducaoPrinter implements Visitor<LinhaProducao> {

    @Override
    public void visit(final LinhaProducao visitee) {
        System.out.printf(visitee.identity().linhaProducaoID() + " || Recorrência Ativada: " + visitee.estadoRecorrencia()
                + " || Último Processamento: " + (visitee.ultimoProcessamento() == null ? "N/A" : visitee.ultimoProcessamento()) +
                " || Período de Recorrência: " + visitee.mostraIntervaloProcessamento() +
                " || Início de Processamento: " + visitee.mostraInicioPeriodo() +
                " || Fim de Processamento: " + visitee.mostraFimPeriodo());
    }
}
