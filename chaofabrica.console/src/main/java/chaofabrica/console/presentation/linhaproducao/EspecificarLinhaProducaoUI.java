package chaofabrica.console.presentation.linhaproducao;

import chaofabrica.application.controllers.EspecificarNovaLinhaProducaoController;
import chaofabrica.domain.model.linhaproducao.LinhaProducao;
import chaofabrica.domain.model.maquina.Maquina;
import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.io.util.Console;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;

@Component
public class EspecificarLinhaProducaoUI extends AbstractUI {

    private static final Logger LOGGER = LogManager.getLogger(chaofabrica.console.presentation.linhaproducao.EspecificarLinhaProducaoUI.class);

    @Autowired
    private EspecificarNovaLinhaProducaoController theController;

    @Override
    protected boolean doShow() {

        final String codigo = Console.readLine("Código");
        final Iterable<Maquina> allMaquinas = theController.maquinas();
        final List<Maquina> sequenciaMaquinas = new SequenciaMaquinaSettings(allMaquinas).setSequenciaMaquinas();
        try {
            LinhaProducao linProducao = theController.addLinhaProducao(codigo, sequenciaMaquinas);
            System.out.println("Foi criada a " + linProducao.toString());
            //'linProducao = theController.getLinhaProducao(linProducao.identity());
            //System.out.println("Foi lida da BD a " + linProducao.toString());
        } catch (final DataIntegrityViolationException e) {
            System.out.println("Linha Produção já existente.");
        } catch (final ConcurrencyException e) {
            LOGGER.error("Isto não devia contecer", e);
            System.out.println(
                    "Erro inesperado na aplicação. Por favor, tente de novo, se erro continuar contactar administrador.");
        } catch (final IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }

        return false;
    }

    @Override
    public String headline() {
        return "Definir nova Linha Produção";
    }
}