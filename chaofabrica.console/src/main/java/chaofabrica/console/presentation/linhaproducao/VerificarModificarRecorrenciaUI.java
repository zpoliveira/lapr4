package chaofabrica.console.presentation.linhaproducao;

import chaofabrica.application.controllers.VerificarModificarRecorrenciaController;
import chaofabrica.domain.model.linhaproducao.LinhaProducao;
import eapli.framework.io.util.Console;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Component
public class VerificarModificarRecorrenciaUI extends AbstractUI {

    @Autowired
    VerificarModificarRecorrenciaController theController;
    Calendar inicio;
    Calendar fim;
    List<LinhaProducao> linhas = new ArrayList<>();
    LinhaProducaoPrinter lp = new LinhaProducaoPrinter();


    @Override
    protected boolean doShow() {
        List<String> simNao = new ArrayList<>();
        simNao.add("Sim");
        simNao.add("Não");
        int repeat = 1;


        do {
            Iterable<LinhaProducao> todasLinhas = theController.mostraTodasLinhas();
            SelectWidget<LinhaProducao> selectorLinha = new SelectWidget<>("Escolha Linha de Produção para modificar recorrência",
                    todasLinhas, lp);
            selectorLinha.show();
            LinhaProducao linha = selectorLinha.selectedElement();

            if (linha != null) {
                if (linha.estadoRecorrencia() == true) {
                    List<String> perg = new ArrayList<>();
                    perg.add("Desativar recorrência do processamento");
                    perg.add("Modificar intervalo de processamento");
                    SelectWidget<String> menu1 = new SelectWidget<>("Pretende:", perg);
                    menu1.show();
                    int opMenu1 = menu1.selectedOption();
                    if (opMenu1 == 1) {
                        linha =  theController.desativaRecorrencia(linha);
                        SelectWidget<String> menu2 = new SelectWidget<>("Pretende agendar um período para processar?", simNao);
                        menu2.show();
                        int opMenu2 = menu2.selectedOption();
                        if (opMenu2 == 1) {
                            LocalDateTime inicio = inicio();
                            LocalDateTime fim = fim();
                            while(fim.isBefore(inicio)){
                                System.out.println("Data de fim não pode ser anterior à de início!");
                                fim = fim();
                            }
                            linha = theController.definirInicioEFimProcessamento(linha,inicio, fim);
                            System.out.println(" O processamento recorrente de mensagens para " + linha.identity() +
                                    " foi desativado, ficando um processamento agendado para o período entre " +
                                    linha.mostraInicioPeriodo() + " e " + linha.mostraFimPeriodo());
                        } else {
                            System.out.println(" O processamento recorrente de mensagens para " + linha.identity() + " foi desativado");
                        }
                    }else {
                        int menu3 = 0;
                        do {
                            menu3 = Console.readInteger("Defina o intervalo de tempo - em min - para o processamento recorrente");
                        } while (menu3 < 1);
                        LocalDateTime inicio = inicio();
                        linha = theController.defineRecorrencia(linha, inicio, menu3);
                        System.out.println("Intervalo de processamento definido para " + menu3 + " min");

                    }
                }else{
                    SelectWidget<String> menu5 = new SelectWidget<>("A recorrencia está desativada. Pretende ativar?", simNao);
                    menu5.show();
                    int escolha = menu5.selectedOption();
                    if(escolha == 1){
                        linha = theController.ativaRecorrencia(linha);
                        LocalDateTime inicio = inicio();
                        int menu3 = 0;
                        do {
                            menu3 = Console.readInteger("Defina o intervalo de tempo - em min - para o processamento recorrente");
                        } while (menu3 < 1);
                        linha = theController.defineRecorrencia(linha, inicio, menu3);
                        System.out.println("Intervalo de processamento definido para " + menu3 + " min");
                    }else{
                        SelectWidget<String> menu2 = new SelectWidget<>("Pretende agendar um período para processar?", simNao);
                        menu2.show();
                        int opMenu2 = menu2.selectedOption();
                        if (opMenu2 == 1) {
                            LocalDateTime inicio2 = inicio();
                            LocalDateTime fim2 = fim();
                            while(fim2.isBefore(inicio2)){
                                System.out.println("Data de fim não pode ser anterior à de início!");
                                fim2 = fim();
                            }
                            linha = theController.definirInicioEFimProcessamento(linha,inicio2, fim2);
                            System.out.println("A " + linha.identity() +
                                    " ficou com um processamento agendado para o período entre " +
                                    linha.mostraInicioPeriodo() + " e " + linha.mostraFimPeriodo());
                        }
                    }
                }
                
        }else{
            System.out.println("Erro na Linha de produção");
        }

        SelectWidget<String> menu4 = new SelectWidget<>("Pretende modificar outra linha?", simNao);
        menu4.show();
        repeat = menu4.selectedOption();


    }while(repeat == 1);

        return false;
}

    private LocalDateTime inicio() {
        DateTimeFormatter ft = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

        inicio = Console.readCalendar("Insira inicio do período de processamento (Formato AAAA-MM-DD HH:MM)",
                "yyyy-MM-dd HH:mm");
        TimeZone tz = inicio.getTimeZone();
        ZoneId zid = tz == null ? ZoneId.systemDefault() : tz.toZoneId();
        return LocalDateTime.ofInstant(inicio.toInstant(), zid);
    }

    private LocalDateTime fim() {
        DateTimeFormatter ft = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime finalPeriodo;

        List<String> simNao = new ArrayList<>();
        simNao.add("Sim");
        simNao.add("Não");

        SelectWidget<String> menu = new SelectWidget<>("Pretende indicar fim do período de processamento?", simNao);
        menu.show();
        int opcao = menu.selectedOption();

        if (opcao == 1) {
            fim = Console.readCalendar("Insira fim do período de processamento (Formato AAAA-MM-DD HH:MM)",
                    "yyyy-MM-dd HH:mm");
            TimeZone tz = fim.getTimeZone();
            ZoneId zid = tz == null ? ZoneId.systemDefault() : tz.toZoneId();
            finalPeriodo = LocalDateTime.ofInstant(fim.toInstant(), zid);
        } else {
            finalPeriodo = LocalDateTime.now();
        }

        return finalPeriodo;
    }

    @Override
    public String headline() {
        return "Verificar ou modificar recorrência do SPM para uma Linha de Produção";
    }
}
