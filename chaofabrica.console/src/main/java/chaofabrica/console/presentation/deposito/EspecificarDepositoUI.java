package chaofabrica.console.presentation.deposito;

import chaofabrica.application.controllers.EspecificarNovoDepositoController;
import chaofabrica.domain.model.deposito.Deposito;
import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.io.util.Console;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;


@Component
public class EspecificarDepositoUI extends AbstractUI {

    private static final Logger LOGGER = LogManager.getLogger(chaofabrica.console.presentation.deposito.EspecificarDepositoUI.class);

    @Autowired
    private EspecificarNovoDepositoController theController;

    @Override
    protected boolean doShow() {

        final String codigo = Console.readLine("Código Alfanúmerico");
        final String descricao = Console.readLine("Descrição do Depósito");

        try {
            Deposito deposito = theController.addDeposito(codigo, descricao);
            System.out.println("Foi criada a " + deposito.toString());
        } catch (final DataIntegrityViolationException e) {
            System.out.println("Máquina já existente.");
        } catch (final ConcurrencyException e) {
            LOGGER.error("Isto não devia contecer", e);
            System.out.println(
                    "Erro inesperado na aplicação. Por favor, tente de novo, se erro continuar contactar administrador.");
        } catch (final IllegalArgumentException e){
            System.out.println(e.getMessage());
            return true;
        }

        return false;

    }

    @Override
    public String headline() {
        return "Definir nova Máquina";
    }
}
