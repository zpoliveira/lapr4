package chaofabrica.console.presentation.maquina;

import chaofabrica.application.controllers.AssociarFicheiroMaquinaController;
import chaofabrica.domain.model.maquina.Maquina;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.framework.io.util.Console;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;
import utils.console.FileSelector;

import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;

@Component
public class AssociarFicheiroMaquinaUI  extends AbstractUI {
    private static final Logger LOGGER = LogManager.getLogger(DefinirMaquinaUI.class);

    @Autowired
    AssociarFicheiroMaquinaController associarFicheiroMaquinaController;

    @Override
    protected boolean doShow() {
        final Iterable<Maquina> maquinas = associarFicheiroMaquinaController.listarMaquinas();
        final SelectWidget<Maquina> selector = new SelectWidget<>("Selecione a Maquina que pretende especificar o Ficheiro de Configuração",
                maquinas, new MaquinaPrinter());
        selector.show();
        if(selector.selectedOption() == 0){
            return false;
        }
        final Maquina selectedMaquina = selector.selectedElement();
        try{
            final File ficheiro = FileSelector.selectFile();
            final String descricaoFicheiro = Console.readLine("Descrição do Ficheiro");
            associarFicheiroMaquinaController.associarFicheiroConfiguracao(selectedMaquina, ficheiro, descricaoFicheiro);
        }catch (DataIntegrityViolationException | IllegalArgumentException | FileNotFoundException e){
            System.out.println(e.getMessage());
        }
        return false;
    }

    @Override
    public String headline() {
        return "Associar Ficheiro de Configuração a Maquina";
    }
}
