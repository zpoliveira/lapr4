package chaofabrica.console.presentation.maquina;

import chaofabrica.domain.model.maquina.Maquina;
import eapli.framework.visitor.Visitor;


public class MaquinaPrinter implements Visitor<Maquina> {

    @Override
    public void visit(final Maquina visitee) {
        System.out.println(visitee.identity() + " || " + visitee.descricao());
    }
}