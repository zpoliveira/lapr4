package chaofabrica.console.presentation.maquina;


import chaofabrica.application.controllers.DefinirNovaMaquinaController;
import chaofabrica.domain.model.maquina.Maquina;
import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.io.util.Console;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;

@Component
public class DefinirMaquinaUI extends AbstractUI {

    private static final Logger LOGGER = LogManager.getLogger(DefinirMaquinaUI.class);

    @Autowired
    private DefinirNovaMaquinaController theController;

    @Override
    protected boolean doShow() {

        final String codigoInterno = Console.readLine("Código Interno da Máquina");
        final String descricao = Console.readLine("Descrição da Máquina");
        final String marca = Console.readLine("Marca da Máquina");
        final String modelo = Console.readLine("Modelo da Máquina");
        final String numSerie = Console.readLine("Número de Série");
        final Integer codigoUnicoProtocolo = Console.readInteger("Código único protocolo");
        final Calendar dataInstalacao = Console.readCalendar("Data de instação [dd-MM-yyyy]");

        try {
            Maquina maq = theController.addMaquina(codigoInterno, numSerie, marca, modelo, descricao,
                    LocalDateTime.ofInstant(dataInstalacao.toInstant(), ZoneId.systemDefault()).toLocalDate(),codigoUnicoProtocolo);
            System.out.println("Foi criada a " + maq.toString());
        } catch (final IntegrityViolationException | DataIntegrityViolationException e) {
            System.out.println("Máquina já existente.");
        } catch (final ConcurrencyException e) {
            LOGGER.error("Isto não devia contecer", e);
            System.out.println(
                    "Erro inesperado na aplicação. Por favor, tente de novo, se erro continuar contactar administrador.");
        } catch (final IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }

        return false;

    }

    @Override
    public String headline() {
        return "Definir nova Máquina";
    }
}
