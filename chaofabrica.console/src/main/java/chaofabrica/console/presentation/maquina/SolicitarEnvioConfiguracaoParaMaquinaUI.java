package chaofabrica.console.presentation.maquina;

import chaofabrica.application.controllers.SolicitarEnvioConfiguracaoParaMaquinaController;
import chaofabrica.domain.model.maquina.FicheiroConfiguracao;
import chaofabrica.domain.model.maquina.Maquina;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SolicitarEnvioConfiguracaoParaMaquinaUI extends AbstractUI {

    private static final Logger LOGGER = LogManager.getLogger(SolicitarEnvioConfiguracaoParaMaquinaUI.class);

    @Autowired
    SolicitarEnvioConfiguracaoParaMaquinaController theController;

    @Override
    protected boolean doShow() {

        Iterable<Maquina> todasMaquinas = theController.todasMaquinas();
        SelectWidget<Maquina> selectorMaquinas = new SelectWidget<>("Selecione a máquina para onde enviar o ficheiro de configuração",
                todasMaquinas, new MaquinaPrinter());
        selectorMaquinas.show();
        Maquina maquina = selectorMaquinas.selectedElement();

        Iterable<FicheiroConfiguracao> ficheiros = maquina.ficheirosConfiguracao();
        SelectWidget<FicheiroConfiguracao> seletorFicheiros = new SelectWidget<>("Selecione ficheiro de configuracao",
                ficheiros);
        seletorFicheiros.show();
        FicheiroConfiguracao fc = seletorFicheiros.selectedElement();
        if(fc != null){
            System.out.println(fc);
            theController.enviaConfiguracaoParaMaquina(maquina, fc);
        }


        return false;
    }

    @Override
    public String headline() {
        return "Solicitar envio de configuração para máquina";
    }
}
