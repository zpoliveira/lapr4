package chaofabrica.console.presentation;

import chaofabrica.application.controllers.ConsultarNotificacaoPorTratarController;
import chaofabrica.console.presentation.deposito.EspecificarDepositoUI;
import chaofabrica.console.presentation.linhaproducao.EspecificarLinhaProducaoUI;
import chaofabrica.console.presentation.linhaproducao.VerificarModificarRecorrenciaUI;
import chaofabrica.console.presentation.maquina.AssociarFicheiroMaquinaUI;
import chaofabrica.console.presentation.maquina.DefinirMaquinaUI;
import chaofabrica.console.presentation.maquina.SolicitarEnvioConfiguracaoParaMaquinaUI;
import chaofabrica.console.presentation.notificacao.ArquivarNotificacaoUI;
import chaofabrica.console.presentation.notificacao.ConsultarNotificacaoArquivadaUI;
import chaofabrica.console.presentation.notificacao.ConsultarNotificacaoPorTratarUI;
import eapli.framework.actions.Actions;
import eapli.framework.actions.menu.Menu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ChaoFabricaMenu {
    private static final String MENU_TITLE = "Chão de Fabrica sub menu";

    private static final int EXIT_OPTION = 0;
    private static final int DEFINIR_MAQUINA = 1;
    private static final int ESPECIFICAR_DEPOSITO=2;
    private  static final int ESPECIFICAR_LINHAPRODUCAO =3;
    private  static final int ARQUIVAR_NOTIFICACAO = 4;
    private  static final int CONSULTAR_NOTIFICACAO = 5;
    private static final int CONSULTAR_NOTIFICACAO_POR_TRATAR = 6;
    private  static final int ASSOCIAR_fICHEIRO_CONFIGURACAO_MAQUINA = 7;
    private  static final int VERIFICAR_OU_MODIFICAR_RECORRENCIA = 8;
    private  static final int SOLICITAR_ENVIO_CONFIGURACAO_MAQUINA = 9;

    @Autowired
    private DefinirMaquinaUI definirMaquinaUI;

    @Autowired
    private EspecificarDepositoUI especificarDepositoUI;

    @Autowired
    private EspecificarLinhaProducaoUI especificarLinhaProducaoUI;

    @Autowired
    private ArquivarNotificacaoUI arquivarNotificacaoUI;

    @Autowired
    private ConsultarNotificacaoArquivadaUI consultarNotificacaoArquivadaUI;

    @Autowired
    private AssociarFicheiroMaquinaUI associarFicheiroMaquinaUI;

    @Autowired
    private ConsultarNotificacaoPorTratarUI consultarNotificacaoPorTratarUI;

    @Autowired
    private VerificarModificarRecorrenciaUI verificarModificarRecorrenciaUI;

    @Autowired
    private SolicitarEnvioConfiguracaoParaMaquinaUI solicitarEnvioConfiguracaoParaMaquinaUI;

    public Menu chaoFabricaSubMenus() {
        final Menu chaoFabricaSubMenu = new Menu(MENU_TITLE);

        chaoFabricaSubMenu.addItem(DEFINIR_MAQUINA, "Definir Máquina", definirMaquinaUI::show);
        chaoFabricaSubMenu.addItem(ESPECIFICAR_DEPOSITO, "Específicar Depósito", especificarDepositoUI::show);
        chaoFabricaSubMenu.addItem(ESPECIFICAR_LINHAPRODUCAO, "Específicar Linha Produção",
                especificarLinhaProducaoUI::show);
        chaoFabricaSubMenu.addItem(ARQUIVAR_NOTIFICACAO, "Arquivar Notificações", arquivarNotificacaoUI::show);
        chaoFabricaSubMenu.addItem(CONSULTAR_NOTIFICACAO, "Consultar Notificações Arquivadas",
                consultarNotificacaoArquivadaUI::show);
        chaoFabricaSubMenu.addItem(CONSULTAR_NOTIFICACAO_POR_TRATAR,"Consultar Notificações por Tratar",
                consultarNotificacaoPorTratarUI::show);
        chaoFabricaSubMenu.addItem(ASSOCIAR_fICHEIRO_CONFIGURACAO_MAQUINA, "Associar Ficheiro de Configuração a Maquina",
                associarFicheiroMaquinaUI::show);
        chaoFabricaSubMenu.addItem(VERIFICAR_OU_MODIFICAR_RECORRENCIA, "Verificar ou modificar recorrência do SPM",
                verificarModificarRecorrenciaUI::show);
        chaoFabricaSubMenu.addItem(SOLICITAR_ENVIO_CONFIGURACAO_MAQUINA, "Solicitar Envio de Configuração para Máquina",
                solicitarEnvioConfiguracaoParaMaquinaUI::show);
        chaoFabricaSubMenu.addItem(EXIT_OPTION, "Return ", Actions.SUCCESS);

        return chaoFabricaSubMenu;
    }
}
