
package producao.dto.produtos;

import org.springframework.stereotype.Component;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.Objects;


/**
 * <p>Java class for TProduto complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TProduto"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CodigoComercial" type="{http://factyfloor.com/CommunTypes}TString1_15"/&gt;
 *         &lt;element name="CodigoFabrico" type="{http://factyfloor.com/CommunTypes}TString1_15"/&gt;
 *         &lt;element name="DescricaoBreve" type="{http://factyfloor.com/CommunTypes}TString1_30"/&gt;
 *         &lt;element name="DescricaoCompleta" type="{http://factyfloor.com/CommunTypes}TString1_50"/&gt;
 *         &lt;element name="FichaProducao" type="{http://factyfloor.com/Produtos}TFichaProducao" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@Component
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TProduto", namespace = "http://factyfloor.com/Produtos", propOrder = {
    "codigoComercial",
    "codigoFabrico",
    "descricaoBreve",
    "descricaoCompleta",
    "fichaProducao"
})
public class TProduto {

    @XmlElement(name = "CodigoComercial", namespace = "http://factyfloor.com/Produtos", required = true)
    protected String codigoComercial;
    @XmlElement(name = "CodigoFabrico", namespace = "http://factyfloor.com/Produtos", required = true)
    protected String codigoFabrico;
    @XmlElement(name = "DescricaoBreve", namespace = "http://factyfloor.com/Produtos", required = true)
    protected String descricaoBreve;
    @XmlElement(name = "DescricaoCompleta", namespace = "http://factyfloor.com/Produtos", required = true)
    protected String descricaoCompleta;
    @XmlElement(name = "FichaProducao", namespace = "http://factyfloor.com/Produtos")
    protected TFichaProducao fichaProducao;

    /**
     * Gets the value of the codigoComercial property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoComercial() {
        return codigoComercial;
    }

    /**
     * Sets the value of the codigoComercial property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoComercial(String value) {
        this.codigoComercial = value;
    }

    /**
     * Gets the value of the codigoFabrico property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoFabrico() {
        return codigoFabrico;
    }

    /**
     * Sets the value of the codigoFabrico property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoFabrico(String value) {
        this.codigoFabrico = value;
    }

    /**
     * Gets the value of the descricaoBreve property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescricaoBreve() {
        return descricaoBreve;
    }

    /**
     * Sets the value of the descricaoBreve property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescricaoBreve(String value) {
        this.descricaoBreve = value;
    }

    /**
     * Gets the value of the descricaoCompleta property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescricaoCompleta() {
        return descricaoCompleta;
    }

    /**
     * Sets the value of the descricaoCompleta property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescricaoCompleta(String value) {
        this.descricaoCompleta = value;
    }

    /**
     * Gets the value of the fichaProducao property.
     * 
     * @return
     *     possible object is
     *     {@link TFichaProducao }
     *     
     */
    public TFichaProducao getFichaProducao() {
        return fichaProducao;
    }

    /**
     * Sets the value of the fichaProducao property.
     * 
     * @param value
     *     allowed object is
     *     {@link TFichaProducao }
     *     
     */
    public void setFichaProducao(TFichaProducao value) {
        this.fichaProducao = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TProduto tProduto = (TProduto) o;
        return codigoComercial.equals(tProduto.codigoComercial) &&
                codigoFabrico.equals(tProduto.codigoFabrico);
    }

    @Override
    public int hashCode() {
        return Objects.hash(codigoComercial, codigoFabrico);
    }
}
