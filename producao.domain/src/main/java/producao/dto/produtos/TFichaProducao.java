package producao.dto.produtos;

import producao.dto.communtypes.TQuantidade;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for TFichaProducao complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="TFichaProducao"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="MateriaPrima" type="{http://factyfloor.com/Produtos}TMateriaPrima" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TFichaProducao", namespace = "http://factyfloor.com/Produtos", propOrder = {
        "componenteMateriaPrimas",
        "quantidade"
})
public class TFichaProducao {


    @XmlElement(name = "Quantidade", namespace = "http://factyfloor.com/Produtos", required = true)
    protected TQuantidade quantidade = new TQuantidade();

    @XmlElement(name = "MateriaPrima", namespace = "http://factyfloor.com/Produtos", required = true)
    protected List<TComponenteMateriaPrima> componenteMateriaPrimas = new ArrayList<>();

    /**
     * Gets the value of the materiaPrima property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the materiaPrima property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMateriaPrima().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TComponenteMateriaPrima }
     */
    public List<TComponenteMateriaPrima> getComponenteMateriaPrimas() {
        if (componenteMateriaPrimas == null) {
            componenteMateriaPrimas = new ArrayList<TComponenteMateriaPrima>();
        }
        return this.componenteMateriaPrimas;
    }

    public TQuantidade getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(TQuantidade quantidade) {
        this.quantidade = quantidade;
    }
}
