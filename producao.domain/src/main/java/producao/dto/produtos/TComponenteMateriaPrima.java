
package producao.dto.produtos;


import producao.dto.communtypes.TUnidade;

import javax.xml.bind.annotation.*;
import java.math.BigInteger;


/**
 * <p>Java class for TMateriaPrima complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TMateriaPrima"&gt;
 *   &lt;simpleContent&gt;
 *     &lt;extension base="&lt;http://factyfloor.com/CommunTypes&gt;TOneWord"&gt;
 *       &lt;attribute name="Quantidade" use="required" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&gt;
 *       &lt;attribute name="Unidade" use="required" type="{http://factyfloor.com/CommunTypes}TUnidade" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/simpleContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TComponenteMateriaPrima", namespace = "http://factyfloor.com/Produtos", propOrder = {
    "value"
})
public class TComponenteMateriaPrima {

    @XmlValue
    protected String value;
    @XmlAttribute(name = "Quantidade", required = true)
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger quantidade;
    @XmlAttribute(name = "Unidade", required = true)
    protected TUnidade unidade;

    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Gets the value of the quantidade property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getQuantidade() {
        return quantidade;
    }

    /**
     * Sets the value of the quantidade property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setQuantidade(BigInteger value) {
        this.quantidade = value;
    }

    /**
     * Gets the value of the unidade property.
     * 
     * @return
     *     possible object is
     *     {@link TUnidade }
     *     
     */
    public TUnidade getUnidade() {
        return unidade;
    }

    /**
     * Sets the value of the unidade property.
     * 
     * @param value
     *     allowed object is
     *     {@link TUnidade }
     *     
     */
    public void setUnidade(TUnidade value) {
        this.unidade = value;
    }

}
