
package producao.dto.communtypes;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * <p>Java class for TFileBlob complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TFileBlob"&gt;
 *   &lt;simpleContent&gt;
 *     &lt;extension base="&lt;http://factyfloor.com/CommunTypes&gt;TBlob"&gt;
 *       &lt;attribute name="hasFile" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/simpleContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TFileBlob", namespace = "http://factyfloor.com/CommunTypes", propOrder = {
    "value"
})
public class TFileBlob {

    @XmlValue
    protected String value;
    @XmlAttribute(name = "hasFile", required = true)
    protected boolean hasFile;

    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Gets the value of the hasFile property.
     * 
     */
    public boolean isHasFile() {
        return hasFile;
    }

    /**
     * Sets the value of the hasFile property.
     * 
     */
    public void setHasFile(boolean value) {
        this.hasFile = value;
    }

}
