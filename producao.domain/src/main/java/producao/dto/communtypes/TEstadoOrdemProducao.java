
package producao.dto.communtypes;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TEstadoOrdemProducao.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TEstadoOrdemProducao"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Pendente"/&gt;
 *     &lt;enumeration value="Em Execução"/&gt;
 *     &lt;enumeration value="Execução Parada Temporariamente"/&gt;
 *     &lt;enumeration value="Concluída"/&gt;
 *     &lt;enumeration value="Interrompida"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "TEstadoOrdemProducao", namespace = "http://factyfloor.com/CommunTypes")
@XmlEnum
public enum TEstadoOrdemProducao {

    @XmlEnumValue("Pendente")
    PENDENTE("Pendente"),
    @XmlEnumValue("Em Execução")
    EM_EXECUCAO("Em Execução"),
    @XmlEnumValue("Execução Parada Temporariamente")
    EXECUCAO_PARADA_TEMPORARIAMENTE("Execução Parada Temporariamente"),
    @XmlEnumValue("Concluída")
    CONCLUIDA("Concluída"),
    @XmlEnumValue("Interrompida")
    INTERROMPIDA("Interrompida");
    private final String value;

    TEstadoOrdemProducao(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TEstadoOrdemProducao fromValue(String v) {
        for (TEstadoOrdemProducao c: TEstadoOrdemProducao.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
