
package producao.dto.categoriamateriaprima;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for TCategoriasMateriaPrima complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TCategoriasMateriaPrima"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CategoriaMateriaPrima" type="{http://factyfloor.com/CommunTypes}TOneWord" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TCategoriasMateriaPrima", namespace = "http://factyfloor.com/CategoriaMateriaPrima", propOrder = {
    "categoriaMateriaPrima"
})
public class TCategoriasMateriaPrima {

    @XmlElement(name = "CategoriaMateriaPrima", namespace = "http://factyfloor.com/CategoriaMateriaPrima", required = true)
    protected List<String> categoriaMateriaPrima;

    /**
     * Gets the value of the categoriaMateriaPrima property.
     *
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getCategoriaMateriaPrima() {
        if (categoriaMateriaPrima == null) {
            categoriaMateriaPrima = new ArrayList<String>();
        }
        return this.categoriaMateriaPrima;
    }

}
