
package producao.dto.materiaprima;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TMateriasPrimas complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TMateriasPrimas"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="MateriaPrima" type="{http://factyfloor.com/MateriaPrima}TMateriaPrima" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TMateriasPrimas", namespace = "http://factyfloor.com/MateriaPrima", propOrder = {
    "materiaPrima"
})
public class TMateriasPrimas {

    @XmlElement(name = "MateriaPrima", namespace = "http://factyfloor.com/MateriaPrima", required = true)
    protected List<TMateriaPrima> materiaPrima;

    /**
     * Gets the value of the materiaPrima property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the materiaPrima property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMateriaPrima().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TMateriaPrima }
     * 
     * 
     */
    public List<TMateriaPrima> getMateriaPrima() {
        if (materiaPrima == null) {
            materiaPrima = new ArrayList<TMateriaPrima>();
        }
        return this.materiaPrima;
    }

}
