package producao.dto.materiaprima;


import producao.dto.communtypes.TFileBlob;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TMateriaPrima complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="TMateriaPrima"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CodigoInterno" type="{http://factyfloor.com/CommunTypes}TOneWord"/&gt;
 *         &lt;element name="Descricao" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="FichaTecnica" type="{http://factyfloor.com/CommunTypes}TFileBlob" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TMateriaPrima", namespace = "http://factyfloor.com/MateriaPrima", propOrder = {
        "codigoInterno",
        "descricao",
        "categoria",
        "fichaTecnica"
})
public class TMateriaPrima {

    @XmlElement(name = "CodigoInterno", namespace = "http://factyfloor.com/MateriaPrima", required = true)
    protected String codigoInterno;
    @XmlElement(name = "Descricao", namespace = "http://factyfloor.com/MateriaPrima", required = true)
    protected String descricao;
    @XmlElement(name = "Categoria", namespace = "http://factyfloor.com/MateriaPrima", required = true)
    protected String categoria;
    @XmlElement(name = "FichaTecnica", namespace = "http://factyfloor.com/MateriaPrima")
    protected String fichaTecnica;

    /**
     * Gets the value of the codigoInterno property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCodigoInterno() {
        return codigoInterno;
    }

    /**
     * Sets the value of the codigoInterno property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCodigoInterno(String value) {
        this.codigoInterno = value;
    }

    /**
     * Gets the value of the descricao property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * Sets the value of the descricao property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setDescricao(String value) {
        this.descricao = value;
    }

    /**
     * Gets the value of the fichaTecnica property.
     *
     * @return possible object is
     * {@link TFileBlob }
     */
    public String getFichaTecnica() {
        return fichaTecnica;
    }

    /**
     * Sets the value of the fichaTecnica property.
     *
     * @param value allowed object is
     *              {@link TFileBlob }
     */
    public void setFichaTecnica(String value) {
        this.fichaTecnica = value;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }
}
