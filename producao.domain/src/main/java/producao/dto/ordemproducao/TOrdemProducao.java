
package producao.dto.ordemproducao;

import producao.dto.communtypes.TEstadoOrdemProducao;
import producao.dto.communtypes.TQuantidade;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for TOrdemProducao complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TOrdemProducao"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="IdOrdemProducao" type="{http://factyfloor.com/CommunTypes}TOneWord"/&gt;
 *         &lt;element name="EstadoOrdemProducao" type="{http://factyfloor.com/CommunTypes}TEstadoOrdemProducao"/&gt;
 *         &lt;element name="CodigoFabricoProduto" type="{http://factyfloor.com/CommunTypes}TOneWord"/&gt;
 *         &lt;element name="Quantidade" type="{http://factyfloor.com/OrdensProducao}TQuantidade"/&gt;
 *         &lt;element name="DataEmissao" type="{http://factyfloor.com/CommunTypes}TData"/&gt;
 *         &lt;element name="DataPrevistaExecucao" type="{http://factyfloor.com/CommunTypes}TData"/&gt;
 *         &lt;sequence&gt;
 *           &lt;element name="IdEncomenda" type="{http://factyfloor.com/CommunTypes}TOneWord"/&gt;
 *         &lt;/sequence&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TOrdemProducao", namespace = "http://factyfloor.com/OrdensProducao", propOrder = {
    "idOrdemProducao",
    "estadoOrdemProducao",
    "codigoFabricoProduto",
    "quantidade",
    "dataEmissao",
    "dataPrevistaExecucao",
    "idEncomenda"
})
public class TOrdemProducao {

    @XmlElement(name = "IdOrdemProducao", namespace = "http://factyfloor.com/OrdensProducao", required = true)
    protected String idOrdemProducao;
    @XmlElement(name = "EstadoOrdemProducao", namespace = "http://factyfloor.com/OrdensProducao", required = true)
    @XmlSchemaType(name = "string")
    protected TEstadoOrdemProducao estadoOrdemProducao;
    @XmlElement(name = "CodigoFabricoProduto", namespace = "http://factyfloor.com/OrdensProducao", required = true)
    protected String codigoFabricoProduto;
    @XmlElement(name = "Quantidade", namespace = "http://factyfloor.com/OrdensProducao", required = true)
    protected TQuantidade quantidade;
    @XmlElement(name = "DataEmissao", namespace = "http://factyfloor.com/OrdensProducao", required = true)
    protected String dataEmissao;
    @XmlElement(name = "DataPrevistaExecucao", namespace = "http://factyfloor.com/OrdensProducao", required = true)
    protected String dataPrevistaExecucao;
    @XmlElement(name = "IdsEncomenda", namespace = "http://factyfloor.com/OrdensProducao", required = true)
    protected List<String> idEncomenda = new ArrayList<>();

    /**
     * Gets the value of the idOrdemProducao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdOrdemProducao() {
        return idOrdemProducao;
    }

    /**
     * Sets the value of the idOrdemProducao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdOrdemProducao(String value) {
        this.idOrdemProducao = value;
    }

    /**
     * Gets the value of the estadoOrdemProducao property.
     * 
     * @return
     *     possible object is
     *     {@link TEstadoOrdemProducao }
     *     
     */
    public TEstadoOrdemProducao getEstadoOrdemProducao() {
        return estadoOrdemProducao;
    }

    /**
     * Sets the value of the estadoOrdemProducao property.
     * 
     * @param value
     *     allowed object is
     *     {@link TEstadoOrdemProducao }
     *     
     */
    public void setEstadoOrdemProducao(TEstadoOrdemProducao value) {
        this.estadoOrdemProducao = value;
    }

    /**
     * Gets the value of the codigoFabricoProduto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoFabricoProduto() {
        return codigoFabricoProduto;
    }

    /**
     * Sets the value of the codigoFabricoProduto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoFabricoProduto(String value) {
        this.codigoFabricoProduto = value;
    }

    /**
     * Gets the value of the quantidade property.
     * 
     * @return
     *     possible object is
     *     {@link TQuantidade }
     *     
     */
    public TQuantidade getQuantidade() {
        if(quantidade == null)
            quantidade = new TQuantidade();
        return quantidade;
    }

    /**
     * Sets the value of the quantidade property.
     * 
     * @param value
     *     allowed object is
     *     {@link TQuantidade }
     *     
     */
    public void setQuantidade(TQuantidade value) {
        this.quantidade = value;
    }

    /**
     * Gets the value of the dataEmissao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataEmissao() {
        return dataEmissao;
    }

    /**
     * Sets the value of the dataEmissao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataEmissao(String value) {
        this.dataEmissao = value;
    }

    /**
     * Gets the value of the dataPrevistaExecucao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataPrevistaExecucao() {
        return dataPrevistaExecucao;
    }

    /**
     * Sets the value of the dataPrevistaExecucao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataPrevistaExecucao(String value) {
        this.dataPrevistaExecucao = value;
    }

    /**
     * Gets the value of the idEncomenda property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public List<String> getIdEncomenda() {
        if(idEncomenda.isEmpty())
            idEncomenda = new ArrayList<>();
        return idEncomenda;
    }

    /**
     * Sets the value of the idEncomenda property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdEncomenda(List<String> value) {
        this.idEncomenda = value;
    }

}
