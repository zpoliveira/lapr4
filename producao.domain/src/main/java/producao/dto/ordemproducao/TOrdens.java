
package producao.dto.ordemproducao;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TOrdens complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TOrdens"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="OrdemProducao" type="{http://factyfloor.com/OrdensProducao}TOrdemProducao" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TOrdens", namespace = "http://factyfloor.com/OrdensProducao", propOrder = {
    "ordemProducao"
})
public class TOrdens {

    @XmlElement(name = "OrdemProducao", namespace = "http://factyfloor.com/OrdensProducao", required = true)
    protected List<TOrdemProducao> ordemProducao = new ArrayList<>();

    /**
     * Gets the value of the ordemProducao property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ordemProducao property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOrdemProducao().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TOrdemProducao }
     * 
     * 
     */
    public List<TOrdemProducao> getOrdemProducao() {
        if (ordemProducao == null) {
            ordemProducao = new ArrayList<>();
        }
        return this.ordemProducao;
    }

}
