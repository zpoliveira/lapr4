package producao.domain.repositories;

import producao.domain.model.ordemProducao.EstadoOrdemProducao;
import producao.domain.model.ordemProducao.IdentificadorOrdemProducao;
import producao.domain.model.ordemProducao.OrdemProducao;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public interface OrdemProducaoRepository {

    OrdemProducao findByIdentificadorOrdemProducao(IdentificadorOrdemProducao identificadorOrdemProducao);

    OrdemProducao save(OrdemProducao ordemProducao);
    Iterable<OrdemProducao> findByEstadoOrdemProducao(EstadoOrdemProducao estadoOrdemProducao);
    OrdemProducao findByPk(Long pk);
    Iterable<OrdemProducao> findByIdsEncomenda_IdEncomenda(String id);
    Iterable<OrdemProducao> findAllByDataPrevistaExecucaoBetween(LocalDate start, LocalDate end);
}
