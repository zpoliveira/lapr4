package producao.domain.repositories;

import producao.domain.model.materiaPrima.CategoriaMatPrima;

public interface CategoriaMatPrimaRepository {

    CategoriaMatPrima save(CategoriaMatPrima categoriaMatPrima);
    Iterable<CategoriaMatPrima> findAll();

}
