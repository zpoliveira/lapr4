package producao.domain.repositories;


import eapli.framework.domain.repositories.DomainRepository;

import producao.domain.model.produto.CodigoFabricoProduto;
import producao.domain.model.produto.Produto;

public interface ProdutoRepository {

    Produto findByPk(Long id);

    Produto findByCodigoFabricoProduto(CodigoFabricoProduto codigoFabricoProduto);

    Iterable<Produto> findByTemFichaProducaoFalse();

    Produto save(Produto produto);

    Iterable<Produto> findAll();
}
