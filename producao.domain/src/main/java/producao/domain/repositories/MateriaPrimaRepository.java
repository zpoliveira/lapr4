package producao.domain.repositories;

import producao.domain.model.materiaPrima.MateriaPrima;

public interface MateriaPrimaRepository{

    MateriaPrima save(MateriaPrima materiaPrima);
    Iterable<MateriaPrima> findAll();
}
