package producao.domain.repositories;

import producao.domain.model.produto.FichaProducao;

public interface FichaProducaoRepository {
    FichaProducao save(FichaProducao fichaProducao);
}
