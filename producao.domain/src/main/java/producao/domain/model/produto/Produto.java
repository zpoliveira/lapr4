package producao.domain.model.produto;

import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.representations.RepresentationBuilder;
import eapli.framework.representations.Representationable;
import eapli.framework.validations.Preconditions;
import org.apache.commons.lang3.builder.EqualsBuilder;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"codigoFabricoProduto"})})
public class Produto implements AggregateRoot<CodigoFabricoProduto>, Serializable, Representationable {

    private static final long serialVersionUID = 1L;

    @Version
    private Long version;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long pk;

    private CodigoComercial codigoComercial;

    private CodigoFabricoProduto codigoFabricoProduto;

    private DescricaoBreve descricaoBreve;

    private DescricaoCompleta descricaoCompleta;

    /**
     * Only used for the sake of simplicity and query performance
     */
    private boolean temFichaProducao = false;

    @OneToOne(cascade = CascadeType.REMOVE)
    private FichaProducao fichaProducao;


    /**
     * empty contructor for JPA/ORM to work
     */
    protected Produto() {
    }


    /**
     * creates a Product with or without FichaProducao in {@link ProdutoBuilder}
     *
     * @param codigoComercial
     * @param codigoFabricoProduto
     * @param descricaoBreve
     * @param descricaoCompleta
     * @param fichaProducao
     */
    protected Produto(CodigoComercial codigoComercial,
                      CodigoFabricoProduto codigoFabricoProduto,
                      DescricaoBreve descricaoBreve,
                      DescricaoCompleta descricaoCompleta,
                      FichaProducao fichaProducao) {
        Preconditions.noneNull(codigoComercial, codigoFabricoProduto, descricaoBreve, descricaoCompleta);
        this.codigoComercial = codigoComercial;
        this.codigoFabricoProduto = codigoFabricoProduto;
        this.descricaoBreve = descricaoBreve;
        this.descricaoCompleta = descricaoCompleta;
        this.fichaProducao = fichaProducao;
        if (fichaProducao != null) {
            this.temFichaProducao = true;
        }
    }

    public CodigoComercial codigoComercial() {
        return this.codigoComercial;
    }

    public DescricaoBreve descricaoBreve() {
        return this.descricaoBreve;
    }

    public boolean temFichaProducao() {
        return this.temFichaProducao;
    }

    public FichaProducao fichaProducao() {
        return this.fichaProducao;
    }

    public void adicionarFichaProducao(FichaProducao fichaProducao) {
        this.fichaProducao = fichaProducao;
        this.temFichaProducao = true;
    }

    @Override
    public <R> R buildRepresentation(RepresentationBuilder<R> builder) {

        builder.withProperty("codigoCom", this.codigoComercial.toString())
                .withProperty("codigoFab", this.codigoFabricoProduto.toString())
                .withProperty("descBreve", this.descricaoBreve.toString())
                .withProperty("descComp", this.descricaoCompleta.toString());


        if (this.temFichaProducao()) {
            builder.startObject("fichaProd")
                    .withProperty("quantidade-val", this.fichaProducao.quantidade().valorNumerico())
                    .withProperty("quantidade-un", this.fichaProducao.quantidade().unidade().label)
                    .endObject();
            this.fichaProducao.componenteMateriaPrima().forEach(componenteMateriaPrima -> {
                builder.startObject("compMatPrima")
                        .withProperty("compMatPrima-val", componenteMateriaPrima.codigoInternoMateriaPrima().codigoInternoMatPrima())
                        .withProperty("quantidade-val", componenteMateriaPrima.quantidade().valorNumerico())
                        .withProperty("quantidade-un", componenteMateriaPrima.quantidade().unidade().label)
                        .endObject();
            });
        }

        return builder.build();
    }


    @Override
    public boolean sameAs(Object other) {
        if (!(other instanceof Produto)) {
            return false;
        } else {
            Produto that = (Produto) other;
            return (new EqualsBuilder()).append(this.codigoFabricoProduto, that.codigoFabricoProduto).isEquals();
        }
    }

    @Override
    public int compareTo(CodigoFabricoProduto other) {
        return this.codigoFabricoProduto.compareTo(other);
    }

    @Override
    public CodigoFabricoProduto identity() {
        return this.codigoFabricoProduto;
    }

    @Override
    public boolean hasIdentity(CodigoFabricoProduto otherId) {
        return otherId.equals(this.codigoFabricoProduto);
    }

    @Override
    public int hashCode() {
        return this.codigoFabricoProduto.hashCode();
    }

    @Override
    public String toString() {
        return this.codigoFabricoProduto.toString() + "-" + this.descricaoBreve.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Produto produto = (Produto) o;
        return codigoFabricoProduto.equals(produto.codigoFabricoProduto);
    }


    /*public ProdutoDTO toDTO() {
        return new ProdutoDTO();
    }*/


}
