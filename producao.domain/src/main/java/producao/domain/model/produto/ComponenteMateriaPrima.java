package producao.domain.model.produto;

import eapli.framework.domain.model.ValueObject;
import eapli.framework.representations.dto.DTOable;
import producao.domain.model.materiaPrima.CodigoInternoMateriaPrima;
import producao.domain.model.Quantidade;
import producao.dto.produtos.TComponenteMateriaPrima;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Objects;

@Embeddable
@Access(AccessType.FIELD)
public class ComponenteMateriaPrima implements ValueObject, Serializable, DTOable<TComponenteMateriaPrima> {
    private static final long serialVersionUID = 1L;

    private CodigoInternoMateriaPrima codigoInternoMateriaPrima;
    private Quantidade quantidade;

    public ComponenteMateriaPrima() {
    }

    public ComponenteMateriaPrima(CodigoInternoMateriaPrima codigoInternoMateriaPrima, Quantidade quantidade) {
        this.codigoInternoMateriaPrima = codigoInternoMateriaPrima;
        this.quantidade = quantidade;
    }

    public CodigoInternoMateriaPrima codigoInternoMateriaPrima(){
        return this.codigoInternoMateriaPrima;
    }

    public Quantidade quantidade(){
        return this.quantidade;
    }

    /**
     * Returns a representation of the content of the object as a DTO.
     *
     * @return a representation of the content of the object as a DTO
     */
    @Override
    public TComponenteMateriaPrima toDTO() {

        TComponenteMateriaPrima tComponenteMateriaPrima = new TComponenteMateriaPrima();

        tComponenteMateriaPrima.setValue(this.codigoInternoMateriaPrima.codigoInternoMatPrima());
        tComponenteMateriaPrima.setQuantidade(BigInteger.valueOf(this.quantidade.valorNumerico()));
        tComponenteMateriaPrima.setUnidade(this.quantidade.unidade().toDTO());

        return tComponenteMateriaPrima;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ComponenteMateriaPrima that = (ComponenteMateriaPrima) o;
        return codigoInternoMateriaPrima.equals(that.codigoInternoMateriaPrima) &&
                quantidade.equals(that.quantidade);
    }

    @Override
    public int hashCode() {
        return Objects.hash(codigoInternoMateriaPrima, quantidade);
    }
}
