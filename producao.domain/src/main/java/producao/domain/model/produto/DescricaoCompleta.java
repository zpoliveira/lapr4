package producao.domain.model.produto;

import eapli.framework.domain.model.ValueObject;
import eapli.framework.util.HashCoder;
import eapli.framework.validations.Preconditions;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class DescricaoCompleta implements ValueObject, Serializable {
    private static final long serialVersionUID = 1L;
    private final static int MAX_LENGTH = 50;
    private String descricaoCompleta;


    public DescricaoCompleta() {
    }

    public DescricaoCompleta(String descricaoCompleta) {
        Preconditions.nonNull(descricaoCompleta);
        Preconditions.nonEmpty(descricaoCompleta);
        Preconditions.ensure(
                descricaoCompleta.length() <= MAX_LENGTH,
                "Código tem de ter menos de " + MAX_LENGTH + " caracteres"
        );
        this.descricaoCompleta = descricaoCompleta;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DescricaoCompleta)) {
            return false;
        }

        final DescricaoCompleta that = (DescricaoCompleta) o;
        return this.descricaoCompleta.equalsIgnoreCase(that.descricaoCompleta);
    }

    @Override
    public int hashCode() {
        return this.descricaoCompleta.hashCode();
    }

    @Override
    public String toString() {
        return  descricaoCompleta;
    }
}
