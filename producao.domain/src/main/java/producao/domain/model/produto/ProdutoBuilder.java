package producao.domain.model.produto;

import eapli.framework.domain.model.DomainFactory;
import eapli.framework.validations.Preconditions;

import javax.persistence.Column;
import javax.persistence.OneToOne;

public class ProdutoBuilder implements DomainFactory<Produto> {
    private final CodigoComercial codigoComercial;
    private final CodigoFabricoProduto codigoFabricoProduto;
    private final DescricaoBreve descricaoBreve;
    private final DescricaoCompleta descricaoCompleta;
    private FichaProducao fichaProducao;
    /**
     * Only used for sake of simplicity and query performance
     */
    private boolean temFichaProducao = false;

    public ProdutoBuilder(final CodigoComercial codigoComercial,
                          final CodigoFabricoProduto codigoFabricoProduto,
                          final DescricaoBreve descricaoBreve,
                          final DescricaoCompleta descricaoCompleta) {
        Preconditions.noneNull(codigoComercial, codigoFabricoProduto, descricaoBreve, descricaoCompleta);
        this.codigoComercial = codigoComercial;
        this.codigoFabricoProduto = codigoFabricoProduto;
        this.descricaoBreve = descricaoBreve;
        this.descricaoCompleta = descricaoCompleta;
    }

    public ProdutoBuilder addFichaProducao(final FichaProducao fichaProducao) {
        this.fichaProducao = fichaProducao;
        return this;
    }

    @Override
    public Produto build() {
        Produto produto = new Produto(
                this.codigoComercial,
                this.codigoFabricoProduto,
                this.descricaoBreve,
                this.descricaoCompleta,
                this.fichaProducao);
        return produto;
    }
}
