package producao.domain.model.produto;

import eapli.framework.domain.model.ValueObject;
import eapli.framework.validations.Preconditions;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class DescricaoBreve implements ValueObject, Serializable {
    private static final long serialVersionUID = 1L;
    private String descricaoBreve;
    private final static int MAX_LENGTH = 30;

    public DescricaoBreve() {
    }

    public DescricaoBreve(String descricaoBreve) {
        Preconditions.nonNull(descricaoBreve);
        Preconditions.nonEmpty(descricaoBreve);
        Preconditions.ensure(
                descricaoBreve.length() <= MAX_LENGTH,
                "Código tem de ter menos de " + MAX_LENGTH + " caracteres"
        );
        this.descricaoBreve = descricaoBreve;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DescricaoBreve that = (DescricaoBreve) o;
        return descricaoBreve.equals(that.descricaoBreve);
    }

    @Override
    public int hashCode() {
        return Objects.hash(descricaoBreve);
    }

    @Override
    public String toString() {
        return descricaoBreve;
    }
}
