package producao.domain.model.produto;

import eapli.framework.domain.model.ValueObject;
import eapli.framework.validations.Preconditions;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class CodigoComercial implements ValueObject, Serializable {
    private static final long serialVersionUID = 1L;
    private String codigoComercial;
    private final static int MAX_LENGTH = 15;

    protected CodigoComercial() {
    }

    public CodigoComercial(String codigoComercial) {
        Preconditions.nonNull(codigoComercial);
        Preconditions.nonEmpty(codigoComercial);
        Preconditions.ensure(
                codigoComercial.length() <= MAX_LENGTH,
                "Código tem de ter menos de " + MAX_LENGTH + " caracteres"
        );
        this.codigoComercial = codigoComercial;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CodigoComercial that = (CodigoComercial) o;
        return codigoComercial.equals(that.codigoComercial);
    }

    @Override
    public int hashCode() {
        return Objects.hash(codigoComercial);
    }

    @Override
    public String toString() {
        return codigoComercial;
    }
}
