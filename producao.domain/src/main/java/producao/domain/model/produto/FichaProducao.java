package producao.domain.model.produto;

import eapli.framework.validations.Preconditions;
import producao.domain.model.Quantidade;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
public class FichaProducao implements Serializable {

    private static final long serialVersionUID = 1L;

    @Version
    private Long version;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long pk;

    private Quantidade quantidadeProduto;

    @ElementCollection(targetClass = ComponenteMateriaPrima.class, fetch = FetchType.EAGER)
    //@CollectionTable(name="fichaProducao")
    private Set<ComponenteMateriaPrima> componenteMateriaPrima = new HashSet<>();

    protected FichaProducao() {
    }

    public FichaProducao(Quantidade quantidadeProduto, Set<ComponenteMateriaPrima> componenteMateriaPrima) {
        Preconditions.nonNull(quantidadeProduto);
        Preconditions.nonNull(componenteMateriaPrima);
        Preconditions.nonEmpty(componenteMateriaPrima);
        this.quantidadeProduto = quantidadeProduto;
        this.componenteMateriaPrima = componenteMateriaPrima;
    }

    public Quantidade quantidade() {
        return this.quantidadeProduto;
    }

    public Set<ComponenteMateriaPrima> componenteMateriaPrima() {
        return this.componenteMateriaPrima;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FichaProducao that = (FichaProducao) o;
        return componenteMateriaPrima.equals(that.componenteMateriaPrima);
    }

    @Override
    public int hashCode() {
        return Objects.hash(componenteMateriaPrima);
    }
}
