package producao.domain.model.produto;

import eapli.framework.domain.model.ValueObject;
import eapli.framework.validations.Preconditions;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class CodigoFabricoProduto implements ValueObject, Serializable, Comparable<CodigoFabricoProduto>{
    private static final long serialVersionUID = 1L;
    private String codigoFabricoProduto;
    private final static int MAX_LENGTH = 15;

    protected CodigoFabricoProduto() {
    }

    public CodigoFabricoProduto(String codigoFabricoProduto) {
        Preconditions.nonNull(codigoFabricoProduto);
        Preconditions.nonEmpty(codigoFabricoProduto);
        Preconditions.ensure(
                codigoFabricoProduto.length() <= MAX_LENGTH,
                "Código tem de ter menos de " + MAX_LENGTH + " caracteres"
        );
        this.codigoFabricoProduto = codigoFabricoProduto;
    }

    @Override
    public int compareTo(CodigoFabricoProduto codigoFabricoProduto) {
        return this.codigoFabricoProduto.compareTo(codigoFabricoProduto.codigoFabricoProduto);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CodigoFabricoProduto that = (CodigoFabricoProduto) o;
        return codigoFabricoProduto.equals(that.codigoFabricoProduto);
    }

    @Override
    public int hashCode() {
        return Objects.hash(codigoFabricoProduto);
    }

    @Override
    public String toString() {
        return codigoFabricoProduto;
    }
}
