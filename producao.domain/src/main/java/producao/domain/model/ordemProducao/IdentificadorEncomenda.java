package producao.domain.model.ordemProducao;

import eapli.framework.domain.model.ValueObject;
import eapli.framework.validations.Preconditions;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Access(AccessType.FIELD)
public class IdentificadorEncomenda implements ValueObject, Serializable, Comparable<IdentificadorEncomenda>{

    private static final long serialVersionUID = 1L;

    private String idEncomenda;

    public IdentificadorEncomenda() {
    }

    public IdentificadorEncomenda(String idEncomenda) {
        Preconditions.nonNull(idEncomenda);
        Preconditions.nonEmpty(idEncomenda);
        Preconditions.ensure(idEncomenda.length() <= 15, "Identificador de Encomenda tem de ter menos de 15 caracteres");
        Preconditions.ensure(idEncomenda.length() >= 1, "Identificador de Encomenda tem de ter no mínimo 1 caracter");

        this.idEncomenda = idEncomenda;
    }

    public String identificadorEncomenda() {
        return idEncomenda;
    }

    @Override
    public int compareTo(IdentificadorEncomenda o) {
        return this.idEncomenda.compareTo(o.idEncomenda);
    }

    @Override
    public String toString() {
        return "Identificador de Encomenda: " + idEncomenda;
    }

    @Override
    public int hashCode() {
        return this.idEncomenda.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        // If the object is compared with itself then return true
        if (obj == this) {
            return true;
        }

        /* Check if o is an instance of Complex or not
          "null instanceof [type]" also returns false */
        if (!(obj instanceof IdentificadorEncomenda)) {
            return false;
        }

        // Compare the data members and return accordingly
        return this.idEncomenda.equalsIgnoreCase(((IdentificadorEncomenda) obj).idEncomenda);
    }
}
