package producao.domain.model.ordemProducao;

import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.representations.RepresentationBuilder;
import eapli.framework.representations.Representationable;
import eapli.framework.validations.Preconditions;
import org.apache.commons.lang3.builder.EqualsBuilder;
import producao.domain.model.Quantidade;
import producao.domain.model.produto.CodigoFabricoProduto;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAttribute;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"idOrdemProducao"})})
public class OrdemProducao implements AggregateRoot<IdentificadorOrdemProducao>, Serializable, Representationable {

    private static final long serialVersionUID = 1L;

    @Version
    private Long version;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long pk;
    @Enumerated(EnumType.ORDINAL)
    private EstadoOrdemProducao estadoOrdemProducao;
    @Embedded
    @Column(unique = true, nullable = false, name = "idOrdemProducao")
    private IdentificadorOrdemProducao identificadorOrdemProducao;
    @Embedded
    private CodigoFabricoProduto codigoFabricoProduto;
    @Embedded
    private Quantidade quantidade;
    @Column(columnDefinition = "DATE")
    private LocalDate dataEmissao;
    @Column(columnDefinition = "DATE")
    private LocalDate dataPrevistaExecucao;
    @ElementCollection(targetClass = IdentificadorEncomenda.class, fetch = FetchType.EAGER)
    private Set<IdentificadorEncomenda> idsEncomenda = new HashSet<>();

    protected OrdemProducao() {
    }

    public OrdemProducao(IdentificadorOrdemProducao identificadorOrdemProducao, CodigoFabricoProduto codigoFabricoProduto,
                         Quantidade quantidade, LocalDate dataEmissao, LocalDate dataPrevistaExecucao,
                         Set<IdentificadorEncomenda> idsEncomenda) {
        Preconditions.noneNull(identificadorOrdemProducao, codigoFabricoProduto, quantidade, dataEmissao,
                dataPrevistaExecucao, idsEncomenda);
        this.identificadorOrdemProducao = identificadorOrdemProducao;
        this.codigoFabricoProduto = codigoFabricoProduto;
        this.quantidade = quantidade;
        this.dataEmissao = dataEmissao;
        this.dataPrevistaExecucao = dataPrevistaExecucao;
        this.idsEncomenda = idsEncomenda;
        this.estadoOrdemProducao = EstadoOrdemProducao.PENDENTE;
    }

    public EstadoOrdemProducao estadoOrdemProducao() {
        return this.estadoOrdemProducao;
    }

    public CodigoFabricoProduto codigoFabricoProduto() {
        return this.codigoFabricoProduto;
    }

    public Quantidade quantidade() {
        return this.quantidade;
    }

    public void mudaEstadoParaPendente() {
        this.estadoOrdemProducao = EstadoOrdemProducao.PENDENTE;
    }

    public void mudaEstadoParaExecucao() {
        this.estadoOrdemProducao = EstadoOrdemProducao.EXECUCAO;
    }

    public void mudaEstadoParaConcluida() {
        this.estadoOrdemProducao = EstadoOrdemProducao.CONCLUIDA;
    }

    public void mudaEstadoParaInterrompida() {
        this.estadoOrdemProducao = EstadoOrdemProducao.INTERROMPIDA;
    }

    public void mudaEstadoParaParada() {
        this.estadoOrdemProducao = EstadoOrdemProducao.PARADA;
    }

    @Override
    public boolean sameAs(Object other) {
        if (!(other instanceof OrdemProducao)) {
            return false;
        } else {
            OrdemProducao that = (OrdemProducao) other;
            return (new EqualsBuilder()).append(this.identificadorOrdemProducao, that.identificadorOrdemProducao).isEquals();
        }
    }

    @Override
    public IdentificadorOrdemProducao identity() {
        return this.identificadorOrdemProducao;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrdemProducao that = (OrdemProducao) o;
        return identificadorOrdemProducao.equals(that.identificadorOrdemProducao);
    }

    @Override
    public int hashCode() {
        return Objects.hash(identificadorOrdemProducao);
    }

    @Override
    public String toString() {
        return identificadorOrdemProducao +
                " | Estado: " + estadoOrdemProducao +
                " | Código de Fabrico do Produto: " + codigoFabricoProduto +
                " | " + quantidade +
                " | Data de Emissao: " + dataEmissao +
                " | Data Prevista de Execucao: " + dataPrevistaExecucao +
                " | Encomenda(s): " + idsEncomenda.toString();
    }

    @Override
    public <R> R buildRepresentation(RepresentationBuilder<R> builder) {
        builder.withProperty("idordem", this.identificadorOrdemProducao.identificadorOrdemProducao())
                .withProperty("estado", this.estadoOrdemProducao.label)
                .withProperty("codigoproduto", this.codigoFabricoProduto.toString())
                .withProperty("quantidade-val", this.quantidade.valorNumerico())
                .withProperty("quantidade-un", this.quantidade.unidade().label)
                .withProperty("dataemissao", this.dataEmissao.toString())
                .withProperty("dataprevisao", this.dataPrevistaExecucao.toString())
                .startObject("encomendas");
        this.idsEncomenda.forEach(identificadorEncomenda ->
                builder.withProperty("idencomenda", identificadorEncomenda.identificadorEncomenda())
        );
        builder.endObject();
        return builder.build();
    }
}
