package producao.domain.model.ordemProducao;

import eapli.framework.domain.model.ValueObject;
import eapli.framework.validations.Preconditions;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class IdentificadorOrdemProducao implements ValueObject, Serializable, Comparable<IdentificadorOrdemProducao>{

    private static final long serialVersionUID = 1L;

    private String idOrdemProducao;

    public IdentificadorOrdemProducao() {
    }

    public IdentificadorOrdemProducao(String idOrdemProducao) {
        Preconditions.nonNull(idOrdemProducao, "Id ordem de produção não pode ser null");
        Preconditions.nonEmpty(idOrdemProducao, "Id ordem de produção não pode ser vazio");
        Preconditions.ensure(idOrdemProducao.length() <= 15, "Identificador de Ordem de Produção tem de ter menos de 15 caracteres");
        Preconditions.ensure(idOrdemProducao.length() >= 1, "Identificador de Ordem de Produção tem de ter no mínimo 1 caracter");

        this.idOrdemProducao = idOrdemProducao;
    }

    public String identificadorOrdemProducao() {
        return idOrdemProducao;
    }

    @Override
    public int compareTo(IdentificadorOrdemProducao o) {
        return this.idOrdemProducao.compareTo(o.idOrdemProducao);
    }

    @Override
    public String toString() {
        return "Identificador de Ordem de Producao: " + idOrdemProducao;
    }

    @Override
    public int hashCode() {
        return this.idOrdemProducao.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        // If the object is compared with itself then return true
        if (obj == this) {
            return true;
        }

        /* Check if o is an instance of Complex or not
          "null instanceof [type]" also returns false */
        if (!(obj instanceof IdentificadorOrdemProducao)) {
            return false;
        }

        // Compare the data members and return accordingly
        return this.idOrdemProducao.equalsIgnoreCase(((IdentificadorOrdemProducao) obj).idOrdemProducao);
    }
}
