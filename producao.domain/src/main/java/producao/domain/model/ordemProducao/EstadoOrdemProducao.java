package producao.domain.model.ordemProducao;

public enum EstadoOrdemProducao {
    PENDENTE("Pendente"),
    EXECUCAO("Em Execução"),
    PARADA("Execução Parada Temporariamente"),
    CONCLUIDA("Concluída"),
    INTERROMPIDA("Interrompida");

    public final String label;

    private EstadoOrdemProducao(String label) {
        this.label = label;
    }
}
