package producao.domain.model;

import eapli.framework.domain.model.ValueObject;
import eapli.framework.representations.dto.DTOable;
import eapli.framework.validations.Preconditions;
import producao.dto.communtypes.TQuantidade;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Objects;


@Embeddable
@Access(AccessType.FIELD)
public class Quantidade implements ValueObject, Serializable, DTOable<TQuantidade> {

    private long quantidade;

    @Enumerated(EnumType.ORDINAL)
    private Unidade unidade;

    public Quantidade() {
    }


    public Quantidade(Long quantidade, Unidade unidade) {
        Preconditions.noneNull(quantidade, unidade);
        Preconditions.isPositive(quantidade);
        this.quantidade = quantidade;
        this.unidade = unidade;
    }

    public long valorNumerico() {
        return this.quantidade;
    }

    public Unidade unidade(){
        return this.unidade;
    }

    /**
     * Returns a representation of the content of the object as a DTO.
     *
     * @return a representation of the content of the object as a DTO
     */
    @Override
    public TQuantidade toDTO() {
        TQuantidade tQuantidade = new TQuantidade();

        tQuantidade.setUnidade(unidade.toDTO());
        tQuantidade.setValue(BigInteger.valueOf(quantidade));

        return tQuantidade;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Quantidade that = (Quantidade) o;
        return quantidade == that.quantidade &&
                unidade.equals(that.unidade);
    }

    @Override
    public int hashCode() {
        return Objects.hash(quantidade, unidade);
    }

    @Override
    public String toString() {
        return "Quantidade: " + quantidade + unidade;
    }
}
