package producao.domain.model.materiaPrima;

import eapli.framework.domain.model.ValueObject;
import eapli.framework.validations.Preconditions;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Access(AccessType.FIELD)
public class CodigoInternoMateriaPrima implements ValueObject, Serializable, Comparable<CodigoInternoMateriaPrima> {

    private static final long serialVersionUID = 1L;

    private String codigoInternoMatPrima;

    public CodigoInternoMateriaPrima() {
    }

    public CodigoInternoMateriaPrima(String codigoInternoMatPrima) {
        Preconditions.nonNull(codigoInternoMatPrima);
        Preconditions.nonEmpty(codigoInternoMatPrima);
        Preconditions.ensure(codigoInternoMatPrima.length() <= 15, "Código Interno tem de ter menos de 15 caracteres");
        Preconditions.ensure(codigoInternoMatPrima.length() >= 1, "Código Interno tem de ter no mínimo 1 caracter");

        this.codigoInternoMatPrima = codigoInternoMatPrima;
    }

    public String codigoInternoMatPrima() {
        return codigoInternoMatPrima;
    }

    @Override
    public int compareTo(CodigoInternoMateriaPrima o) {
        return this.codigoInternoMatPrima.compareTo(o.codigoInternoMatPrima);
    }

    @Override
    public String toString() {
        return "CodigoInternoMateriaPrima: " + codigoInternoMatPrima;
    }

    @Override
    public int hashCode() {
        return this.codigoInternoMatPrima.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        // If the object is compared with itself then return true
        if (obj == this) {
            return true;
        }

        /* Check if o is an instance of Complex or not
          "null instanceof [type]" also returns false */
        if (!(obj instanceof CodigoInternoMateriaPrima)) {
            return false;
        }

        // Compare the data members and return accordingly
        return this.codigoInternoMatPrima.equalsIgnoreCase(((CodigoInternoMateriaPrima) obj).codigoInternoMatPrima);
    }
}
