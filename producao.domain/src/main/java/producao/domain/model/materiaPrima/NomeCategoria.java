package producao.domain.model.materiaPrima;

import eapli.framework.domain.model.ValueObject;
import eapli.framework.validations.Preconditions;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class NomeCategoria implements ValueObject, Serializable {

    private String nomeCategoria;

    protected NomeCategoria() {
        // for ORM only
        nomeCategoria = null;
    }

    public NomeCategoria(final String nomeCategoria) {
        Preconditions.nonNull(nomeCategoria);
        Preconditions.nonEmpty(nomeCategoria);
        Preconditions.ensure(nomeCategoria.length() <= 15, "Nome de Categoria tem de ter menos de 15 caracteres");


        this.nomeCategoria = nomeCategoria;
    }

    @Override
    public int hashCode() {
        return this.nomeCategoria.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        // If the object is compared with itself then return true
        if (obj == this) {
            return true;
        }

        /* Check if o is an instance of Complex or not
          "null instanceof [type]" also returns false */
        if (!(obj instanceof NomeCategoria)) {
            return false;
        }

        // Compare the data members and return accordingly
        return this.nomeCategoria.equalsIgnoreCase(((NomeCategoria) obj).nomeCategoria);

    }



    @Override
    public String toString() {
        return this.nomeCategoria;
    }
}
