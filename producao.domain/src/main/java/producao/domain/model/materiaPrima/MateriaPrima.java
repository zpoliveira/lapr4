package producao.domain.model.materiaPrima;

import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.representations.RepresentationBuilder;
import eapli.framework.representations.Representationable;
import eapli.framework.validations.Preconditions;
import org.apache.commons.lang3.builder.EqualsBuilder;

import javax.persistence.*;

@Entity
public class MateriaPrima implements AggregateRoot<CodigoInternoMateriaPrima>, Representationable {

    @Version
    private Long version;

    @EmbeddedId
    private CodigoInternoMateriaPrima codigoInternoMateriaPrima;
    @OneToOne(cascade = CascadeType.MERGE)
    private CategoriaMatPrima categoriaMatPrima;
    @Embedded
    private DescricaoMateriaPrima descricaoMateriaPrima;
    @Embedded
    private FichaTecnica fichaTecnica;

    protected MateriaPrima() {
    }

    public MateriaPrima(final CodigoInternoMateriaPrima codigoInternoMateriaPrima, final CategoriaMatPrima categoriaMatPrima,
                        final DescricaoMateriaPrima descricaoMateriaPrima, final FichaTecnica fichaTecnica) {
        Preconditions.nonNull(codigoInternoMateriaPrima);
        Preconditions.nonNull(categoriaMatPrima);
        Preconditions.nonNull(descricaoMateriaPrima);
        Preconditions.nonNull(fichaTecnica);
        this.codigoInternoMateriaPrima = codigoInternoMateriaPrima;
        this.categoriaMatPrima = categoriaMatPrima;
        this.descricaoMateriaPrima = descricaoMateriaPrima;
        this.fichaTecnica = fichaTecnica;
    }

    public CategoriaMatPrima categoria() {
        return categoriaMatPrima;
    }

    @Override
    public boolean sameAs(Object other) {
        if (!(other instanceof MateriaPrima)) {
            return false;
        } else {
            MateriaPrima that = (MateriaPrima) other;
            return (new EqualsBuilder()).append(this.codigoInternoMateriaPrima, that.codigoInternoMateriaPrima).isEquals();
        }
    }

    @Override
    public int compareTo(CodigoInternoMateriaPrima other) {
        return this.codigoInternoMateriaPrima.codigoInternoMatPrima().compareTo(other.codigoInternoMatPrima());
    }

    @Override
    public CodigoInternoMateriaPrima identity() {
        return this.codigoInternoMateriaPrima;
    }

    @Override
    public boolean hasIdentity(CodigoInternoMateriaPrima otherId) {
        return otherId.codigoInternoMatPrima().equalsIgnoreCase(this.codigoInternoMateriaPrima.codigoInternoMatPrima());
    }

    @Override
    public int hashCode() {
        return this.codigoInternoMateriaPrima.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        // If the object is compared with itself then return true
        if (obj == this) {
            return true;
        }

        /* Check if o is an instance of Complex or not
          "null instanceof [type]" also returns false */
        if (!(obj instanceof MateriaPrima)) {
            return false;
        }

        // Compare the data members and return accordingly
        return this.codigoInternoMateriaPrima.toString().equalsIgnoreCase(((MateriaPrima) obj).codigoInternoMateriaPrima.toString());
    }

    @Override
    public String toString() {
        return "MateriaPrima: " + codigoInternoMateriaPrima +
                " - " + categoriaMatPrima +
                " - " + descricaoMateriaPrima;
    }

    @Override
    public <R> R buildRepresentation(RepresentationBuilder<R> builder) {
        builder.withProperty("codigo", this.codigoInternoMateriaPrima.codigoInternoMatPrima())
                .withProperty("descricao", this.descricaoMateriaPrima.DescricaoMateriaPrima())
                .withProperty("fichaTec", this.codigoInternoMateriaPrima.codigoInternoMatPrima() + ".pdf")
                .withProperty("nomeCat", this.categoriaMatPrima.identity().toString());
        return builder.build();
    }
}
