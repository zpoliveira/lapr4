package producao.domain.model.materiaPrima;

import eapli.framework.domain.model.ValueObject;
import eapli.framework.validations.Preconditions;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class DescricaoMateriaPrima implements ValueObject, Serializable{

    private static final long serialVersionUID = 1L;

    private final String descricaoMateriaPrima;

    protected DescricaoMateriaPrima() {
        // for ORM only
        descricaoMateriaPrima = null;
    }

    public DescricaoMateriaPrima(final String descricaoMateriaPrima) {
        Preconditions.nonNull(descricaoMateriaPrima);
        Preconditions.nonEmpty(descricaoMateriaPrima);
        Preconditions.ensure(descricaoMateriaPrima.length() <= 50, "Descrição tem de ter menos de 50 caracteres");
        Preconditions.ensure(descricaoMateriaPrima.length() >= 1, "Descrição tem de ter no mínimo 1 caracter");
        this.descricaoMateriaPrima = descricaoMateriaPrima;
    }

    public String DescricaoMateriaPrima() {
        return descricaoMateriaPrima;
    }
}
