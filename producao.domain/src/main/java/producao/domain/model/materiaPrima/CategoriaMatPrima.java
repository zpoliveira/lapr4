package producao.domain.model.materiaPrima;

import eapli.framework.general.domain.model.Designation;
import eapli.framework.representations.RepresentationBuilder;
import eapli.framework.representations.Representationable;
import eapli.framework.validations.Preconditions;
import org.apache.commons.lang3.builder.EqualsBuilder;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(uniqueConstraints =
@UniqueConstraint(columnNames = "nomeCategoria"))
public class CategoriaMatPrima implements Serializable, Representationable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long pk;

    @Column(nullable = false, name = "nomeCategoria")
    @Embedded
    private NomeCategoria nomeCategoria;

    protected CategoriaMatPrima() {
    }

    public CategoriaMatPrima(final NomeCategoria nomeCategoria) {
        Preconditions.nonNull(nomeCategoria);
        Preconditions.ensure(nomeCategoria.toString().length() <= 15, "Nome da Categoria tem de ter menos de 15 caracteres");
        Preconditions.ensure(nomeCategoria.toString().length() >= 1, "Nome da categoria tem de ter no mínimo 1 caracter");
        this.nomeCategoria = nomeCategoria;
    }

    public NomeCategoria identity() {
        return this.nomeCategoria;
    }

    public boolean hasIdentity(final Designation id) {
        return id.toString().equalsIgnoreCase(this.nomeCategoria.toString());
    }

    public boolean sameAs(final Object other) {
        if (!(other instanceof CategoriaMatPrima)) {
            return false;
        } else {
            CategoriaMatPrima that = (CategoriaMatPrima) other;
            return (new EqualsBuilder()).append(this.nomeCategoria, that.nomeCategoria).isEquals();
        }
    }

    public String toString() {
        return this.nomeCategoria.toString();
    }


    @Override
    public int hashCode() {
        return this.nomeCategoria.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        // If the object is compared with itself then return true
        if (obj == this) {
            return true;
        }

        /* Check if o is an instance of Complex or not
          "null instanceof [type]" also returns false */
        if (!(obj instanceof CategoriaMatPrima)) {
            return false;
        }

        // Compare the data members and return accordingly
        return this.nomeCategoria.toString().equalsIgnoreCase(((CategoriaMatPrima) obj).nomeCategoria.toString());
    }


    @Override
    public <R> R buildRepresentation(RepresentationBuilder<R> builder) {

        builder.withProperty("nome", this.nomeCategoria.toString());

        return builder.build();
    }
}

