package producao.domain.model.materiaPrima;

import eapli.framework.domain.model.ValueObject;
import eapli.framework.representations.dto.DTOable;
import eapli.framework.validations.Preconditions;
import org.apache.commons.io.FileUtils;
import producao.dto.communtypes.TFileBlob;

import javax.persistence.*;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.Arrays;

@Embeddable
@Access(AccessType.FIELD)
public class FichaTecnica implements ValueObject, Serializable, DTOable<TFileBlob> {

    private static final long serialVersionUID = 1L;

    @Lob
    @Column(name = "FichaTecnica", columnDefinition = "BLOB")
    private byte[] FichaTecnica = new byte[0];


    protected FichaTecnica() {
        // for ORM only
    }

    public FichaTecnica(final File ft) {
        Preconditions.nonNull(ft);

        try {
            this.FichaTecnica = FileUtils.readFileToByteArray(ft);
        } catch (IOException e) {
            System.out.println("Unable to convert file to byte array.");
            e.getMessage();
        }

    }

    /**
     * Returns a representation of the content of the object as a DTO.
     *
     * @return a representation of the content of the object as a DTO
     */
    @Override
    public TFileBlob toDTO() {
        TFileBlob tFileBlob = new TFileBlob();

        tFileBlob.setHasFile(FichaTecnica.length > 0);
        tFileBlob.setValue(Arrays.toString(FichaTecnica.clone()));

        return tFileBlob;
    }

    public byte[] FichaTecnica() {
        return FichaTecnica;
    }

}
