package producao.domain.model;

import eapli.framework.representations.dto.DTOable;
import producao.dto.communtypes.TUnidade;

public enum Unidade implements DTOable<TUnidade> {
    KG("Kg"),
    UN("Un"),
    L("L"),
    TON("ton");

    public final String label;


    private Unidade(String label) {
        this.label = label;
    }

    /**
     * Returns a representation of the content of the object as a DTO.
     *
     * @return a representation of the content of the object as a DTO
     */
    @Override
    public TUnidade toDTO() {
        return TUnidade.fromValue(label);
    }
}
