package producao.domain.model.materiaPrima;


import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

public class MateriaPrimaTest {


    @Test(expected = IllegalArgumentException.class)
    public void testCodigoInternoNull() {

        MateriaPrima materiaPrimaPrima = new MateriaPrima(null, new CategoriaMatPrima(new NomeCategoria("Vidro")),
                new DescricaoMateriaPrima("Desc"), new FichaTecnica(new File("dummy")));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCodigoInternoUnico() {

        Set<MateriaPrima> setCat = new HashSet<>();

        MateriaPrima mat1 = new MateriaPrima(new CodigoInternoMateriaPrima("mat1"),
                new CategoriaMatPrima(new NomeCategoria("Vidro")), new DescricaoMateriaPrima("desc"),
                new FichaTecnica(new File("dummy")));
        MateriaPrima mat2 = new MateriaPrima(new CodigoInternoMateriaPrima("mat1"),
                new CategoriaMatPrima(new NomeCategoria("Ferro")), new DescricaoMateriaPrima("desc2"),
                new FichaTecnica(new File("dummy2")));
        setCat.add(mat1);
        if(setCat.contains(mat2)){
            throw new IllegalArgumentException("Categoria existente");
        }

    }

    @Test
    public void testCodigoInternoUnico2() {

        Set<MateriaPrima> setCat = new HashSet<>();

        MateriaPrima mat1 = new MateriaPrima(new CodigoInternoMateriaPrima("mat1"),
                new CategoriaMatPrima(new NomeCategoria("Vidro")), new DescricaoMateriaPrima("desc"),
                new FichaTecnica(new File("dummy")));
        MateriaPrima mat2 = new MateriaPrima(new CodigoInternoMateriaPrima("mat2"),
                new CategoriaMatPrima(new NomeCategoria("Ferro")), new DescricaoMateriaPrima("desc2"),
                new FichaTecnica(new File("dummy2")));
        setCat.add(mat1);
        if(setCat.contains(mat2)){
            throw new IllegalArgumentException("Categoria existente");
        }else{
            setCat.add(mat2);
        }

        Assert.assertNotEquals(mat1,mat2);
        Assert.assertEquals(2,setCat.size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCategoriaMatPrimaNull() {
        MateriaPrima materiaPrimaPrima = new MateriaPrima(new CodigoInternoMateriaPrima("V1"), null,
                new DescricaoMateriaPrima("Desc"), new FichaTecnica(new File("dummy")));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCategoriaMatPrimaNãoExiste() {

        Set<CategoriaMatPrima> setCat = new HashSet<>();
        CategoriaMatPrima V1 = new CategoriaMatPrima(new NomeCategoria("Vidro"));
        setCat.add(V1);

        MateriaPrima mat1 = new MateriaPrima(new CodigoInternoMateriaPrima("mat1"),
                new CategoriaMatPrima(new NomeCategoria("V2")), new DescricaoMateriaPrima("desc"),
                new FichaTecnica(new File("dummy")));

        if(!setCat.contains(mat1.categoria())){
            throw new IllegalArgumentException("Categoria existente");
        }

    }

    @Test(expected = IllegalArgumentException.class)
    public void testDescricaoMatPrimaNull() {
        MateriaPrima materiaPrimaPrima = new MateriaPrima(new CodigoInternoMateriaPrima("V1"),
                new CategoriaMatPrima(new NomeCategoria("Vidro")), null,
                new FichaTecnica(new File("dummy")));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFichaTecnicaNull() {

        MateriaPrima materiaPrimaPrima = new MateriaPrima(new CodigoInternoMateriaPrima("V1"),
                new CategoriaMatPrima(new NomeCategoria("Vidro")), new DescricaoMateriaPrima("Desc"), null);
    }
}