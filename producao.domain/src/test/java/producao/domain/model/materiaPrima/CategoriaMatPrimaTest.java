package producao.domain.model.materiaPrima;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.security.InvalidParameterException;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;



public class CategoriaMatPrimaTest {


    @Test(expected = IllegalArgumentException.class)
    public void testNomeCategoriaNull() {

        CategoriaMatPrima categoriaMatPrima = new CategoriaMatPrima(null);
    }


    @Test(expected = IllegalArgumentException.class)
    public void testNomeCategoriaUnico() {

        Set<CategoriaMatPrima> setCat = new HashSet<>();

        CategoriaMatPrima vidro = new CategoriaMatPrima(new NomeCategoria("Vidro"));
        CategoriaMatPrima vidro2 = new CategoriaMatPrima(new NomeCategoria("Vidro"));
        setCat.add(vidro);
        if(setCat.contains(vidro2)){
            throw new IllegalArgumentException("Categoria existente");
        }

    }

    @Test
    public void testNomeCategoriaUnico2() {

        Set<CategoriaMatPrima> setCat = new HashSet<>();

        CategoriaMatPrima vidro = new CategoriaMatPrima(new NomeCategoria("Vidro"));
        CategoriaMatPrima vidro2 = new CategoriaMatPrima(new NomeCategoria("Vidro2"));
        setCat.add(vidro);
        if(setCat.contains(vidro2)){
            throw new IllegalArgumentException("Categoria existente");
        }else{
            setCat.add(vidro2);
        }

        Assert.assertNotEquals(vidro,vidro2);
        Assert.assertEquals(2,setCat.size());
    }
}