package producao.domain.model;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class QuantidadeTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test(expected = IllegalArgumentException.class)
    public void testQuantidadeNotNull(){
        Quantidade q = new Quantidade(null, Unidade.KG);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testQuantidadeIsPositive(){
        Quantidade q = new Quantidade(0l, Unidade.KG);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUnidadeNotNull(){
        Quantidade q = new Quantidade(1l, null);
    }

}