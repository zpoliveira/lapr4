package producao.domain.model.produto;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import producao.domain.model.Quantidade;
import producao.domain.model.Unidade;
import producao.domain.model.materiaPrima.CodigoInternoMateriaPrima;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class FichaProducaoTest {

    Set<ComponenteMateriaPrima> componenteMateriaPrimas = new HashSet<>();
    CodigoInternoMateriaPrima codigoInternoMateriaPrima = new CodigoInternoMateriaPrima("cimp");
    Quantidade quantidade = new Quantidade(2l, Unidade.KG);
    ComponenteMateriaPrima componenteMateriaPrima = new ComponenteMateriaPrima(codigoInternoMateriaPrima, quantidade);

    @Before
    public void setUp() throws Exception {
        codigoInternoMateriaPrima = new CodigoInternoMateriaPrima("cimp");
        quantidade = new Quantidade(2l, Unidade.KG);
        componenteMateriaPrima = new ComponenteMateriaPrima(codigoInternoMateriaPrima, quantidade);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test(expected = IllegalArgumentException.class)
    public void testQuantidadeProdutoNotNull(){
        FichaProducao fp =  new FichaProducao(null, componenteMateriaPrimas);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testComponenteMateriaPrimasNotNull(){
        FichaProducao fp =  new FichaProducao(quantidade, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testComponenteMateriaPrimasNotEmpty(){
        Set<ComponenteMateriaPrima> componenteMateriaPrimas = new HashSet<>();
        FichaProducao fp =  new FichaProducao(quantidade, componenteMateriaPrimas);
    }

}