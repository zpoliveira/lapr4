package producao.domain.model.produto;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import producao.domain.model.Quantidade;
import producao.domain.model.Unidade;
import producao.domain.model.materiaPrima.CodigoInternoMateriaPrima;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class ProdutoTest {
    FichaProducao F2;

    @Before
    public void setUp() throws Exception {
        Set<ComponenteMateriaPrima> CMP7 = new HashSet<>();
        ComponenteMateriaPrima C3 = new ComponenteMateriaPrima(new CodigoInternoMateriaPrima("A1"),
                new Quantidade(100L, Unidade.UN));
        ComponenteMateriaPrima C4 = new ComponenteMateriaPrima(new CodigoInternoMateriaPrima("F1"),
                new Quantidade(20L, Unidade.L));
        CMP7.add(C3);
        CMP7.add(C4);
        this.F2 = new FichaProducao(new Quantidade(2L, Unidade.UN), CMP7);
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test(expected = IllegalArgumentException.class)
    public void testeCodigoComercialNotNull() {
        Produto p = new Produto(null, new CodigoFabricoProduto("teste"),
                new DescricaoBreve("teste"), new DescricaoCompleta("teste"), this.F2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testeCodigoFabricoNotNull() {
        Produto p = new Produto(new CodigoComercial("teste"), null,
                new DescricaoBreve("teste"), new DescricaoCompleta("teste"), this.F2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testeDescricaoBreveNotNull() {
        Produto p = new Produto(new CodigoComercial("teste"), new CodigoFabricoProduto("teste"),
                null, new DescricaoCompleta("teste"), this.F2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testeDescricaoCompletaNotNull() {
        Produto p = new Produto(new CodigoComercial("teste"), new CodigoFabricoProduto("teste"),
                new DescricaoBreve("teste"), null, this.F2);
    }


    @Test(expected = IllegalArgumentException.class)
    public void testProdutoUnico() {
        Set<Produto> setProdutos = new HashSet<>();
        Produto produto = new Produto(new CodigoComercial("teste"), new CodigoFabricoProduto("teste"),
                new DescricaoBreve("teste"), new DescricaoCompleta("teste"), this.F2);
        Produto produto2 = new Produto(new CodigoComercial("teste2"), new CodigoFabricoProduto("teste"),
                new DescricaoBreve("teste2"), new DescricaoCompleta("teste2"), this.F2);
        setProdutos.add(produto);
        if (setProdutos.contains(produto2)) {
            throw new IllegalArgumentException("Produto já existente");
        }
    }

    @Test
    public void testAddProdutoDif() {
        Set<Produto> setProdutos = new HashSet<>();
        Produto produto = new Produto(new CodigoComercial("teste"), new CodigoFabricoProduto("teste"),
                new DescricaoBreve("teste"), new DescricaoCompleta("teste"), this.F2);
        Produto produto2 = new Produto(new CodigoComercial("teste"), new CodigoFabricoProduto("teste2"),
                new DescricaoBreve("teste"), new DescricaoCompleta("teste"), this.F2);
        setProdutos.add(produto);
        if (setProdutos.contains(produto2)) {
            throw new IllegalArgumentException("Produto já existente");
        } else {
            setProdutos.add(produto2);
        }
        Assert.assertNotEquals(produto, produto2);
        Assert.assertEquals(2, setProdutos.size());
    }


}