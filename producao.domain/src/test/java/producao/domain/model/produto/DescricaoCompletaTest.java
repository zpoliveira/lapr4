package producao.domain.model.produto;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class DescricaoCompletaTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test(expected = IllegalArgumentException.class)
    public void testeCodigoComercialNotNull(){
        DescricaoCompleta cfp = new DescricaoCompleta(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testeCodigoComercialNotEmpty(){
        DescricaoCompleta cfp = new DescricaoCompleta("");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testeCodigoComercialNotExcedMaxLenth(){
        DescricaoCompleta cfp = new DescricaoCompleta("12345678901234567812345678901234567891234563456782345678324567823456");
    }
}