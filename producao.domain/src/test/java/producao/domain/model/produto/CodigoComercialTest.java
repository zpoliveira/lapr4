package producao.domain.model.produto;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CodigoComercialTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test(expected = IllegalArgumentException.class)
    public void testeCodigoComercialNotNull(){
        CodigoComercial cc = new CodigoComercial(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testeCodigoComercialNotEmpty(){
        CodigoComercial cc = new CodigoComercial("");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testeCodigoComercialNotExcedMaxLenth(){
        CodigoComercial cc = new CodigoComercial("123456789012345678");
    }
}