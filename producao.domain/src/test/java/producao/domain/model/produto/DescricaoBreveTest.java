package producao.domain.model.produto;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class DescricaoBreveTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test(expected = IllegalArgumentException.class)
    public void testeCodigoComercialNotNull(){
        DescricaoBreve cfp = new DescricaoBreve(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testeCodigoComercialNotEmpty(){
        DescricaoBreve cfp = new DescricaoBreve("");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testeCodigoComercialNotExcedMaxLenth(){
        DescricaoBreve cfp = new DescricaoBreve("1234567890123456781234567890123456789123456");
    }
}