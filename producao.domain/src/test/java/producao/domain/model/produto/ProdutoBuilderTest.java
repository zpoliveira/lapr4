package producao.domain.model.produto;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import producao.domain.model.Quantidade;
import producao.domain.model.Unidade;
import producao.domain.model.materiaPrima.CodigoInternoMateriaPrima;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class ProdutoBuilderTest {
    FichaProducao F1;
    @Before
    public void setUp() throws Exception {
        Set<ComponenteMateriaPrima> CMP7 = new HashSet<>();
        ComponenteMateriaPrima C3 = new ComponenteMateriaPrima(new CodigoInternoMateriaPrima("A1"),
                new Quantidade(100L, Unidade.UN));
        ComponenteMateriaPrima C4 = new ComponenteMateriaPrima(new CodigoInternoMateriaPrima("F1"),
                new Quantidade(20L, Unidade.L));
        CMP7.add(C3);
        CMP7.add(C4);
        this.F1= new FichaProducao(new Quantidade(2L, Unidade.UN), CMP7);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test(expected = IllegalArgumentException.class)
    public void testeCodigoComercialNotNull() {
        Produto p = new ProdutoBuilder(null, new CodigoFabricoProduto("teste"),
                new DescricaoBreve("teste"), new DescricaoCompleta("teste")).build();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testeCodigoFabricoNotNull() {
        Produto p = new ProdutoBuilder(new CodigoComercial("teste"), null,
                new DescricaoBreve("teste"), new DescricaoCompleta("teste")).build();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testeDescricaoBreveNotNull() {
        Produto p = new ProdutoBuilder(new CodigoComercial("teste"), new CodigoFabricoProduto("teste"),
                null, new DescricaoCompleta("teste")).build();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testeDescricaoCompletaNotNull() {
        Produto p = new ProdutoBuilder(new CodigoComercial("teste"), new CodigoFabricoProduto("teste"),
                new DescricaoBreve("teste"), null).build();
    }

    @Test
    public void testeTemNaoTemFichaProducao(){
        Produto p = new ProdutoBuilder(new CodigoComercial("teste"), new CodigoFabricoProduto("teste"),
                new DescricaoBreve("teste"), new DescricaoCompleta("teste")).build();
        Produto p2 = new ProdutoBuilder(new CodigoComercial("teste"), new CodigoFabricoProduto("teste"),
                new DescricaoBreve("teste"), new DescricaoCompleta("teste")).addFichaProducao(this.F1).build();

        assertTrue(p2.temFichaProducao());
        assertFalse(p.temFichaProducao());
    }
}