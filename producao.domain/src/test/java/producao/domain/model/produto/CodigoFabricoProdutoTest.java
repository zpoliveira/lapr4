package producao.domain.model.produto;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CodigoFabricoProdutoTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test(expected = IllegalArgumentException.class)
    public void testeCodigoComercialNotNull(){
        CodigoFabricoProduto cfp = new CodigoFabricoProduto(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testeCodigoComercialNotEmpty(){
        CodigoFabricoProduto cfp = new CodigoFabricoProduto("");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testeCodigoComercialNotExcedMaxLenth(){
        CodigoFabricoProduto cfp = new CodigoFabricoProduto("123456789012345678");
    }
}