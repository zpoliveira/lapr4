package producao.domain.model.ordemProducao;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import producao.domain.model.Quantidade;
import producao.domain.model.Unidade;
import producao.domain.model.materiaPrima.CodigoInternoMateriaPrima;
import producao.domain.model.produto.*;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class OrdemProducaoTest {

    @Autowired
    ProdutoBuilder produtoBuilder;


    @Test(expected = IllegalArgumentException.class)
    public void testDataEmissaoNotNull() {
        Set<IdentificadorEncomenda> identificadorEncomendas = new HashSet<>();
        identificadorEncomendas.add(new IdentificadorEncomenda("Enc1"));
        OrdemProducao ordemProducao = new OrdemProducao(new IdentificadorOrdemProducao("OP1"), new CodigoFabricoProduto("P1"),
                new Quantidade(1L, Unidade.KG), null,
                LocalDate.of(2020,5,23), identificadorEncomendas);


    }

    @Test(expected = IllegalArgumentException.class)
    public void testDataPrevistaExecucaoNotNull() {
        Set<IdentificadorEncomenda> identificadorEncomendas = new HashSet<>();
        identificadorEncomendas.add(new IdentificadorEncomenda("Enc1"));
        OrdemProducao ordemProducao = new OrdemProducao(new IdentificadorOrdemProducao("OP1"), new CodigoFabricoProduto("P1"),
                new Quantidade(1L, Unidade.KG),
                LocalDate.of(2020,5,23), null, identificadorEncomendas);

    }

    @Test(expected = IllegalArgumentException.class)
    public void testCodigoFabricoProdutoNotNull() {
        Set<IdentificadorEncomenda> identificadorEncomendas = new HashSet<>();
        identificadorEncomendas.add(new IdentificadorEncomenda("Enc1"));
        OrdemProducao ordemProducao = new OrdemProducao(new IdentificadorOrdemProducao("OP1"), null,
                new Quantidade(1L, Unidade.KG),
                LocalDate.of(2020,5,23), LocalDate.now(), identificadorEncomendas);

    }

    @Test(expected = IllegalArgumentException.class)
    public void testCodigoFabricoProdutoNotEmpty() {
        Set<IdentificadorEncomenda> identificadorEncomendas = new HashSet<>();
        identificadorEncomendas.add(new IdentificadorEncomenda("Enc1"));
        OrdemProducao ordemProducao = new OrdemProducao(new IdentificadorOrdemProducao("OP1"), new CodigoFabricoProduto(""),
                new Quantidade(1L, Unidade.KG),
                LocalDate.of(2020,5,23), LocalDate.now(), identificadorEncomendas);

    }

    @Test(expected = IllegalArgumentException.class)
    public void testEncomendaIdNotNull() {
        Set<IdentificadorEncomenda> identificadorEncomendas = new HashSet<>();
        identificadorEncomendas.add(new IdentificadorEncomenda("Enc1"));
        OrdemProducao ordemProducao = new OrdemProducao(new IdentificadorOrdemProducao("OP1"), new CodigoFabricoProduto(""),
                new Quantidade(1L, Unidade.KG),
                LocalDate.of(2020,5,23), LocalDate.now(), null);

    }

    @Test(expected = IllegalArgumentException.class)
    public void testEncomendaIdNotEmpty() {
        Set<IdentificadorEncomenda> identificadorEncomendas = new HashSet<>();
        OrdemProducao ordemProducao = new OrdemProducao(new IdentificadorOrdemProducao("OP1"), new CodigoFabricoProduto(""),
                new Quantidade(1L, Unidade.KG),
                LocalDate.of(2020,5,23), LocalDate.now(), identificadorEncomendas);

    }

    @Test
    public void testEstadoOrdemProducaoPendente() {
        Set<IdentificadorEncomenda> identificadorEncomendas = new HashSet<>();
        identificadorEncomendas.add(new IdentificadorEncomenda("Enc1"));
        OrdemProducao ordemProducao = new OrdemProducao(new IdentificadorOrdemProducao("OP1"), new CodigoFabricoProduto("P1"),
                new Quantidade(1L, Unidade.KG),
                LocalDate.of(2020,5,23), LocalDate.now(), identificadorEncomendas);

        assertEquals(EstadoOrdemProducao.PENDENTE, ordemProducao.estadoOrdemProducao());


    }

}