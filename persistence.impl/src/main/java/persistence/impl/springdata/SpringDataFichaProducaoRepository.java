package persistence.impl.springdata;

import org.springframework.data.repository.Repository;
import producao.domain.model.produto.FichaProducao;
import producao.domain.repositories.FichaProducaoRepository;

public interface SpringDataFichaProducaoRepository extends FichaProducaoRepository, Repository<FichaProducao, Long> {
}
