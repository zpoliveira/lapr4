package persistence.impl.springdata;

import org.springframework.data.repository.Repository;
import producao.domain.model.materiaPrima.CodigoInternoMateriaPrima;
import producao.domain.model.materiaPrima.MateriaPrima;
import producao.domain.repositories.MateriaPrimaRepository;

public interface SpringDataMateriaPrimaRepository extends MateriaPrimaRepository, Repository<MateriaPrima, CodigoInternoMateriaPrima> {
}
