package persistence.impl.springdata;

import chaofabrica.domain.model.maquina.CodigoInternoMaquina;
import chaofabrica.domain.model.maquina.Maquina;
import chaofabrica.domain.repositories.MaquinaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

public interface SpringDataMaquinaRepository extends MaquinaRepository, Repository<Maquina, CodigoInternoMaquina> {

    @Override
    @Query("SELECT m FROM Maquina m left join SequenciaMaquina sm on m = sm.maquina")
    Iterable<Maquina> allMaquinasWithoutLinhaProducao();

    @Override
    Iterable<Maquina> findAll();

    @Override
    @Query("SELECT m FROM Maquina m WHERE m.codigoUnicoProtocolo.codigoUnicoProtocolo= ?1")
    Maquina findByCodigoUnicoProtocolo(int codigo);
}
