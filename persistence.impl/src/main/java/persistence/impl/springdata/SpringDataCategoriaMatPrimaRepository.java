package persistence.impl.springdata;

import org.springframework.data.repository.Repository;
import producao.domain.model.materiaPrima.CategoriaMatPrima;
import producao.domain.repositories.CategoriaMatPrimaRepository;

public interface SpringDataCategoriaMatPrimaRepository extends CategoriaMatPrimaRepository, Repository<CategoriaMatPrima, Long> {
}
