package persistence.impl.springdata;

import chaofabrica.domain.model.maquina.FicheiroConfiguracao;
import chaofabrica.domain.repositories.FicheiroConfiguracaoMaquinaRepository;
import org.springframework.data.repository.Repository;

public interface SpringDataFicheiroConfiguracaoMaquinaRepository extends FicheiroConfiguracaoMaquinaRepository, Repository<FicheiroConfiguracao, Long> {

    @Override
    FicheiroConfiguracao save(FicheiroConfiguracao ficheiroConfiguracao);
}
