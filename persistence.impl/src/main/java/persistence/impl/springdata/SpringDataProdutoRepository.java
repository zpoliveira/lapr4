package persistence.impl.springdata;

import org.springframework.data.repository.Repository;
import producao.domain.model.produto.CodigoFabricoProduto;
import producao.domain.model.produto.Produto;
import producao.domain.repositories.ProdutoRepository;

public interface SpringDataProdutoRepository extends ProdutoRepository, Repository<Produto, Long> {

    @Override
    Produto findByPk(Long id);

    @Override
    Produto findByCodigoFabricoProduto(CodigoFabricoProduto codigoFabricoProduto);

    @Override
    Iterable<Produto> findByTemFichaProducaoFalse();


}
