package persistence.impl.springdata;

import chaofabrica.domain.model.deposito.CodigoAlfanumerico;
import chaofabrica.domain.model.deposito.Deposito;
import chaofabrica.domain.repositories.DepositoRepository;
import org.springframework.data.repository.Repository;

public interface SpringDataDepositoRepository extends DepositoRepository, Repository<Deposito, CodigoAlfanumerico> {

}
