package persistence.impl.springdata;

import chaofabrica.domain.model.linhaproducao.LinhaProducao;
import chaofabrica.domain.model.linhaproducao.LinhaProducaoIdentificador;
import chaofabrica.domain.repositories.LinhaProducaoRepository;
import org.springframework.data.repository.Repository;
import producao.domain.model.produto.Produto;

import java.util.List;

public interface SpringDataLinhaProducaoRepository extends LinhaProducaoRepository, Repository<LinhaProducao, LinhaProducaoIdentificador> {

    @Override
    Iterable<LinhaProducao> findAll();

    @Override
    LinhaProducao findByCodigo_linhaProducaoID(String idLinha);

    @Override
    List<LinhaProducao> findByRecorrenciaProcessamento_estadoProcessamentoTrue();

}
