package persistence.impl.springdata;

import chaofabrica.domain.model.notificacao.Notificacao;
import chaofabrica.domain.model.notificacao.TipoErro;
import chaofabrica.domain.repositories.NotificacaoRepository;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface SpringDataNotificacaoRepository extends NotificacaoRepository, PagingAndSortingRepository<Notificacao, Long> {

    @Override
    @Query("SELECT n FROM Notificacao n WHERE n.notificacaoPorTartar = true")
    Slice<Notificacao> listaNotificacoesPorTratar(Pageable pageable);

    @Override
    @Query("SELECT n FROM Notificacao n WHERE n.notificacaoPorTartar = true")
    Iterable<Notificacao> listaNotificacoesPorTratar();

    @Override
    @Query("SELECT n FROM Notificacao n WHERE n.notificacaoPorTartar = true AND n.tipoErro =:tipoErro")
    Slice<Notificacao> listaNotificacoesPorTratarPorTipoErro(@Param("tipoErro") TipoErro tipoErro, Pageable pageable);

    @Override
    @Query("SELECT n FROM Notificacao n WHERE n.notificacaoPorTartar = true AND n.tipoErro =:tipoErro AND n.idLinha =:linhaProducao")
    Slice<Notificacao> listaNotificacoesPorTratarPorTipoErroLinhaProducao(@Param("linhaProducao") String linhaProducao,@Param("tipoErro") TipoErro tipoErro, Pageable pageable);

    @Override
    @Query("SELECT n FROM Notificacao n WHERE n.notificacaoPorTartar = true AND n.idLinha =:linhaProducao")
    Slice<Notificacao> listaNotificacoesPorTratarLinhaProducao(@Param("linhaProducao") String linhaProducaoID, Pageable page);

    @Override
    @Query("SELECT n FROM Notificacao n WHERE n.notificacaoPorTartar = false")
    Iterable<Notificacao> listaNotificacoesArquivadas();

    @Override
    @Query("SELECT n FROM Notificacao n WHERE n.notificacaoPorTartar = false AND n.tipoErro =:tipoErro")
    Iterable<Notificacao> listaNotificacoesArquivadasPorTipoErro(@Param("tipoErro")TipoErro erro);

    @Modifying
    @Transactional
    @Query("update Notificacao n set n.notificacaoPorTartar = false where n =:noti")
    int arquivaNotificacao(@Param("noti") Notificacao notificacao);

    @Override
    Iterable<Notificacao> findByIdMensagem(Long pk);
}
