package persistence.impl.springdata;

import org.springframework.data.repository.Repository;
import producao.domain.model.ordemProducao.EstadoOrdemProducao;
import producao.domain.model.ordemProducao.IdentificadorOrdemProducao;
import producao.domain.model.ordemProducao.OrdemProducao;
import producao.domain.repositories.OrdemProducaoRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public interface SpringDataOrdemProducaoRepository extends OrdemProducaoRepository, Repository<OrdemProducao, Long> {

    @Override
    OrdemProducao findByIdentificadorOrdemProducao(IdentificadorOrdemProducao identificadorOrdemProducao);

    @Override
    Iterable<OrdemProducao> findByEstadoOrdemProducao(EstadoOrdemProducao estadoOrdemProducao);

    @Override
    OrdemProducao findByPk(Long pk);

    @Override
    Iterable<OrdemProducao> findByIdsEncomenda_IdEncomenda(String id);

    @Override
    Iterable<OrdemProducao> findAllByDataPrevistaExecucaoBetween(LocalDate start, LocalDate end);

}
