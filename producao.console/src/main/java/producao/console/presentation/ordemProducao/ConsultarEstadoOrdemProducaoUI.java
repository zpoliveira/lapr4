package producao.console.presentation.ordemProducao;

import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import producao.application.controllers.ConsultarEstadoOrdemProducaoController;
import producao.domain.model.ordemProducao.EstadoOrdemProducao;
import producao.domain.model.ordemProducao.OrdemProducao;

@Component
public class ConsultarEstadoOrdemProducaoUI extends AbstractUI {

    private static final Logger LOGGER = LogManager.getLogger(ConsultarEstadoOrdemProducaoUI.class);
    private OrdemProducaoPrinter opp = new OrdemProducaoPrinter();

    @Autowired
    ConsultarEstadoOrdemProducaoController theController;

    @Override
    protected boolean doShow() {

        final Iterable<EstadoOrdemProducao> estados = theController.mostraEstadosOrdemProducao();
        final SelectWidget<EstadoOrdemProducao> selector = new SelectWidget<>("Selecione Estado de Ordem de Produção",
                estados);
        selector.show();
        final EstadoOrdemProducao oEstado = selector.selectedElement();
        if(oEstado == null){
            return false;
        }

        final Iterable<OrdemProducao> ordens = theController.mostraOrdensProducaoPorEstado(oEstado);
        if(!ordens.iterator().hasNext()){
            System.out.println("Não existem Ordens de produção no estado: " + oEstado);
        }else{
            for(OrdemProducao op : ordens){
                opp.visit(op);
            }
        }

        return false;
    }

    @Override
    public String headline() {
        return "Consultar Ordens de Produção por Estado";
    }
}
