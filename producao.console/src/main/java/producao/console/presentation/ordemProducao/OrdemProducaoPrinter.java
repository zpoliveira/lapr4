package producao.console.presentation.ordemProducao;

import eapli.framework.visitor.Visitor;
import producao.domain.model.ordemProducao.OrdemProducao;

public class OrdemProducaoPrinter implements Visitor<OrdemProducao> {
    @Override
    public void visit(OrdemProducao visitee) {
        System.out.println(visitee.toString());
    }
}
