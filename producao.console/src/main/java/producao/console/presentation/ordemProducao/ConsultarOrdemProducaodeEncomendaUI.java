package producao.console.presentation.ordemProducao;

import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.io.util.Console;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import producao.application.controllers.ConsultarOrdemProducaodeEncomendaController;
import producao.domain.model.ordemProducao.OrdemProducao;

@Component
public class ConsultarOrdemProducaodeEncomendaUI extends AbstractUI {

    private static final Logger LOGGER = LogManager.getLogger(ConsultarOrdemProducaodeEncomendaUI.class);
    private OrdemProducaoPrinter opp = new OrdemProducaoPrinter();

    @Autowired
    ConsultarOrdemProducaodeEncomendaController theController;


    @Override
    protected boolean doShow() {

        String idEncomenda = Console.readLine("Insira Identificador de Encomenda");

        Iterable<OrdemProducao> listaOrdens = theController.mostraOrdemProducaoPorEncomenda(idEncomenda);
        if(!listaOrdens.iterator().hasNext()){
            System.out.println("Identificador de Encomenda inexistente");
        }else{
            for(OrdemProducao op : listaOrdens){
                opp.visit(op);
            }
        }
        return false;
    }

    @Override
    public String headline() {
        return "Consultar Ordens de Produção por Encomenda";
    }
}
