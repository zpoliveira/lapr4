package producao.console.presentation.ordemProducao;

import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.framework.io.util.Console;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;
import producao.application.controllers.AdicionarOrdemProducaoController;
import producao.console.presentation.SeletorUnidade;
import producao.console.presentation.produto.ProdutoPrinter;
import producao.domain.model.Quantidade;
import producao.domain.model.Unidade;
import producao.domain.model.ordemProducao.IdentificadorEncomenda;
import producao.domain.model.ordemProducao.IdentificadorOrdemProducao;
import producao.domain.model.ordemProducao.OrdemProducao;
import producao.domain.model.produto.CodigoFabricoProduto;
import producao.domain.model.produto.Produto;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Component
public class AdicionarOrdemProducaoUI extends AbstractUI {

    private static final Logger LOGGER = LogManager.getLogger(AdicionarOrdemProducaoUI.class);

    private SeletorUnidade seletorUnidade = SeletorUnidade.getInstance();

    @Autowired
    AdicionarOrdemProducaoController theController;

    @Override
    protected boolean doShow() {

        int opcao;
        CodigoFabricoProduto codigoFabricoProduto = null;
        Produto produto;
        OrdemProducao ordemProducao;
        String dataEmissao, dataPrevistaExecucao, id, idOrdemProducao;
        Set<IdentificadorEncomenda> conjuntoEncomendas = new HashSet<>();

        do{
             opcao = Console.readInteger("Pretende:\n" +
                    "1 - Selecionar produto de uma lista\n" +
                    "2 - Introduzir Código de Fabrico do Produto");
        }while(opcao != 1 && opcao != 2);

        switch (opcao){
            case 1:
                final Iterable<Produto> todosProdutos = theController.todosProdutos();
                final SelectWidget<Produto> selector = new SelectWidget<>("Selecione Produto",
                        todosProdutos, new ProdutoPrinter());
                selector.show();
                final Produto oProduto = selector.selectedElement();
                if(oProduto == null){
                    return false;
                }
                codigoFabricoProduto = oProduto.identity();
                break;
            case 2:
                String codigoProduto = Console.readLine("Insira Código de Fabrico de Produto");
                produto = theController.procuraCodigoFabricoProduto(new CodigoFabricoProduto(codigoProduto));
                if(produto != null){
                    codigoFabricoProduto = produto.identity();
                }

                while(produto == null){
                    System.out.println("Código de fabrico inexistente");
                    codigoProduto = Console.readLine("Insira Código de Fabrico de Produto");
                    produto = theController.procuraCodigoFabricoProduto(new CodigoFabricoProduto(codigoProduto));
                    codigoFabricoProduto = produto.identity();
                }
                System.out.println("Produto selecionado: " + produto);
                break;
        }

        Long quant = Console.readLong("Insira Quantidade");
        int unidadeSelected = Console.readInteger("Selecione a unidade \n" + seletorUnidade.toString());
        Unidade unidade = seletorUnidade.getUnidadesMapping().get(unidadeSelected);
        Quantidade quantidade = new Quantidade(quant,unidade);


        do {
            idOrdemProducao = Console.readLine("Insira Identificador da Ordem de Produção");
            ordemProducao = theController.procuraIdentificadorOrdemProducao(
                    new IdentificadorOrdemProducao(idOrdemProducao));
        }while(ordemProducao != null);

        do{
            dataEmissao = Console.readLine("Insira data de emissão da Ordem de Produção (Formato AAAA-MM-DD)");
        }while(!dataEmissao.matches("\\d{4}-\\d{2}-\\d{2}"));

        while(LocalDate.parse(dataEmissao).isBefore(LocalDate.now())){
            System.out.println("Data inválida. Não pode ser anterior ao dia de hoje");
            dataEmissao = Console.readLine("Insira data de emissão da Ordem de Produção (Formato AAAA-MM-DD)");
        }

        do{
            dataPrevistaExecucao =
                    Console.readLine("Insira data de prevista de execução da Ordem de Produção (Formato AAAA-MM-DD)");
        }while(!dataPrevistaExecucao.matches("\\d{4}-\\d{2}-\\d{2}"));

        while(LocalDate.parse(dataPrevistaExecucao).isBefore(LocalDate.now())){
            System.out.println("Data inválida. Não pode ser anterior ao dia de hoje");
            dataPrevistaExecucao = Console.readLine("Insira data de execução da Ordem de Produção (Formato AAAA-MM-DD)");
        }

        id = Console.readLine("Insira Id da(s) Encomenda(s) (0 - termina)");
        while(!id.equalsIgnoreCase("0")) {
            conjuntoEncomendas.add(new IdentificadorEncomenda(id));
            id = Console.readLine("Insira Id da(s) Encomenda(s) (0 - termina)");
        }

        try {
            theController.addOrdemProducao(idOrdemProducao,codigoFabricoProduto,quantidade,dataEmissao,
                    dataPrevistaExecucao,conjuntoEncomendas);
            System.out.println("Criada a Ordem de Produção: " + idOrdemProducao);
        } catch (final DataIntegrityViolationException e) {
            System.out.println("Ordem de Produção já existente");
        } catch (final ConcurrencyException e) {
            LOGGER.error("Isto não devia acontecer", e);
            System.out.println(
                    "Erro inesperado na aplicação. Por favor, tente de novo, se erro continuar contactar administrador.");
        }catch (final IllegalArgumentException e){
            System.out.println(e.getMessage());
            return true;
        }
        return false;
    }

    @Override
    public String headline() {
        return "Adicionar Ordem de Produção Manual";
    }
}
