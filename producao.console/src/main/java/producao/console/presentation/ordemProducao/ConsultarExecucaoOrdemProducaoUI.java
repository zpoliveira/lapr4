package producao.console.presentation.ordemProducao;

import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import producao.application.controllers.ConsultarExecucaoOrdemProducaoController;
import producao.domain.model.ordemProducao.EstadoOrdemProducao;
import spm.domain.execucaoOrdemProducao.ExecucaoOrdemProducao;

@Component
public class ConsultarExecucaoOrdemProducaoUI extends AbstractUI {

    private static final Logger LOGGER = LogManager.getLogger(ConsultarExecucaoOrdemProducaoUI.class);

    @Autowired
    ConsultarExecucaoOrdemProducaoController theController;

    @Override
    protected boolean doShow() {

        Iterable<ExecucaoOrdemProducao> todasExecucoes = theController.mostraTodasExecucoes();
        final SelectWidget<ExecucaoOrdemProducao> selector = new SelectWidget<>("Selecione Execução de Ordem de Produção",
                todasExecucoes, new ExecucaoordemProducaoPrinter());
        selector.show();
        final ExecucaoOrdemProducao aExecucao = selector.selectedElement();
        if(aExecucao == null){
            System.out.println("Ainda não foi realizado o processamento de nenhuma Ordem de Produção");
            return false;
        }else{
            System.out.println(aExecucao);
        }


        return false;
    }

    @Override
    public String headline() {
        return "Consultar Execução de Ordem de Produção";
    }
}
