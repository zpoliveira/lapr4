package producao.console.presentation.ordemProducao;

import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import producao.application.controllers.ImportarOrdensProducaoController;
import utils.console.FileSelector;

import java.io.File;
import java.io.IOException;

@Component
public class ImportarOrdensProducaoUI extends AbstractUI {
    private static final Logger LOGGER = LogManager.getLogger(ImportarOrdensProducaoUI.class);

    @Autowired
    private ImportarOrdensProducaoController controller;


    @Override
    protected boolean doShow() {

        try {
            final File ficheiro = FileSelector.selectFile();
            LOGGER.info(String.format("Selecionado o ficheiro -> %s", ficheiro.getName()));

            String caminhoFicheiro = ficheiro.getAbsolutePath();

            int numOrdProdImportadas = controller.importarProdutos(caminhoFicheiro);
            if (numOrdProdImportadas > -1) {
                LOGGER.info(String.format("Importadas %d ordens de produção com sucesso.", numOrdProdImportadas));
            } else {
                LOGGER.error("Falha na importacao de ordens de produção");
            }

        } catch (final IntegrityViolationException | IOException e) {
            LOGGER.error("Falha na importacao de ordens de produção");
        } catch (final ConcurrencyException e) {
            LOGGER.error("Isto não devia acontecer", e);
            LOGGER.fatal(
                    "Erro inesperado na aplicação. Por favor, tente de novo, se erro continuar contactar administrador.");
        } catch (final IllegalArgumentException e) {
            LOGGER.fatal(e.getMessage());
            return true;
        }
        return false;
    }

    @Override
    public String headline() {
        return "Importar Ordens de Produção";
    }
}
