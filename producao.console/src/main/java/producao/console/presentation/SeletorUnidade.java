package producao.console.presentation;

import producao.domain.model.Unidade;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public final class SeletorUnidade {
    private static SeletorUnidade SELETOR_UNIDADE;
    private final Map<Integer, Unidade> unidadesMapping;

    private SeletorUnidade(){
        HashMap<Integer, Unidade> tmp = new HashMap<>();
        int i =0;
        for (Unidade value : Unidade.values()) {
            i++;
            tmp.put(i,value);
        }
        unidadesMapping = Collections.unmodifiableMap(tmp);
    }

    public static SeletorUnidade getInstance(){
        if(SELETOR_UNIDADE == null){
            SELETOR_UNIDADE = new SeletorUnidade();
        }
        return SELETOR_UNIDADE;
    }

    public Map<Integer, Unidade> getUnidadesMapping() {
        return unidadesMapping;
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        for(Map.Entry<Integer, Unidade> entry : unidadesMapping.entrySet()){
            str.append(entry.getKey() + " - " + entry.getValue().label + "\n");
        }
        return str.toString();
    }
}
