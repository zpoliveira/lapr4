package producao.console.presentation;

import eapli.framework.actions.Actions;
import eapli.framework.actions.menu.Menu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import producao.console.presentation.catmatprima.DefinCatMatPrimaUI;
import producao.console.presentation.materiaprima.AddMateriaPrimaUI;
import producao.console.presentation.ordemProducao.*;
import producao.console.presentation.produto.AddProdutoUI;
import producao.console.presentation.produto.EspecificarFichaProducaoUI;
import producao.console.presentation.produto.ImportarCatalogoProdutoUI;
import producao.console.presentation.produto.ListarProdutosSemFPUI;

@Component
public class ProducaoMenu {

    private static final String MENU_TITLE = "Produção sub menu";

    private static final int EXIT_OPTION = 0;
    private static final int DEFINIR_CAT_MAT_PRIMA = 1;
    private static final int ADICIONAR_MATERIA_PRIMA = 2;
    private static final int ADD_PRODUTO = 3;
    private static final int ESPECIFICAR_FICHA_PRODUCAO = 4;
    private static final int LISTAR_PRODUTOS_SEM_FP = 5;
    private static final int IMPORT_CATALOGO_PRODUTO = 6;
    private static final int ADICIONAR_ORDEM_PRODUCAO_MANUAL = 7;
    private static final int CONSULTAR_ORDEM_PRODUCAO_POR_ESTADO = 8;
    private static final int CONSULTAR_ORDEM_PRODUCAO_POR_ENCOMENDA = 9;
    private static final int IMPORTAR_ORDENS_PRODUCAO = 10;
    private static final int CONSULTAR_EXECUCAO_ORDENS_PRODUCAO = 11;

    @Autowired
    private AddProdutoUI addProdutoUI;
    @Autowired
    private DefinCatMatPrimaUI definCatMatPrimaUI;
    @Autowired
    private AddMateriaPrimaUI addMateriaPrimaUI;
    @Autowired
    private EspecificarFichaProducaoUI especificarFichaProducaoUI;
    @Autowired
    private ListarProdutosSemFPUI listarProdutosSemFPUI;
    @Autowired
    private ImportarCatalogoProdutoUI importarCatalogoProdutoUI;
    @Autowired
    private AdicionarOrdemProducaoUI adicionarOrdemProducaoUI;
    @Autowired
    private ConsultarEstadoOrdemProducaoUI consultarEstadoOrdemProducaoUI;
    @Autowired
    private ConsultarOrdemProducaodeEncomendaUI consultarOrdemProducaodeEncomendaUI;
    @Autowired
    private ImportarOrdensProducaoUI importarOrdensProducaoUI;
    @Autowired
    private ConsultarExecucaoOrdemProducaoUI consultarExecucaoOrdemProducaoUI;

    public Menu producaoSubMenus() {
        final Menu prodSubMenu = new Menu(MENU_TITLE);

        prodSubMenu.addItem(DEFINIR_CAT_MAT_PRIMA, "Definir Categoria de Matéria-Prima", definCatMatPrimaUI::show);
        prodSubMenu.addItem(ADICIONAR_MATERIA_PRIMA, "Adicionar Matéria-Prima", addMateriaPrimaUI::show);
        prodSubMenu.addItem(ADD_PRODUTO, "Adicionar Novo Produto", addProdutoUI::show);
        prodSubMenu.addItem(ESPECIFICAR_FICHA_PRODUCAO, "Especificar Ficha Produção", especificarFichaProducaoUI::show);
        prodSubMenu.addItem(LISTAR_PRODUTOS_SEM_FP, "Listar Produtos Sem Ficha Produção", listarProdutosSemFPUI::show);
        prodSubMenu.addItem(IMPORT_CATALOGO_PRODUTO, "Importar Catálogo de Produtos ", importarCatalogoProdutoUI::show);
        prodSubMenu.addItem(ADICIONAR_ORDEM_PRODUCAO_MANUAL, "Adicionar Ordem de Produção Manual",
                adicionarOrdemProducaoUI::show);
        prodSubMenu.addItem(CONSULTAR_ORDEM_PRODUCAO_POR_ESTADO, "Consultar Ordens de Produção por Estado",
                consultarEstadoOrdemProducaoUI::show);
        prodSubMenu.addItem(CONSULTAR_ORDEM_PRODUCAO_POR_ENCOMENDA, "Consultar Ordens de Produção por Encomenda",
                consultarOrdemProducaodeEncomendaUI::show);
        prodSubMenu.addItem(IMPORTAR_ORDENS_PRODUCAO, "Importar Ordens de Produção",
                importarOrdensProducaoUI::show);
        prodSubMenu.addItem(CONSULTAR_EXECUCAO_ORDENS_PRODUCAO, "Consultar execução de Ordem de Produção",
                consultarExecucaoOrdemProducaoUI::show);
        prodSubMenu.addItem(EXIT_OPTION, "Return ", Actions.SUCCESS);

        return prodSubMenu;
    }

}
