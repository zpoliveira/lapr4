package producao.console.presentation.materiaprima;

import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.framework.io.util.Console;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;
import producao.application.controllers.AdicionarMateriaPrimaController;
import producao.console.presentation.catmatprima.CategoriasMatPrimaPrinter;
import producao.domain.model.materiaPrima.CategoriaMatPrima;
import utils.console.FileSelector;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URLConnection;


@Component
public class AddMateriaPrimaUI extends AbstractUI {

    private static final Logger LOGGER = LogManager.getLogger(AddMateriaPrimaUI.class);

    @Autowired
    private AdicionarMateriaPrimaController theController;

    @Override
    protected boolean doShow() {

        String nomeFichaTecnica = "";
        final Iterable<CategoriaMatPrima> categorias = theController.todasCatMatPrima();
        final SelectWidget<CategoriaMatPrima> selector = new SelectWidget<>("Selecione categoria de matéria-Prima",
                categorias, new CategoriasMatPrimaPrinter());
        selector.show();
        final CategoriaMatPrima aCategoria = selector.selectedElement();
        if(aCategoria == null){
            return false;
        }

        String codigoInternoMatPrima = Console.readLine("Código Interno da Matéria-Prima");
        while(codigoInternoMatPrima.length() > 15 || codigoInternoMatPrima.length() < 1 || codigoInternoMatPrima == null){
            System.out.println("Código Interno da MAtéria-Prima tem de ter mais que 1 e menos de 15 caractéres");
            codigoInternoMatPrima = Console.readLine("Código Interno da Matéria-Prima");
        }

        try {
            final File ficheiro = FileSelector.selectFile("*.pdf");
            System.out.println("Selecionado o ficheiro. " + ficheiro.getName());
            final String descricaoMateriaPrima = Console.readLine("Descrição da Matéria-Prima");
            theController.addMatPrima(codigoInternoMatPrima, aCategoria, descricaoMateriaPrima, ficheiro);
            System.out.println("Foi criada a Matéria-Prima com: Cód. Interno: " + codigoInternoMatPrima
                    + " Categoria: " + aCategoria + " Descrição: " + descricaoMateriaPrima + " Nome FT: " + ficheiro.getName()
                    + " Caminho FT: " + ficheiro.getPath());
        } catch (final DataIntegrityViolationException e) {
            System.out.println("Essa Matéria-Prima já existe.");
        } catch (final ConcurrencyException e) {
            LOGGER.error("Isto não devia contecer", e);
            System.out.println(
                    "Erro inesperado na aplicação. Por favor, tente de novo, se erro continuar contactar administrador.");
        }catch (final IllegalArgumentException | FileNotFoundException e){
            System.out.println(e.getMessage());
            return true;
        }

        return false;
    }

    @Override
    public String headline() {
        return "Adicionar Matéria-Prima";
    }

}
