package producao.console.presentation.materiaprima;

import eapli.framework.visitor.Visitor;
import producao.domain.model.materiaPrima.MateriaPrima;

public class MateriasPrimasPrinter implements Visitor<MateriaPrima> {

    @Override
    public void visit(final MateriaPrima visitee) {
        System.out.printf(visitee.identity().toString());
    }
}
