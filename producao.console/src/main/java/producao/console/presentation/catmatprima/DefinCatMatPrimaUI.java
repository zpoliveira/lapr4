package producao.console.presentation.catmatprima;

import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.io.util.Console;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;
import producao.application.controllers.DefinCatMatPrimaController;

@Component
public class DefinCatMatPrimaUI extends AbstractUI {

    private static final Logger LOGGER = LogManager.getLogger(DefinCatMatPrimaUI.class);

    @Autowired
    private DefinCatMatPrimaController theController;

    @Override
    protected boolean doShow() {
        String nomeCategoria = Console.readLine("Nome da Categoria de Matéria-Prima:");
        while(nomeCategoria.length() > 15 || nomeCategoria.length() < 1 || nomeCategoria == null){
            System.out.println("Nome de categoria tem de ter mais que 1 e menos de 15 caractéres");
            nomeCategoria = Console.readLine("Nome da Categoria de Matéria-Prima:");
        }

        try {
            theController.definCatMatPrima(nomeCategoria);
            System.out.println("Criada a Categoria de Matérias-Primas: " + nomeCategoria);
        } catch (final DataIntegrityViolationException e) {
            System.out.println("Nome de categoria já utilizado");
        } catch (final ConcurrencyException e) {
            LOGGER.error("Isto não devia acontecer", e);
            System.out.println(
                    "Erro inesperado na aplicação. Por favor, tente de novo, se erro continuar contactar administrador.");
        }catch (final IllegalArgumentException e){
            System.out.println(e.getMessage());
            return true;
        }
        return false;
    }

    @Override
    public String headline() {
        return "Definir Categoria de Matéria-Prima";
    }
}
