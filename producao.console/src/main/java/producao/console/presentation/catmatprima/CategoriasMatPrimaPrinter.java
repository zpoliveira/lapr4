package producao.console.presentation.catmatprima;

import eapli.framework.visitor.Visitor;
import producao.domain.model.materiaPrima.CategoriaMatPrima;

public class CategoriasMatPrimaPrinter implements Visitor<CategoriaMatPrima> {

    @Override
    public void visit(final CategoriaMatPrima visitee) {
        System.out.printf(visitee.identity().toString());
    }
}
