package producao.console.presentation.produto;

import eapli.framework.presentation.console.AbstractListUI;
import eapli.framework.visitor.Visitor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import producao.application.controllers.ListarProdutosService;
import producao.domain.model.produto.Produto;


@Component
public class ListarProdutosSemFPUI extends AbstractListUI<Produto> {

    @Autowired
    ListarProdutosService listarProdutosService;


    @Override
    protected Iterable<Produto> elements() {
        return listarProdutosService.listarProdutosSemFichaProducao();
    }

    @Override
    protected Visitor<Produto> elementPrinter() {
        return new ProdutoPrinter();
    }

    @Override
    protected String elementName() {
        return "Produto";
    }

    @Override
    protected String listHeader() {
        return String.format("#  %-20s%-30s%s", "Codigo Fabrico", "Codigo Comercial", "Descrição Breve");
    }
    @Override
    protected String emptyMessage() {
        return "No data.";
    }


    @Override
    public String headline() {
        return "Produtos Sem Ficha Produção";
    }
}
