package producao.console.presentation.produto;

import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.framework.io.util.Console;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;
import producao.application.controllers.EspecificarFichaProducaoController;
import producao.application.controllers.ListarMateriasPrimasService;
import producao.application.controllers.ListarProdutosService;
import producao.console.presentation.SeletorUnidade;
import producao.console.presentation.materiaprima.MateriasPrimasPrinter;
import producao.domain.model.Quantidade;
import producao.domain.model.Unidade;
import producao.domain.model.materiaPrima.CodigoInternoMateriaPrima;
import producao.domain.model.materiaPrima.MateriaPrima;
import producao.domain.model.produto.ComponenteMateriaPrima;
import producao.domain.model.produto.Produto;

import java.util.HashSet;
import java.util.Set;

@Component
public class EspecificarFichaProducaoUI extends AbstractUI {

    private static final Logger LOGGER = LogManager.getLogger(EspecificarFichaProducaoUI.class);

    @Autowired
    private ListarProdutosService listarProdutosService;
    @Autowired
    private EspecificarFichaProducaoController especificarFichaProducaoController;
    @Autowired
    private ListarMateriasPrimasService listarMateriasPrimasService;

    private SeletorUnidade seletorUnidade = SeletorUnidade.getInstance();

    @Override
    protected boolean doShow() {
        final Iterable<Produto> produtos = listarProdutosService.listarProdutosSemFichaProducao();
        final SelectWidget<Produto> selector = new SelectWidget<>("Selecione o produto que pretende especificar a ficha de produção",
                produtos, new ProdutoPrinter());
        selector.show();
        final Produto selectedProduto = selector.selectedElement();
        try {
            Quantidade quantidadeProduto = selecionarQuantidadeProduto();
            Set<ComponenteMateriaPrima> componetesFichaProducao = this.gerarComponentesFichaProducao();
            this.especificarFichaProducaoController.especificarFichaProducaoProduto(
                    selectedProduto,
                    quantidadeProduto,
                    componetesFichaProducao);
        } catch (IllegalArgumentException | DataIntegrityViolationException e) {
            System.out.println("dados invalidos: " + e.getMessage());
        }
        return false;
    }

    protected boolean doShow(String codigoComercial,
                             String codigoFabrico,
                             String descricaoBreve,
                             String descricaoCompleta) {
        try {
            Quantidade quantidadeProduto = selecionarQuantidadeProduto();
            Set<ComponenteMateriaPrima> componetesFichaProducao = this.gerarComponentesFichaProducao();
            this.especificarFichaProducaoController.adicionarFichaProducaoProduto(
                    codigoComercial, codigoFabrico, descricaoBreve,
                    descricaoCompleta, quantidadeProduto, componetesFichaProducao);
        } catch (IllegalArgumentException | DataIntegrityViolationException e) {
            System.out.println("dados invalidos: " + e.getMessage());
        }

        return false;
    }

    @Override
    public String headline() {
        return "Especificar Ficha de Produção";
    }


    private Set<ComponenteMateriaPrima> gerarComponentesFichaProducao() {
        final Iterable<MateriaPrima> materiasPrimas = listarMateriasPrimasService.todasMatPrima();
        int selectedOption;
        Set<ComponenteMateriaPrima> componenteMateriaPrimas = new HashSet<>();
        do {
            final SelectWidget<MateriaPrima> selectorMateriPrima = new SelectWidget<>("Selecione a materia prima",
                    materiasPrimas, new MateriasPrimasPrinter());
            selectorMateriPrima.show();
            selectedOption = selectorMateriPrima.selectedOption();
            if (selectedOption != 0) {
                final CodigoInternoMateriaPrima selectedCodMatriaPrima = selectorMateriPrima.selectedElement().identity();
                long quantidade = Console.readLong("Intruduza a quantidade");
                int unidadeSelected = Console.readInteger("Selecione a unidade \n" + seletorUnidade.toString());
                Unidade unidade = seletorUnidade.getUnidadesMapping().get(unidadeSelected);
                Quantidade quantidadeObj = especificarFichaProducaoController.createQuantidade(quantidade, unidade);

                final ComponenteMateriaPrima componenteMateriaPrima =
                        especificarFichaProducaoController.createComponenteMateriaPrima(selectedCodMatriaPrima, quantidadeObj);
                componenteMateriaPrimas.add(componenteMateriaPrima);
            }
        } while (selectedOption != 0);
        return componenteMateriaPrimas;
    }

    private Quantidade selecionarQuantidadeProduto() {
        long quantidadeProduto = Console.readLong("Quantidade de Produto a ser produzida");
        int unidadeSelected = Console.readInteger("Selecione a unidade \n" + seletorUnidade.toString());
        Unidade unidade = seletorUnidade.getUnidadesMapping().get(unidadeSelected);
        return especificarFichaProducaoController.createQuantidade(quantidadeProduto, unidade);
    }

}
