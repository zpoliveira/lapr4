package producao.console.presentation.produto;

import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import producao.application.controllers.ImportarCatalogoController;
import utils.console.FileSelector;

import java.io.File;
import java.io.IOException;

@Component
public class ImportarCatalogoProdutoUI extends AbstractUI {

    private static final Logger LOGGER = LogManager.getLogger(ImportarCatalogoProdutoUI.class);

    @Autowired
    private ImportarCatalogoController controller;


    @Override
    protected boolean doShow() {

        try {
            final File ficheiro = FileSelector.selectFile();
            LOGGER.info(String.format("Selecionado o ficheiro -> %s", ficheiro.getName()));

            String caminhoFicheiro = ficheiro.getAbsolutePath();

            int numProdutosImportados = controller.importarProdutos(caminhoFicheiro);
            if (numProdutosImportados > -1) {
                LOGGER.info(String.format("Importados %d produtos com sucesso.", numProdutosImportados));
            } else {
                LOGGER.error("Falha na importacao de catálogo de produtos");
            }

        } catch (final IntegrityViolationException | IOException e) {
            LOGGER.error("Falha na importacao de catálogo de produtos");
        } catch (final ConcurrencyException e) {
            LOGGER.error("Isto não devia acontecer", e);
            LOGGER.fatal(
                    "Erro inesperado na aplicação. Por favor, tente de novo, se erro continuar contactar administrador.");
        } catch (final IllegalArgumentException e) {
            LOGGER.fatal(e.getMessage());
            return true;
        }
        return false;
    }

    @Override
    public String headline() {
        return "Importar Catálogo de Produtos";
    }


}
