package producao.console.presentation.produto;

import eapli.framework.visitor.Visitor;
import producao.domain.model.produto.Produto;

public class ProdutoPrinter implements Visitor<Produto> {

    @Override
    public void visit(final Produto visitee) {
        System.out.printf("%-20s%-30s%s", visitee.identity(), visitee.codigoComercial(), visitee.descricaoBreve());
    }
}
