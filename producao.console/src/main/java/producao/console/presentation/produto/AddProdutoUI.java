package producao.console.presentation.produto;

import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.io.util.Console;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;
import producao.application.controllers.AddProdutoController;
import producao.domain.model.produto.Produto;
import producao.domain.model.produto.ProdutoBuilder;

@Component
public class AddProdutoUI extends AbstractUI {

    private static final Logger LOGGER = LogManager.getLogger(AddProdutoUI.class);

    @Autowired
    private AddProdutoController addProdutoController;

    @Autowired
    EspecificarFichaProducaoUI especificarFichaProducaoUI;

    @Override
    protected boolean doShow() {
        final String codigoComercial = Console.readLine("Cod Comercial:");
        final String codigoFabrico = Console.readLine("Cod Fabrico:");
        final String descricaoBreve = Console.readLine("Descrição Breve:");
        final String descricaoCompleta = Console.readLine("Descrição Completa:");

        return this.showEspecificarFichaProducao(codigoComercial,
                codigoFabrico,
                descricaoBreve,
                descricaoCompleta);
    }

    @Override
    public String headline() {
        return "Adicionar Novo Produto";
    }

    private boolean showEspecificarFichaProducao(String codigoComercial,
                                              String codigoFabrico,
                                              String descricaoBreve,
                                              String descricaoCompleta) {
        final String choice = Console.readLine("Deseja especificar Ficha de Produção?: \n" +
                "1. Sim \n" +
                "2. Não");
        if (choice.equalsIgnoreCase("1")) {
           return especificarFichaProducaoUI.doShow(codigoComercial,
                   codigoFabrico,
                   descricaoBreve,
                   descricaoCompleta);
        } else {
           return this.addProduto(codigoComercial,
                    codigoFabrico,
                    descricaoBreve,
                    descricaoCompleta);
        }
    }

    private boolean addProduto(String codigoComercial,
                               String codigoFabrico,
                               String descricaoBreve,
                               String descricaoCompleta) {
        try {
            Produto novoProduto =
                    addProdutoController.addProduto(
                            codigoComercial,
                            codigoFabrico,
                            descricaoBreve,
                            descricaoCompleta
                    );

            System.out.println(novoProduto.toString() + " criado com sucesso");
        } catch (final DataIntegrityViolationException e) {
            System.out.println("Codigo Fabrico de Produto já utilizado");
        } catch (final ConcurrencyException e) {
            LOGGER.error("Isto não devia acontecer", e);
            System.out.println(
                    "Erro inesperado na aplicação. Por favor, tente de novo, se erro continuar contactar administrador.");
        } catch (final IllegalArgumentException e) {
            System.out.println(e.getMessage());
            return true;
        }
        return false;
    }

}
