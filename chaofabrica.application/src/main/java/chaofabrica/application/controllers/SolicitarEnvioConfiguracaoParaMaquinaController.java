package chaofabrica.application.controllers;

import chaofabrica.application.socket.SocketClientSCM;
import chaofabrica.domain.model.maquina.FicheiroConfiguracao;
import chaofabrica.domain.model.maquina.Maquina;
import chaofabrica.domain.repositories.MaquinaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SolicitarEnvioConfiguracaoParaMaquinaController {

    @Autowired
    MaquinaRepository maquinaRepository;
    @Autowired
    SocketClientSCM socketClientSCM;

    public Iterable<Maquina> todasMaquinas(){
        return maquinaRepository.findAll();
    }

    public void enviaConfiguracaoParaMaquina(Maquina maquina, FicheiroConfiguracao ficheiroConfiguracao){
        socketClientSCM.sendConfigMessageToScm(maquina, ficheiroConfiguracao.devolveFicheiro());
    }
}
