package chaofabrica.application.controllers;

import chaofabrica.domain.model.notificacao.Notificacao;
import chaofabrica.domain.repositories.NotificacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ArquivarNotificacaoController{

    @Autowired
    private NotificacaoRepository notificacaoRepository;


    public Iterable<Notificacao> listaNotificacoesAtivas(){
        return notificacaoRepository.listaNotificacoesPorTratar();
    }

    public int arquivaNotificacao(Notificacao notificacao){
        return notificacaoRepository.arquivaNotificacao(notificacao);
    }

    public Notificacao saveNotificacao(Notificacao notificacao){
        return notificacaoRepository.save(notificacao);
    }
}
