package chaofabrica.application.controllers;

import chaofabrica.domain.model.deposito.CodigoAlfanumerico;
import chaofabrica.domain.model.deposito.Deposito;
import chaofabrica.domain.model.deposito.DescricaoDeposito;
import chaofabrica.domain.repositories.DepositoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class EspecificarNovoDepositoController {

    @Autowired
    private DepositoRepository depositoRepository;

    @Transactional
    public Deposito addDeposito(final String codigo, final String descricao) throws DataIntegrityViolationException {
        final Deposito newDeposito = new Deposito(
                new CodigoAlfanumerico(codigo),
                new DescricaoDeposito(descricao)
        );
        return depositoRepository.save(newDeposito);
    }


}
