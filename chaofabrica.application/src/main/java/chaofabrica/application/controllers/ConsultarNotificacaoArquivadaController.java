package chaofabrica.application.controllers;

import chaofabrica.domain.model.linhaproducao.LinhaProducao;
import chaofabrica.domain.model.maquina.CodigoInternoMaquina;
import chaofabrica.domain.model.maquina.Maquina;
import chaofabrica.domain.model.notificacao.Notificacao;
import chaofabrica.domain.model.notificacao.TipoErro;
import chaofabrica.domain.repositories.LinhaProducaoRepository;
import chaofabrica.domain.repositories.MaquinaRepository;
import chaofabrica.domain.repositories.NotificacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import spm.domain.mensagem.Mensagem;
import spm.repository.MensagemRepository;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

@Component
public class ConsultarNotificacaoArquivadaController {

    @Autowired
    private NotificacaoRepository notificacaoRepository;
    @Autowired
    private LinhaProducaoRepository linhaProducaoRepository;
    @Autowired
    private MaquinaRepository maquinaRepository;
    @Autowired
    private MensagemRepository mensagemRepository;

    public Iterable<Notificacao> listaNotificacoesArquivadas(){
        return notificacaoRepository.listaNotificacoesArquivadas();
    }

    public Iterable<Notificacao> listaNotificacoesPorTipoErro(TipoErro erro){
        return notificacaoRepository.listaNotificacoesArquivadasPorTipoErro(erro);
    }

    public List<TipoErro> mostraTipoErro(){
        TipoErro[] todosErros = TipoErro.class.getEnumConstants();
        return Arrays.asList(todosErros);
    }

    public Iterable<LinhaProducao> mostraTodasLinhas(){
        return linhaProducaoRepository.findAll();
    }

    public Iterable<Maquina> mostraTodasMaquinas(){
        return maquinaRepository.findAll();
    }

    public Iterable<Mensagem> listaMensagensPorMaquina(CodigoInternoMaquina codigoInternoMaquina){
        return mensagemRepository.findByCodigoInternoMaquina(codigoInternoMaquina);
    }

    public Iterable<Notificacao>listaNotificacoesPorMensagem(Long pk){
        return notificacaoRepository.findByIdMensagem(pk);
    }

    public Mensagem mostraMensagemPorId(Long pk){
        return mensagemRepository.findByPk(pk);
    }

    public Iterable<Mensagem> mostraMensagensPorData(LocalDateTime inicio, LocalDateTime fim){
        return mensagemRepository.findByDataHoraBetween(inicio, fim);
    }
}
