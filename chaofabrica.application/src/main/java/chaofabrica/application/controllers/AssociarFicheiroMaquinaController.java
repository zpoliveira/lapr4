package chaofabrica.application.controllers;

import chaofabrica.domain.model.maquina.DescricaoFicheiroConfiguracao;
import chaofabrica.domain.model.maquina.FicheiroConfiguracao;
import chaofabrica.domain.model.maquina.Maquina;
import chaofabrica.domain.repositories.FicheiroConfiguracaoMaquinaRepository;
import chaofabrica.domain.repositories.MaquinaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;

@Component
public class AssociarFicheiroMaquinaController {

    @Autowired
    MaquinaRepository maquinaRepository;

    @Autowired
    FicheiroConfiguracaoMaquinaRepository ficheiroConfiguracaoMaquinaRepository;

    public Iterable<Maquina> listarMaquinas(){
        return maquinaRepository.findAll();
    }

    @Transactional
    public FicheiroConfiguracao associarFicheiroConfiguracao(Maquina maquina,
                                                             File ficheiro,
                                                             String descricao)
            throws DataIntegrityViolationException, IllegalArgumentException {
        final DescricaoFicheiroConfiguracao descricaoFicheiroConfiguracao = new DescricaoFicheiroConfiguracao(descricao);
        final FicheiroConfiguracao ficheiroConfiguracao = new FicheiroConfiguracao(descricaoFicheiroConfiguracao, ficheiro);
        maquina.addFicheiroConfiguracao(ficheiroConfiguracao);
        final FicheiroConfiguracao fc = ficheiroConfiguracaoMaquinaRepository.save(ficheiroConfiguracao);
        maquinaRepository.save(maquina);
        return fc;
    }

}
