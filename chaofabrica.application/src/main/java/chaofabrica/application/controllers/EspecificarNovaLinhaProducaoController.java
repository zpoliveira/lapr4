package chaofabrica.application.controllers;

import chaofabrica.domain.model.linhaproducao.LinhaProducao;
import chaofabrica.domain.model.linhaproducao.LinhaProducaoIdentificador;
import chaofabrica.domain.model.maquina.Maquina;
import chaofabrica.domain.repositories.LinhaProducaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;

@Component
public class EspecificarNovaLinhaProducaoController {

    @Autowired
    private LinhaProducaoRepository linhaProducaoRepository;

    @Autowired
    private ListMaquinasWithoutLinhaProducaoService serviceMaquinas;

    public LinhaProducao addLinhaProducao(final String codigo, final List<Maquina> maquinas){
       final LinhaProducao lp = new LinhaProducao(new LinhaProducaoIdentificador(codigo), maquinas);
       return linhaProducaoRepository.save(lp);
    }


    /*public LinhaProducao getLinhaProducao(LinhaProducaoIdentificador str){

        return linhaProducaoRepository.findByPk(str);
    }*/
    public Iterable<Maquina> maquinas() {
        return serviceMaquinas.allMaquinasWithoutLinhaProducao();
    }




}
