package chaofabrica.application.controllers;

import chaofabrica.domain.model.maquina.Maquina;
import chaofabrica.domain.repositories.MaquinaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ListMaquinasWithoutLinhaProducaoService {

    @Autowired
    private MaquinaRepository maquinaRepository;


    public Iterable<Maquina> allMaquinasWithoutLinhaProducao() {
        return maquinaRepository.allMaquinasWithoutLinhaProducao();
    }
}
