package chaofabrica.application.controllers;

import chaofabrica.domain.model.maquina.*;
import chaofabrica.domain.repositories.MaquinaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.time.LocalDate;
import java.util.Set;

@Component
public class DefinirNovaMaquinaController {

    @Autowired
    private MaquinaRepository maquinaRepository;

    @Transactional
    public Maquina addMaquina(final String codigoInterno, final String numSerie, final String marca,
                              final String modelo, final String descricao, final LocalDate dtInstalacao,
                              final Integer codigoUnicoProtocolo
                              ) throws DataIntegrityViolationException {
        final Maquina newMaquina = new Maquina(new CodigoInternoMaquina(codigoInterno),
                                                new NumSerie(numSerie),
                                                new DescricaoMaquina(descricao),
                                                new Marca(marca),
                                                new Modelo(modelo),
                                                new DataInstalacao(dtInstalacao),
                                                new CodigoUnicoProtocolo(codigoUnicoProtocolo));
        return maquinaRepository.save(newMaquina);
    }


}
