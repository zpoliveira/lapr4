package chaofabrica.application.controllers;

import chaofabrica.application.socket.SocketClientSPM;
import chaofabrica.domain.model.linhaproducao.LinhaProducao;
import chaofabrica.domain.repositories.LinhaProducaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class VerificarModificarRecorrenciaController {

    @Autowired
    LinhaProducaoRepository linhaProducaoRepository;
    @Autowired
    SocketClientSPM socketClientSPM;

    public Iterable<LinhaProducao> mostraTodasLinhas(){
        return linhaProducaoRepository.findAll();
    }


    public LinhaProducao desativaRecorrencia(LinhaProducao linhaProducao){
        linhaProducao.desativaProcessamento();
        this.guardaLinhaComRecorrencia(linhaProducao);
        this.socketClientSPM.enviaLinhaRecorreciaSpm(linhaProducao.identity().linhaProducaoID());
        return linhaProducao;
    }

    public LinhaProducao ativaRecorrencia(LinhaProducao linhaProducao){
        linhaProducao.ativaProcessamento();
        return linhaProducao;
    }

    public LinhaProducao definirInicioEFimProcessamento(LinhaProducao linhaProducao, LocalDateTime inicio, LocalDateTime fim){
        linhaProducao.definirInicioEFimProcessamento(inicio, fim);
        this.guardaLinhaComRecorrencia(linhaProducao);
        this.socketClientSPM.enviaIntervaloSpm(linhaProducao.identity().linhaProducaoID(), inicio, fim);
        return linhaProducao;
    }

    public LinhaProducao defineRecorrencia(LinhaProducao linhaProducao, LocalDateTime inicio, int intervalo){
        linhaProducao.defineRecorrencia(inicio, intervalo);
        this.guardaLinhaComRecorrencia(linhaProducao);
        this.socketClientSPM.enviaLinhaRecorreciaSpm(linhaProducao.identity().linhaProducaoID());
        return linhaProducao;
    }

    private LinhaProducao guardaLinhaComRecorrencia(LinhaProducao linhaProducao){
        return linhaProducaoRepository.save(linhaProducao);
    }
}
