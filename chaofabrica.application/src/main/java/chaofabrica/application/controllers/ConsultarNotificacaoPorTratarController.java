package chaofabrica.application.controllers;

import chaofabrica.domain.model.linhaproducao.LinhaProducao;
import chaofabrica.domain.model.notificacao.Notificacao;
import chaofabrica.domain.model.notificacao.TipoErro;
import chaofabrica.domain.repositories.LinhaProducaoRepository;
import chaofabrica.domain.repositories.NotificacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class ConsultarNotificacaoPorTratarController {

    @Autowired
    private NotificacaoRepository notificacaoRepository;
    @Autowired
    private LinhaProducaoRepository linhaProducaoRepository;

    public Slice<Notificacao> listaNotificacoesPorTratar(Pageable page){
        return notificacaoRepository.listaNotificacoesPorTratar(page);
    }

    public Slice<Notificacao> listaNotificacoesPorTratarTipoErro(TipoErro erro,Pageable page){
        return notificacaoRepository.listaNotificacoesPorTratarPorTipoErro(erro,page);
    }

    public Slice<Notificacao> listaNotificacoesPorTratarLinhaProducao(String linhaProducaoID,Pageable page){
        return notificacaoRepository.listaNotificacoesPorTratarLinhaProducao(linhaProducaoID,page);
    }

    public Slice<Notificacao> listaNotificacoesPorTratarTipoErroLinhaProducao(String linhaProducaoID,TipoErro erro,Pageable page){
        return notificacaoRepository.listaNotificacoesPorTratarPorTipoErroLinhaProducao(linhaProducaoID,erro,page);
    }

    public List<TipoErro> mostraTipoErro(){
        TipoErro[] todosErros = TipoErro.class.getEnumConstants();
        return Arrays.asList(todosErros);
    }

    public Iterable<LinhaProducao> listLinhasProducao()
    {
        return linhaProducaoRepository.findAll();
    }

}
