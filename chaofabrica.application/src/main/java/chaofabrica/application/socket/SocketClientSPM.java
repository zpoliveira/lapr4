package chaofabrica.application.socket;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.time.LocalDateTime;

@Component
public class SocketClientSPM {
    @Value("${spm.address}")
    String address;
    @Value("${spm.port}")
    int port;

    public void enviaLinhaRecorreciaSpm(String idLinha) {
        try (Socket socket = new Socket(this.address, this.port)) {

            OutputStream output = socket.getOutputStream();
            PrintWriter writer = new PrintWriter(output, true);
            writer.println(idLinha);
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void enviaIntervaloSpm(String idLinha, LocalDateTime inicio, LocalDateTime fim){
        try (Socket socket = new Socket(this.address, this.port)) {
            OutputStream output = socket.getOutputStream();
            PrintWriter writer = new PrintWriter(output, true);
            writer.println(idLinha + ";" + inicio +";"+fim);
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
