package chaofabrica.application.socket;

import chaofabrica.domain.model.maquina.Maquina;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.net.ssl.*;
import java.io.*;

@Component
public class SocketClientSCM {
    @Value("${scm.address}")
    String address;
    @Value("${scm.port}")
    int port;
    static final String KEYSTORE_PASS = "factyfloor";

    @Value("${scm.cetificates}")
    private String certificatesPath;

    public void sendConfigMessageToScm(Maquina maquina, byte[] ficheiro) {

        byte[] data = new byte[2];
        try {
            String homePath = System.getProperty("user.home");
            // Trust these certificates provided by servers
            System.setProperty("javax.net.ssl.trustStore", homePath + certificatesPath +"Maquina.jks");
            System.setProperty("javax.net.ssl.trustStorePassword",KEYSTORE_PASS);

            // Use this certificate and private key for client certificate when requested by the server
            System.setProperty("javax.net.ssl.keyStore",homePath + certificatesPath +"Maquina.jks");
            System.setProperty("javax.net.ssl.keyStorePassword",KEYSTORE_PASS);

            SSLSocketFactory sslSocketFactory = (SSLSocketFactory) SSLSocketFactory.getDefault();
            SSLSocket socket = (SSLSocket) sslSocketFactory.createSocket(this.address, this.port);
            socket.startHandshake();

            DataOutputStream output = new DataOutputStream(socket.getOutputStream());
            output.write(controiMensagem(maquina, ficheiro));
            DataInputStream sIn = new DataInputStream(socket.getInputStream());
            while (true) {
                int i = sIn.read(data);
                if (i == -1) {
                    break;
                }
                if (data[1] == (byte) 150) {
                    System.out.println("Ack from SCM");
                    break;
                } else {
                    System.out.println("Nack from SCM");
                    break;
                }
            }

        } catch (IOException ex) {
            ex.printStackTrace();
            System.out.println(ex.getMessage());
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private byte[] controiMensagem(Maquina maquina, byte[] ficheiro) {

        int size = ficheiro.length;
        byte[] configMessage = new byte[6 + size];
        configMessage[0] = 0;
        configMessage[1] = (byte) 2;
        configMessage[2] = (byte) (maquina.codigoUnicoProtocolo() % 256);
        configMessage[3] = (byte) (maquina.codigoUnicoProtocolo() / 256);
        configMessage[4] = (byte) (size % 256);
        configMessage[5] = (byte) (size / 256);
        for (int i = 0; i < size; i++) {
            configMessage[6 + i] = ficheiro[i];
        }

        return configMessage;
    }
}
