package chaofabrica.domain.repositories;

import chaofabrica.domain.model.linhaproducao.LinhaProducao;

import java.util.List;

public interface LinhaProducaoRepository {

    LinhaProducao save(LinhaProducao lin);
    Iterable<LinhaProducao> findAll();
    LinhaProducao findByCodigo_linhaProducaoID(String idLinha);
    List<LinhaProducao> findByRecorrenciaProcessamento_estadoProcessamentoTrue();
}
