package chaofabrica.domain.repositories;

import chaofabrica.domain.model.deposito.Deposito;

public interface DepositoRepository {
    Iterable<Deposito> findAll();

    Deposito save(Deposito dep);
}
