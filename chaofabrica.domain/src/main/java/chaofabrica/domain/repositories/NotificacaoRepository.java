package chaofabrica.domain.repositories;

import chaofabrica.domain.model.notificacao.Notificacao;
import chaofabrica.domain.model.notificacao.TipoErro;
import eapli.framework.infrastructure.repositories.Repository;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.repository.query.Param;

public interface NotificacaoRepository extends Repository<Notificacao,Long> {

    Notificacao save(Notificacao notificacao);
    Iterable<Notificacao> listaNotificacoesArquivadas();
    Iterable<Notificacao> listaNotificacoesArquivadasPorTipoErro(TipoErro erro);
    int arquivaNotificacao(Notificacao notificacao);
    Slice<Notificacao> listaNotificacoesPorTratar(Pageable pageable);
    Iterable<Notificacao> listaNotificacoesPorTratar();
    Slice<Notificacao> listaNotificacoesPorTratarPorTipoErro(TipoErro tipoErro, Pageable pageable);
    Iterable<Notificacao> findByIdMensagem(Long pk);
    Slice<Notificacao> listaNotificacoesPorTratarPorTipoErroLinhaProducao(String linhaProducao,TipoErro tipoErro, Pageable pageable);
    Slice<Notificacao> listaNotificacoesPorTratarLinhaProducao(String linhaProducaoID, Pageable page);
}
