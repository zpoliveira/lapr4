package chaofabrica.domain.repositories;

import chaofabrica.domain.model.maquina.Maquina;

public interface MaquinaRepository {

    Iterable<Maquina> allMaquinasWithoutLinhaProducao();

    Iterable<Maquina> findAll();

    Maquina save(Maquina maq);

    Maquina findByCodigoUnicoProtocolo(int codigo);
    
}
