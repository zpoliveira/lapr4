package chaofabrica.domain.repositories;

import chaofabrica.domain.model.maquina.FicheiroConfiguracao;

public interface FicheiroConfiguracaoMaquinaRepository {

    FicheiroConfiguracao save(FicheiroConfiguracao ficheiroConfiguracao);
}
