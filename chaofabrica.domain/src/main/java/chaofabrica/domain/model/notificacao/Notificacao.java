package chaofabrica.domain.model.notificacao;

import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.validations.Preconditions;
import org.apache.commons.lang3.builder.EqualsBuilder;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
public class Notificacao implements AggregateRoot<Long>, Serializable {

    private static final long serialVersionUID = 1L;

    @Version
    private Long version;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long pk;

    @Enumerated(EnumType.ORDINAL)
    private TipoErro tipoErro;

    private Long idMensagem;

    private Boolean notificacaoPorTartar = true;

    private String descricaoErro;

    private String idLinha;

    /**
     * empty contructor for JPA/ORM to work
     */
    protected Notificacao() {
    }

    public Notificacao(TipoErro tipoErro, Long idMensagem) {
        Preconditions.noneNull(tipoErro, idMensagem);
        Preconditions.nonNegative(idMensagem);
        this.tipoErro = tipoErro;
        this.idMensagem = idMensagem;
        this.notificacaoPorTartar = true;
    }

    public Notificacao(TipoErro tipoErro, String erro, String linha) {
        this.tipoErro = tipoErro;
        this.descricaoErro = erro;
        this.idLinha = linha;
        this.notificacaoPorTartar = true;
    }

    @Override
    public boolean sameAs(Object other) {
        if (!(other instanceof Notificacao)) {
            return false;
        } else {
            Notificacao that = (Notificacao) other;
            return (new EqualsBuilder()).append(this.idMensagem, that.idMensagem).isEquals();
        }
    }

    @Override
    public Long identity() {
        return this.idMensagem;
    }

    public boolean isPorTartar(){
        return this.notificacaoPorTartar;
    }

    public TipoErro tipoErro(){
        return this.tipoErro;
    }

    public void arquivaNotificacao(){
        this.notificacaoPorTartar = false;
    }

    public String descricaoErro(){
        return this.descricaoErro;
    }

    public String idLinha(){
        return this.idLinha;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Notificacao that = (Notificacao) o;
        return idMensagem.equals(that.idMensagem);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idMensagem);
    }

    @Override
    public String toString() {
        return "Notificacao: " +
                " | Tipo de Erro: " + tipoErro +
                " | id da Mensagem que gerou o erro: " + idMensagem +
                " | Tratada: " +  !notificacaoPorTartar;
    }
}
