package chaofabrica.domain.model.notificacao;

import eapli.framework.domain.model.ValueObject;

public enum TipoErro implements ValueObject{
    DADOS_INVALIDOS("Dados inválidos"),
    ELEMENTOS_NAO_ESPECIFICADOS("Elementos não específicados no sistema"),
    PARAGEM_FORCADA_MAQUINA("Paragem forçada de máquina"),
    ERRO_ENRIQUECIMENTO("Erro no processo de enriquecimento. mensagem não gravadas.");

    public final String label;

    private TipoErro(String label) {
        this.label = label;
    }
}
