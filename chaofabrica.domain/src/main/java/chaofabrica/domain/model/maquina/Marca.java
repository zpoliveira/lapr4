package chaofabrica.domain.model.maquina;

import eapli.framework.domain.model.ValueObject;
import eapli.framework.validations.Preconditions;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class Marca implements ValueObject, Serializable {
    private static final long serialVersionUID = 1L;

    private final static int MAX_LENGTH = 15;

    @Column(length = MAX_LENGTH)
    @Size(min= 1, max = MAX_LENGTH)
    private String marca;

    protected Marca() {
        marca = null;
    }

    public Marca(String marca) {
        Preconditions.nonNull(marca,"A Marca não pode ser nula.");
        Preconditions.nonEmpty(marca, "A Marca não pode ser vazia.");
        Preconditions.ensure(
                marca.length() <= MAX_LENGTH,
                "Código tem de ter menos de " + MAX_LENGTH + " caracteres"
        );
        this.marca = marca;
    }

    public String marca() {
        return marca;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Marca marca1 = (Marca) o;
        return marca.equals(marca1.marca());
    }

    @Override
    public int hashCode() {
        return Objects.hash(marca);
    }

    @Override
    public String toString() {
        return marca;
    }
}
