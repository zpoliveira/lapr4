package chaofabrica.domain.model.maquina;

import eapli.framework.domain.model.ValueObject;
import eapli.framework.validations.Preconditions;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Embeddable;
import java.util.Objects;

@Embeddable
@Access(AccessType.FIELD)
public class CodigoUnicoProtocolo implements ValueObject,Comparable<CodigoUnicoProtocolo> {

    private static final long serialVersionUID = 1L;

    private final static int MIN_VALUE = 1;
    private final static int MAX_VALUE = 65535;

    private Integer codigoUnicoProtocolo;

    public CodigoUnicoProtocolo() {
    }

    public CodigoUnicoProtocolo(Integer codigoUnicoProtocolo) {
        Preconditions.nonNull(codigoUnicoProtocolo,"O Código Interno do protocolo não pode ser nulo.");
        Preconditions.ensure(codigoUnicoProtocolo >= MIN_VALUE && codigoUnicoProtocolo <= MAX_VALUE,"O Código Único do protocolo tem de ter uma valor entre " + MIN_VALUE + " e " + MAX_VALUE + ", inclusive.");
        this.codigoUnicoProtocolo = codigoUnicoProtocolo;
    }

    public Integer CodigoInternoProtocolo(){
        return this.codigoUnicoProtocolo;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CodigoUnicoProtocolo that = (CodigoUnicoProtocolo) o;
        return Objects.equals(codigoUnicoProtocolo, that.codigoUnicoProtocolo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(codigoUnicoProtocolo);
    }

    @Override
    public int compareTo(CodigoUnicoProtocolo o) {
        return this.codigoUnicoProtocolo.compareTo(o.CodigoInternoProtocolo());
    }

    @Override
    public String toString() {
        return codigoUnicoProtocolo.toString() ;
    }
}
