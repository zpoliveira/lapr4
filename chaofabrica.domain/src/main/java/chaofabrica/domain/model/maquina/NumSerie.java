package chaofabrica.domain.model.maquina;

import eapli.framework.domain.model.ValueObject;
import eapli.framework.validations.Preconditions;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;


@Embeddable
public class NumSerie implements ValueObject, Serializable {
    private static final long serialVersionUID = 1L;

    private final static int MAX_LENGTH = 30;

    @Column(length = MAX_LENGTH)
    @Size(min= 1, max = MAX_LENGTH)
    private String numSerie;

    protected NumSerie() {
        numSerie = null;
    }

    public NumSerie(String numSerie) {
        Preconditions.nonNull(numSerie,"O Nº de Série não pode ser nulo.");
        Preconditions.nonEmpty(numSerie,"O Nº de Série não pode ser vazio.");
        Preconditions.ensure(
                numSerie.length() <= MAX_LENGTH,
                "Código tem de ter menos de " + MAX_LENGTH + " caracteres."
        );
        this.numSerie = numSerie;
    }

    public String numSerie() {
        return numSerie;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NumSerie numSerie1 = (NumSerie) o;
        return numSerie.equals(numSerie1.numSerie);
    }

    @Override
    public int hashCode() {
        return Objects.hash(numSerie);
    }

    @Override
    public String toString() {
        return "NumSerie{" +
                "numSerie='" + numSerie + '\'' +
                '}';
    }
}