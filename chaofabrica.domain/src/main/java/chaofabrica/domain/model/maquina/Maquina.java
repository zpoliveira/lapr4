package chaofabrica.domain.model.maquina;

import chaofabrica.domain.dto.maquina.TMaquina;
import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.representations.RepresentationBuilder;
import eapli.framework.representations.Representationable;
import eapli.framework.representations.dto.DTOable;
import eapli.framework.validations.Preconditions;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(uniqueConstraints =
@UniqueConstraint(columnNames = "codigoInternoMaquina"))
public class Maquina implements AggregateRoot<CodigoInternoMaquina>, Representationable {

    @Version
    private Long version;

    // ORM primary key
    @Id
    @GeneratedValue
    private Long pk;

    // business ID
    @Column(unique = true, nullable = false)
    private CodigoInternoMaquina codigoInternoMaquina;

    @Column(unique = true, nullable = false)
    private CodigoUnicoProtocolo codigoUnicoProtocolo;

    @Column(unique = true, nullable = false)
    private NumSerie numSerie;

    private DescricaoMaquina descMaquina;

    private Marca marca;

    private Modelo modelo;

    private DataInstalacao dataInstalacao;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @Fetch(FetchMode.JOIN)
    private Set<FicheiroConfiguracao> ficheiroConfiguracaoSet;

    public Maquina() {
        codigoInternoMaquina = null;
        numSerie = null;
        descMaquina = null;
        marca = null;
        modelo = null;
        dataInstalacao = null;
    }

    public Maquina(final CodigoInternoMaquina codigoInternoMaquina, final NumSerie numSerie,
                   final DescricaoMaquina descMaquina, final Marca marca, final Modelo modelo,
                   final DataInstalacao dataInstalacao, final CodigoUnicoProtocolo codigoUnicoProtocolo) {
        Preconditions.nonNull(codigoInternoMaquina, "O Código Interno não pode ser nulo.");
        Preconditions.nonNull(codigoUnicoProtocolo, "O Código Unico do Protocolo não pode ser nulo.");
        Preconditions.nonNull(numSerie, "O Nº de Série não pode ser nulo.");
        Preconditions.nonNull(descMaquina, "A descrição não poder ser nula.");
        Preconditions.nonNull(marca, "A Marca não pode ser nula.");
        Preconditions.nonNull(modelo, "O Modelo não pode ser nulo");
        Preconditions.nonNull(dataInstalacao, "A Data Instalação não pode ser nula.");

        this.codigoInternoMaquina = codigoInternoMaquina;
        this.numSerie = numSerie;
        this.descMaquina = descMaquina;
        this.marca = marca;
        this.modelo = modelo;
        this.dataInstalacao = dataInstalacao;
        this.codigoUnicoProtocolo = codigoUnicoProtocolo;
    }

    public String descricao() {
        return this.descMaquina.descricao();
    }

    public CodigoUnicoProtocolo codigoProtocolo(){
        return this.codigoUnicoProtocolo;
    }

    public void addFicheiroConfiguracao(FicheiroConfiguracao ficheiroConfiguracao) {
        this.ficheiroConfiguracaoSet.add(ficheiroConfiguracao);
    }

    public int codigoUnicoProtocolo(){
        return this.codigoUnicoProtocolo.CodigoInternoProtocolo();
    }

    public Set<FicheiroConfiguracao> ficheirosConfiguracao(){
        return this.ficheiroConfiguracaoSet;
    }

    @Override
    public boolean sameAs(Object other) {
        if (!(other instanceof Maquina)) {
            return false;
        } else {
            Maquina that = (Maquina) other;
            return this.codigoInternoMaquina.equals(that.codigoInternoMaquina);
        }
    }

    @Override
    public int compareTo(CodigoInternoMaquina other) {
        return this.codigoInternoMaquina.codigoInternoMaquina().compareTo(other.codigoInternoMaquina());
    }

    @Override
    public CodigoInternoMaquina identity() {
        return this.codigoInternoMaquina;
    }

    @Override
    public boolean hasIdentity(CodigoInternoMaquina otherId) {
        return otherId.codigoInternoMaquina().equals(this.codigoInternoMaquina.codigoInternoMaquina());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Maquina maquina = (Maquina) o;
        return codigoInternoMaquina.equals(maquina.codigoInternoMaquina);
    }

    @Override
    public int hashCode() {
        return Objects.hash(codigoInternoMaquina);
    }

    @Override
    public String toString() {
        return "Maquina{" +
                " codigoInternoMaquina=" + codigoInternoMaquina.codigoInternoMaquina() +
                ", numSerie=" + numSerie.numSerie() +
                ", descMaquina=" + descMaquina.descricao() +
                ", marca=" + marca.marca() +
                ", modelo=" + modelo.modelo() +
                ", dataInstalacao=" + dataInstalacao.dataInstalacao().toString() +
                '}';
    }

    @Override
    public <R> R buildRepresentation(RepresentationBuilder<R> builder) {
        builder.withProperty("codigo", this.codigoInternoMaquina.codigoInternoMaquina())
                .withProperty("descricao", this.descMaquina.descricao())
                .withProperty("marca", this.marca.marca())
                .withProperty("modelo", this.modelo.modelo())
                .withProperty("dataInstall", this.dataInstalacao.dataInstalacao().toString())
                .withProperty("numSerie", this.numSerie.numSerie());

        return builder.build();
    }
}
