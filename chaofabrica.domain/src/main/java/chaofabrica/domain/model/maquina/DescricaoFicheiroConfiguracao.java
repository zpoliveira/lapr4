package chaofabrica.domain.model.maquina;

import eapli.framework.domain.model.ValueObject;
import eapli.framework.validations.Preconditions;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
@Access(AccessType.FIELD)
public class DescricaoFicheiroConfiguracao implements ValueObject, Serializable {

    private static final long serialVersionUID = 1L;

    private final static int MAX_LENGTH = 50;

    @Column(length = MAX_LENGTH)
    @Size(min= 1, max = MAX_LENGTH)
    private String descricaoFicheiroConfiguracao;

    protected DescricaoFicheiroConfiguracao() {
    }

    public DescricaoFicheiroConfiguracao(@Size(min = 1, max = MAX_LENGTH) String descricaoFicheiroConfiguracao) {
        Preconditions.nonNull(descricaoFicheiroConfiguracao,"A Descrição não pode ser nula.");
        Preconditions.nonEmpty(descricaoFicheiroConfiguracao, "A Descrição não pode ser vazia.");
        Preconditions.ensure(
                descricaoFicheiroConfiguracao.length() <= MAX_LENGTH,
                "Tem de ter menos de " + MAX_LENGTH + " caracteres"
        );
        this.descricaoFicheiroConfiguracao = descricaoFicheiroConfiguracao;
    }

    public String descricao() {
        return this.descricaoFicheiroConfiguracao;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DescricaoFicheiroConfiguracao that = (DescricaoFicheiroConfiguracao) o;
        return descricaoFicheiroConfiguracao.equals(that.descricaoFicheiroConfiguracao);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.descricaoFicheiroConfiguracao);
    }

    @Override
    public String toString() {
        return "Descrição do Ficheiro: " +  descricaoFicheiroConfiguracao;
    }
}
