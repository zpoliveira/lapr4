package chaofabrica.domain.model.maquina;

import eapli.framework.domain.model.ValueObject;
import eapli.framework.validations.Preconditions;
import eapli.framework.strings.util.StringPredicates;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
@Access(AccessType.FIELD)
public class CodigoInternoMaquina implements ValueObject, Serializable,Comparable<CodigoInternoMaquina> {
    private static final long serialVersionUID = 1L;

    private final static int MAX_LENGTH = 15;

    @Column(length = MAX_LENGTH)
    @Size(min= 1, max = MAX_LENGTH)
    private String codigoInternoMaquina;

    protected CodigoInternoMaquina() {
        codigoInternoMaquina = null;
    }

    public CodigoInternoMaquina(String codigoInterno) {
        Preconditions.nonNull(codigoInterno,"O Código Interno não pode ser nulo.");
        Preconditions.nonEmpty(codigoInterno,"O Código Interno não pode ser vazio.");
        Preconditions.ensure(StringPredicates.isSingleWord(codigoInterno), "O código não pode conter espaços.");
        Preconditions.ensure(
                codigoInterno.length() <= MAX_LENGTH,
                "Código tem de ter menos de " + MAX_LENGTH + " caracteres"
        );
        this.codigoInternoMaquina = codigoInterno;
    }

    public String codigoInternoMaquina() {
        return codigoInternoMaquina;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CodigoInternoMaquina that = (CodigoInternoMaquina) o;
        return codigoInternoMaquina.equals(that.codigoInternoMaquina);
    }

    @Override
    public int hashCode() {
        return Objects.hash(codigoInternoMaquina);
    }

    @Override
    public int compareTo(CodigoInternoMaquina o) {
       return this.codigoInternoMaquina.compareTo(o.codigoInternoMaquina);
    }

    @Override
    public String toString() {
        return "CodigoInternoMaquina: " + codigoInternoMaquina;
    }
}
