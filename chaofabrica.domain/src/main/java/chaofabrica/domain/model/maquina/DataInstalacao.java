package chaofabrica.domain.model.maquina;

import eapli.framework.domain.model.ValueObject;
import eapli.framework.validations.Preconditions;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

@Embeddable
public class DataInstalacao implements ValueObject, Serializable {
    private static final long serialVersionUID = 1L;

    private LocalDate dataInstalacao;

    public DataInstalacao() {
        dataInstalacao = null;
    }

    public DataInstalacao(LocalDate dataInstalacao) {
        Preconditions.nonNull(dataInstalacao,"A Data Instalação não pode ser nula.");
        Preconditions.ensure(
                dataInstalacao.isBefore(LocalDate.now().plusDays(1)),
                "A data de instalação não pode ser superior ao dia atual."
        );
        this.dataInstalacao = dataInstalacao;
    }

    public LocalDate dataInstalacao() {
        return dataInstalacao;
    }


    @Override
    public String toString() {
        return "DataInstalacao{" +
                "dataInstalacao=" + dataInstalacao.toString() +
                '}';
    }
}
