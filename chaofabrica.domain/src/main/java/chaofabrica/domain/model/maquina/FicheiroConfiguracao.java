package chaofabrica.domain.model.maquina;

import eapli.framework.validations.Preconditions;
import org.apache.commons.io.FileUtils;
import org.springframework.dao.DataIntegrityViolationException;

import javax.persistence.*;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Objects;

@Entity
public class FicheiroConfiguracao implements Serializable {

    private static final long serialVersionUID = 1L;

    @Version
    private Long version;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long pk;


    private DescricaoFicheiroConfiguracao descricaoFicheiroConfiguracao;

    @Lob
    @Column(name = "ficheiroConfiguracao", columnDefinition = "BLOB")
    private byte[] ficheiroConfiguracao = new byte[0];

    protected FicheiroConfiguracao() {
    }

    public FicheiroConfiguracao(final DescricaoFicheiroConfiguracao descricaoFicheiroConfiguracao,
                                final File ficheiroConfiguracao)
            throws DataIntegrityViolationException, IllegalArgumentException {
        Preconditions.noneNull(descricaoFicheiroConfiguracao, ficheiroConfiguracao);
        if(ficheiroConfiguracao.length() == 0){
            throw new IllegalArgumentException("Ficheiro sem conteudo");
        }
        this.descricaoFicheiroConfiguracao = descricaoFicheiroConfiguracao;
        try {
            this.ficheiroConfiguracao = FileUtils.readFileToByteArray(ficheiroConfiguracao);
        } catch (IOException e) {
            System.out.println("Unable to convert file to byte array.");
            e.getMessage();
        }
    }

    public byte[] devolveFicheiro(){
        return this.ficheiroConfiguracao;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FicheiroConfiguracao that = (FicheiroConfiguracao) o;
        return descricaoFicheiroConfiguracao.equals(that.descricaoFicheiroConfiguracao) &&
                Arrays.equals(ficheiroConfiguracao, that.ficheiroConfiguracao);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(descricaoFicheiroConfiguracao);
        result = 31 * result + Arrays.hashCode(ficheiroConfiguracao);
        return result;
    }

    @Override
    public String toString() {
        return descricaoFicheiroConfiguracao.toString();
    }
}
