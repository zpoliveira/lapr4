package chaofabrica.domain.model.maquina;

import eapli.framework.domain.model.ValueObject;
import eapli.framework.validations.Preconditions;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class DescricaoMaquina implements ValueObject, Serializable {
    private static final long serialVersionUID = 1L;

    private final static int MAX_LENGTH = 50;

    @Column(length = MAX_LENGTH)
    @Size(min= 1, max = MAX_LENGTH)
    private String descricaoMaquina;

    protected DescricaoMaquina() {
        descricaoMaquina = null;
    }

    public DescricaoMaquina(String descricaoMaquina) {
        Preconditions.nonNull(descricaoMaquina,"A Descrição não pode ser nula.");
        Preconditions.nonEmpty(descricaoMaquina, "A Descrição não pode ser vazia.");
        Preconditions.ensure(
                descricaoMaquina.length() <= MAX_LENGTH,
                "Código tem de ter menos de " + MAX_LENGTH + " caracteres"
        );
        this.descricaoMaquina = descricaoMaquina;
    }

    public String descricao() {
        return descricaoMaquina;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DescricaoMaquina that = (DescricaoMaquina) o;
        return descricaoMaquina.equals(that.descricaoMaquina);
    }

    @Override
    public int hashCode() {
        return Objects.hash(descricaoMaquina);
    }

    @Override
    public String toString() {
        return "Descricao Maquina: " + descricaoMaquina;
    }
}
