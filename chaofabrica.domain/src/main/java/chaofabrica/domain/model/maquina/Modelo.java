package chaofabrica.domain.model.maquina;

import eapli.framework.domain.model.ValueObject;
import eapli.framework.validations.Preconditions;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class Modelo implements ValueObject, Serializable {
    private static final long serialVersionUID = 1L;

    private final static int MAX_LENGTH = 15;

    @Column(length = MAX_LENGTH)
    @Size(min= 1, max = MAX_LENGTH)
    private String modelo;

    protected Modelo() {
        modelo = null;
    }

    public Modelo(String modelo) {
        Preconditions.nonNull(modelo);
        Preconditions.nonEmpty(modelo);
        Preconditions.ensure(
                modelo.length() <= MAX_LENGTH,
                "Código tem de ter menos de " + MAX_LENGTH + " caracteres"
        );
        this.modelo = modelo;
    }

    public String modelo() {
        return modelo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Modelo modelo1 = (Modelo) o;
        return modelo.equals(modelo1.modelo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(modelo);
    }

    @Override
    public String toString() {
        return "Modelo{" +
                "modelo='" + modelo + '\'' +
                '}';
    }
}
