package chaofabrica.domain.model.linhaproducao;

import eapli.framework.validations.Preconditions;

import javax.persistence.*;
import java.time.LocalDateTime;

@Embeddable
@Access(AccessType.FIELD)
public class RecorrenciaProcessamento {

    @Column(nullable = true)
    private boolean estadoProcessamento;
    private LocalDateTime fimProcessamento;
    private LocalDateTime inicioProcessamento;
    private int intervaloProcessamento;
    private LocalDateTime ultimoProcessamento;

    protected RecorrenciaProcessamento() {
    }

    public RecorrenciaProcessamento(LocalDateTime inicioProcessamento, int intervaloProcessamento) {
        Preconditions.noneNull(intervaloProcessamento, inicioProcessamento);
        this.inicioProcessamento = inicioProcessamento;
        this.estadoProcessamento = true;
        this.intervaloProcessamento = intervaloProcessamento;
    }

    public RecorrenciaProcessamento(LocalDateTime inicioProcessamento, LocalDateTime fimProcessamento) {
        Preconditions.noneNull(fimProcessamento,inicioProcessamento);
        this.fimProcessamento = fimProcessamento;
        this.inicioProcessamento = inicioProcessamento;
        this.estadoProcessamento = false;
    }

    public void mudaUltimoProcessamento(LocalDateTime ultimoProcessamento){
        this.ultimoProcessamento = ultimoProcessamento;
    }

    public void definirInicioEFimProcessamento(LocalDateTime inicio, LocalDateTime fim){
        this.inicioProcessamento = inicio;
        this.fimProcessamento = fim;
    }

    public LocalDateTime mostraInicioProcessamento(){
        return this.inicioProcessamento;
    }

    public LocalDateTime mostraFimProcessamento(){
        return this.fimProcessamento;
    }

    public void defineRecorrencia(LocalDateTime inicio, int min){
        this.intervaloProcessamento = min;
        this.inicioProcessamento = inicio;
    }

    public boolean estadoProcessamentoRecorrente(){
        return this.estadoProcessamento;
    }

    public LocalDateTime ultimoProcessamento(){
        return this.ultimoProcessamento;
    }

    public void desativaProcessamento(){
        this.estadoProcessamento = false;
    }

    public void ativaProcessamento(){
        this.estadoProcessamento = true;
    }

    public int mostraIntervaloProcessamento(){
        return this.intervaloProcessamento;
    }

    @Override
    public String toString() {
        if(estadoProcessamento == true){
            return "Recorrência de Processamento: " + estadoProcessamento + inicioProcessamento + intervaloProcessamento
                    + ultimoProcessamento;
        }else{
            return "Recorrência de Processamento: " + estadoProcessamento + inicioProcessamento + fimProcessamento
                    + ultimoProcessamento;
        }
    }


}
