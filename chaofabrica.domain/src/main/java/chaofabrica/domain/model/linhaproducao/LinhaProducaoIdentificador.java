package chaofabrica.domain.model.linhaproducao;

import eapli.framework.domain.model.ValueObject;
import eapli.framework.validations.Preconditions;
import eapli.framework.strings.util.StringPredicates;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class LinhaProducaoIdentificador implements ValueObject, Serializable, Comparable<LinhaProducaoIdentificador> {
    private static final long serialVersionUID = 1L;

    private final static int MAX_LENGTH = 15;

    @Column(length = MAX_LENGTH)
    @Size(min= 1, max = MAX_LENGTH)
    private String linhaProducaoID;

    protected LinhaProducaoIdentificador() {
        linhaProducaoID = null;
    }

    public LinhaProducaoIdentificador(String id) {
        Preconditions.nonNull(id,"O Código Interno não pode ser nulo.");
        Preconditions.nonEmpty(id,"O Código Interno não pode ser vazio.");
        Preconditions.ensure(StringPredicates.isSingleWord(id), "O código não pode conter espaços.");
        Preconditions.ensure(
                id.length() <= MAX_LENGTH,
                "Código tem de ter menos de " + MAX_LENGTH + " caracteres"
        );
        this.linhaProducaoID = id;
    }

    public String linhaProducaoID(){
        return linhaProducaoID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LinhaProducaoIdentificador that = (LinhaProducaoIdentificador) o;
        return linhaProducaoID.equals(that.linhaProducaoID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(linhaProducaoID);
    }

    @Override
    public String toString() {
        return linhaProducaoID;
    }

    @Override
    public int compareTo(LinhaProducaoIdentificador o) {
        return this.linhaProducaoID.compareTo(o.linhaProducaoID);
    }
}
