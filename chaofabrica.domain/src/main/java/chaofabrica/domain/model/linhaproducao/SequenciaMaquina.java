package chaofabrica.domain.model.linhaproducao;

import chaofabrica.domain.dto.linhasproducao.TSequenciaMaquina;
import chaofabrica.domain.model.maquina.Maquina;
import eapli.framework.domain.model.ValueObject;
import eapli.framework.representations.RepresentationBuilder;
import eapli.framework.representations.Representationable;
import eapli.framework.representations.dto.DTOable;
import eapli.framework.validations.Preconditions;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Entity
public class SequenciaMaquina  implements ValueObject, Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Long pk;

    @ManyToOne
    private final Maquina maquina;

    public SequenciaMaquina() {
        maquina = null;
    }

    public SequenciaMaquina(Maquina maquina) {
        Preconditions.nonNull(maquina);
        Preconditions.nonNull(maquina);
        this.maquina = maquina;
    }

    public Maquina maquina() {
        return maquina;
    }

    @Override
    public String toString() {
        return maquina.identity().codigoInternoMaquina() ;
    }

}
