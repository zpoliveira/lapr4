package chaofabrica.domain.model.linhaproducao;

import chaofabrica.domain.model.maquina.Maquina;
import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.representations.RepresentationBuilder;
import eapli.framework.representations.Representationable;
import eapli.framework.validations.Preconditions;
import org.apache.commons.lang3.builder.EqualsBuilder;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
public class LinhaProducao implements AggregateRoot<LinhaProducaoIdentificador>, Serializable, Representationable {

    private final static int MAX_LENGTH = 15;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @OrderColumn(name = "INDEX")
    private final List<SequenciaMaquina> maquinas = new ArrayList<>();
    @Version
    private Long version;
    @EmbeddedId
    private LinhaProducaoIdentificador codigo;
    @Embedded
    private RecorrenciaProcessamento recorrenciaProcessamento;

    public LinhaProducao() {
    }

    public LinhaProducao(final LinhaProducaoIdentificador codigo, final List<Maquina> maquinas) {
        Preconditions.nonNull(codigo, "O Código não pode ser nulo.");
        Preconditions.nonNull(maquinas, "Têm de indicar pelo menos 1 máquina.");
        Preconditions.ensure(!maquinas.isEmpty(), "Têm de indicar pelo menos 1 máquina.");
        this.codigo = codigo;
        addMaquinas(maquinas);
        this.recorrenciaProcessamento = new RecorrenciaProcessamento(LocalDateTime.now(), LocalDateTime.now().plusNanos(1));
    }

    public boolean addMaquina(final Maquina maq) {
        final SequenciaMaquina sequenciaMaquina = new SequenciaMaquina(maq);
        return maquinas.add(sequenciaMaquina);
    }

    private void addMaquinas(final List<Maquina> maquinas) {
        for (final Maquina maq : maquinas) {
            addMaquina(maq);
        }
    }


    public List<SequenciaMaquina> listaMaquinas() {
        return this.maquinas;
    }

    public boolean estadoRecorrencia(){
        return this.recorrenciaProcessamento.estadoProcessamentoRecorrente();
    }

    public LocalDateTime ultimoProcessamento(){
        return this.recorrenciaProcessamento.ultimoProcessamento();
    }

    public void mudaUltimoProcesamento(LocalDateTime ultimoProcessamento){
        this.recorrenciaProcessamento.mudaUltimoProcessamento(ultimoProcessamento);
    }

    public void desativaProcessamento(){
        this.recorrenciaProcessamento.desativaProcessamento();
    }

    public void ativaProcessamento(){
        this.recorrenciaProcessamento.ativaProcessamento();
    }


    public void definirInicioEFimProcessamento(LocalDateTime inicio, LocalDateTime fim){
        this.recorrenciaProcessamento.definirInicioEFimProcessamento(inicio, fim);
    }

    public LocalDateTime mostraInicioPeriodo(){
        return this.recorrenciaProcessamento.mostraInicioProcessamento();
    }

    public LocalDateTime mostraFimPeriodo(){
        return this.recorrenciaProcessamento.mostraFimProcessamento();
    }

    public void defineRecorrencia(LocalDateTime inicio, int min){
        this.recorrenciaProcessamento.defineRecorrencia(inicio, min);
    }

    public int mostraIntervaloProcessamento(){
        return this.recorrenciaProcessamento.mostraIntervaloProcessamento();
    }

    @Override
    public boolean sameAs(Object other) {
        if (!(other instanceof Maquina)) {
            return false;
        } else {
            LinhaProducao that = (LinhaProducao) other;
            return (new EqualsBuilder()).append(this.codigo, that.codigo).isEquals();
        }
    }

    @Override
    public int compareTo(LinhaProducaoIdentificador other) {
        return this.codigo.linhaProducaoID().compareTo(other.linhaProducaoID());
    }

    @Override
    public LinhaProducaoIdentificador identity() {
        return this.codigo;
    }

    @Override
    public boolean hasIdentity(LinhaProducaoIdentificador otherId) {
        return otherId.equals(this.codigo);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LinhaProducao that = (LinhaProducao) o;
        return codigo.equals(that.codigo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(codigo);
    }

    @Override
    public String toString() {
        return "LinhaProducao{" +
                "codigo='" + codigo + '\'' +
                ", maquinas=" + maquinas +
                '}';
    }

    @Override
    public <R> R buildRepresentation(RepresentationBuilder<R> builder) {
        builder.startObject("linha")
                .withProperty("codigo", this.codigo.linhaProducaoID());
        if (!this.maquinas.isEmpty()) {
            this.maquinas.forEach(sequencia ->
                    builder.startObject("sequencia")
                            .withProperty("maquina",
                                    String.format("%d;%s",
                                            maquinas.indexOf(sequencia),
                                            sequencia.maquina().identity().codigoInternoMaquina()))
                            .endObject());
        }

        return builder.build();
    }
}
