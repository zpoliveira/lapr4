package chaofabrica.domain.model.deposito;

import eapli.framework.domain.model.ValueObject;
import eapli.framework.validations.Preconditions;
import eapli.framework.strings.util.StringPredicates;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
@Access(AccessType.FIELD)
public class CodigoAlfanumerico implements ValueObject, Serializable,Comparable<CodigoAlfanumerico> {

    private static final long serialVersionUID = 1L;

    private final static int MAX_LENGTH = 15;

    @Column(length = MAX_LENGTH)
    @Size(min= 1, max = MAX_LENGTH)
    private String codigoAlfaNumerico;

    public CodigoAlfanumerico() {
    }

    public CodigoAlfanumerico(@Size(min = 1, max = MAX_LENGTH) String codigoAlfaNumerico) {
            Preconditions.nonNull(codigoAlfaNumerico,"O Código não pode ser nulo.");
            Preconditions.nonEmpty(codigoAlfaNumerico,"O Código não pode ser vazio.");
            Preconditions.ensure(StringPredicates.isSingleWord(codigoAlfaNumerico),"O Código não pode conter espaços.");
            Preconditions.ensure(
                    codigoAlfaNumerico.length() <= MAX_LENGTH,
                    "Código tem de ter menos de " + MAX_LENGTH + " caracteres"
            );
            Preconditions.ensure(codigoAlfaNumerico.matches("[A-Za-z0-9]+"),"O Código só pode conter carateres alfanúmericos");
            this.codigoAlfaNumerico = codigoAlfaNumerico;
    }

    public String codigo(){
        return codigoAlfaNumerico;
    }

    @Override
    public int compareTo(CodigoAlfanumerico o) {
        return this.codigoAlfaNumerico.compareTo(o.codigoAlfaNumerico);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CodigoAlfanumerico that = (CodigoAlfanumerico) o;
        return Objects.equals(codigoAlfaNumerico, that.codigoAlfaNumerico);
    }

    @Override
    public int hashCode() {
        return Objects.hash(codigoAlfaNumerico);
    }

    @Override
    public String toString() {
        return codigoAlfaNumerico;
    }
}
