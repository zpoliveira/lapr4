package chaofabrica.domain.model.deposito;

import eapli.framework.domain.model.ValueObject;
import eapli.framework.validations.Preconditions;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class DescricaoDeposito implements ValueObject, Serializable {

    private static final long serialVersionUID = 1L;

    private final static int MAX_LENGTH = 50;

    @Column(length = MAX_LENGTH)
    @Size(min= 1, max = MAX_LENGTH)
    private String descricaoDeposito;

    public DescricaoDeposito() {
    }

    public DescricaoDeposito(@Size(min = 1, max = MAX_LENGTH) String descricaoDeposito) {
        Preconditions.nonNull(descricaoDeposito,"O Descrição não pode ser nula.");
        Preconditions.nonEmpty(descricaoDeposito,"O Descrição não pode ser vazia.");
        Preconditions.ensure(
                descricaoDeposito.length() <= MAX_LENGTH,
                "Código tem de ter menos de " + MAX_LENGTH + " caracteres."
        );
        this.descricaoDeposito = descricaoDeposito;
    }

    public String Descricao(){
        return descricaoDeposito;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DescricaoDeposito that = (DescricaoDeposito) o;
        return descricaoDeposito.equals(that.descricaoDeposito);
    }

    @Override
    public int hashCode() {
        return Objects.hash(descricaoDeposito);
    }

    @Override
    public String toString() {
        return descricaoDeposito;
    }
}
