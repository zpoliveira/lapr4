package chaofabrica.domain.model.deposito;


import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.representations.RepresentationBuilder;
import eapli.framework.representations.Representationable;
import eapli.framework.validations.Preconditions;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Version;
import java.util.Objects;

@Entity
public class Deposito implements AggregateRoot<CodigoAlfanumerico>, Representationable {

    @Version
    private Long version;

    @EmbeddedId
    private CodigoAlfanumerico codigo;

    private DescricaoDeposito descricao;

    public Deposito() {
    }

    public Deposito(CodigoAlfanumerico codigo, DescricaoDeposito descricao) {
        Preconditions.nonNull(codigo, "O Código não pode ser nulo.");
        Preconditions.nonNull(descricao, "A Descrição não pode ser nula.");
        this.codigo = codigo;
        this.descricao = descricao;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Deposito deposito = (Deposito) o;
        return codigo.equals(deposito.codigo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(codigo);
    }

    @Override
    public boolean sameAs(Object other) {
        return false;
    }

    @Override
    public String toString() {
        return "Deposito{" +
                "codigo=" + codigo.toString() +
                ", descricao=" + descricao.toString() +
                '}';
    }

    @Override
    public CodigoAlfanumerico identity() {
        return codigo;
    }

    @Override
    public <R> R buildRepresentation(RepresentationBuilder<R> builder) {
        builder.withProperty("codigo", this.codigo.codigo())
                .withProperty("descricao", this.descricao.Descricao());

        return builder.build();
    }
}
