
package chaofabrica.domain.dto.deposito;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TDepositos complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TDepositos"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Deposito" type="{http://factyfloor.com/Depositos}TDeposito" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TDepositos", namespace = "http://factyfloor.com/Depositos", propOrder = {
    "deposito"
})
public class TDepositos {

    @XmlElement(name = "Deposito", namespace = "http://factyfloor.com/Depositos", required = true)
    protected List<TDeposito> deposito;

    /**
     * Gets the value of the deposito property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the deposito property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDeposito().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TDeposito }
     * 
     * 
     */
    public List<TDeposito> getDeposito() {
        if (deposito == null) {
            deposito = new ArrayList<TDeposito>();
        }
        return this.deposito;
    }

}
