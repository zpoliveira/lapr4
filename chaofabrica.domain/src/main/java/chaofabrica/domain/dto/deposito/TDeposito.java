
package chaofabrica.domain.dto.deposito;

import eapli.framework.representations.RepresentationBuilder;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TDeposito complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TDeposito"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CodigoAlfanumerico" type="{http://factyfloor.com/Depositos}TCodigoAlfanumerico"/&gt;
 *         &lt;element name="Descricao" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TDeposito", namespace = "http://factyfloor.com/Depositos", propOrder = {
    "codigoAlfanumerico",
    "descricao"
})
public class TDeposito {

    @XmlElement(name = "CodigoAlfanumerico", namespace = "http://factyfloor.com/Depositos", required = true)
    protected String codigoAlfanumerico;
    @XmlElement(name = "Descricao", namespace = "http://factyfloor.com/Depositos", required = true)
    protected String descricao;

    /**
     * Gets the value of the codigoAlfanumerico property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoAlfanumerico() {
        return codigoAlfanumerico;
    }

    /**
     * Sets the value of the codigoAlfanumerico property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoAlfanumerico(String value) {
        this.codigoAlfanumerico = value;
    }

    /**
     * Gets the value of the descricao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * Sets the value of the descricao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescricao(String value) {
        this.descricao = value;
    }

}
