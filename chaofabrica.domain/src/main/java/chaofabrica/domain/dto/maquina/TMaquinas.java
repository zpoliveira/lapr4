
package chaofabrica.domain.dto.maquina;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for TMaquinas complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TMaquinas"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Maquina" type="{http://factyfloor.com/Maquinas}TMaquina" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TMaquinas", namespace = "http://factyfloor.com/Maquinas", propOrder = {
    "maquina"
})
public class TMaquinas {

    @XmlElement(name = "Maquina", namespace = "http://factyfloor.com/Maquinas", required = true)
    protected List<TMaquina> maquina;

    /**
     * Gets the value of the maquina property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the maquina property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMaquina().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TMaquina }
     * 
     * 
     */
    public List<TMaquina> getMaquina() {
        if (maquina == null) {
            maquina = new ArrayList<TMaquina>();
        }
        return this.maquina;
    }

}
