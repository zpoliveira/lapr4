
package chaofabrica.domain.dto.maquina;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TMaquina complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TMaquina"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;all&gt;
 *         &lt;element name="CodigoInterno" type="{http://factyfloor.com/CommunTypes}TOneWord_1_15"/&gt;
 *         &lt;element name="Descricao" type="{http://factyfloor.com/CommunTypes}TString1_50"/&gt;
 *         &lt;element name="Marca" type="{http://factyfloor.com/CommunTypes}TString1_15"/&gt;
 *         &lt;element name="Modelo" type="{http://factyfloor.com/CommunTypes}TString1_15"/&gt;
 *         &lt;element name="DataInstalacao" type="{http://factyfloor.com/CommunTypes}TData"/&gt;
 *         &lt;element name="NumeroSerie" type="{http://factyfloor.com/CommunTypes}TString1_50"/&gt;
 *       &lt;/all&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TMaquina", namespace = "http://factyfloor.com/Maquinas", propOrder = {

})
public class TMaquina {

    @XmlElement(name = "CodigoInterno", namespace = "http://factyfloor.com/Maquinas", required = true)
    protected String codigoInterno;
    @XmlElement(name = "Descricao", namespace = "http://factyfloor.com/Maquinas", required = true)
    protected String descricao;
    @XmlElement(name = "Marca", namespace = "http://factyfloor.com/Maquinas", required = true)
    protected String marca;
    @XmlElement(name = "Modelo", namespace = "http://factyfloor.com/Maquinas", required = true)
    protected String modelo;
    @XmlElement(name = "DataInstalacao", namespace = "http://factyfloor.com/Maquinas", required = true)
    protected String dataInstalacao;
    @XmlElement(name = "NumeroSerie", namespace = "http://factyfloor.com/Maquinas", required = true)
    protected String numeroSerie;

    /**
     * Gets the value of the codigoInterno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoInterno() {
        return codigoInterno;
    }

    /**
     * Sets the value of the codigoInterno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoInterno(String value) {
        this.codigoInterno = value;
    }

    /**
     * Gets the value of the descricao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * Sets the value of the descricao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescricao(String value) {
        this.descricao = value;
    }

    /**
     * Gets the value of the marca property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMarca() {
        return marca;
    }

    /**
     * Sets the value of the marca property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMarca(String value) {
        this.marca = value;
    }

    /**
     * Gets the value of the modelo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModelo() {
        return modelo;
    }

    /**
     * Sets the value of the modelo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModelo(String value) {
        this.modelo = value;
    }

    /**
     * Gets the value of the dataInstalacao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataInstalacao() {
        return dataInstalacao;
    }

    /**
     * Sets the value of the dataInstalacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataInstalacao(String value) {
        this.dataInstalacao = value;
    }

    /**
     * Gets the value of the numeroSerie property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroSerie() {
        return numeroSerie;
    }

    /**
     * Sets the value of the numeroSerie property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroSerie(String value) {
        this.numeroSerie = value;
    }

}
