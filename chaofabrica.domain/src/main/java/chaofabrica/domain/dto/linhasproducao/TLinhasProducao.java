
package chaofabrica.domain.dto.linhasproducao;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TLinhasProducao complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TLinhasProducao"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="LinhaProducao" type="{http://factyfloor.com/LinhasProducao}TLinhaProducao" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TLinhasProducao", namespace = "http://factyfloor.com/LinhasProducao", propOrder = {
    "linhaProducao"
})
public class TLinhasProducao {

    @XmlElement(name = "LinhaProducao", namespace = "http://factyfloor.com/LinhasProducao", required = true)
    protected List<TLinhaProducao> linhaProducao;

    /**
     * Gets the value of the linhaProducao property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the linhaProducao property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLinhaProducao().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TLinhaProducao }
     * 
     * 
     */
    public List<TLinhaProducao> getLinhaProducao() {
        if (linhaProducao == null) {
            linhaProducao = new ArrayList<TLinhaProducao>();
        }
        return this.linhaProducao;
    }

}
