
package chaofabrica.domain.dto.linhasproducao;

import javax.xml.bind.annotation.*;
import java.math.BigInteger;


/**
 * <p>Java class for TSequenciaMaquina complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TSequenciaMaquina"&gt;
 *   &lt;simpleContent&gt;
 *     &lt;extension base="&lt;http://factyfloor.com/CommunTypes&gt;TOneWord_1_15"&gt;
 *       &lt;attribute name="Index" use="required" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/simpleContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TSequenciaMaquina", namespace = "http://factyfloor.com/LinhasProducao", propOrder = {
    "value"
})
public class TSequenciaMaquina {

    @XmlValue
    protected String value;
    @XmlAttribute(name = "Index", required = true)
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger index;

    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Gets the value of the index property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getIndex() {
        return index;
    }

    /**
     * Sets the value of the index property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setIndex(BigInteger value) {
        this.index = value;
    }

}
