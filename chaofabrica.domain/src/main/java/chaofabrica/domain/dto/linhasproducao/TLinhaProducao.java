
package chaofabrica.domain.dto.linhasproducao;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for TLinhaProducao complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TLinhaProducao"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SequenciaMaquina" type="{http://factyfloor.com/LinhasProducao}TSequenciaMaquina" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="Codigo" type="{http://factyfloor.com/CommunTypes}TString1_15" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TLinhaProducao", namespace = "http://factyfloor.com/LinhasProducao", propOrder = {
    "sequenciaMaquina"
})
public class TLinhaProducao {

    @XmlElement(name = "SequenciaMaquina", namespace = "http://factyfloor.com/LinhasProducao", required = true)
    protected List<TSequenciaMaquina> sequenciaMaquina;
    @XmlAttribute(name = "Codigo")
    protected String codigo;

    /**
     * Gets the value of the sequenciaMaquina property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sequenciaMaquina property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSequenciaMaquina().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TSequenciaMaquina }
     * 
     * 
     */
    public List<TSequenciaMaquina> getSequenciaMaquina() {
        if (sequenciaMaquina == null) {
            sequenciaMaquina = new ArrayList<TSequenciaMaquina>();
        }
        return this.sequenciaMaquina;
    }

    /**
     * Gets the value of the codigo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * Sets the value of the codigo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigo(String value) {
        this.codigo = value;
    }

}
