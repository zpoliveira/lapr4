package chaofabrica.domain.model.maquina;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.*;

public class FicheiroConfiguracaoTest {

    private File emptyFile = new File("");
    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFicheiroNotNull(){
        DescricaoFicheiroConfiguracao d = new DescricaoFicheiroConfiguracao("teste");
        FicheiroConfiguracao f = new FicheiroConfiguracao(d,null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFicheiroNotEmpty(){
        DescricaoFicheiroConfiguracao d = new DescricaoFicheiroConfiguracao("teste");
        FicheiroConfiguracao f = new FicheiroConfiguracao(d,emptyFile);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDescricaoNotNull(){
        DescricaoFicheiroConfiguracao d = new DescricaoFicheiroConfiguracao("teste");
        FicheiroConfiguracao f = new FicheiroConfiguracao(null,emptyFile);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDescricaoNotEmpty(){
        DescricaoFicheiroConfiguracao d = new DescricaoFicheiroConfiguracao("");
        FicheiroConfiguracao f = new FicheiroConfiguracao(d,emptyFile);
    }
}