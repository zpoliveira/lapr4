package chaofabrica.domain.model.maquina;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class DescricaoFicheiroConfiguracaoTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDescricaoNotNull(){
        DescricaoFicheiroConfiguracao d = new DescricaoFicheiroConfiguracao(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDescricaoNotEmpty(){
        DescricaoFicheiroConfiguracao d = new DescricaoFicheiroConfiguracao("");
    }
}