package chaofabrica.domain.model.maquina;

import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDate;


public class MaquinaTest {

    @Test(expected = IllegalArgumentException.class)
    public void testCodigoInternoNull() {
        Maquina maq = new Maquina(null,new NumSerie("1"), new DescricaoMaquina("desc"), new Marca("marca"), new Modelo("modelo"), new DataInstalacao(LocalDate.now()),new CodigoUnicoProtocolo(1));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCodigoInternoEmpty() {
        Maquina maq = new Maquina(new CodigoInternoMaquina(""),new NumSerie("1"), new DescricaoMaquina("desc"), new Marca("marca"), new Modelo("modelo"), new DataInstalacao(LocalDate.now()),new CodigoUnicoProtocolo(1));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNumSerieNull() {
        Maquina maq = new Maquina(new CodigoInternoMaquina("cod"),null, new DescricaoMaquina("desc"), new Marca("marca"), new Modelo("modelo"), new DataInstalacao(LocalDate.now()),new CodigoUnicoProtocolo(1));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNumSerieEmpty() {
        Maquina maq = new Maquina(new CodigoInternoMaquina("cod"),new NumSerie(""), new DescricaoMaquina("desc"), new Marca("marca"), new Modelo("modelo"), new DataInstalacao(LocalDate.now()),new CodigoUnicoProtocolo(1));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDescNull() {
        Maquina maq = new Maquina(new CodigoInternoMaquina("cod"),new NumSerie("1"), null, new Marca("marca"), new Modelo("modelo"), new DataInstalacao(LocalDate.now()),new CodigoUnicoProtocolo(1));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDescEmpty() {
        Maquina maq = new Maquina(new CodigoInternoMaquina("cod"),new NumSerie("1"), new DescricaoMaquina(""), new Marca("marca"), new Modelo("modelo"), new DataInstalacao(LocalDate.now()),new CodigoUnicoProtocolo(1));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMarcaNull() {
        Maquina maq = new Maquina(new CodigoInternoMaquina("cod"),new NumSerie("1"), new DescricaoMaquina("desc"), null, new Modelo("modelo"), new DataInstalacao(LocalDate.now()),new CodigoUnicoProtocolo(1));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMarcaEmpty() {
        Maquina maq = new Maquina(new CodigoInternoMaquina("cod"),new NumSerie("1"), new DescricaoMaquina("desc"), new Marca(""), new Modelo("modelo"), new DataInstalacao(LocalDate.now()),new CodigoUnicoProtocolo(1));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testModeloNull() {
        Maquina maq = new Maquina(new CodigoInternoMaquina("cod"),new NumSerie("1"), new DescricaoMaquina("desc"), new Marca("marca"), null, new DataInstalacao(LocalDate.now()),new CodigoUnicoProtocolo(1));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testModeloEmpty() {
        Maquina maq = new Maquina(new CodigoInternoMaquina("cod"),new NumSerie("1"), new DescricaoMaquina("desc"), new Marca("marca"), new Modelo(""), new DataInstalacao(LocalDate.now()),new CodigoUnicoProtocolo(1));
    }
    @Test(expected = IllegalArgumentException.class)
    public void testDataInstalacaoNull() {
        Maquina maq = new Maquina(new CodigoInternoMaquina("cod"),new NumSerie("1"), new DescricaoMaquina("desc"), new Marca("marca"), new Modelo("modelo"), null,new CodigoUnicoProtocolo(1));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDataInstalacaoMaiorQueDiaAtual() {
        Maquina maq = new Maquina(new CodigoInternoMaquina("cod"),new NumSerie("1"), new DescricaoMaquina("desc"), new Marca("marca"), new Modelo("modelo"), new DataInstalacao(LocalDate.now().plusDays(1)),new CodigoUnicoProtocolo(1));
    }

}