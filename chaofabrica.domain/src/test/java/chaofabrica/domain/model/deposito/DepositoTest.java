package chaofabrica.domain.model.deposito;
import org.junit.Test;

public class DepositoTest {

    @Test(expected = IllegalArgumentException.class)
    public void testCodigoNull() {
        Deposito dep = new Deposito(null,new DescricaoDeposito("desc"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCodigoEmpty() {
        Deposito dep = new Deposito(new CodigoAlfanumerico(""),new DescricaoDeposito("desc"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCodigoNotAlfaNumeric() {
        Deposito dep = new Deposito(new CodigoAlfanumerico("gd56.sd"),new DescricaoDeposito("desc"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDescricaoNull() {
        Deposito dep = new Deposito(new CodigoAlfanumerico("Cod"),null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDescricaoEmpty() {
        Deposito dep = new Deposito(new CodigoAlfanumerico("Cod"),new DescricaoDeposito(""));
    }

}