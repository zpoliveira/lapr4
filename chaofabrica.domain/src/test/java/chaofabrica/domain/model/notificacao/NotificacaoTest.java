package chaofabrica.domain.model.notificacao;

import org.junit.Test;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class NotificacaoTest {

    @Test(expected = IllegalArgumentException.class)
    public void testeEstadoNotificacaoUnico() {

        Set<Notificacao> setNot = new HashSet<>();

        Notificacao notificacao = new Notificacao(TipoErro.DADOS_INVALIDOS, 1L);
        Notificacao not2 = new Notificacao(TipoErro.DADOS_INVALIDOS, 1L);
        not2.arquivaNotificacao();
        setNot.add(notificacao);
        if(setNot.contains(not2) && setNot.iterator().next().isPorTartar() != not2.isPorTartar()){
            throw new IllegalArgumentException("Notificação não pode ser ativa e arquivada ao mesmo tempo");
        }

    }

    @Test
    public void testeEstadoNotificacaoAtivo() {

        Notificacao notificacao = new Notificacao(TipoErro.ELEMENTOS_NAO_ESPECIFICADOS, 1L);
        Boolean expected = true;

        assertEquals(expected, notificacao.isPorTartar());

    }

    @Test(expected = IllegalArgumentException.class)
    public void testeErroNotificacaoNull() {

        Notificacao notificacao = new Notificacao(null, 1L);

    }

    @Test(expected = IllegalArgumentException.class)
    public void testeErroNotificacaoUnico() {

        Set<Notificacao> setNot = new HashSet<>();

        Notificacao notificacao = new Notificacao(TipoErro.DADOS_INVALIDOS, 1L);
        Notificacao not2 = new Notificacao(TipoErro.ELEMENTOS_NAO_ESPECIFICADOS, 1L);
        setNot.add(notificacao);
        if(setNot.contains(not2) && setNot.iterator().next().tipoErro().ordinal() != not2.tipoErro().ordinal()){
            throw new IllegalArgumentException("Notificação não pode ter mais que um tipo de erro");
        }

    }

}