package chaofabrica.domain.model.linhaproducao;

import chaofabrica.domain.model.maquina.*;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.util.*;

public class LinhaProducaoTest {

    final private List<Maquina> maquinas= new ArrayList<>();

    @Before
    public void setUp() throws Exception {
        maquinas.add(new Maquina(new CodigoInternoMaquina("001"),new NumSerie("123"),new DescricaoMaquina("Máquina 2"), new Marca("Marca"), new Modelo("Modelo"), new DataInstalacao(LocalDate.now()), new CodigoUnicoProtocolo(1)));
        maquinas.add(new Maquina(new CodigoInternoMaquina("002"),new NumSerie("456"),new DescricaoMaquina("Máquina 2"), new Marca("Marca"), new Modelo("Modelo"), new DataInstalacao(LocalDate.now()), new CodigoUnicoProtocolo(2)));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCodigoNull() {
        LinhaProducao lin = new LinhaProducao(null,maquinas);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCodigoEmpty() {
        LinhaProducao lin = new LinhaProducao(new LinhaProducaoIdentificador(""),maquinas);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMaquinasNull() {
        LinhaProducao lin = new LinhaProducao(new LinhaProducaoIdentificador("LIN1"),null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMaquinasEmpty() {
        LinhaProducao lin = new LinhaProducao(new LinhaProducaoIdentificador("LIN1"), new ArrayList<>());
    }
}