package chaofabrica.domain.model.linhaproducao;


import chaofabrica.domain.model.maquina.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

import org.junit.Test;

public class RecorrenciaProcessamentoTest {


    @Test
    public void ultimoProcessamento() {
        LocalDate d1 = LocalDate.of(2000, Month.AUGUST, 1);
        Maquina m1 = new Maquina(new CodigoInternoMaquina("M1"), new NumSerie("1"), new DescricaoMaquina("M1"),
                new Marca("Marca1"), new Modelo("Modelo1"), new DataInstalacao(d1), new CodigoUnicoProtocolo(1));
        Maquina m2 = new Maquina(new CodigoInternoMaquina("M2"), new NumSerie("2"), new DescricaoMaquina("M2"),
                new Marca("Marca2"), new Modelo("Modelo2"), new DataInstalacao(d1), new CodigoUnicoProtocolo(2));
        List<Maquina> lista = new ArrayList<>();
        lista.add(m1);
        lista.add(m2);

        LinhaProducao linha = new LinhaProducao(new LinhaProducaoIdentificador("Linha1"), lista);


        LocalDateTime d2 = LocalDateTime.of(2020,Month.AUGUST,20,10,10);
        linha.mudaUltimoProcesamento(d2);


        assertEquals(d2, linha.ultimoProcessamento());
    }

    @Test
    public void desativaProcessamento() {
        LocalDate d1 = LocalDate.of(2000, Month.AUGUST, 1);
        Maquina m1 = new Maquina(new CodigoInternoMaquina("M1"), new NumSerie("1"), new DescricaoMaquina("M1"),
                new Marca("Marca1"), new Modelo("Modelo1"), new DataInstalacao(d1), new CodigoUnicoProtocolo(1));
        Maquina m2 = new Maquina(new CodigoInternoMaquina("M2"), new NumSerie("2"), new DescricaoMaquina("M2"),
                new Marca("Marca2"), new Modelo("Modelo2"), new DataInstalacao(d1), new CodigoUnicoProtocolo(2));
        List<Maquina> lista = new ArrayList<>();
        lista.add(m1);
        lista.add(m2);

        LinhaProducao linha = new LinhaProducao(new LinhaProducaoIdentificador("Linha1"), lista);

        linha.desativaProcessamento();

        assertFalse(linha.estadoRecorrencia());
    }

    @Test
    public void ativaProcessamento() {

        LocalDate d1 = LocalDate.of(2000, Month.AUGUST, 1);
        Maquina m1 = new Maquina(new CodigoInternoMaquina("M1"), new NumSerie("1"), new DescricaoMaquina("M1"),
                new Marca("Marca1"), new Modelo("Modelo1"), new DataInstalacao(d1), new CodigoUnicoProtocolo(1));
        Maquina m2 = new Maquina(new CodigoInternoMaquina("M2"), new NumSerie("2"), new DescricaoMaquina("M2"),
                new Marca("Marca2"), new Modelo("Modelo2"), new DataInstalacao(d1), new CodigoUnicoProtocolo(2));
        List<Maquina> lista = new ArrayList<>();
        lista.add(m1);
        lista.add(m2);

        LinhaProducao linha = new LinhaProducao(new LinhaProducaoIdentificador("Linha1"), lista);

        linha.ativaProcessamento();

        assertTrue(linha.estadoRecorrencia());
    }


    @Test(expected = IllegalArgumentException.class)
    public void inicioProcessamentoNull(){
        LocalDateTime inicio = LocalDateTime.of(2020, Month.DECEMBER, 25, 10, 10);
        LocalDateTime fim = LocalDateTime.of(2020, Month.DECEMBER, 25, 20, 30);

        RecorrenciaProcessamento rec = new RecorrenciaProcessamento(null, fim);
    }

    @Test(expected = IllegalArgumentException.class)
    public void fimProcessamentoNull(){
        LocalDateTime inicio = LocalDateTime.of(2020, Month.DECEMBER, 25, 10, 10);
        LocalDateTime fim = LocalDateTime.of(2020, Month.DECEMBER, 25, 20, 30);

        RecorrenciaProcessamento rec = new RecorrenciaProcessamento(inicio, null);
    }


}