<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="3.0" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:cat="http://factyfloor.com/CategoriaMateriaPrima"
    xmlns:d="http://factyfloor.com/Depositos" xmlns:mp="http://factyfloor.com/MateriaPrima"
    xmlns:p="http://factyfloor.com/Produtos" xmlns:m="http://factyfloor.com/Maquinas"
    xmlns:lp="http://factyfloor.com/LinhasProducao" xmlns:ord="http://factyfloor.com/OrdensProducao"
    xmlns:exe="http://factyfloor.com/ExecucaoOrdensProducao" xmlns:f="http://factyfloor.com/Fabrica">
    <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
    <!--Criar Elemento Raíz-->
    <xsl:template match="/Fabrica">
        <xsl:element name="Produtos">
            <xsl:attribute name="qtd">
                <xsl:value-of select="count(//p:Produto)"/>
            </xsl:attribute>
            <xsl:apply-templates select="f:Producao/f:Produtos">
                
            </xsl:apply-templates>
        </xsl:element>       
    </xsl:template>
    
    <!--Template -->
    <xsl:template match="f:Producao/f:Produtos">
        <xsl:for-each select="p:Produto">
            <xsl:element name="Produto">
                
                <xsl:element name="CodigoComercial">
                    <xsl:value-of select="p:CodigoComercial"/>
                </xsl:element>
                <xsl:element name="CodigoFabrico">
                    <xsl:value-of select="p:CodigoFabrico"/>
                </xsl:element>
                <xsl:element name="DescBreve">
                    <xsl:value-of select="p:DescricaoBreve"/>
                </xsl:element>
                <xsl:element name="DescCompleta">
                    <xsl:value-of select="p:DescricaoCompleta"/>
                </xsl:element>
                <xsl:element name="FichaProducao">
                    <xsl:attribute name="qtdMatPrima">
                        <xsl:value-of select="count(./p:FichaProducao/p:MateriaPrima)"/>
                    </xsl:attribute>
                    <xsl:for-each select="p:FichaProducao/p:MateriaPrima">
                        <xsl:element name="MatPrima">
                            <xsl:variable name="mpCod" select="."/>
                            <xsl:variable name="mp" select="//mp:MateriaPrima[mp:CodigoInterno = $mpCod]"/>
                            <xsl:attribute name="qtd">
                                <xsl:value-of select="@Quantidade"/>
                            </xsl:attribute>
                            <xsl:attribute name="un">
                                <xsl:value-of select="@Unidade"/>
                            </xsl:attribute>
                            <xsl:element name="Codigo">
                                <xsl:value-of select="$mpCod"/>
                             </xsl:element>
                            <xsl:if test="$mp">
                                <xsl:element name="Descricao">
                                    <xsl:value-of select="$mp/mp:Descricao"/>
                                </xsl:element>
                                <xsl:element name="Categoria">
                                    <xsl:value-of select="$mp/mp:Categoria"/>
                                </xsl:element>
                            </xsl:if>
                            
                        </xsl:element>
                    </xsl:for-each>
                </xsl:element>
            </xsl:element>
            
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>