<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="3.0" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:cat="http://factyfloor.com/CategoriaMateriaPrima"
    xmlns:d="http://factyfloor.com/Depositos" xmlns:mp="http://factyfloor.com/MateriaPrima"
    xmlns:p="http://factyfloor.com/Produtos" xmlns:m="http://factyfloor.com/Maquinas"
    xmlns:lp="http://factyfloor.com/LinhasProducao" xmlns:ord="http://factyfloor.com/OrdensProducao"
    xmlns:exe="http://factyfloor.com/ExecucaoOrdensProducao" xmlns:f="http://factyfloor.com/Fabrica">
    <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
    <!--Criar Elemento Raíz-->
    <xsl:template match="/Fabrica">
        <xsl:element name="LinhasProducao">
            <xsl:apply-templates select="f:ChaoFabrica/f:LinhasProducao"/>
        </xsl:element>       
    </xsl:template>
    
    <!--Template -->
    <xsl:template match="f:ChaoFabrica/f:LinhasProducao">
        <xsl:for-each select="lp:LinhaProducao">
            <xsl:element name="LinhaProducao">
                <xsl:attribute name="NumMaquinas">
                    <xsl:value-of select="count(./lp:SequenciaMaquina)"/>
                </xsl:attribute>
                <xsl:element name="Nome">
                    <xsl:value-of select="@Codigo"/>
                </xsl:element>
                <xsl:for-each select="lp:SequenciaMaquina">
                    <xsl:element name="Maquina">
                        <xsl:attribute name="Codigo">
                            <xsl:value-of select="current()"/>
                        </xsl:attribute>
                        <xsl:attribute name="Posicao">
                            <xsl:value-of select="@Index"/>
                        </xsl:attribute>
                    
                    <xsl:variable name="codMaq" select="current()"/>
                    <xsl:variable name="maq" select="//m:Maquina[m:CodigoInterno = $codMaq]"/>
                    <xsl:element name="Descricao">
                        <xsl:value-of select="$maq/m:Descricao"/>
                    </xsl:element>
                    <xsl:element name="Marca">
                        <xsl:value-of select="$maq/m:Marca"/>
                    </xsl:element>
                    <xsl:element name="Modelo">
                        <xsl:value-of select="$maq/m:Modelo"/>
                    </xsl:element>
                    <xsl:element name="NumSerie">
                        <xsl:value-of select="$maq/m:NumeroSerie"/>
                    </xsl:element>
                    <xsl:element name="DtInstalacao">
                        <xsl:value-of select="format-date($maq/m:DataInstalacao,'[D01]/[M01]/[Y0001]')"/>
                    </xsl:element> 
                    </xsl:element>
                </xsl:for-each>
            </xsl:element>
        </xsl:for-each>
        
    </xsl:template>
    
    
</xsl:stylesheet>