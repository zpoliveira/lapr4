<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="3.0" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:cat="http://factyfloor.com/CategoriaMateriaPrima"
    xmlns:d="http://factyfloor.com/Depositos" xmlns:mp="http://factyfloor.com/MateriaPrima"
    xmlns:p="http://factyfloor.com/Produtos" xmlns:m="http://factyfloor.com/Maquinas"
    xmlns:lp="http://factyfloor.com/LinhasProducao" xmlns:ord="http://factyfloor.com/OrdensProducao"
    xmlns:exe="http://factyfloor.com/ExecucaoOrdensProducao" xmlns:f="http://factyfloor.com/Fabrica">
    <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
    <!--Criar Elemento Raíz-->
    <xsl:template match="/Fabrica">
        <xsl:element name="OrdensProducao">
            <xsl:attribute name="NumOrdens">
                <xsl:value-of select="count(//ord:OrdemProducao)"/>
            </xsl:attribute>
            <xsl:apply-templates select="f:Producao/f:OrdensProducao">
                
            </xsl:apply-templates>
        </xsl:element>       
    </xsl:template>
    <!-- Variaveis -->
    <xsl:variable name="ListOrdensExecucao" select="//exe:ExecucaoOrdemProducao"/>
    
    <!--Template -->
    <xsl:template match="f:Producao/f:OrdensProducao">
        <xsl:for-each select="ord:OrdemProducao">
            <xsl:sort select="ord:DataPrevistaExecucao"/>  
            <xsl:element name="OrdemProducao">                
                <xsl:attribute name="Codigo">
                    <xsl:value-of select="ord:IdOrdemProducao"/>
                </xsl:attribute>
                <xsl:element name="Estado">
                    <xsl:value-of select="ord:EstadoOrdemProducao"/>
                </xsl:element>
                <xsl:element name="DataExecucao">
                    <xsl:value-of select="ord:DataPrevistaExecucao"/>
                </xsl:element>
                <xsl:element name="Producao">
                    <xsl:attribute name="Qtd">
                        <xsl:value-of select="ord:Quantidade"/>
                    </xsl:attribute>
                    <xsl:attribute name="Unidade">
                        <xsl:value-of select="ord:Quantidade/@Unidade"/>
                    </xsl:attribute>
                    <xsl:value-of select="ord:CodigoFabricoProduto"/>
                </xsl:element>
                <xsl:element name="DataEmissao">
                    <xsl:value-of select="ord:DataEmissao"/> 
                </xsl:element>
                <xsl:element name="Encomendas">
                    <xsl:value-of select="ord:IdEncomenda"/>
                </xsl:element>
                <xsl:variable name="NumOrdProd" select="ord:IdOrdemProducao"/>
                <xsl:variable name="ExecucaoOrdProducao" select="$ListOrdensExecucao[exe:IdOrdemProducao = $NumOrdProd]"/>
                <xsl:variable name="Qtd_Prod" select="($ExecucaoOrdProducao/exe:DesvioProducao/exe:QuantidadeRealProduzida,0)[1]"/>
                <xsl:element name="QtdProduzida">
                    <xsl:variable name="Qtd" select="ord:Quantidade"/>
                    <xsl:variable name="Desvio" select="number($Qtd_Prod) - number($Qtd)"/>
                    <xsl:attribute name="Desvio">
                        <xsl:value-of select="$Desvio"/>
                    </xsl:attribute>
                    <xsl:value-of select="$Qtd_Prod"/>
                    
                </xsl:element>
            </xsl:element>
        </xsl:for-each>
        
    </xsl:template>
    
    
</xsl:stylesheet>