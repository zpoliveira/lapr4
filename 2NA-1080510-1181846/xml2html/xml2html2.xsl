<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="3.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:cat="http://factyfloor.com/CategoriaMateriaPrima"
    xmlns:d="http://factyfloor.com/Depositos" xmlns:mp="http://factyfloor.com/MateriaPrima"
    xmlns:p="http://factyfloor.com/Produtos" xmlns:m="http://factyfloor.com/Maquinas"
    xmlns:lp="http://factyfloor.com/LinhasProducao" xmlns:ord="http://factyfloor.com/OrdensProducao"
    xmlns:exe="http://factyfloor.com/ExecucaoOrdensProducao" xmlns:f="http://factyfloor.com/Fabrica">
    <xsl:output method="xml" indent="yes"/>
    <!--Criar Elemento Raíz-->
    <xsl:template match="/">
        <xsl:apply-templates select="Fabrica"/>
    </xsl:template>
    <!-- Variaveis -->
    <xsl:variable name="ListOrdensExecucao" select="//exe:ExecucaoOrdemProducao"/>
    <!--Template Relatório-->
    <xsl:template match="Fabrica">
        <html>
            <head>
                <meta charset="UTF-8"/>
                <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                <title>Gestão chão de fabrica</title>
            </head>
            <body>
                <style>
                    
                    table.borda  td,
                    table.borda  th {
                        padding: 8px;
                        border: 1px solid #ddd;
                        text-align: left;
                    }
                    
                    table {
                        border-collapse: collapse;
                        width: 100%;
                        //height:100%;
                    }
                    
                  
                </style>
                <xsl:apply-templates select="f:Producao/f:OrdensProducao"/>
            </body>
        </html>
    </xsl:template>

    <!-- Ordens Produção -->
    <xsl:template match="f:Producao/f:OrdensProducao">
        <h3>Ordens Produção</h3>
        <table class="borda">
            <tr>
                <th height="60px">Código</th>
                <th height="60px">Encomendas</th>
                <th height="60px">Produção</th>
                <th height="60px">Execução</th>
            </tr>
            <xsl:for-each select="ord:OrdemProducao">
                <xsl:variable name="NumOrdProd" select="ord:IdOrdemProducao"/>
                <xsl:variable name="ExecucaoOrdProducao" select="$ListOrdensExecucao[exe:IdOrdemProducao = $NumOrdProd]"/>
                <xsl:variable name="Qtd_Prod" select="($ExecucaoOrdProducao/exe:DesvioProducao/exe:QuantidadeRealProduzida,0)[1]"/>
               
                <tr>
                    <td>
                        <xsl:value-of select="ord:IdOrdemProducao"/>
                    </td>
                    <td style="padding:0;">
                        <table>
                        <xsl:for-each select="ord:IdsEncomenda">
                            <tr>
                                <td >
                                    <xsl:value-of select="current()"/>
                                </td>
                            </tr>
                        </xsl:for-each>
                        </table>
                    </td>
                    <td style="padding:0;">
                        <table>
                            <tr>
                                <th>Produto</th>
                                <td>
                                    <xsl:value-of select="ord:CodigoFabricoProduto"/>
                                </td> 
                            </tr>
                            <tr>
                                <th>Estado</th>
                                <td>
                                    <xsl:value-of select="ord:EstadoOrdemProducao"/>   
                                </td>                             
                            </tr>
                            <tr>
                                <th>Data Emissão</th>
                                <td>
                                    <xsl:value-of select="format-date(ord:DataEmissao, '[D01]-[M01]-[Y0001]')"/>
                                </td>
                            </tr>
                            <tr>
                                <th>Data Execução</th>
                                <td>
                                    <xsl:value-of
                                        select="format-date(ord:DataPrevistaExecucao, '[D01]-[M01]-[Y0001]')"/>
                                </td>
                            </tr>
                        </table>
                        </td>
                    <td style="padding:0;">
                        <table>
                            <tr>
                                <th>Qtd. Produção</th>
                                <td>
                                    <xsl:value-of select="ord:Quantidade"/>
                                </td>
                            </tr>
                            <tr>
                                <th>Qtd. Produzida</th>
                                <td>                     
                                    <xsl:value-of select="$Qtd_Prod"/>
                                </td>
                            </tr>
                            <tr>
                                <th>Desvio</th>
                                <td>
                                    <xsl:variable name="Qtd" select="ord:Quantidade"/>
                                    <xsl:variable name="Desvio" select="number($Qtd_Prod) - number($Qtd)"/>
                                    <xsl:choose>
                                        <xsl:when test="$Desvio &lt; 0">
                                            <div style="color:red">
                                                <xsl:value-of select="$Desvio"/>
                                            </div>
                                        </xsl:when>
                                        <xsl:when test="$Desvio &gt;= 0">
                                            <div style="color:green">
                                                <xsl:value-of select="$Desvio"/>
                                            </div>
                                        </xsl:when>
                                    </xsl:choose>
                                    
                                </td>
                            </tr>
                            <tr>
                                <th>Tempo Efetivo</th>
                                <td>
                                    <xsl:value-of select="$ExecucaoOrdProducao/exe:TempoEfetivoExecucao"/>
                                </td>
                            </tr>
                            <tr>
                                <th>Tempo Bruto</th>
                                    <td>
                                        <xsl:value-of
                                            select="$ExecucaoOrdProducao/exe:TempoBrutoExecucao"/>
                                    </td>
                            </tr>
                        </table>
                    </td> 
                </tr>
            </xsl:for-each>
        </table>
    </xsl:template>
</xsl:stylesheet>