<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="3.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:cat="http://factyfloor.com/CategoriaMateriaPrima"
    xmlns:d="http://factyfloor.com/Depositos"
    xmlns:mp="http://factyfloor.com/MateriaPrima"
    xmlns:p="http://factyfloor.com/Produtos"
    xmlns:m="http://factyfloor.com/Maquinas"
    xmlns:lp="http://factyfloor.com/LinhasProducao"
    xmlns:ord="http://factyfloor.com/OrdensProducao"
    xmlns:exe="http://factyfloor.com/ExecucaoOrdensProducao"
    xmlns:f="http://factyfloor.com/Fabrica">
    <xsl:output method="xml" indent="yes"/>
    <!--Criar Elemento Raíz-->
    <xsl:template match="/">
        <xsl:apply-templates select="Fabrica" />
    </xsl:template>

    <!--Template -->
    <xsl:template match="Fabrica">
        <html>
            <head>
                <meta charset="UTF-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                <title>Gestão chão de fabrica</title>
            </head>
            <body>
                <style>
                    table, td, th {  
                    border: 1px solid #ddd;
                    text-align: left;
                    }
                    
                    table {
                    border-collapse: collapse;
                    width: 100%;
                    }
                    
                    th, td {
                    padding: 15px;
                    }
                </style>
                <xsl:apply-templates select="f:ChaoFabrica/f:Maquinas"/>
                <xsl:apply-templates select="f:ChaoFabrica/f:LinhasProducao"/>
            </body>
        </html>
    </xsl:template>
    <!-- Maquinas -->
    <xsl:template match="f:ChaoFabrica/f:Maquinas">
       <h3>Máquinas</h3>
       <table>
           <tr>
               <th></th>
               <th>Código</th>
               <th>Descrição</th>
               <th>Marca</th>
               <th>Modelo</th>
               <th>Nº Série</th>                   
           </tr>
           <xsl:apply-templates select="m:Maquina">
               <xsl:sort select="m:Descricao" data-type="text" order="ascending"/>
           </xsl:apply-templates>
           <tr>
               <td colspan="5"><b>Total:</b></td>
               <td><b>
                   <xsl:value-of select="count(//m:Maquina)"/>
               </b></td>
           </tr>
       </table>
   </xsl:template>
    <!-- Maquina -->
    <xsl:template match="m:Maquina">
        <tr>
            <td>
                <xsl:value-of select="position()"/>
            </td>
            <td>
                <xsl:value-of select="m:CodigoInterno"/>
            </td>
            <td>
                <xsl:value-of select="m:Descricao"/>
            </td>
            <td>
                <xsl:value-of select="m:Marca"/>
            </td>
            <td>
                <xsl:value-of select="m:Modelo"/>
            </td>
            <td>
                <xsl:value-of select="m:NumeroSerie"/>
            </td>
        </tr>
    </xsl:template>
    <!-- Linhas Produção -->
    <xsl:template match="f:ChaoFabrica/f:LinhasProducao">
        <h3>Linhas Produção</h3>
        <table>
            <tr>
                <th>Código</th>
                <th>Posição</th>
                <th>Maquina</th>
                <th></th>                  
            </tr>
            <xsl:for-each-group select="lp:LinhaProducao" group-by="@Codigo">
               
                    <xsl:for-each select="current-group()/lp:SequenciaMaquina">
                        <xsl:choose>
                            <xsl:when test="position()=1">
                                <tr>
                                    <td rowspan="{count(current-group()/lp:SequenciaMaquina)}">
                                        <xsl:value-of select="current-grouping-key()"/>
                                    </td>
                                    <td>
                                        <xsl:value-of select="@Index"/>
                                    </td>
                                    <td>
                                        <xsl:value-of select="current()"/>
                                    </td>
                                    <td rowspan="{count(current-group()/lp:SequenciaMaquina)}">
                                        <xsl:value-of select="count(current-group()/lp:SequenciaMaquina)"/>
                                    </td>
                                    </tr>
                            </xsl:when>
                            <xsl:otherwise>
                                <tr>
                                <td>
                                    <xsl:value-of select="@Index"/>
                                </td>
                                <td>
                                    <xsl:value-of select="current()"/>
                                </td>
                                </tr>
                            </xsl:otherwise>
                        </xsl:choose>
                </xsl:for-each>
                <xsl:if test="position() = last()">
                    <tr>
                        <td colspan="3"><b>Total Máquinas:</b></td>
                        <td><b><xsl:value-of select="count(//lp:SequenciaMaquina)"/></b></td>
                    </tr>
                    <tr>
                        <td colspan="3"><b>Total Linhas Produção:</b></td>
                        <td><b><xsl:value-of select="last()"/></b></td>
                    </tr>
                </xsl:if>          
            </xsl:for-each-group>
        </table>
        
    </xsl:template>
    
</xsl:stylesheet>