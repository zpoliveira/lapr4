<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="3.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:cat="http://factyfloor.com/CategoriaMateriaPrima"
    xmlns:d="http://factyfloor.com/Depositos" xmlns:mp="http://factyfloor.com/MateriaPrima"
    xmlns:p="http://factyfloor.com/Produtos" xmlns:m="http://factyfloor.com/Maquinas"
    xmlns:lp="http://factyfloor.com/LinhasProducao" xmlns:ord="http://factyfloor.com/OrdensProducao"
    xmlns:exe="http://factyfloor.com/ExecucaoOrdensProducao" xmlns:f="http://factyfloor.com/Fabrica">
    <xsl:output method="xml" indent="yes"/>
    <!--Criar Elemento Raíz-->
    <xsl:template match="/">
        <xsl:apply-templates select="Fabrica"/>
    </xsl:template>
    <!--Template Relatório-->
    <xsl:template match="Fabrica">
        <html>
            <head>
                <meta charset="UTF-8"/>
                <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                <title>Gestão chão de fabrica</title>
            </head>
            <body>
                <style>
                    
                    table.borda  td,
                    table.borda  th {
                        padding: 8px;
                        border: 1px solid #ddd;
                        text-align: left;
                    }
                    
                    table {
                        border-collapse: collapse;
                        width: 100%;
                        //height:100%;
                    }
                    
                  
                </style>
                <xsl:apply-templates select="f:Producao/f:Produtos"/>
            </body>
        </html>
    </xsl:template>

    <!-- Ordens Produção -->
    <xsl:template match="f:Producao/f:Produtos">
        <h3>Produtos</h3>
        <table class="borda">
            <tr>
                <th >Código Comercial</th>
                <th >Código Fabrico</th>
                <th >Descrição Breve</th>
                <td style="padding:0;">
                    <table>
                        <tr>
                            <th colspan="3">Ficha Produção</th>
                        </tr>
                        <tr>
                            <th style="width:60%">Matéria Prima</th>
                            <th style="width:30%">Quantidade</th>
                            <th style="width:10%">Total</th>
                        </tr>
                    </table>
                </td>
            </tr>
            <xsl:for-each select="p:Produto">
                <tr>
                    <td>
                        <xsl:value-of select="p:CodigoComercial"/>
                    </td>
                    <td>
                        <xsl:value-of select="p:CodigoFabrico"/>
                    </td>
                    <td>
                        <xsl:value-of select="p:DescricaoBreve"/>
                    </td>
                    
                    <td style="padding:0;">
                       <xsl:apply-templates select="p:FichaProducao"/>
                    </td>
                </tr>
                <tr>
                    <th>Descrição Completa</th>
                    <td colspan="3">
                        <xsl:value-of select="p:DescricaoCompleta"/>
                    </td>
                </tr>
            </xsl:for-each>
        </table>
    </xsl:template>
    
    <xsl:template match="p:FichaProducao">
        <table>
            <xsl:for-each select="p:MateriaPrima">
                <tr>
                   
                    <td style="width:60%">
                        <xsl:value-of select="current()"/>
                    </td>   
                    <td style="width:30%">
                        <xsl:value-of select="@Quantidade"/>
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="@Unidade"/>
                    </td>
                    <xsl:if test="position() = 1">
                        <td rowspan="{last()}">
                            <xsl:value-of select="last()"/>
                        </td>
                    </xsl:if>
                </tr>
            </xsl:for-each>
        </table>
    </xsl:template>
</xsl:stylesheet>