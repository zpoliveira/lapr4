<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="3.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ns="http://www.dei.isep.ipp.pt/lprog">
    <xsl:output method="html" indent="yes" />
    <!--Criar Elemento Raíz-->
    <xsl:template match="/">
        <xsl:apply-templates select="ns:relatório" />
    </xsl:template>
    <!--Template Relatório-->
    <xsl:template match="ns:relatório">
        <html>
            <head>
                <meta charset="UTF-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                <title>Relatório LPROG</title>
            </head>
            <body>
                <style>
                    td, th {
                    text-align: center;
                    padding: 8px;
                    }
                </style>
                <xsl:apply-templates select="ns:páginaRosto" />
                <xsl:apply-templates select="ns:corpo" />
                <xsl:apply-templates select="ns:anexos" />
            </body>
        </html>
    </xsl:template>
    <!--Template PáginaRosto-->
    <xsl:template match="ns:páginaRosto">
        <div style="width:100%; text-align:center;">

            <xsl:apply-templates select="ns:logotipoDEI" />
            <h2 style="margin-left: 0%;">
                <xsl:value-of select="ns:tema" />
            </h2>
            <xsl:apply-templates select="ns:disciplina" />
            <div style="width:100%;">
                <xsl:call-template name="autores" />
                <xsl:call-template name="professores" />
            </div>

        </div>
    </xsl:template>
    <!-- Template LogotipoDei-->
    <xsl:template match="ns:logotipoDEI">
        <img>
            <xsl:attribute name="src">
                <xsl:value-of select="current()" />
            </xsl:attribute>
        </img>
    </xsl:template>
    <!--Template Disciplina-->
    <xsl:template match="ns:disciplina">
        <div>
            <h3>
                <xsl:value-of select="ns:designação" />
                [
                <xsl:value-of select="ns:sigla" />
                ]
            </h3>
            <h3>
                Ano Curricular:
                <xsl:value-of select="ns:anoCurricular" />
                <p>
                    Data:
                    <xsl:value-of select="format-date(../ns:data,'[D01]-[M01]-[Y0001]')" />
                </p>
            </h3>
        </div>
    </xsl:template>
    <!-- Template autores -->
    <xsl:template name="autores">
        <div style="float:left;  width:50%; margin:0 auto;">
            <table style="margin: 0 auto;">
                <caption style="padding-bottom:20px;">Autores</caption>
                <tr>
                    
                    <th>Número</th>
                    <th>Nome</th>
                    <th>Mail</th>

                </tr>
                <xsl:for-each select="ns:autor">
                    <tr>
                        
                        <td>
                            <xsl:value-of select="ns:número" />
                        </td>
                        <td>
                            <xsl:value-of select="ns:nome" />
                    </td>
                        <td>
                            <xsl:value-of select="ns:mail" />
                        </td>
                    </tr>
                </xsl:for-each>
            </table>
            
        </div>
    </xsl:template>
    <!-- Template professores-->
    <xsl:template name="professores">
    <div style="float:left; width:50%; margin:0 auto;">
        <table style="margin: 0 auto;">
            <caption style="padding-bottom:20px;">Docentes</caption>
            <tr>
                <th>Tipo Aula</th>
                <th>Sigla</th>
                <th>Nome</th>
            </tr>
            <xsl:for-each select="ns:professor">
                <tr>
                    <td>
                            <xsl:value-of select="@tipoAula" />
                    </td>
                    <td>
                        <xsl:value-of select="@sigla" />
                    </td>
                    <td>
                        <xsl:value-of select="current()" />
                    </td>
                </tr>
            </xsl:for-each>
        </table>
        
    </div>
</xsl:template>

    <!--Template Corpo-->
    <xsl:template match="ns:corpo">
        <!-- Indice-->
        <h2>Índice</h2>
        <div class="Indice">
            <ol class="listaIndice">
                <xsl:for-each select="//*[@tituloSecção]">
                    <li>
                        <a>
                            <xsl:attribute name="href">
                                <xsl:value-of select="concat('#',@id)" />
                            </xsl:attribute>
                            <xsl:value-of select="@tituloSecção" />
                        </a>
                    </li>
                    <xsl:for-each select=".//ns:subsecção">
                        <xsl:if test="string(.)">
                            <ul>
                                <li>
                                    <a>
                                        <xsl:attribute name="href">
                                            <xsl:value-of select="concat('#',@id)" />
                                        </xsl:attribute>
                                        <xsl:value-of select="." />
                                    </a>
                                </li>
                            </ul>
                        </xsl:if>
                    </xsl:for-each>
                </xsl:for-each>
            </ol>
        </div>
        <!-- Introdução-->
        <xsl:apply-templates select="ns:introdução" />
        <!-- Outras Secções-->
        <xsl:apply-templates select="ns:outrasSecções" />
        <xsl:apply-templates select="ns:conclusão" />
        <xsl:apply-templates select="ns:referências" />
    </xsl:template>
    <!-- Template Anexos-->
    <xsl:template match="ns:anexos">
        <h2>
            <xsl:attribute name="id">
                <xsl:value-of select="@id" />
            </xsl:attribute>
            <xsl:value-of select="@tituloSecção" />
        </h2>
        <xsl:apply-templates select="ns:listaItems" mode="anexo" />
    </xsl:template>
    <!-- Template Figuras-->
    <xsl:template match="ns:figura">
        <h4 style="margin-left:10%">
            <xsl:value-of select="@descrição" />
        </h4>
        <img>
            <xsl:attribute name="src">
                <xsl:value-of select="@src" />
            </xsl:attribute>
            <xsl:attribute name="align">
                <xsl:value-of select="middle" />
            </xsl:attribute>
        </img>
        <br />
    </xsl:template>
    <!-- Template Introdução-->
    <xsl:template match="ns:introdução">
        <h2>
            <xsl:attribute name="id">
                <xsl:value-of select="@id" />
            </xsl:attribute>
            Introdução
        </h2>
        <xsl:apply-templates />
    </xsl:template>
    <!-- Template Paragrafo-->
    <xsl:template match="ns:parágrafo">
        <p>
            <xsl:apply-templates />
        </p>
    </xsl:template>
    <!-- Template Negrito-->
    <xsl:template match="ns:negrito">
        <b>
            <xsl:apply-templates />
        </b>
    </xsl:template>
    <!-- Template Itálico-->
    <xsl:template match="ns:itálico">
        <i>
            <xsl:apply-templates />
        </i>
    </xsl:template>
    <!-- Template Sublinhado-->
    <xsl:template match="ns:sublinhado">
        <u>
            <xsl:apply-templates />
        </u>
    </xsl:template>
    <!-- Template Sublinhado-->
    <xsl:template match="ns:citação">
        <a>
            <xsl:attribute name="href">
                <xsl:value-of select="concat('#',current())" />
            </xsl:attribute>
            <xsl:value-of select="current()" />
        </a>        
    </xsl:template>
    <!-- Template Outras Secções-->
    <xsl:template match="ns:outrasSecções">
        <xsl:for-each select="./*[@tituloSecção]">
            <div>
                <h2>
                    <xsl:attribute name="id">
                        <xsl:value-of select="@id" />
                    </xsl:attribute>
                    <xsl:value-of select="@tituloSecção" />
                </h2>
                <xsl:for-each select="./*">

                    <xsl:choose>
                        <xsl:when test="name() = 'subsecção'">
                            <h3>
                                <xsl:attribute name="id">
                                    <xsl:value-of select="@id" />
                                </xsl:attribute>
                                <xsl:value-of select="." />
                            </h3>
                        </xsl:when>
                    
                        <xsl:when test="local-name() = 'parágrafo'">
                            
                                <xsl:apply-templates select="current()" />
                            
                        </xsl:when>
                        <xsl:when test="local-name() = 'listaItems'">
                            <xsl:apply-templates select="." />
                        </xsl:when>
                        <xsl:when test="local-name() = 'codigo'">
                            <xsl:apply-templates select="." />
                        </xsl:when>
                    </xsl:choose>
                </xsl:for-each>
            </div>
        </xsl:for-each>
    </xsl:template>
    <!-- Template Conclusão-->
    <xsl:template match="ns:conclusão">
        <h2>
            <xsl:attribute name="id">
                <xsl:value-of select="@id" />
            </xsl:attribute>
            <xsl:value-of select="@tituloSecção" />
        </h2>
        <xsl:apply-templates select="ns:subsecção" />
        <xsl:apply-templates select="ns:parágrafo" />
    </xsl:template>
    <!-- Template Referências -->
    <xsl:template match="ns:referências">
        <h2>
            <xsl:attribute name="id">
                <xsl:value-of select="@id" />
            </xsl:attribute>
            <xsl:value-of select="@tituloSecção" />
        </h2>
        <dl>
        <xsl:apply-templates select="ns:refBibliográfica" />
        <xsl:apply-templates select="ns:refWeb" />
        </dl>
    </xsl:template>
    <!-- Template refBibliográfica-->
    <xsl:template match="ns:refBibliográfica">
        <dt>
            <xsl:attribute name="id">
                <xsl:value-of select="@idRef" />
            </xsl:attribute>
            <b><xsl:value-of select="@idRef" /></b>
        </dt>
        <dd>
            <xsl:value-of select="concat('Título: ',ns:título)" />
        </dd>
        <dd>
            <xsl:value-of select="concat('Data Publicação: ',ns:dataPublicação)" />
        </dd>
        <dd>
            <xsl:value-of select="concat('Autor: ',ns:autor)" />
        </dd>
    </xsl:template>
    <!-- Template refWeb-->
    <xsl:template match="ns:refWeb">
        <dt>
            <xsl:attribute name="id">
                <xsl:value-of select="@idRef" />
            </xsl:attribute>
            <b><xsl:value-of select="@idRef" /></b>
        </dt>
        <dd>
            URL:
            <a>
                <xsl:attribute name="href">
                    <xsl:value-of select="ns:URL" />
                </xsl:attribute>
                <xsl:value-of select="ns:URL" />
            </a>
        </dd>
        <dd>
            <xsl:value-of select="concat('Descrição: ',ns:descrição)" />
        </dd>
        <dd>
            <xsl:value-of select="concat('Consultado em: ',ns:consultadoEm)" />
        </dd>
    </xsl:template>
    <!-- Template Subsecção-->
    <xsl:template match="ns:subsecção">
        <xsl:if test="string(.)">
            <h3>
                <xsl:attribute name="id">
                    <xsl:value-of select="@id" />
                </xsl:attribute>
                <xsl:value-of select="." />
            </h3>
            <xsl:apply-templates select="./../ns:parágrafo" />
        </xsl:if>
    </xsl:template>
    <!-- Template Lista de Items-->
    <xsl:template match="ns:listaItems">
        <ul>
            <xsl:for-each select="./*">
                <li>
                    <xsl:choose>
                        <xsl:when test="not(current() = '')">
                            <div style="font-size:1.1em; font-style:italic; ">
                                <xsl:value-of select="@label"/>
                            </div>
                            <xsl:text> - </xsl:text>
                            <xsl:value-of select="current()"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="@label"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </li>
            </xsl:for-each>
        </ul>
    </xsl:template>

    <!-- Template Lista de Items-->
    <xsl:template match="ns:listaItems" mode="anexo">
        <ul>
            <xsl:for-each select="./*">
                <li>
                    <a>
                        <xsl:attribute name="href">
                            <xsl:value-of select="concat('',@label)" />
                        </xsl:attribute>
                        <xsl:value-of select="." />
                    </a>
                </li>
            </xsl:for-each>
        </ul>
    </xsl:template>
    <!-- template para Codigo -->
    <xsl:template match="ns:codigo">
        <div>
            <xsl:for-each select="./*">
                <pre>
                <code>
                    <xsl:value-of select="."/>
                </code>
                </pre>
            </xsl:for-each>
        </div>
    </xsl:template>
</xsl:stylesheet>