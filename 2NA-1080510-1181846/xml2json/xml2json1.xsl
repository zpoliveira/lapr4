<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="3.0"
    xmlns:cat="http://factyfloor.com/CategoriaMateriaPrima"
    xmlns:d="http://factyfloor.com/Depositos" xmlns:mp="http://factyfloor.com/MateriaPrima"
    xmlns:p="http://factyfloor.com/Produtos" xmlns:m="http://factyfloor.com/Maquinas"
    xmlns:lp="http://factyfloor.com/LinhasProducao" xmlns:ord="http://factyfloor.com/OrdensProducao"
    xmlns:exe="http://factyfloor.com/ExecucaoOrdensProducao" xmlns:f="http://factyfloor.com/Fabrica">
    <xsl:output method="text" encoding="UTF-8" indent="yes"
        media-type="application/json"/>


    <xsl:template match="/*[node()]">
        <xsl:text>{</xsl:text>
        <xsl:apply-templates select="f:ChaoFabrica/f:Maquinas"/>
        <xsl:text>, </xsl:text>
        <xsl:apply-templates select="f:ChaoFabrica/f:LinhasProducao"/>
        <xsl:text>}</xsl:text>
    </xsl:template>


    <!-- Maquinas -->
    <xsl:template match="f:ChaoFabrica/f:Maquinas">
        <xsl:text>"Maquinas": </xsl:text>
        <xsl:choose>
            <xsl:when test="count(m:Maquina) = 0">
                <xsl:text>null</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:if test="count(m:Maquina) &gt; 1">[ </xsl:if>
                <xsl:for-each select="m:Maquina">
                    <xsl:apply-templates select="current()" mode="obj-content"/>
                    <xsl:if test="position() &lt; last()">, </xsl:if>
                </xsl:for-each>
                <xsl:if test="count(m:Maquina) &gt; 1">] </xsl:if>
            </xsl:otherwise>
        </xsl:choose>

    </xsl:template>

    <!-- Linhas Produção -->
    <xsl:template match="f:ChaoFabrica/f:LinhasProducao">
        <xsl:text>"LinhasProducao": </xsl:text>
        <xsl:choose>
            <xsl:when test="count(lp:LinhaProducao) = 0">
                <xsl:text>null</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:if test="count(lp:LinhaProducao) &gt; 1">[ </xsl:if>
                <xsl:for-each select="lp:LinhaProducao">
                    <xsl:apply-templates select="current()" mode="obj-content"/>
                    <xsl:if test="position() &lt; last()">, </xsl:if>
                </xsl:for-each>
                <xsl:if test="count(lp:LinhaProducao) &gt; 1">] </xsl:if>
            </xsl:otherwise>
        </xsl:choose> 
    </xsl:template>
    
    
    <!-- LinhaProducao -->
    <xsl:template match="lp:LinhaProducao" mode="obj-content">
        <xsl:text>{</xsl:text>
                <xsl:text>"Codigo": "</xsl:text>
                 <xsl:value-of select="@Index"/>
                   <xsl:text>",</xsl:text> 
                   <xsl:text>"Maquinas" :[</xsl:text>
                    <xsl:for-each select="lp:SequenciaMaquina">
                        <xsl:apply-templates select="." mode="obj-content"/>
                        <xsl:if test="position() &lt; last()">, </xsl:if>
                    </xsl:for-each>
                    <xsl:text>]</xsl:text>
        <xsl:text>}</xsl:text>
    </xsl:template>
    
    <!-- Seququencia -->
    <xsl:template match="lp:SequenciaMaquina" mode="obj-content">
        <xsl:text>{</xsl:text>
        <xsl:text>"Index": "</xsl:text>
        <xsl:value-of select="@Index"/>
        <xsl:text>",</xsl:text>
        <xsl:text>"Maquina": "</xsl:text>
        <xsl:value-of select="."/>
        <xsl:text>"</xsl:text>
        <xsl:text>}</xsl:text>
    </xsl:template>

    <xsl:template match="*" mode="detect">
        <xsl:choose>
            <xsl:when
                test="name(preceding-sibling::*[1]) = name(current()) and name(following-sibling::*[1]) != name(current())">
                <xsl:apply-templates select="." mode="obj-content"/>
                <xsl:text>]</xsl:text>
                <xsl:if test="count(following-sibling::*[name() != name(current())]) &gt; 0">,
                </xsl:if>
            </xsl:when>
            <xsl:when test="name(preceding-sibling::*[1]) = name(current())">
                <xsl:apply-templates select="." mode="obj-content"/>
                <xsl:if test="name(following-sibling::*) = name(current())">, </xsl:if>
            </xsl:when>
            <xsl:when test="following-sibling::*[1][name() = name(current())]">
                <xsl:text>"</xsl:text>
                <!-- commo é uma lista vai ler o nome elemento pai que é o nome da lista -->
                <xsl:choose>
                    <xsl:when test="local-name() = ('SequenciaMaquina', 'MateriaPrima')">
                        <xsl:value-of select="local-name()"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="local-name(..)"/>
                    </xsl:otherwise>
                </xsl:choose>

                <xsl:text>" : [</xsl:text>
                <xsl:apply-templates select="." mode="obj-content"/>
                <xsl:text>, </xsl:text>
            </xsl:when>
            <xsl:when test="count(./child::*) > 0 or count(@*) > 0">

                <xsl:apply-templates select="." mode="obj-content"/>

                <xsl:if test="count(following-sibling::*) &gt; 0">, </xsl:if>
            </xsl:when>
            <xsl:when test="count(./child::*) = 0">
                <xsl:text>"</xsl:text>
                <xsl:value-of select="local-name()"/>" : "<xsl:apply-templates select="."/>
                <xsl:text>"</xsl:text>
                <xsl:if test="count(following-sibling::*) &gt; 0">, </xsl:if>
            </xsl:when>

        </xsl:choose>
    </xsl:template>

    <xsl:template match="*" mode="obj-content">


        <xsl:text>{</xsl:text>
        <xsl:apply-templates select="@*" mode="attr"/>
        <xsl:if test="count(@*) &gt; 0 and (count(child::*) &gt; 0 or text())">, </xsl:if>
        <xsl:apply-templates select="./*" mode="detect"/>
        <xsl:if test="count(child::*) = 0 and text() and not(@*)">
            <xsl:text>"</xsl:text>
            <xsl:value-of select="local-name()"/>" : "<xsl:value-of select="text()"/>
            <xsl:text>"</xsl:text>
        </xsl:if>
        <xsl:if test="count(child::*) = 0 and text() and @*">
            <xsl:choose>
                <xsl:when test="local-name() = 'SequenciaMaquina'">
                    <xsl:text>"Maquina": "</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>"text" : "</xsl:text>
                </xsl:otherwise>
            </xsl:choose>

            <xsl:value-of select="text()"/>
            <xsl:text>"</xsl:text>
        </xsl:if>
        <xsl:text>}</xsl:text>

        <xsl:if test="position() &lt; last()">, </xsl:if>
    </xsl:template>

    <xsl:template match="@*" mode="attr">
        <xsl:text>"</xsl:text>
        <xsl:value-of select="local-name()"/>" : "<xsl:value-of select="."/>
        <xsl:text>"</xsl:text>
        <xsl:if test="position() &lt; last()">,</xsl:if>
    </xsl:template>

    <xsl:template match="node/@TEXT | text()" name="removeBreaks">
        <xsl:param name="pText" select="normalize-space(.)"/>
        <xsl:choose>
            <xsl:when test="not(contains($pText, '&#xA;'))">
                <xsl:copy-of select="$pText"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="concat(substring-before($pText, '&#xD;&#xA;'), ' ')"/>
                <xsl:call-template name="removeBreaks">
                    <xsl:with-param name="pText" select="substring-after($pText, '&#xD;&#xA;')"/>
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

</xsl:stylesheet>