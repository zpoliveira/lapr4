<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="3.0"
    xmlns:cat="http://factyfloor.com/CategoriaMateriaPrima"
    xmlns:d="http://factyfloor.com/Depositos" xmlns:mp="http://factyfloor.com/MateriaPrima"
    xmlns:p="http://factyfloor.com/Produtos" xmlns:m="http://factyfloor.com/Maquinas"
    xmlns:lp="http://factyfloor.com/LinhasProducao" xmlns:ord="http://factyfloor.com/OrdensProducao"
    xmlns:exe="http://factyfloor.com/ExecucaoOrdensProducao" xmlns:f="http://factyfloor.com/Fabrica">
    <xsl:output method="text" encoding="UTF-8" omit-xml-declaration="yes" indent="no"
        media-type="application/json"/>


    <xsl:template match="/*[node()]">
        <xsl:text>{</xsl:text>
        <xsl:apply-templates select="f:Producao/f:OrdensProducao"/>
        <xsl:text>, </xsl:text>
        <xsl:apply-templates select="f:Producao/f:ExecucaoOrdensProducao"/>
        <xsl:text>}</xsl:text>
    </xsl:template>


    <!-- Ordens Producao -->
    <xsl:template match="f:Producao/f:OrdensProducao">
        <xsl:text>"OrdensProducao": </xsl:text>
        <xsl:choose>
            <xsl:when test="count(ord:OrdemProducao) = 0">
                <xsl:text>null</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:if test="count(ord:OrdemProducao) &gt; 1">[ </xsl:if>
                <xsl:for-each select="ord:OrdemProducao">
                    <xsl:text>{ </xsl:text>
                    <xsl:for-each select="./*">
                        <xsl:choose>
                            <xsl:when test="local-name() = 'IdsEncomenda'">
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:text>"</xsl:text>
                                <xsl:value-of select="local-name()"/>
                                <xsl:text>": </xsl:text>
                                <xsl:text>"</xsl:text>
                                <xsl:value-of select="current()"/>
                                <xsl:text>"</xsl:text>
                                <xsl:if test="@Unidade">
                                    <xsl:text>, "</xsl:text>
                                    <xsl:value-of select="local-name(@Unidade)"/>
                                    <xsl:text>": </xsl:text>
                                    <xsl:text>"</xsl:text>
                                    <xsl:value-of select="@Unidade"/>
                                    <xsl:text>"</xsl:text>
                                </xsl:if>
                                <xsl:if test="position() &lt; last()">, </xsl:if>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:for-each>
                    <xsl:text>"Encomendas": </xsl:text>
                    <xsl:choose>
                        <xsl:when test="count(ord:IdsEncomenda) = 0">
                            null
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:text>[ </xsl:text>
                            <xsl:for-each select="ord:IdsEncomenda">
                                <xsl:text>"</xsl:text>
                                <xsl:value-of select="."/>
                                <xsl:text>"</xsl:text>
                                <xsl:if test="position() &lt; last()">, </xsl:if>
                            </xsl:for-each>
                            <xsl:text>] </xsl:text>
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:text>} </xsl:text>
                    <xsl:if test="position() &lt; last()">, </xsl:if>
                </xsl:for-each>
                <xsl:if test="count(ord:OrdemProducao) &gt; 1">] </xsl:if>
            </xsl:otherwise>
        </xsl:choose>

    </xsl:template>



    <!-- Execucao Ordens Produção -->
    <xsl:template match="f:Producao/f:ExecucaoOrdensProducao">
        <xsl:text>"ExecucaoOrdensProducao": </xsl:text>
        <xsl:if test="count(exe:ExecucaoOrdemProducao) &gt; 1">[ </xsl:if>
        <xsl:for-each select="exe:ExecucaoOrdemProducao">
            <xsl:apply-templates select="current()" mode="obj-content"/>
            <xsl:if test="position() &lt; last()">, </xsl:if>
        </xsl:for-each>
        <xsl:if test="count(exe:ExecucaoOrdemProducao) &gt; 1">] </xsl:if>
    </xsl:template>


    <xsl:template match="exe:ExecucaoOrdemProducao" mode="obj-content">
        <xsl:text>{</xsl:text>
        <xsl:text>"</xsl:text>
        <xsl:value-of select="exe:IdOrdemProducao/local-name()"/>
        <xsl:text>": </xsl:text>
        <xsl:text>"</xsl:text>
        <xsl:value-of select="exe:IdOrdemProducao"/>
        <xsl:text>", </xsl:text>
        
        <!-- Desvio Producao -->
        <xsl:text>"DesvioProducao": </xsl:text>
        <xsl:choose>
            <xsl:when test="exe:DesvioProducao">
                <xsl:apply-templates select="exe:DesvioProducao" mode="obj-content"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>null</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
       <xsl:text>,</xsl:text>
       
        <!-- Consumos Materia Prima -->
        <xsl:text>"ConsumosMateriaPrima": </xsl:text>
        <xsl:text>[ </xsl:text>
        <xsl:for-each select="exe:ConsumoRealMateriaPrima/exe:MovimentoConsumoMP">
            <xsl:apply-templates select="current()" mode="obj-content"/>
            <xsl:if test="position() &lt; last()">, </xsl:if>
        </xsl:for-each>
        <xsl:text>], </xsl:text>
        
        <!-- desvios Materia prima -->
        <xsl:text>"DesviosMateriaPrima": </xsl:text>
        <xsl:text>[ </xsl:text>
        <xsl:for-each select="exe:DesviosConsumoMateriaPrima/exe:DesvioConsumoMateriaPrima">
            <xsl:apply-templates select="current()" mode="obj-content"/>
            <xsl:if test="position() &lt; last()">, </xsl:if>
        </xsl:for-each>
        <xsl:text>] </xsl:text>
        
        <xsl:text>}</xsl:text>
    </xsl:template>
    
    
    <xsl:template match="*" mode="obj-content">
        <xsl:text>{</xsl:text>
        <xsl:for-each select="./*">
            <xsl:text>"</xsl:text>
            <xsl:value-of select="local-name()"/>
            <xsl:text>": </xsl:text>
            <xsl:text>"</xsl:text>
            <xsl:value-of select="current()"/>
            <xsl:text>"</xsl:text>
            <xsl:if test="position() &lt; last()">, </xsl:if>
        </xsl:for-each>
        <xsl:text>}</xsl:text>
    </xsl:template>

</xsl:stylesheet>