<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="3.0"
    xmlns:cat="http://factyfloor.com/CategoriaMateriaPrima"
    xmlns:d="http://factyfloor.com/Depositos" xmlns:mp="http://factyfloor.com/MateriaPrima"
    xmlns:p="http://factyfloor.com/Produtos" xmlns:m="http://factyfloor.com/Maquinas"
    xmlns:lp="http://factyfloor.com/LinhasProducao" xmlns:ord="http://factyfloor.com/OrdensProducao"
    xmlns:exe="http://factyfloor.com/ExecucaoOrdensProducao" xmlns:f="http://factyfloor.com/Fabrica">
    <xsl:output method="text" encoding="UTF-8" omit-xml-declaration="yes" indent="no"
        media-type="application/json"/>


    <xsl:template match="/*[node()]">
        <xsl:text>{</xsl:text>
        <xsl:choose>
            <xsl:when test="count(//p:Produto) &gt; 0">
                <xsl:apply-templates select="f:Producao/f:Produtos" mode="produtos"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>"Produtos": null</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:text>, </xsl:text>
        <xsl:choose>
            <xsl:when test="count(//p:FichaProducao) &gt; 0">
                <xsl:apply-templates select="f:Producao/f:Produtos" mode="fichaproducao"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>"FichasProducao": null</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:text>}</xsl:text>
    </xsl:template>


    <!-- Produtos -->
    <xsl:template match="f:Producao/f:Produtos" mode="produtos">
        <xsl:text>"Produtos": </xsl:text>
        <xsl:choose>
            <xsl:when test="count(p:Produto) = 0">
                <xsl:text>null</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:if test="count(p:Produto) &gt; 1">[ </xsl:if>
                <xsl:for-each select="p:Produto">
                    <xsl:apply-templates select="current()" mode="obj-content"/>
                    <xsl:if test="position() &lt; last()">, </xsl:if>
                </xsl:for-each>
                <xsl:if test="count(p:Produto) &gt; 1">] </xsl:if>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- produto -->
    <xsl:template match="p:Produto" mode="obj-content">
        <xsl:text>{</xsl:text>
        <xsl:for-each select="./*">
            <xsl:if test="not(local-name() = 'FichaProducao')">
                <xsl:text>"</xsl:text>
                <xsl:value-of select="local-name()"/>
                <xsl:text>": </xsl:text>
                <xsl:text>"</xsl:text>
                <xsl:value-of select="current()"/>
                <xsl:text>"</xsl:text>
                <xsl:if test="not(local-name()='DescricaoCompleta' or local-name()='FichaTecnica')">, </xsl:if>
            </xsl:if>
        </xsl:for-each>
        <xsl:text>}</xsl:text>
    </xsl:template>

    <!-- Fichas Producao -->
    <xsl:template match="f:Producao/f:Produtos" mode="fichaproducao">
        <xsl:text>"FichasProducao": </xsl:text>
        <xsl:choose>
            <xsl:when test="count(//p:FichaProducao) = 0">
                <xsl:text>null</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:if test="count(//p:FichaProducao) &gt; 1">[ </xsl:if>
                <xsl:for-each select="//p:FichaProducao">
                    <xsl:apply-templates select="current()" mode="obj-content"/>
                    <xsl:if test="position() &lt; last()">, </xsl:if>
                </xsl:for-each>
                <xsl:if test="count(p:Produto) &gt; 1">] </xsl:if>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- Ficha Produção -->
    <xsl:template match="p:FichaProducao" mode="obj-content">
        <xsl:text>{</xsl:text>
        <xsl:text>"Produto": "</xsl:text>
        <xsl:value-of select="../p:CodigoComercial"/>
        <xsl:text>", </xsl:text>
        <xsl:text>"Quantidade": "</xsl:text>
        <xsl:value-of select="p:Quantidade"/>
        <xsl:text>", </xsl:text>
        <xsl:text>"Unidade": "</xsl:text>
        <xsl:value-of select="p:Quantidade/@Unidade"/>
        <xsl:text>", </xsl:text>
        <xsl:text>"MateriasPrimas": [</xsl:text>
        <xsl:for-each select="p:MateriaPrima">
            <xsl:text>{</xsl:text>
            <xsl:text>"MateriaPrima": "</xsl:text>
            <xsl:value-of select="."/>
            <xsl:text>", </xsl:text>
            <xsl:text>"Quantidade": "</xsl:text>
            <xsl:value-of select="@Quantidade"/>
            <xsl:text>", </xsl:text>
            <xsl:text>"Unidade": "</xsl:text>
            <xsl:value-of select="@Unidade"/>
            <xsl:text>" }</xsl:text>
            <xsl:if test="position() &lt; last()">, </xsl:if>
        </xsl:for-each>
        <xsl:text>] </xsl:text>
        <xsl:text>}</xsl:text>
    </xsl:template>
</xsl:stylesheet>