#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <sys/select.h>
#include <fcntl.h>
#include <errno.h>
#include <pthread.h>

#include <strings.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <netdb.h>

#define PROTOCOL_VERSION 0
#define SERVER_PORT "31403"
#define SERVER_ADDR "127.0.0.1"
//#define SERVER_ADDR SERVER_ADDR_SCM
#define SERVER_ADDR_SCM "172.18.1.3"
#define SERVER_ADDR_SMM "172.18.1.5"

#define BUFFER_MAX 65535
#define UDP_MAX 512

#define NUMBER_MANDATORY_ARGS 4 // First argument is always executable name
#define FILENAME_ARG_POS 1
#define INTERVAL_ARG_POS 2
#define MACH_ID_ARG_POS 3

#define HELLO 0
#define MSG 1
#define CONFIG 2
#define RESET 3
#define ACK 150
#define NACK 151

//Shared Memory Configs
#define SHM_NAME "/shm_mem"
#define BUFFER_SIZE 1000

typedef struct
{
    unsigned short id_code;
    char messageType;
    char actualMessage[BUFFER_MAX];
} message;

typedef struct
{
    int interval;
    FILE* file;
    message* shm;
} client_args;

typedef struct
{
    message* shm;
} server_args;

pthread_mutex_t mutex;

#define READ_TIMEOUT_SEC 1

#include "Packet.c"
#include "Client.c"
#include "UdpServer.c"

void create_message(unsigned short id_code, char message_type, unsigned short message_size, char *raw_message, char *packet);
int getTipoMensagem(char* data);
unsigned short getMaquina(char* data);
void* server_udp_start_async(void* arg);
void* client_tcp_start_async(void* arg);


#endif /* CONFIGURATION_H */