#include "Configuration.h"

#include <openssl/crypto.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/conf.h>
#include <openssl/x509.h>
#define SERVER_SSL_CERT_FILE "ScmServer.pem"

#ifndef CLIENT_C
#define CLIENT_C

void *client_tcp_start_async(void *arg)
{
    printf("[CLI] Client started\n");

    client_args vals = *((client_args *)arg);

    char currentline[BUFFER_MAX];

    unsigned char response[6 + BUFFER_MAX];

    unsigned char handshake[6];

    int err, sock, res, i;

    struct addrinfo req, *list;
    fd_set rfds;

    printf("[CLI] Client mem init done...\n");
    bzero((char *)&req, sizeof(req));
    // let getaddrinfo set the family depending on the supplied server address
    req.ai_family = AF_UNSPEC;
    req.ai_socktype = SOCK_STREAM;
    err = getaddrinfo(SERVER_ADDR_SCM, SERVER_PORT, &req, &list);
    if (err)
    {
        printf("Failed to get server address, error: %s\n", gai_strerror(err));
        pthread_exit(NULL);
    }

    printf("[CLI] Client starting ...\n");
    sock = socket(list->ai_family, list->ai_socktype, list->ai_protocol);
    if (sock == -1)
    {
        printf("Failed to open socket\n");
        freeaddrinfo(list);
        pthread_exit(NULL);
    }

    if (connect(sock, (struct sockaddr *)list->ai_addr, list->ai_addrlen) == -1)
    {
        printf("Failed connect\n");
        freeaddrinfo(list);
        close(sock);
        pthread_exit(NULL);
    }

    printf("[CLI] Client socket started ...\n");
    printf("[CLI] Client SSL started ...\n");

    const SSL_METHOD *method = SSLv23_client_method();
    SSL_CTX *ctx = SSL_CTX_new(method);
    SSL_CTX_use_certificate_file(ctx, "Maquina.pem", SSL_FILETYPE_PEM);
    SSL_CTX_use_PrivateKey_file(ctx, "Maquina.key", SSL_FILETYPE_PEM);
    if (!SSL_CTX_check_private_key(ctx))
    {
        printf("Error loading client's certificate/key\n");
        close(sock);
        exit(1);
    }
    SSL_CTX_set_verify(ctx, SSL_VERIFY_PEER, NULL);

    // THE SERVER'S CERTIFICATE IS TRUSTED
    SSL_CTX_load_verify_locations(ctx, SERVER_SSL_CERT_FILE, NULL);

    // Restrict TLS version and cypher suites
    SSL_CTX_set_min_proto_version(ctx, TLS1_2_VERSION);
    SSL_CTX_set_cipher_list(ctx, "HIGH:!aNULL:!kRSA:!PSK:!SRP:!MD5:!RC4");

    SSL *sslConn = SSL_new(ctx);
    SSL_set_fd(sslConn, sock);
    if (SSL_connect(sslConn) != 1)
    {
        printf("TLS handshake error\n");
        SSL_free(sslConn);
        close(sock);
        exit(1);
    }
    printf("TLS version: %s\nCypher suite: %s\n", SSL_get_cipher_version(sslConn), SSL_get_cipher(sslConn));

    if (SSL_get_verify_result(sslConn) != X509_V_OK)
    {
        printf("Sorry: invalid server certificate\n");
        SSL_free(sslConn);
        close(sock);
        exit(1);
    }

    X509 *cert = SSL_get_peer_certificate(sslConn);
    X509_free(cert);

    if (cert == NULL)
    {
        printf("Sorry: no certificate provided by the server\n");
        SSL_free(sslConn);
        close(sock);
        exit(1);
    }
    printf("[CLI] Client certs done...\n");
    freeaddrinfo(list);

    struct timeval timeout;
    timeout.tv_sec = READ_TIMEOUT_SEC;
    timeout.tv_usec = 0;

    if (setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(timeout)) < 0)
        perror("setsockopt failed\n");

    FD_ZERO(&rfds);
    FD_SET(0, &rfds);
    FD_SET(sock, &rfds);

    printf("[CLI] CONNECTED TO SOCKET\n");
    char packetHello[6];
    char packetACK[6];

    pthread_mutex_lock(&mutex);
    create_message(vals.shm->id_code, HELLO, 0, "0", packetHello);
    create_message(vals.shm->id_code, ACK, 0, "0", packetACK);
    pthread_mutex_unlock(&mutex);

    printf("sayinng hello\n");
    SSL_write(sslConn, packetHello, sizeof(packetHello));

    bzero(response, sizeof(response));
    while (response[1] == 0)
    {
        if (FD_ISSET(sock, &rfds))
        {
            SSL_read(sslConn, &response, sizeof(response));
            printf("%d\n", response[1]);
        }
    }

    printf("[CLI] SCM RESPONSE -> %d\n", response[1]);
    unsigned int msg_size;
    while (response[1] == ACK && fgets(currentline, BUFFER_MAX, vals.file) != NULL)
    {
        printf("[CLI]\tSending message: %s\n", currentline);
        msg_size = strlen(currentline);
        char packet[6 + msg_size];
        bzero(packet, sizeof(packet));

        pthread_mutex_lock(&mutex);
        create_message(vals.shm->id_code, MSG, msg_size, currentline, packet);
        pthread_mutex_unlock(&mutex);

        SSL_write(sslConn, packet, sizeof(packet));

        bzero(response, sizeof(response));

        res = SSL_read(sslConn, &response, sizeof(response));
        if (res == -1)
        {
            if (errno = EINTR)
            {
                printf("Read timed out!\n");
                printf("retrying\n");
                res = SSL_read(sslConn, &response, sizeof(response));
                if (res == -1)
                {
                    if (errno = EINTR)
                    {
                        printf("Read timed out again!\n");
                        printf("Stoping...!\n");
                        //teremos de persistir as ultimas resposta na memoria partilhada. Aqui, se houver erro de timeout
                        pthread_mutex_lock(&mutex); /*fechar o mutex*/
                        response[1] == NACK;
                        vals.shm->messageType = response[1];
                        pthread_mutex_unlock(&mutex);
                    }
                    else
                    {
                        printf((strerror(errno)));
                    }
                }
            }
            else
            {
                printf((strerror(errno)));
            }
        }

        if (response[1] == CONFIG)
        {
            printf("[CLI] Máquina em simulação,\n\tespere para enviar nova configuração!\n");
            response[1] = ACK;
        }

        pthread_mutex_lock(&mutex); /*fechar o mutex*/
        vals.shm->messageType = response[1];
        for (i = 0; i < msg_size; i++)
        {
            vals.shm->actualMessage[i] = response[6 + i];
        }
        pthread_mutex_unlock(&mutex);

        sleep(vals.interval);
    }

    FILE *configFile = fopen("config.txt", "w+");
    bzero(response, sizeof(response));
    printf("\nWaiting for requests...\n");
    while (response[1] == 0)
    {
        //printf("[LCN] teste2\n");
        SSL_read(sslConn, &response, sizeof(response));
        //printf("\t0x%02hhx \n", response[1]);

        if (response[1] == CONFIG)
        {
            printf("Recebi config do SCM\n");
            int rawdatasize = response[4] + response[5] * 256;
            for (i = 0; i < rawdatasize; i++)
            {
                fprintf(configFile, "%c", response[6 + i]);
            }
            //enviar ack para o scm
            SSL_write(sslConn, packetACK, sizeof(packetACK));
        }
    }

    fclose(configFile);

    SSL_free(sslConn);
    close(sock);
    pthread_exit(NULL);
}

#endif
