#include "Configuration.h"

#ifndef UDPSERVER_C
#define UDPSERVER_C

int getTipoMensagem(char *data)
{
	return (int)data[1];
}

unsigned short getMaquina(char *data)
{
	return (data[2] << 8) | data[3];
}

void *server_udp_start_async(void *arg)
{

	server_args vals = *((server_args *)arg);

	struct sockaddr_storage client;
	int err, sock, res, i;
	unsigned int adl;
	char line[UDP_MAX], line1[UDP_MAX];
	char cliIPtext[UDP_MAX], cliPortText[UDP_MAX];
	struct addrinfo req, *list;

	bzero((char *)&req, sizeof(req));
	// request a IPv6 local address will allow both IPv4 and IPv6 clients to use it
	req.ai_family = AF_INET6;
	req.ai_socktype = SOCK_DGRAM;
	req.ai_flags = AI_PASSIVE; // local address

	err = getaddrinfo(NULL, SERVER_PORT, &req, &list);

	if (err)
	{
		printf("Failed to get local address, error: %s\n", gai_strerror(err));
		pthread_exit(NULL);
	}

	sock = socket(list->ai_family, list->ai_socktype, list->ai_protocol);
	if (sock == -1)
	{
		perror("Failed to open socket");
		freeaddrinfo(list);
		pthread_exit(NULL);
	}

	if (bind(sock, (struct sockaddr *)list->ai_addr, list->ai_addrlen) == -1)
	{
		perror("Bind failed");
		close(sock);
		freeaddrinfo(list);
		pthread_exit(NULL);
	}

	freeaddrinfo(list);

	printf("Listening for UDP requests (both over IPv6 or IPv4). Use CTRL+C to terminate the server\n");

	adl = sizeof(client);
	while (1)
	{
		res = recvfrom(sock, line, UDP_MAX, 0, (struct sockaddr *)&client, &adl);
		if (res < 0)
		{
			fprintf(stderr, "socket() failed: %s\n", strerror(errno));
		}
		if (!getnameinfo((struct sockaddr *)&client, adl,
						 cliIPtext, UDP_MAX, cliPortText, UDP_MAX, NI_NUMERICHOST | NI_NUMERICSERV))
			printf("Request from node %s, port number %s\n", cliIPtext, cliPortText);
		else
			printf("Got request, but failed to get client address\n");

		if (line[1] == RESET)
		{
			printf("Recebi RESET do SMM\n");
			printf("HELLO!\n");
			//ENVIAR HELLO?
		}
		if (line[1] == HELLO)
		{
			// ler da memória partilhada os dados da máquina.

			bzero(line1, sizeof(line1));

			pthread_mutex_lock(&mutex);
			unsigned short nrMaq = vals.shm->id_code;
			char msgType = vals.shm->messageType;
			char *message = vals.shm->actualMessage;
			pthread_mutex_unlock(&mutex);

			create_message(nrMaq, msgType, strlen(message), message, line1);

			sendto(sock, (const char *)line1, sizeof(line1), 0, (struct sockaddr *)&client, adl);
		}
	}

	close(sock);
	pthread_exit(NULL);
}

#endif