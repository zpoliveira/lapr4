#include "Configuration.h"

void create_message(unsigned short id_code, char message_type, unsigned short message_size, char *raw_message, char *packet)
{
    int i = 0;
    int final_size = message_size >= BUFFER_MAX ? BUFFER_MAX : message_size;

    packet[0] = (char)PROTOCOL_VERSION;
    packet[1] = message_type;
    packet[2] = id_code % 256;
    packet[3] = id_code / 256;
    packet[4] = final_size % 256;
    packet[5] = final_size / 256;

    /* 
    DEBUG PRINTING
    printf("Protocol... \n");
    printf("\t0x%02hhx \tDONE\n", packet[0]);
    printf("MSG TYPE... \n");
    printf("\t0x%02hhx \tDONE\n", packet[1]);
    printf("MCH CODE... \n");
    printf("\t%d %d \tDONE\n", packet[2], packet[3]);
    printf("MSG SIZE... \n");
    printf("\t%d %d \tDONE\n", packet[5], packet[4]);
    */

    if (message_size > 0)
    {
        for (i = 0; i < final_size; i++)
        {
            packet[6 + i] = raw_message[i];
        }
    }
    
}