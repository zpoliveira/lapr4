/* 
 * File:   main.c
 *
 * Created on 27 May 2020, 22:35
 */

#include "Configuration.h"

/**
 *
 * Initial start of simulation
 *
 * Instantiates a process for both the Client application and one for the Server application
 *
 * Client:
 *          - Simulates a Machine and its execution
 *          - Machine send messages using the connection service
 *
 * Server:
 *          - Simulates the serving capabilities of a machine and responds to status requests
 * ./main [File path] [Transmission interval] [Machine id for protocol]
 */

int main(int argc, char const *argv[])
{
    if (argc != NUMBER_MANDATORY_ARGS)
    {
        printf("Wrong number of arguments.\n Total %d.\n", NUMBER_MANDATORY_ARGS - 1);
        exit(EXIT_FAILURE);
    }

    char *filename;
    char *str_interval;
    client_args cli_arg;
    server_args srv_arg;

    filename = argv[FILENAME_ARG_POS];
    str_interval = argv[INTERVAL_ARG_POS];

    FILE *tFile = fopen(filename, "r");
    if (tFile == NULL)
    {
        perror("Não foi possivel abrir o ficheiro");
        exit(EXIT_FAILURE);
    }
    cli_arg.file = tFile;

    /*
        SHARED DATA INITIALIZATION
    */
    message *shared_data;                                                                 //data to be shared
    int size = sizeof(shared_data);                                                       //size of data
    char *shm_full_name = SHM_NAME;                                                       //
                                                                                          /* char *pid_str = "";                                                                   //
    int pid = getpid();                                                                   //
    snprintf(pid_str, sizeof(int), "%d", pid);                                            //
    strcat(shm_full_name, pid_str);   */
                                                                                          //
    int fd = shm_open(shm_full_name, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);                // Creates and opens a shared memory area
    ftruncate(fd, size);                                                                  // Defines the size
    shared_data = (message *)mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0); // Get a pointer for the shared memory

    cli_arg.shm = shared_data;
    srv_arg.shm = shared_data;

    cli_arg.interval = strtod(str_interval, NULL);
    cli_arg.shm->id_code = atoi(argv[MACH_ID_ARG_POS]);
    srv_arg.shm->id_code =  cli_arg.shm->id_code;

    srv_arg.shm->messageType = NACK;
    cli_arg.shm->messageType = NACK;
    /* 
        mutex init
    */

    if (pthread_mutex_init(&mutex, NULL) != 0)
    {
        perror("Error creating the mutex!");
        exit(EXIT_FAILURE);
    }

    /*
        THREAD INIT
    */
    enum THREADS_POS
    {
        T_SERVER = 0,
        T_CLIENT = 1,
        T_CONFIG = 2
    };

    pthread_t threads[3];

    // SERVER THREAD
    if (pthread_create(&threads[T_SERVER], NULL, server_udp_start_async, &srv_arg) != 0)
    {
        perror("Error creating the server thread!");
        exit(EXIT_FAILURE);
    }

    // CLIENT THREAD
    if (pthread_create(&threads[T_CLIENT], NULL, client_tcp_start_async, &cli_arg) != 0)
    {
        perror("Error creating the client thread!");
        exit(EXIT_FAILURE);
    }

    pthread_join(threads[T_CLIENT], NULL);
    pthread_join(threads[T_SERVER], NULL);

    //end mutex
    if (pthread_mutex_destroy(&mutex) != 0)
    {
        perror("Error destroying the mutex!");
        exit(EXIT_FAILURE);
    }

    // close file
    fclose(tFile);

    // Undo mapping
    if (munmap(shared_data, sizeof(*shared_data)) == -1)
    {
        perror("munmap failed");
        exit(EXIT_FAILURE);
    }

    // Close file descriptor
    if (close(fd) == -1)
    {
        perror("close failed");
        exit(EXIT_FAILURE);
    }

    // Remove file from system
    if (shm_unlink(shm_full_name) < 0)
    {
        perror("Error at unlink");
        exit(EXIT_FAILURE);
    }

    return 0;
}
